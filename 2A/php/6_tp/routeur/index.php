<?php
require 'AltoRouter.php';
require 'Controller/UserController.php';
$router = new AltoRouter();
$router->setBasePath('/routeur');
//$router->map('GET', '/', 'AppController#create');
$router->map('GET', '/', 'AppController');
$router->map( 'GET|POST', '/user/[i:id]/[a:action]?', 'UserController');
$router->map( 'GET|POST', '/[a:action]/[i:page]/[i:id]?', 'UserController');

$page = 0;

$id =0;
$match = $router->match();
//var_dump($match);

$action = array();
$id=array();
if (!$match) { echo "404"; die; }

if ($match) {
//list($controller, $action) = explode('#', $match['target'] );
    $controller=$match['target'] ?? null;
    $action=$match['params']['action'] ?? null;
    $id=$match['params']['id'] ?? null;
    $page=$match['params']['page'] ?? null;
    print 'user Id received '.$id.'<br>';
    print 'controleur appelé '.$controller .'<br>';
    print $action .'<br>';
    print $id .'<br>';
    print 'vous avez demandé la page '.$page.'<br>';

try {
    $controller = '\\Controller\\' . $controller;
    $controller = new $controller;
    if (is_callable(array($controller, $action))) {
        call_user_func_array(array($controller, $action),
            array($match['params']));
    }
}
catch (Error $error){print 'pas de controller';}
}
?>