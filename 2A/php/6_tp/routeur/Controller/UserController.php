<?php
namespace Controller;

Class UserController
{

    public function __construct()
    {
        print 'I am a controller<br>';
    }


    public function add(array $params): void
    {
        $id = $params['id'] ??  -1;
        print 'using add action ; id:' . $id;
    }

    public function page(array $params): void
    {
        $page = $params['page'] ?? -1;
        print 'using page action ; page : ' . $page;
    }
}

?>