<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* vuephp1.html */
class __TwigTemplate_b9d4fd87e58f6104955b79f240508016 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"UTF-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
    <title>Personne - formulaire</title>
    <script type=\"text/javascript\">
      function clearForm(oForm) {
        const elements = oForm.elements;
        oForm.reset();

        for (i = 0; i < elements.length; i++) {
          field_type = elements[i].type.toLowerCase();

          switch (field_type) {
            case \"text\":
            case \"password\":
            case \"textarea\":
            case \"hidden\":
              elements[i].value = \"\";
              break;

            case \"radio\":
            case \"checkbox\":
              if (elements[i].checked) {
                elements[i].checked = false;
              }
              break;

            case \"select-one\":
            case \"select-multi\":
              elements[i].selectedIndex = -1;
              break;

            default:
              break;
          }
        }
      }
    </script>
  </head>

  <body>
    <!-- on vérifie les données provenant du modèle -->
    ";
        // line 45
        if (array_key_exists("dVue", $context)) {
            // line 46
            echo "      <div align=\"center\">
        ";
            // line 47
            if ((array_key_exists("dVueEreur", $context) && (twig_length_filter($this->env, ($context["dVueEreur"] ?? null)) > 0))) {
                // line 48
                echo "          <h2>ERREUR !!!!!</h2>
          ";
                // line 49
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["dVueEreur"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                    // line 50
                    echo "            <p>";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "</p>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "        ";
            }
            // line 53
            echo "
        <h2>Personne - formulaire</h2>
        <hr />
        <!-- affichage de données provenant du modèle -->
        ";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dVue"] ?? null), "data", [], "any", false, false, false, 57), "html", null, true);
            echo "

        <form method=\"post\" name=\"myform\" id=\"myform\">
          <table>
            <tr>
              <td>Nom</td>
              <td>
                <input name=\"txtNom\" value=\"";
            // line 64
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dVue"] ?? null), "nom", [], "any", false, false, false, 64), "html", null, true);
            echo "\" type=\"text\" size=\"20\" />
              </td>
            </tr>
            <tr>
              <td>Age</td>
              <td>
                <input
                  name=\"txtAge\"
                  value=\"";
            // line 72
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dVue"] ?? null), "age", [], "any", false, false, false, 72), "html", null, true);
            echo "\"
                  type=\"text\"
                  size=\"3\"
                  required
                />
              </td>
            </tr>
            <tr></tr>
          </table>
          <table>
            <tr>
              <td><input type=\"submit\" value=\"Envoyer\" /></td>
              <td><input type=\"reset\" value=\"Rétablir\" /></td>
              <td>
                <input
                  type=\"button\"
                  value=\"Effacer\"
                  onclick=\"clearForm(this.form);\"
                />
              </td>
            </tr>
          </table>
          <!-- action !!!!!!!!!! -->
          <input type=\"hidden\" name=\"action\" value=\"validationFormulaire\" />
        </form>
      </div>
    ";
        } else {
            // line 99
            echo "      <p>Erreur !!<br />utilisation anormale de la vuephp</p>
    ";
        }
        // line 101
        echo "    <p>
      Essayez de mettre du code html dans nom -> Correspond à une attaque de type injection
    </p>
  </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "vuephp1.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 101,  166 => 99,  136 => 72,  125 => 64,  115 => 57,  109 => 53,  106 => 52,  97 => 50,  93 => 49,  90 => 48,  88 => 47,  85 => 46,  83 => 45,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "vuephp1.html", "/Users/Perederii/IUT/2A/php/5_tp/mvc_PSR4_twig/templates/vuephp1.html");
    }
}
