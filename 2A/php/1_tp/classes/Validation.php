<?php

class Validation {
    // Méthode pour valider une adresse email
    public static function validerEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function validerAge($age) {
        return filter_var($age, FILTER_VALIDATE_INT);
    }

    // Méthode pour nettoyer une chaîne de caractères
    public static function nettoyerChaine($chaine) {
        return filter_var($chaine, FILTER_DEFAULT);
    }
}

?>
