<?php

namespace Classes;
class Personne {
    private $nom;
    private $prenom;
    private $anneeNaissance;
    private $email;

    // Constructeur
    public function __construct($nom, $prenom, $anneeNaissance, $email) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->anneeNaissance = $anneeNaissance;
        $this->email = $email;
    }

    // Méthode magique pour l'affichage
    public function __toString() {
        return "Nom: {$this->nom}, Prénom: {$this->prenom}, Année de Naissance: {$this->anneeNaissance}, Email: {$this->email}";
    }
}