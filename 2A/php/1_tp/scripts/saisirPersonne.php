<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Saisie de Personne</title>
</head>
<body>
    <h1>Saisie de Personne</h1>
    <form action="verif.php" method="post">
        <label for="nom">Nom :</label>
        <input type="text" name="nom" id="nom" required>
        <br>
        <label for="prenom">Prénom :</label>
        <input type="text" name="prenom" id="prenom" required>
        <br>
        <br>
        <label for="age">Age :</label>
        <input type="text" name="age" id="age" required>
        <br>
        <label for="email">Email :</label>
        <input type="email" name="email" id="email" required>
        <br>
        <input type="submit" value="Valider">
    </form>
</body>
</html>
