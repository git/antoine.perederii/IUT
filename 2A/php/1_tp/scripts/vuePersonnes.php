<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vue des Personnes</title>
</head>
<body>
    <h1>Liste des Personnes</h1>
    <ul>
        <?php
        // Parcourir le tableau de personnes et afficher les propriétés
        foreach ($personnes as $personne) {
            echo "<li>{$personne}</li>";
        }
        ?>
    </ul>
</body>
</html>
