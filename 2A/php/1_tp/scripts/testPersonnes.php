<?php


// Inclure la classe Personne
require_once('../classes/Personne.php');
use Classes\Personne;

// Instancier plusieurs objets Personne
$personnes = [
    new Personne("Doe", "John", 1990, "john.doe@example.com"),
    new Personne("Smith", "Jane", 1985, "jane.smith@example.com"),
    new Personne("DEoe", "Johqn", 1990, "john.doe@exdample.com"),
    new Personne("Smsith", "Jaddne", 100, "jane.smithdd@example.com"),
    new Personne("Dodsfe", "Johqzn", 1990, "john.doe@dexample.com"),
    new Personne("Smitdfsh", "Jazne", 1985, "jane.smidsth@example.com"),
    new Personne("Dcxvoe", "Johne", 1990, "john.doe@exdsample.com"),
    new Personne("Smcxith", "Janee", 1985, "jane.smith@dsexample.com"),
    // Ajoutez autant d'instances que nécessaire
];

// Appeler le script de vue
require('vuePersonnes.php');


?>