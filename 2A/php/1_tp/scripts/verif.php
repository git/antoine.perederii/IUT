<?php

require_once('../classes/Personne.php');
require_once('../classes/Validation.php');
use Classes\Personne;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupérer les données du formulaire
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $age = $_POST['age'];
    $email = $_POST['email'];

    // Utiliser la classe Validation pour valider l'adresse email
    if (!Validation::validerEmail($email)) {
        die("Adresse email invalide");
    }

    // Utiliser la classe Validation pour valider l'age
    if (!Validation::validerAge($age)) {
        die("Age invalide");
    }

    // Utiliser la classe Validation pour nettoyer le nom et le prénom
    $nom = Validation::nettoyerChaine($nom);
    $prenom = Validation::nettoyerChaine($prenom);

    // Créer une instance de la classe Personne
    $personne = new Personne($nom, $prenom, $age, $email);

    // Afficher les données saisies, filtrées et nettoyées
    echo "Nom : " . $nom . "<br>";
    echo "Prénom : " . $prenom . "<br>";
    echo "Age : " . $age . "<br>";
    echo "Email : " . $email . "<br>";

    echo "<h2>Instance de Personne :</h2>";
    echo "$personne";
} else {
    // Rediriger vers la page de saisie si la requête n'est pas POST
    header("Location: saisirPersonne.php");
}
?>