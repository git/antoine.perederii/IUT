<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Erreur</title>
    </head>

    <body>
        <h1>Messages d'erreur :</h1>

        <?php
        // Vérifie si le tableau TMessage est défini
        if (!empty($TMessage)) {
            // Parcourir le tableau TMessage et afficher chaque message d'erreur
            echo "<ul>";
            foreach ($TMessage as $erreur) {
                echo "<li>$erreur</li>";
            }
            echo "</ul>";
        } else {
            echo "<p>Aucun message d'erreur à afficher.</p>";
        }
        ?>

    </body>
</html>