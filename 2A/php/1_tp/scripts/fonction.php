<?php
function pourcentageAvis(string $typeAvis, int $nbAvisFav, int $nbAvisDefav): string
{
    // Vérifier que le type d'avis est soit "favorable" soit "defavorable"
    if ($typeAvis !== 'favorable' && $typeAvis !== 'defavorable') {
        return "Type d'avis non valide.";
    }

    // Calculer le pourcentage en fonction du type d'avis
    $totalAvis = $nbAvisFav + $nbAvisDefav;

    if ($totalAvis === 0) {
        return "Aucun avis disponible.";
    }

    $pourcentage = ($typeAvis === 'favorable') ? ($nbAvisFav / $totalAvis) * 100 : ($nbAvisDefav / $totalAvis) * 100;

    // Formater le résultat
    return "Le pourcentage d'avis de type $typeAvis est de : " . number_format($pourcentage, 2) . "%";
}


function pourcentageAvis2(string $typeAvis, int $nbAvisFav, int $nbAvisDefav, array &$TMessage): string
{
    // Vérifier que le type d'avis est soit "favorable" soit "defavorable"
    if ($typeAvis !== 'favorable' && $typeAvis !== 'defavorable') {
        return "Type d'avis non valide.";
    }

    try {
        // Calculer le pourcentage en fonction du type d'avis
        $totalAvis = $nbAvisFav + $nbAvisDefav;

        if ($totalAvis === 0) {
            throw new Exception("Division par zéro - Aucun avis disponible.");
        }

        $pourcentage = ($typeAvis === 'favorable') ? ($nbAvisFav / $totalAvis) * 100 : ($nbAvisDefav / $totalAvis) * 100;

        // Formater le résultat
        return "Le pourcentage d'avis de type $typeAvis est de : " . number_format($pourcentage, 2) . "%";

    } catch (Exception $e) {
        // En cas d'erreur, ajouter le message dans la table TMessage
        $TMessage[] = $e->getMessage();
        return "Attention : calcul du pourcentage impossible - division par zéro";
    }
}

?>