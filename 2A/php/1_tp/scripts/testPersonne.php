<?php

// Inclusion de la classe Personne
require_once('../classes/Personne.php');

use Classes\Personne;

// Instanciation de la classe Personne
$unePersonne = new Personne("Doe", "John", 1990, "john.doe@example.com");

// Affichage des propriétés à l'aide de la méthode magique __toString()
echo $unePersonne;