#include "codeRetour.h"
#include <stdio.h>

void afficheMessageCodeRetour(CodeRetour code){
  switch(code){
    case NON :
      printf("non ");
      break;
    case OUI:
      printf("oui ");
      break;
    case OK :
      printf("ok, opération effectuée ");
      break;
    case PB_MEMOIRE:
      printf("problème mémoire ");
      break;
    case PB_NOEUD_DEJA_EXISTANT:
      printf("problème : noeud déjà existant ");
      break;
    case PB_NOEUD_DEP_NON_EXISTANT:
      printf("problème : noeud de départ non existant ");
      break;
    case PB_NOEUD_ARR_NON_EXISTANT:
      printf("problème : noeud d'arrivée non existant ");
      break;
    case PB_NOEUD_NON_EXISTANT:
      printf("problème : noeud non existant ");
      break;
    case PB_ARC_DEJA_EXISTANT:
      printf("problème : arc déjà existant ");
      break;
    case PB_ARC_NON_EXISTANT:
      printf("problème : arc non existant ");
      break;
    case PB_OUVERTURE_FICHIER:
      printf("problème ouverture du fichier ");
      break;
    case PB_RESEAU_ERRONNE:
      printf("problème : réseau erroné ");
      break;
    default :
      printf("ERREUR : code retour inconnu\n");
    }
    printf("\n");



}