#ifndef ABR_H
#define ABR_H

typedef struct noeud {
    int val;
    struct noeud *fg, *fd;
} Noeud;

typedef Noeud* Abr;

typedef enum{FALSE, TRUE} Booleen;

Abr creerArbreVide(void);
Booleen estArbreVide(Abr a);
void insererEnFeuille(Abr* pta, int val);
void viderArbre(Abr* pta);
void afficherCroissant(Abr a);
void afficherDecroissant(Abr a);
void afficherCouche(Abr a);
Booleen rechercherVal(Abr a, int val);
Booleen supprimerVal(Abr* pta, int val);


#endif //ABR_H