#include "abr.h"

#include <stdlib.h>
#include <stdio.h>

Abr creerArbreVide(void) {
    return NULL;
}

Booleen estArbreVide(Abr a){
    if(a == NULL) return TRUE;
    return FALSE;
}

Noeud* creerNoeud(int val){
    Noeud* tmp = (Noeud*)malloc(sizeof(Noeud));
    if(tmp == NULL) exit(1);
    tmp->val = val;
    tmp->fd = NULL;
    tmp->fg = NULL;
    return tmp;
}

void insererEnFeuille(Abr* pta, int val){
    if(*pta == NULL)
        *pta = creerNoeud(val);
    else if(val <= (*pta)->val)
        insererEnFeuille(&(*pta)->fg, val);
    else
        insererEnFeuille(&(*pta)->fd, val);
}

void viderArbre(Abr *pta){
    if(*pta == NULL) return;
    viderArbre(&(*pta)->fg);
    viderArbre(&(*pta)->fd);
    free(*pta);
    *pta = NULL;
}

void afficherCroissant(Abr a){
    if(a == NULL) return;
    
    afficherCroissant(a->fg);
    printf("%d ", a->val);
    afficherCroissant(a->fd);
}

void afficherDecroissant(Abr a){
    if(a == NULL) return;
    
    afficherDecroissant(a->fd);
    printf("%d ", a->val);
    afficherDecroissant(a->fg);
}

void afficherNtab(int n){
    for(int i = 0; i<n; i++) 
        printf("\t");
}

void afficherCoucheRec(Abr a, int retrait){
    if(a->fd != NULL){
        afficherCoucheRec(a->fd, retrait+1);
        afficherNtab(retrait); printf("     / \n");
    }

    afficherNtab(retrait); printf("%d\n", a->val);

    if(a->fg != NULL){
        afficherNtab(retrait); printf("     \\ \n");
        afficherCoucheRec(a->fg, retrait+1);
    }
}

void afficherCouche(Abr a){
    if(a!=NULL)
    afficherCoucheRec(a,0);
}

Booleen rechercherVal(Abr a, int val){
    Booleen rep;
    if(a == NULL) return FALSE;
    if(a->val == val) return TRUE;
    rep = rechercherVal(a->fg, val);
    if(rep == FALSE || !rep)
        rep = rechercherVal(a->fd, val);
    return rep;
}

int oterMax(Abr* pta){
    Noeud* tmp;
    int val;
    if((*pta)->fd == NULL){
        tmp = *pta;
        val = tmp->val;
        *pta = (*pta)->fg;
        free(tmp);
    }
    else{
        val = oterMax(&(*pta)->fd);
    }
    return val;
}

Booleen supprimerVal(Abr *pta, int val){
    Noeud* tmp;

    if(*pta == NULL) return FALSE;

    if((*pta)->val == val){
        if((*pta)->fg == NULL){
            tmp = *pta;
            *pta = (*pta)->fd; //! si fd est aussi nul, alors *pta sera null !
            free(tmp);
        }
        else if((*pta)->fd == NULL){
            tmp = *pta;
            *pta = (*pta)->fg;
            free(tmp);
        }
        else{
            (*pta)->val = oterMax(&(*pta)->fg);
        }
        return TRUE;
    }

    //sinon on continue dans la suite de l'arbre
    else if(val < (*pta)->val)
        return supprimerVal(&(*pta)->fg, val);
    else
        return supprimerVal(&(*pta)->fd, val);
}
