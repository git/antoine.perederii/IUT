#include "abr.h"

#include <stdio.h>
#include <stdlib.h>

#include <time.h>

void testAbr(void){
    Abr a = creerArbreVide();
    Abr b = creerArbreVide();
    Booleen resA, resB, resRechA;
    if(a == NULL && b == NULL) printf("Arbre créée avec succès.\n");
    else {
        printf("Erreur lors de la création de l'arbre !\n");
        exit(1);
    }
    resA = estArbreVide(a);
    resB = estArbreVide(b);
    if(resA == TRUE && resB == TRUE) printf("Aucun Pb, les arbres sont bien vides.\n");
    else printf("Pb, arbres non vide !\n");

    insererEnFeuille(&a, 7);
    insererEnFeuille(&a, 9);
    insererEnFeuille(&a, 8);
    insererEnFeuille(&a, 6);

    resA = estArbreVide(a);
    if(resA == TRUE) printf("Erreur, l'abre incrémenté est vide !\n");
    else printf("Aucun pb.\n");

    viderArbre(&a);

    resA = estArbreVide(a);
    if(resA == TRUE) printf("Tout fonctionne, l'arbre A est bien vidé.\n");
    else printf("Pb, l'arbre a n'a pas été vidé correctement !\n");

    insererEnFeuille(&a, 7);
    insererEnFeuille(&a, 9);
    insererEnFeuille(&a, 8);
    insererEnFeuille(&a, 6);
    insererEnFeuille(&a, 1);
    insererEnFeuille(&a, 2);
    insererEnFeuille(&a, 18);
    insererEnFeuille(&a, 11);

    afficherCroissant(a);
    afficherDecroissant(a);
    printf("\n\n");
    afficherCouche(a);
    printf("\n");

    int val1 = 3;
    char arbreDeRecherche = 'a';

    resRechA = rechercherVal(a, val1);

    if(resRechA) printf("La valeur %d est bien dans l'arbre %c\n", val1, arbreDeRecherche);
    else printf("La valeur %d n'est pas présente dans l'arbre %c\n", val1, arbreDeRecherche);

    supprimerVal(&a, 2);
    printf("Valeur supprimé avec succès.");
    supprimerVal(&a, 3);
}

int main(void) {
    testAbr();
    return 0;
}