#ifndef AVL_H
#define AVL_H

typedef struct noeud {
    int val;
    int h;
    struct noeud *fg, *fd;
} Noeud;

typedef Noeud* Avl;

typedef enum{FALSE, TRUE} Booleen;

Avl creerAvlVide(void);
Booleen estAvlVide(Avl a);
void viderAvl(Avl *pta);

void afficherCroissantAvl(Avl a);
void afficherDecroissantAvl(Avl a);

void afficherCoucheAvl(Avl a);
Booleen rechercherValAvl(Avl a, int val);
void insererEquilibre(Avl* pta, int val);
Booleen supprimerValEquilibre(Avl* pta, int val);


#endif //AVL_H