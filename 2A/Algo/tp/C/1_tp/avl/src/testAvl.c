#include "avl.h"

#include <stdio.h>
#include <stdlib.h>

#include <time.h>

void testAvl(void) {
    Avl a = creerAvlVide();
    Avl b = creerAvlVide();
    Booleen resA, resB, resSupA;
    if(a == NULL && b == NULL) printf("Arbre créée avec succès.\n");
    else {
        printf("Erreur lors de la création de l'arbre !\n");
        exit(1);
    }
    resA = estAvlVide(a);
    resB = estAvlVide(b);
    if(resA == TRUE && resB == TRUE) printf("Aucun Pb, les arbres sont bien vides.\n");
    else printf("Pb, arbres non vide !\n");

    insererEquilibre(&a, 7);
    insererEquilibre(&a, 9);
    insererEquilibre(&a, 8);
    insererEquilibre(&a, 6);

    resA = estAvlVide(a);
    if(resA == TRUE) printf("Erreur, l'abre incrémenté est vide !\n");
    else printf("Aucun pb.\n");

    viderAvl(&a);

    resA = estAvlVide(a);
    if(resA == TRUE) printf("Tout fonctionne, l'arbre A est bien vidé.\n");
    else printf("Pb, l'arbre a n'a pas été vidé correctement !\n");

    insererEquilibre(&a, 7);
    insererEquilibre(&a, 9);
    insererEquilibre(&a, 8);
    insererEquilibre(&a, 6);
    insererEquilibre(&a, 1);
    insererEquilibre(&a, 2);
    insererEquilibre(&a, 18);
    insererEquilibre(&a, 11);

    afficherCoucheAvl(a);

    // afficherCroissant(a);
    // afficherDecroissant(a);

    resSupA = supprimerValEquilibre(&a, 2);
    if(resSupA == 1) printf("Suppression efféctuée avec succès.");
    else printf("Pb suppression.");
    afficherCoucheAvl(a);
    insererEquilibre(&a, 19);
    insererEquilibre(&a, 20);
    insererEquilibre(&a, 21);
    afficherCoucheAvl(a);
    afficherCroissantAvl(a);
    printf("\n");
    afficherDecroissantAvl(a);
    printf("\n");
    // printf("%d", resSupA);
}

// void testTempAvl(void){
//     int insertion, deb;
//     deb = time();
//     Avl a = creerAvlVide();
//     for(int i = 1; i < 10000; i++) {
//         insererEquilibre(&a, i);
//     }
//     insertion = time() - deb;
//     printf("%d \n", insertion);
    
// }

int main(void) {
    testAvl();
    // testTempAvl();
    return 0;
}