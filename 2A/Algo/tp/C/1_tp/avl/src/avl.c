#include "avl.h"

#include <stdio.h>
#include <stdlib.h>

Avl creerAvlVide(void) {
    return NULL;
}

Booleen estAvlVide(Avl a){
    if(a == NULL) return TRUE;
    return FALSE;
}

void viderAvl(Avl *pta){
    if(*pta == NULL) return;
    viderAvl(&(*pta)->fg);
    viderAvl(&(*pta)->fd);
    free(*pta);
    *pta = NULL;
}

void afficherCroissantAvl(Avl a){
    if(a == NULL) return;
    
    afficherCroissantAvl(a->fg);
    printf("%d ", a->val);
    afficherCroissantAvl(a->fd);
}

void afficherDecroissantAvl(Avl a){
    if(a == NULL) return;
    
    afficherDecroissantAvl(a->fd);
    printf("%d ", a->val);
    afficherDecroissantAvl(a->fg);
}

void afficherNtabAvl(int n){
    for(int i = 0; i<n; i++) 
        printf("\t");
}

void afficherCoucheRecAvl(Avl a, int retrait){
    if(a->fd != NULL){
        afficherCoucheRecAvl(a->fd, retrait+1);
        afficherNtabAvl(retrait); printf("     / \n");
    }

    afficherNtabAvl(retrait); printf("%d\n", a->val);

    if(a->fg != NULL){
        afficherNtabAvl(retrait); printf("     \\ \n");
        afficherCoucheRecAvl(a->fg, retrait+1);
    }
}

void afficherCoucheAvl(Avl a){
    if(a!=NULL) {
        printf("====================    Affichage   ====================\n");
        afficherCoucheRecAvl(a,0);
        printf("====================    Fin Affichage   ====================\n");
    }
}

Booleen rechercherValAvl(Avl a, int val){
    Booleen rep;
    if(a == NULL) return FALSE;
    if(a->val == val) return TRUE;
    rep = rechercherValAvl(a->fg, val);
    if(rep == FALSE || !rep)
        rep = rechercherValAvl(a->fd, val);
    return rep;
}

void mettreAjourHauteur(Noeud* ptn) {
    int hg, hd;

    hg = (ptn->fg==NULL)? -1 : ptn->fd->h;
    hd = (ptn->fd==NULL)? -1 : ptn->fd->h;

    ptn->h = (hg>hd)? 1+hg : 1+hd;
}

void rotationGauche(Avl *pta) {
    Noeud* racine;
    racine = (*pta)->fd;
    (*pta)->fd = racine->fg;
    racine->fg = *pta;
    *pta = racine;
    mettreAjourHauteur((*pta)->fg);
    mettreAjourHauteur(*pta);
}

void rotationDroite(Avl *pta) {
    Noeud* racine;
    racine = (*pta)->fg;
    (*pta)->fg = racine->fd;
    racine->fd = *pta;
    *pta = racine;
    mettreAjourHauteur((*pta)->fd);
    mettreAjourHauteur(*pta);
}

void reequilibrer(Avl* pta) {
    int hg, hd, hgf, hdf;
    hg = ((*pta)->fg != NULL)? (*pta)->fg->h : -1;
    hd = ((*pta)->fd != NULL)? (*pta)->fd->h : -1;
    if (hg - hd == 2){ // déséquilibre : hg > hd de 2
        hgf = ((*pta)->fg->fg != NULL)? (*pta)->fg->fg->h : -1;
        hdf = ((*pta)->fg->fd != NULL)? (*pta)->fg->fd->h : -1;
        if (hdf > hgf) rotationGauche (& (*pta)->fg);
        rotationDroite (pta);
    }
    else if(hd - hg == 2){ // déséquilibre : hd > hg de 2
        hgf = ((*pta)->fd->fg != NULL)? (*pta)->fd->fg->h : -1;
        hdf = ((*pta)->fd->fd != NULL)? (*pta)->fd->fd->h :
        -1;
        if (hgf > hdf) rotationDroite (& (*pta)->fd);
        rotationGauche (pta);
    }
    else{ // pas de déséquilibre, on remet juste la hauteur à jour
        if (hd > hg) (*pta) ->h = hd+1;
        else (*pta) ->h = hg+1;
    }
}

Noeud* creerNoeudAvl(int val){
    Noeud* tmp = (Noeud*)malloc(sizeof(Noeud));
    if(tmp == NULL) exit(1);
    tmp->val = val;
    tmp->h = 0;
    tmp->fd = NULL;
    tmp->fg = NULL;
    return tmp;
}

void insererEquilibre(Avl* pta, int val) {
    if(*pta == NULL)
        *pta = creerNoeudAvl(val);
    else {
        if(val <= (*pta)->val)
            insererEquilibre(&(*pta)->fg, val);
        else
            insererEquilibre(&(*pta)->fd, val);
        reequilibrer(pta);
    }
}

int oterMaxEquilibre(Avl* pta){
    Noeud* tmp;
    int val;
    if((*pta)->fd == NULL){
        tmp = *pta;
        val = tmp->val;
        *pta = (*pta)->fg;
        free(tmp);
    }
    else{
        val = oterMaxEquilibre(&(*pta)->fd);
        reequilibrer(pta);
    }
    return val;
}

Booleen supprimerValEquilibre(Avl *pta, int val){
    Noeud* tmp;
    Booleen b;

    if(*pta == NULL) return FALSE;

    if((*pta)->val == val){
        if((*pta)->fg == NULL){
            tmp = *pta;
            *pta = (*pta)->fd; //! si fd est aussi nul, alors *pta sera null !
            free(tmp);
        }
        else if((*pta)->fd == NULL){
            tmp = *pta;
            *pta = (*pta)->fg;
            free(tmp);
        }
        else{
            (*pta)->val = oterMaxEquilibre(&(*pta)->fg);
            reequilibrer(pta);
        }
        return TRUE;
    }

    //sinon on continue dans la suite de l'arbre
    else {
        if(val < (*pta)->val)
            b = supprimerValEquilibre(&(*pta)->fg, val);
        else
            b = supprimerValEquilibre(&(*pta)->fd, val);
        if(b) reequilibrer(pta);
    }
    return b;
}