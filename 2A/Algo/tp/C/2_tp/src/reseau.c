#include "reseau.h"
#include "codeRetour.h"

#include <stdio.h> // pour printf
#include <stdlib.h> //pour le reste


Reseau creerReseauVide(void) {
    return NULL;
}

Booleen estReseauVide(Reseau r) {
    if(r == NULL) return TRUE;
    return FALSE;
}

void afficherReseau(Reseau r) {
    Noeud * tmp = r;
    Arc * tmpa;
    while(tmp != NULL) {
        printf("%d :\n", tmp->num);
        tmpa = tmp->listeArcs;
        while(tmpa != NULL) {
            printf("\t-> %d \n", tmpa->ext->num);
            tmpa = tmpa->suiv;
        }
        tmp = tmp->suiv;
    }
}

Noeud * rechPtSurNoeud(Reseau r, int num) {
    Noeud * tmp = r;
    while(tmp != NULL) {
        if(tmp->num == num) return tmp;
        tmp = tmp->suiv;
    }
    return NULL;
}

Booleen rechercherNoeud(Reseau r, int num) {
    if(rechPtSurNoeud(r, num) != NULL) return TRUE;
    return FALSE;
}

CodeRetour ajouterNoeud(Reseau * ptr, int num) {
    Noeud * tmp;
    if(rechPtSurNoeud(*ptr, num) != NULL) {
        return PB_NOEUD_DEJA_EXISTANT;
    }
    tmp = (Noeud *)malloc(sizeof(Noeud));
    if(tmp == NULL) return PB_MEMOIRE;
    tmp->suiv = *ptr;
    *ptr = tmp;
    tmp->num = num;
    tmp->listeArcs = NULL;
    return OK;
}

CodeRetour rechercherArcPt(Noeud * dep, Noeud * arr) {
    Arc * tmpa = dep->listeArcs;
    while(tmpa != NULL) {
        if(tmpa->ext == arr) {
            return OUI;
        }
        tmpa = tmpa->suiv;
    }
    return NON;
}

CodeRetour rechercherArc(Reseau r, int dep, int arr) {
    Noeud *pa, *pb;

    pa = rechPtSurNoeud(r, dep);
    if(pa == NULL) return PB_NOEUD_DEP_NON_EXISTANT;

    pb = rechPtSurNoeud(r, arr);
    if(pb == NULL) return PB_NOEUD_ARR_NON_EXISTANT;

    return rechercherArcPt(pa, pb);
}

CodeRetour ajouterArc(Reseau r, int dep, int arr) {
    Noeud * ptDep, * ptArr;
    Arc * tmpa;
    ptDep = rechPtSurNoeud(r, dep);
    if(ptDep == NULL) return PB_NOEUD_DEP_NON_EXISTANT;
    ptArr = rechPtSurNoeud(r, arr);
    if(ptArr == NULL) return PB_NOEUD_ARR_NON_EXISTANT;

    if(rechercherArcPt(ptDep, ptArr) == OUI) return PB_ARC_DEJA_EXISTANT;

    tmpa = (Arc *)malloc(sizeof(Arc));
    if(tmpa == NULL) return PB_MEMOIRE;

    tmpa->ext = ptArr;
    tmpa->suiv = ptDep->listeArcs;
    ptDep->listeArcs = tmpa;
    return OK;
}

/// precondition d'usage, cette fonction ne peut etre appelé que si on est sur que l'arc n'est pas vide.
CodeRetour supprimerPtArc(Noeud *ptDep, Noeud *ptArr) {
    Arc * tmpa = NULL, * tmp = NULL;

    if(ptDep->listeArcs->ext == ptArr) {
        tmpa = ptDep->listeArcs;
        ptDep->listeArcs = tmpa->suiv;
        free(tmpa);
        return OK;
    }
    else {
        tmpa = ptDep->listeArcs->suiv;
        tmp = ptDep->listeArcs;
        while(tmpa != NULL) {
            if(tmpa->ext == ptArr) {
                tmp->suiv = tmpa->suiv;
                free(tmpa);
                return OK;
            }
            tmpa = tmpa->suiv;
            tmp = tmp->suiv;
        }
    }
    return NON;
}

CodeRetour supprimerArc(Reseau r, int dep, int arr) {
    Noeud * ptDep, * ptArr;
    ptDep = rechPtSurNoeud(r, dep);
    if(ptDep == NULL) return PB_NOEUD_DEP_NON_EXISTANT;
    ptArr = rechPtSurNoeud(r, arr);
    if(ptArr == NULL) return PB_NOEUD_ARR_NON_EXISTANT;

    if(rechercherArcPt(ptDep, ptArr) == NON) return PB_ARC_NON_EXISTANT;

    return supprimerPtArc(ptDep, ptArr);
}

CodeRetour supprimerArcs(Arc * arcSurNoeud) {
    Arc *tmp = arcSurNoeud;
    while(tmp != NULL) {
        arcSurNoeud = arcSurNoeud->suiv;
        free(tmp);
        tmp = arcSurNoeud;
    }

    return OK;
}

CodeRetour supprimerNoeud(Reseau r, int noeud) {
    Noeud * ptSurNoeud;
    CodeRetour codeRetour;
    ptSurNoeud = rechPtSurNoeud(r, noeud);
    if(ptSurNoeud == NULL) return PB_NOEUD_NON_EXISTANT;

    codeRetour = supprimerArcs(ptSurNoeud->listeArcs);
    afficheMessageCodeRetour(codeRetour);
    return OK;
}
