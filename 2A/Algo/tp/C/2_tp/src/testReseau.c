#include "reseau.h"
#include "codeRetour.h"

#include <stdio.h>
#include <stdlib.h>

void testReseau(void) {
    Reseau a = creerReseauVide();
    Booleen boolA;
    CodeRetour codeA, sup;
    boolA = estReseauVide(a);
    if(boolA == TRUE) printf("OK\n");
    else printf("Pas OK\n");
    afficherReseau(a);
    codeA = ajouterNoeud(&a, 4);
    afficheMessageCodeRetour(codeA);
    codeA = ajouterNoeud(&a, 3);
    afficheMessageCodeRetour(codeA);
    codeA = ajouterNoeud(&a, 2);
    afficheMessageCodeRetour(codeA);
    codeA = ajouterNoeud(&a, 1);
    afficheMessageCodeRetour(codeA);
    afficherReseau(a);
    boolA = rechercherNoeud(a, 5);
    if(boolA == TRUE) printf("OK dedans A\n");
    else printf("Pas OK pas dedans A\n");
    boolA = rechercherNoeud(a, 2);
    if(boolA == TRUE) printf("OK dedans A\n");
    else printf("Pas OK pas dedans A\n");
    ajouterArc(a, 1, 2);
    ajouterArc(a, 1, 3);
    ajouterArc(a, 2, 4);
    ajouterArc(a, 3, 3);
    ajouterArc(a, 3, 2);
    afficherReseau(a);
    sup = supprimerArc(a, 1, 2);
    afficheMessageCodeRetour(sup);
    afficherReseau(a);
    sup = supprimerNoeud(a, 2);
    afficheMessageCodeRetour(sup);
    afficherReseau(a);
}

int main() {
    testReseau();
    return 0;
}