#ifndef GRAPHE_H
#define GRAPHE_H

#include "codeRetour.h"

typedef enum{FALSE, TRUE} Booleen;

typedef struct CellNoeud{
    int num;
    Booleen marque;
    struct CellNoeud *suiv;
    struct CellArc *listeArcs;
}Noeud;

typedef struct CellArc{
    Noeud *ext;
    struct CellArc *suiv;
}Arc;

typedef Noeud *Reseau;

Reseau creerReseauVide(void);
Booleen estReseauVide(Reseau r);
void afficherReseau(Reseau r);
Booleen rechercherNoeud(Reseau r, int num);
CodeRetour ajouterNoeud(Reseau * ptr, int num);
CodeRetour rechercherArc(Reseau r, int dep, int arr);
CodeRetour ajouterArc(Reseau r, int dep, int arr);
CodeRetour supprimerArc(Reseau r, int dep, int arr);
CodeRetour supprimerNoeud(Reseau r, int noeud);



#endif // GRAPHE_H