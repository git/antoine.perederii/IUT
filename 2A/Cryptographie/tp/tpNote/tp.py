################################
###         TP NOTE          ###
### étudiants :              ###
### Damien Nortier (PM4)     ###
### Antoine Perederi (PM4)   ###
################################

import sqlite3
import numpy.random  as npr
#### Ouvrir la connection ou creer la base
connection = sqlite3.connect("donnees.db")
#### Creer un curseur
curseur = connection.cursor()
#### Creer la table : executer une seule fois
try:
    curseur.execute("CREATE TABLE utilisateurs (name TEXT, password TEXT)")
except:
    print()

# Exercice 1

def AjouterUtilisateur(curseur):
    login = str(input("login : "))
    curseur.execute("SELECT count(*) FROM utilisateurs WHERE name = \"" + str(login) + "\"")
    nb = (curseur.fetchall())[0][0]
    if nb != 0 :
        print("utilisateur déjà existant")
    else :
        mdp = str(input("mot de passe : "))
        curseur.execute("INSERT INTO utilisateurs VALUES(\"{}\",\"{}\")".format(login, mdp))

def TestAjouterUtilisateur(curseur) :
    nb = int(input("nb insertions : "))
    for i in range(nb):
        AjouterUtilisateur(curseur)

# TestAjouterUtilisateur(curseur)

def AfficherBdd(curseur):
    curseur.execute("SELECT * FROM utilisateurs")
    utilisateurs = curseur.fetchall()
    if(utilisateurs):
        for utilisateur in utilisateurs:
            print(utilisateur)
    else:
        print("Aucun users dans la db")

def TestAfficherBdd(curseur) :
    for i in range (5) :
        AjouterUtilisateur(curseur)
    AfficherBdd(curseur)

# TestAfficherBdd(curseur)

def Verification(curseur):
    login = input("login :")
    password = input("password :")
    curseur.execute("SELECT * FROM utilisateurs WHERE name=? and password=?", (login, password))
    if curseur.fetchone():
        print("Authentification ok")
    else :
        print("login ou MdP incorrect")


# Exercice 2
import hashlib

def AjouterUtilisateurHash(curseur):
    login = input("login :")

    curseur.execute("SELECT * FROM utilisateurs WHERE name=?", (login, ))
    if curseur.fetchone():
        print("login deja existant")
        return # pour quitter la fct
    
    password = input("password :")

    password2 = input("password Verification :")
    if password != password2:
        print("MdP differents")
        return # pour quitter la fct
    
    passwordHash = hashlib.sha256(password.encode()).hexdigest()

    curseur.execute("INSERT INTO utilisateurs VALUES (?, ?)", (login, passwordHash))
    connection.commit()

    print("User insert with succes")


def VerificationHash(curseur):
    login = input("login :")
    password = input("password :")
    passwordHash = hashlib.sha256(password.encode()).hexdigest()
    curseur.execute("SELECT * FROM utilisateurs WHERE name=? and password=?", (login, passwordHash))
    if curseur.fetchone():
        print("Authentification ok")
    else :
        print("login ou MdP incorrect")

# AjouterUtilisateurHash(curseur)
# VerificationHash(curseur)

# Exercice 3
difMdP = "LaDbDeDamienEtAntoine"

def AjouterUtilisateurHashSel(curseur, sel):
    login = input("login :")

    curseur.execute("SELECT * FROM utilisateurs WHERE name=?", (login, ))
    if curseur.fetchone():
        print("login deja existant")
        return # pour quitter la fct
    
    password = input("password :")

    password2 = input("password Verification :")
    if password != password2:
        print("MdP differents")
        return # pour quitter la fct

    passwordSel = password + sel
    
    passwordHash = hashlib.sha256(passwordSel.encode()).hexdigest()

    curseur.execute("INSERT INTO utilisateurs VALUES (?, ?)", (login, passwordHash))
    connection.commit()

    print("User insert with succes")

def VerificationHashSel(curseur, sel):
    login = input("login :")
    password = input("password :")
    passwordHash = hashlib.sha256(password.encode() + sel.encode()).hexdigest()
    curseur.execute("SELECT * FROM utilisateurs WHERE name=? and password=?", (login, passwordHash))
    if curseur.fetchone():
        print("Authentification ok")
    else :
        print("login ou MdP incorrecte")

# AjouterUtilisateurHashSel(curseur, difMdP)
# AfficherBdd(curseur)
# VerificationHashSel(curseur, difMdP)

# Exercice 4

def AjouterUtilisateurHashSelRandom(curseur) :
    try:
        curseur.execute("CREATE TABLE hash (name TEXT, sel NUMERIC)")
    except:
        a = 0 # juste pour pas laisser ça vide
    sel = npr.randint(1, 10)
    login = input("login :")

    curseur.execute("SELECT * FROM utilisateurs WHERE name=?", (login, ))
    if curseur.fetchone():
        print("login deja existant")
        return # pour quitter la fct
    
    password = input("password :")

    password2 = input("password Verification :")
    if password != password2:
        print("MdP differents")
        return # pour quitter la fct

    passwordSel = password + str(sel)
    
    passwordHash = hashlib.sha256(passwordSel.encode()).hexdigest()

    curseur.execute("INSERT INTO utilisateurs VALUES (?, ?)", (login, passwordHash))
    curseur.execute("INSERT INTO hash VALUES (?, ?)", (login, str(sel)))
    connection.commit()

    print("User insert with success")


def AfficherBddPourTestSelRandom(curseur):
    print("\t\tutilisateurs")
    curseur.execute("SELECT * FROM utilisateurs")
    utilisateurs = curseur.fetchall()
    if(utilisateurs):
        for utilisateur in utilisateurs:
            print(utilisateur)
    else:
        print("Aucun users dans la db")
    print("\thash")
    curseur.execute("SELECT * FROM hash")
    hashs = curseur.fetchall()
    if(hashs):
        for sel in hashs:
            print(sel)
    else:
        print("Aucun hash dans la db")

def TestAjouterUtilisateurHashSelRandom(curseur) :
    for i in range (5) :
        AjouterUtilisateurHashSelRandom(curseur)
    AfficherBddPourTestSelRandom(curseur)

# TestAjouterUtilisateurHashSelRandom(curseur)


# Exercice 5
import bcrypt


def AjouterUtilisateurBcrypt(curseur):
    login = input("login :")

    curseur.execute("SELECT * FROM utilisateurs WHERE name=?", (login, ))
    if curseur.fetchone():
        print("login deja existant")
        return # pour quitter la fct
    
    password = input("password :")

    password2 = input("password Verification :")
    if password != password2:
        print("MdP differents")
        return # pour quitter la fct
    
    passwordHash = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

    curseur.execute("INSERT INTO utilisateurs VALUES (?, ?)", (login, passwordHash.decode()))
    connection.commit()

    print("User insert with succes")

def VerificationBcrypt(curseur):
    login = input("login :")
    password = input("password :")

    curseur.execute("SELECT password FROM utilisateurs WHERE name=?", (login,))

    bdPassword = curseur.fetchone()

    if bdPassword:
        hashS = bdPassword[0]
        if bcrypt.checkpw(password.encode(), hashS.encode()):
            print("login ok")
        else:
            print("login ou MdP incorrect")
    else:
        print("login ou MdP incorrect")

# AjouterUtilisateurBcrypt(curseur)
# AfficherBdd(curseur)
# VerificationBcrypt(curseur)


# Exercice 6
from Crypto.Cipher import AES
# from Crypto.random import get_random_bytes
from Crypto.Util.Padding import pad, unpad

CLE_AES = b'DAMIEN ANTOINEPE'

# curseur.execute("CREATE TABLE utilisateursExo6 (name TEXT, passwordHash TEXT, nonce TEXT)")

def chiffrementAES(message):
    res = AES.new(CLE_AES, AES.MODE_CTR)
    nonce = cipher.nonce
    ciphertext = ciph.encrypt(pad(message.encode(), AES.block_size))
    return ciphertext, nonce 

def dechiffrementAES(ciphertext, nonce):
    res = AES.new(CLE_AES, AES.MODE_CTR, nonce=nonce)
    decrypted = unpad(cipher.decrypt(ciphertext), AES.block_size)
    return decrypted.decode()

def AjouterUtilisateurAES(curseur):
    login = input("login :")

    curseur.execute("SELECT * FROM utilisateurs WHERE name=?", (login, ))
    if curseur.fetchone():
        print("login deja existant")
        return # pour quitter la fct
    
    password = input("password :")

    password2 = input("password Verification :")
    if password != password2:
        print("MdP differents")
        return # pour quitter la fct
    
    passwordHash = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

    encryptedHash, nonce = chiffrementAES(passwordHash.decode())

    curseur.execute("INSERT INTO utilisateursExo6 VALUES (?, ?, ?)", (login, encryptedHash, nonce.hex()))
    connection.commit()

    print("User insert with succes")

def VerificationAES(curseur):
    login = input("login :")
    password = input("password :")

    curseur.execute("SELECT passwordHash, nonce FROM utilisateursExo6 WHERE name=?", (login,))

    bdPassword = curseur.fetchone()

    if bdPassword:
        hashS = bdPassword[0]
        nonceDb = bdPassword[1]
        decryptedHash = dechiffrementAES(hashS, nonceDb)

        if bcrypt.checkpw(password.encode(), decryptedHash.encode()):
            print("login ok")
        else:
            print("login ou MdP incorrect")
    else:
        print("login ou MdP incorrect")

AjouterUtilisateurAES(curseur)
AfficherBdd(curseur)
VerificationAES(curseur)

# Exercice 7
import random
import string

# 1

def generateurMdP(N, n, p):
    MdP = []
    for i in range(N):
        mpd = ''.join(random.choice(alph) for j in range(n))
        MdP.append(mdp)
    return MdP

