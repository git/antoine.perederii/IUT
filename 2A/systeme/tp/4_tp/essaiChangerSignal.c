#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>


void coucou(int sig) {
    printf(" Coucou , sig %d\n", sig);
}
int main () {
    struct sigaction action, save;
    sigemptyset(&action.sa_mask);
    action.sa_handler = coucou;
    action.sa_flags = 0;
    if(sigaction(SIGUSR1, &action, &save)) {
        perror(" Installation coucou "); exit(1);
    }
    sleep(20);
    printf(" Fin pause \n");
    if(sigaction(SIGUSR1, &save, NULL)) {
        perror(" Restauration signaux "); exit(1);
    }
    return 0;
}