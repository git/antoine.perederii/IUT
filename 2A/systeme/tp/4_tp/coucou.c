#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h> // pour pid

#define TAILLE_BUF 80 // 80 = taille d'une ligne

void coucou(int sig) {
    printf("Coucou , sig %d\n", sig);
}

void codeDuFils(int tube[2]) {
    char buffer[TAILLE_BUF];
    unsigned int cpt = 0;
    while(read(tube[0], buffer, TAILLE_BUF)){
        cpt ++;
        printf("\t\e[1;33mFils : %d : lecture de\e[1;31m %s\e[0m\n", cpt, buffer);
        // kill(getppid(),SIGUSR1);
    }
    printf("Fils se termine sur fin de lecteure du tube\n");
    close(tube[0]);
    exit(0);
}

void codeDuPere(int tube[2]) {
    char buffer[TAILLE_BUF];
    struct sigaction action, save;
    sigemptyset(&action.sa_mask);
    action.sa_handler = coucou ;
    action.sa_flags = 0;

    do {
        errno = 0;
        while(fgets(buffer, TAILLE_BUF, stdin) != NULL){
            write(tube[1], buffer, TAILLE_BUF);
            if(sigaction(SIGUSR1, &action, &save)) {
                perror("Installation coucou"); exit(1);
            }
        }
    } while(errno == EINTR && !feof(stdin));

    if(sigaction(SIGUSR1, &save, NULL)) {
        perror("Restauration signaux"); exit(1);
    }

    close(tube[1]);
    wait(NULL);
    printf("Pere se termine apès son fils.\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	int tube[2];
    if(pipe(tube)== -1) { 
        perror(" pipe "); 
        exit(errno); 
    }

    switch(fork()) {
        case -1 : 
            perror(" fork "); 
            exit(errno);
        case 0 : // le fils
            close(tube[1]);
            codeDuFils(tube);
            exit(0);
        default : // le pere
            close(tube[0]);
            codeDuPere(tube);
    }
    return 0;
}