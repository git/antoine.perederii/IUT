#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h> // pour pid

#define TAILLE_BUF 80 // 80 = taille d'une ligne

volatile pid_t FilsPid; //volatile permet de mettre à jour la valeur pour tout le processus dans le registre du processeur
volatile unsigned int cpt;

void coucou(int sig) {
    // printf("Coucou , sig %d\n", sig);
    kill(FilsPid, SIGUSR1);
}

void remisAzeroCpt(int sig) {
    cpt = 0;
}

void codeDuFils(int tube[2]) {
    char buffer[TAILLE_BUF];
    cpt = 0;
    struct sigaction actionFils, saveFils;
    sigemptyset(&actionFils.sa_mask);
    actionFils.sa_handler = remisAzeroCpt;
    actionFils.sa_flags = 0;

    if(sigaction(SIGUSR1, &actionFils, &saveFils)) {
        perror("Installation coucou"); exit(1);
    }

    do {
        errno = 0;
        while(read(tube[0], buffer, TAILLE_BUF)){
            cpt ++;
            printf("\t\e[1;33mFils : %d : lecture de\e[1;31m %s\e[0m\n", cpt, buffer);
        }
    } while(errno == EINTR && !feof(stdin));
    
    if(sigaction(SIGUSR1, &saveFils, NULL)) { // 0 c'est faux et tout le reste est vraie. Et quand sigaction reussi il retourne 0
        perror("Restauration signaux"); exit(1);
    }

    printf("Fils se termine sur fin de lecteure du tube\n");
    close(tube[0]);
    exit(0);
}

void codeDuPere(int tube[2]) {
    char buffer[TAILLE_BUF];
    struct sigaction actionPere, savePere;
    sigemptyset(&actionPere.sa_mask);
    actionPere.sa_handler = coucou ;
    actionPere.sa_flags = 0;

    if(sigaction(SIGUSR1, &actionPere, &savePere)) {
        perror("Installation coucou"); exit(1);
    }

    do {
        errno = 0;
        while(fgets(buffer, TAILLE_BUF, stdin) != NULL){
            write(tube[1], buffer, TAILLE_BUF);
        }
    } while(errno == EINTR && !feof(stdin));

    // do {

    // } while (...); // forcement vraie au premieer tour

    if(sigaction(SIGUSR1, &savePere, NULL)) { // 0 c'est faux et tout le reste est vraie. Et quand sigaction reussi il retourne 0
        perror("Restauration signaux"); exit(1);
    }

    close(tube[1]);
    wait(NULL);
    printf("Pere se termine apès son fils.\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	int tube[2];
    if(pipe(tube)== -1) { 
        perror(" pipe "); 
        exit(errno); 
    }

    switch(FilsPid = fork()) {
        case -1 : 
            perror(" fork "); 
            exit(errno);
        case 0 : // le fils
            close(tube[1]);
            codeDuFils(tube);
            exit(0);
        default : // le pere
            close(tube[0]);
            codeDuPere(tube);
    }
    return 0;
}