#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAILLE_BUF 80 // 80 = taille d'une ligne

void fermeTubesFils(int N, int tubes[N][2]) {
    for (int i = 0; i < N; i++) {
        close(tubes[i][1]);
    }
}

void codeDuFils(int N, int tubes[N][2], int numFils) {
    char buffer[TAILLE_BUF];
    unsigned int cpt = 0;

    close(tubes[numFils][1]);  // Fermer l'extrémité d'écriture inutile du tube

    while (read(tubes[numFils][0], buffer, TAILLE_BUF) > 0) {
        cpt++;
        printf("\t\e[1;33mFils n° %d : %d : lecture de\e[1;31m %s\e[0m\n", numFils, cpt, buffer);
    }

    printf("Fils se termine sur fin de lecture du tube\n");
    close(tubes[numFils][0]);
    exit(0);
}

void codeDuPere(int N, int tubes[N][2], int i) {
    char buffer[TAILLE_BUF];
    close(tubes[i][0]);  // Fermer l'extrémité de lecture inutile du tube

    while (fgets(buffer, TAILLE_BUF, stdin) != NULL) {
        int dest_process;
        char text[TAILLE_BUF];
        if (sscanf(buffer, "%d %[^\n]", &dest_process, text) == 2 && dest_process >= 0 && dest_process < N) {
            // Écriture du texte dans le tube du processus destinataire approprié
            write(tubes[dest_process][1], text, strlen(text));
        } else {
            fprintf(stderr, "Message invalide (il faut mettre le numero du fils en 1er) : %s", buffer);
        }
    }

    fermeTubesFils(N, tubes);
    for (int j = 0; j < N; j++) {
        wait(NULL);  // Attendre que tous les fils se terminent
    }

    printf("Père se termine après ses fils.\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
    int i, N;
    pid_t pid;
    int tubes[100][2];

    if (argc != 2) {
        fputs("Donnez un argument entier.\n", stderr);
        exit(1);
    }

    N = atoi(argv[1]);
    if (N <= 0 || N > 100) {
        fprintf(stderr, "N doit être un entier positif ou inférieur à 100.\n");
        exit(1);
    }

    // Créez les tubes pour la communication entre les processus
    for (i = 0; i < N; i++) {
        if (pipe(tubes[i]) == -1) {
            perror("pipe");
            exit(errno);
        }
    }

    /* Le père va créer N fils */
    for (i = 0; i < N; i++) {
        if ((pid = fork()) == -1) {
            perror("pb fork");
            exit(errno);
        } else if (pid == 0) {
            codeDuFils(N, tubes, i);  // Code du fils
        }
    }

    codeDuPere(N, tubes, 0);  // Code du père (utilisation du premier tube)
    return 0;
}
