#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

# define TAILLE_BUF 80 // 80 = taille d'une ligne


void codeDuFils(int tube[2]) {
    char buffer[TAILLE_BUF];
    unsigned int cpt = 0;
    while(read(tube[0], buffer, TAILLE_BUF)){
        cpt ++;
        printf("\t\e[1;33mFils : %d : lecture de\e[1;31m %s\e[0m\n", cpt, buffer);
    }
    printf("Fils se termine sur fin de lecteure du tube\n");
    close(tube[0]);
    exit(0);
}

void codeDuPere(int tube[2]) {
    char buffer[TAILLE_BUF];

    while(fgets(buffer, TAILLE_BUF, stdin) != NULL){
        write(tube[1], buffer, TAILLE_BUF);
    }

    close(tube[1]);
    wait(NULL);
    printf("Pere se termine apès son fils.\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	int tube[2];
    if(pipe(tube)== -1) { 
        perror(" pipe "); 
        exit(errno); 
    }

    switch(fork()) {
        case -1 : 
            perror(" fork "); 
            exit(errno);
        case 0 : // le fils
            close(tube[1]);
            codeDuFils(tube);
            exit(0);
        default : // le pere
            close(tube[0]);
            codeDuPere(tube);
    }
    return 0;
}