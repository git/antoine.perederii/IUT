#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>

int main(int argc, char **argv) {

	pid_t pid;
	int i, etat;
	struct timespec t;

	switch(pid=fork()) {
		case -1 : /* Oups !!! fork n'a pas marché !*/
            perror("fork"); exit(errno);

		case  0 : /* Code du fils */
	       	printf("[fils]: je suis le fils de pid %d\n", getpid());
            for (i=0; i<10 ; i++) {
				printf("[fils]: %d\n",i);
				t.tv_sec=0;
				t.tv_nsec=500000000;
				nanosleep(&t,NULL);
			}
			system("ps -f");
			exit(2);
            break;

        default : /* Code du père*/
			if ((pid=wait(&etat))==-1) {
				perror("pb avec le wait");
				exit(errno);
			}
			if (WIFEXITED(etat))
				printf("[pere]: mon fils %d a retourne le code %d\n", pid, WEXITSTATUS(etat));
			else
				printf("[pere]: mon fils %d s est mal termine\n",pid);
			printf("[pere]: Fin du processus pere de pid %d.\n", getpid());
       }

      

       exit(0);
}
