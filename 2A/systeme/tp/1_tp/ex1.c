#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	for(int i = 0; i < argc; ++i)
		printf("argument %d : %s \n", i, argv[i]);
		
	puts("\n----------------\n"); //mieux qu'un printf car moins gourmand pour char()
	printf("PATH => %s\n", getenv("PATH"));

	return 0;
}
