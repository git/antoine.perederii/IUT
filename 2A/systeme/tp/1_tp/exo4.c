#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <time.h>

/* Correction TP1 exo4 by O Guinaldo */

void codeDuFils(int n) {
	int i;
	struct timespec t;

	for (i=0 ; i<10 ; i++) {
		printf("(numero %d dit :) %d\n", n, i);
		t.tv_sec=0;
		t.tv_nsec=300000000+n*100000000;
		/* il n'affiche pas au meme rythme */
		nanosleep(&t, NULL);
	}
	exit(n); /*le fils retourne son numero comme code de retour */
}

int main (int argc, char* argv[]){

	int i, etat, N;
	pid_t pid;

	if (argc!=2) {
		fputs("Donner un arg entier\n", stderr);
		exit(1);
	}
	/* le atoi ne g�re pas les erreurs
	preferer le sscanf (un scanf dans une chaine) */
	N=atoi(argv[1]); 	

	/* le pere va creer N fils */
	for (i=0 ; i<N ; i++) {
		if((pid=fork())==-1) {
			perror("pb fork");
			exit(errno);
		}
		else if (pid==0) {
			codeDuFils(i);
			exit(0); 
			/* le ieme fils ne doit pas retourner dans la boucle */
		}
	}

	/* la suite n'est faite que par le p�re */ 	
	for (i=0 ; i<N ; i++) {
	if ((pid=wait(&etat))==-1) {perror("pb wait"); exit(errno);}
	if (WIFEXITED(etat))
		printf("(pere:) fils %d a retourne le code %d\n", pid, WEXITSTATUS(etat));
	else
		printf("(pere:) fils %d s'est mal termine\n", pid);
	}
	puts("(pere:) appli terminee");

	exit(0);
}
