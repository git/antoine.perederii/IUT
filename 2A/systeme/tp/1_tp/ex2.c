#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main(void) {
	
	if(system("ps -f") == -1) {
		perror("pb appel systeme");
		exit(errno);
	}
	if(execlp("/bin/ps", "ps", "-f", NULL) == -1) {
		perror("ZUT, pb avec le exec");
		exit(errno);
	}

	printf("\nTerminé !");

	exit(0);
}
