#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

void showFileInfos(char *fileName) {
    struct stat infos;

    if (stat(fileName, &infos) == -1) {
         perror("stat ");
         exit(EXIT_SUCCESS);
    }

    printf("Nom : %s\nTaille en octets : %lld octets\nDate modif : %lld s\n\n", \
            fileName, (long long) infos.st_size, (long long) infos.st_mtime);

}

// affiche tout les fichiers dans son rep par ordre de création
int listDirContent(char *dirName){
    DIR *dir;
    int len;
    struct dirent *dp;

    if((dir=opendir(dirName)) == NULL){
        perror("opendir");
        exit(errno);
    }

    while((dp =readdir(dir)) != NULL) {
        printf("%s\n", dp->d_name);
    }

    printf("\n");

    closedir(dir);
    return 0;
}

int showDir(char *dirName){
    DIR *dir;
    int len = strlen(dirName);
    struct dirent *dp;
    int rep;

    if((dir=opendir(dirName)) == NULL){
        perror("opendir");
        exit(errno);
    }

    showFileInfos(dirName);

    while((dp =readdir(dir)) != NULL) {
        char * path; // dirName + / + dp->d_name

        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0){
            continue; //passe au tour de boucle suivant
        }

        if((path=malloc(len+strlen(dp->d_name)+2)) == NULL){
            perror("malloc");
            exit(errno);
        }

        strcpy(path, dirName); 
        strcat(path, "/");
        strcat(path, dp->d_name);

        if(dp->d_type == DT_DIR) {
            rep = showDir(path);
        }
        else showFileInfos(path);

        free(path);
    }

    closedir(dir);
    return 0;
}

int main(int argc, char* argv[])
{
    int rep;
    if(argc != 2){
        fprintf(stderr, "Usage : %s <pathName>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    rep = showDir(argv[1]);

    return 0;
}
// si rep et != . et ..