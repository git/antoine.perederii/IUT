#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>

void showFileInfos(char *fileName) {
    struct stat infos;

    if (stat(fileName, &infos) == -1) {
         perror("stat");
         exit(EXIT_SUCCESS);
    }

    printf("Nom : %s\nTaille en octets : %lld octets\nDate modif : %lld s\n", \
            fileName, (long long) infos.st_size, (long long) infos.st_mtime);

}

int main(int argc, char* argv[])
{
    if(argc != 2){
        fprintf(stderr, "Usage : %s <pathName>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    showFileInfos(argv[1]);
    
    return 0;
}