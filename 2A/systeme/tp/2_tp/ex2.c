#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

void showFileInfos(char *fileName) {
    struct stat infos;

    if (stat(fileName, &infos) == -1) {
         perror("stat");
         exit(EXIT_SUCCESS);
    }

    printf("Nom : %s\nTaille en octets : %lld octets\nDate modif : %lld s\n", \
            fileName, (long long) infos.st_size, (long long) infos.st_mtime);

}

// affiche tout les fichiers dans son rep par ordre de création
void listDirContent(char *dirName){
    DIR *dir;
    struct dirent *dp;

    if((dir=opendir(dirName)) == NULL){
        perror("opendir");
        exit(errno);
    }

    while((dp =readdir(dir)) != NULL) {
        printf("%s\n", dp->d_name);
    }
    
    printf("\n");

    closedir(dir);
}

int main(int argc, char* argv[])
{
    if(argc != 2){
        fprintf(stderr, "Usage : %s <pathName>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    // showFileInfos(argv[1]);
    listDirContent(argv[1]);

    return 0;
}