package launch;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.Fenetre;

import java.io.IOException;
import java.util.Objects;

public class Launch extends Application {
    Parent root = FXMLLoader.load(getClass().getResource("/fxml/Fenetre.fxml"));

    public Launch() throws IOException {
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}