import java.util.*

object Main {
    private const val ECHAPEMENT = "_"
    private fun verifieChaqueCaractereEtConcatener(motATrouver: String, motSaisi: String): String {
        var retour = ""
        if(motATrouver.length != motSaisi.length) {
            return "____ \nPb, taille des mots différents"
        }
        for (i in 0 until motATrouver.length) {
            retour += if (prendSiIdentique(
                    motATrouver.lowercase(Locale.getDefault())[i],
                    motSaisi.lowercase(Locale.getDefault())[i]
                )
            ) motSaisi[i]
            else ECHAPEMENT
        }
        return retour
    }

    private fun prendSiIdentique(aTrouver: Char, proposition: Char) = aTrouver == proposition

    @JvmStatic
    fun main(args: Array<String>) {
        val MOT_A_TROUVER = "clermo"
//        val saisie = readLine() ?: ""
//        val saisie = Scanner(System.`in`) // Readline à utiliser readline?
        val foundLetters = mutableListOf<Int>()
        var vie = "♥ ♥ ♥ ♥ "
        var MOT_EN_RECHERCHE = ""
        for (i in 0 until MOT_A_TROUVER.length) {
            MOT_EN_RECHERCHE += "_"
        }
        println("Vous devez trouver ${MOT_EN_RECHERCHE}")
        while (!vie.isEmpty()) {
            println("PV : $vie")
            print("Entrez votre proposition $MOT_EN_RECHERCHE:")
            val motSaisi = readLine() ?: ""
            val res = verifieChaqueCaractereEtConcatener(MOT_A_TROUVER, motSaisi)
            println(res)
            //val res = resultat.replace("_", "")
            for (i in 0 until MOT_A_TROUVER.length) {
                if (res.get(i) != '_' && MOT_EN_RECHERCHE[i] == '_') {
                    foundLetters += i
                    if (foundLetters.contains(i)) {

                        MOT_EN_RECHERCHE.set [i] = MOT_A_TROUVER[i]
                    } else
                        MOT_EN_RECHERCHE[i] = res[i]
                }
            }
            //MOT_EN_RECHERCHE = .replace("_", "")
            if (res.length == 0 && vie.length != 0) {
                vie = vie.substring(0, vie.length - 2)
            } else if (MOT_EN_RECHERCHE == MOT_A_TROUVER) {
                break
            }
        }
        if (vie.isEmpty()) {
            println("C'est perdu")
        } else {
            println("C'est gagné")
        }
    }
}