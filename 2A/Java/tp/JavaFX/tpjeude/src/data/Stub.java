package data;

import model.Game;
import model.Player;

public class Stub {
    public Game loadPlayer() {
        Game game = new Game();
        game.addPlayer(new Player("Cedric"));
        game.addPlayer(new Player("Jérome"));
        return game;
    }
}
