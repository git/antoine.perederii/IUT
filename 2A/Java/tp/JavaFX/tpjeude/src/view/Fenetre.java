package view;

import data.Stub;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Game;
import model.Player;

import java.util.List;

public class Fenetre {

    @FXML
    private Button buttonHold;

    @FXML
    private Button buttonRoll;

    @FXML
    private Text topText;

    @FXML
    private Text bottomText;
    @FXML
    private TextField joueur1;
    @FXML
    private TextField joueur2;

    @FXML
    private ImageView diceImage;
    private boolean hasRolled = false;
    private final Game game = new Stub().loadPlayer();

    @FXML
    private void onButtonHoldClicked() {
        joueur1.setDisable(true);
        joueur2.setDisable(true);
//        hasRolled = true;
        game.passToNextPlayer();
        updateUI();
    }

    @FXML
    private void onButtonRollClicked() {
        joueur1.setDisable(true);
        joueur2.setDisable(true);
//        hasRolled = true;
        game.rollDice();
        updateUI();
    }


    private void updateUI() {
        Player currentPlayer = game.getCurrentPlayer();

        int diceValue = 0;
        try {
//            if(hasRolled) {
                diceValue = game.getDiceValue();
//            }
        } catch (IllegalAccessException e) {
            topText.setStyle("-fx-fill: black;");
            topText.textProperty().unbind();
            topText.textProperty().bind(game.getCurrentPlayer().nameProperty().concat(", press R to roll the dice or H to hold your score."));
            bottomText.textProperty().unbind();
            bottomText.textProperty().bind(game.getCurrentPlayer().nameProperty().concat(" (").concat(game.getCurrentPlayer().totalScoreProperty()).concat(") has currently ").concat(game.getCurrentPlayer().currentScoreProperty()));
//            e.printStackTrace();
            return;
        }
        if (currentPlayer.getTotalScore() == Game.LOSE_DICE_VALUE) {
            game.passToNextPlayer();
            topText.textProperty().unbind();
            topText.textProperty().bind(game.getCurrentPlayer().nameProperty().concat(" à perdue\n").concat(game.getCurrentPlayer().nameProperty()).concat(", press R to roll the dice or H to hold your score."));
//            hasRolled = false;
        } else if (currentPlayer.getTotalScore() >= Game.SCORE_TO_WIN) {
            topText.textProperty().unbind();
            topText.textProperty().bind(currentPlayer.nameProperty().concat(" wins the game"));
            topText.setStyle("-fx-fill: #e74c3c;");
            buttonRoll.setDisable(true);
            buttonHold.setDisable(true);
        } else {
            topText.setStyle("-fx-fill: black;");
            topText.textProperty().unbind();
            topText.textProperty().bind(game.getCurrentPlayer().nameProperty().concat(", press R to roll the dice or H to hold your score."));
            bottomText.textProperty().unbind();
            bottomText.textProperty().bind(game.getCurrentPlayer().nameProperty().concat(" (").concat(game.getCurrentPlayer().totalScoreProperty()).concat(") has currently ").concat(game.getCurrentPlayer().currentScoreProperty()));
        }
        String imagePath = "/dice/" + diceValue + ".png";
        Image image = new Image(imagePath);
        diceImage.setImage(image);
    }


    public void initialize() {
        joueur1.textProperty().bindBidirectional(game.getCurrentPlayer().nameProperty());
        game.passToNextPlayer();
        joueur2.textProperty().bindBidirectional(game.getCurrentPlayer().nameProperty());
        updateUI();
    }
}
