package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Player {
    private static final String BASE_PLAYER_NAME_DEFAULT = "Player ";
    private static int num = 0;
    private StringProperty name = new SimpleStringProperty();
        public String getName() { return name.get(); }
        public StringProperty nameProperty() { return name; }
        private void setName(String name) { this.name.set(name); }

    private IntegerProperty totalScore = new SimpleIntegerProperty();
        public Integer getTotalScore() { return totalScore.get(); }
        public IntegerProperty totalScoreProperty() { return totalScore; }
        public void setTotalScore(Integer totalScore) { this.totalScore.set(totalScore); }
    private IntegerProperty currentScore = new SimpleIntegerProperty();
        public Integer getCurrentScore() { return currentScore.get(); }
        public IntegerProperty currentScoreProperty() { return currentScore; }
        public void setCurrentScore(Integer currentScore) { this.currentScore.set(currentScore); }

    public Player(String name) {
        setName(name);
    }

    public Player() {
        this(BASE_PLAYER_NAME_DEFAULT + ++num);
    }

    @Override
    public String toString() {
        return name + " (" + totalScore + ") has currently " + currentScore;
    }
}
