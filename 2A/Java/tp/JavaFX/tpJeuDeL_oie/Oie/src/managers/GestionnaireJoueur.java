package managers;

import javafx.beans.Observable;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import models.Joueur;

import java.util.ArrayList;
import java.util.List;

public class GestionnaireJoueur {
    private  static int index = 0;
    private final ObservableList<Joueur> myList = FXCollections.observableArrayList();
    private final ListProperty<Joueur> lesJoueurs = new SimpleListProperty(myList);
        public ObservableList<Joueur> getLesJoueurs() {
            return lesJoueurs.get();
        }
        public ListProperty<Joueur> lesJoueursProperty() {
            return lesJoueurs;
        }
    private ObjectProperty<Joueur> currentJoueur = new SimpleObjectProperty<>();
        public Joueur getCurrentJoueur() {
            return currentJoueur.get();
        }
        public ObjectProperty<Joueur> currentJoueurProperty() {
            return currentJoueur;
        }
        public void setCurrentJoueur(Joueur joueur) {
            currentJoueur.set(joueur);
        }
    //    private List<Joueur> lesJoueurs = new ArrayList<>();
//    private StringProperty name = new SimpleStringProperty();
//        public String getName() { return name.get(); }
//        public StringProperty nameProperty() { return name; }
//        private void setName(String name) { this.name.set(name); }

//    public List<Joueur> getLesJoueurs() {
//        return this.lesJoueurs;
//    }
//    public Joueur getCurrentJoueur() {
//        return currentJoueur;
//    }
    public Joueur getJoueurSuivant() {
        return this.myList.get((currentJoueur.get().getIdJoueur() + 1) % lesJoueurs.size());
    }
    public void ajouterJoueur(Joueur joueur) {
        myList.add(joueur);
        if(myList.size() == 1) {
            currentJoueur.set(myList.get(0));
        }
    }
    public Joueur nextPlayer() {
        index = (index +1) % this.myList.size();
        setCurrentJoueur(myList.get(index));
        return getCurrentJoueur();
//        return myList.get((currentJoueur.getIdJoueur()) % lesJoueurs.size());
    }
}
