package managers;

import models.Case;
import models.Joueur;
import models.Plateau;

public class DeplaceurJoueur {
//    private boolean verifPresenceJoueur(Joueur currentJoueur, int valeurDe, Plateau plateau) {
//        int nextIndex = (currentJoueur.getMyCase().getIdCase() + valeurDe) % plateau.getCaseList().size();
//        Case caseSuivante = plateau.getCaseList().get(nextIndex);
//        return caseSuivante.getCurrentJoueur() != null;
//    }

//    private void alternerJoueurs(Joueur joueur1, Joueur joueur2) {
//        // Échanger les positions des joueurs
//        Case caseJoueur1 = joueur1.getMyCase();
//        Case caseJoueur2 = joueur2.getMyCase();
//
//        caseJoueur1.setCurrentJoueur(joueur2);
//        caseJoueur2.setCurrentJoueur(joueur1);
//
//        joueur1.setMyCase(caseJoueur2);
//        joueur2.setMyCase(caseJoueur1);
//
//        System.out.println("Échange de positions entre le joueur " + joueur1.getIdJoueur() + " et le joueur " + joueur2.getIdJoueur());
//    }
    public void alternerPosition(Joueur joueurActuel, Joueur joueurSuivant) {
        Case caseActuelle = joueurActuel.getMyCase();
        Case caseSuivante = joueurSuivant.getMyCase();

        if(caseActuelle == null) {
            caseActuelle.setCurrentJoueur(null);
        } else {
            caseActuelle.setCurrentJoueur(joueurSuivant);
        }
        caseSuivante.setCurrentJoueur(joueurActuel);

        joueurActuel.setMyCase(caseSuivante);
        joueurSuivant.setMyCase(caseActuelle);
    }
//    public void deplacerJoueur(Joueur currentJoueur, int nbCases, Plateau plateau) {
//        if (verifPresenceJoueur(currentJoueur, nbCases, plateau)) {
//            // La case suivante est occupée ou en dehors du plateau, alterner les joueurs ou prendre une action appropriée
//            alternerJoueurs(currentJoueur, currentJoueur);
//        } else {
//            int positionActuelle = currentJoueur.getMyCase().getIdCase();
//            int nouvellePosition = (positionActuelle + nbCases) % plateau.getCaseList().size();
//
//            Case caseSuivante = plateau.getCaseList().get(nouvellePosition);
//            currentJoueur.getMyCase().setCurrentJoueur(null);
//
//            caseSuivante.setCurrentJoueur(currentJoueur);
//            currentJoueur.setMyCase(caseSuivante);
//
//            System.out.println("Le joueur " + currentJoueur.getIdJoueur() + " se déplace sur la case " + caseSuivante.getIdCase());
//        }
//    }
    public void deplacerJoueur(Case newCase, Joueur currentJoueur){
        if(newCase == null)
            return;
        if (currentJoueur.getMyCase() != null){
            currentJoueur.getMyCase().setCurrentJoueur(null);
        }
        if (newCase.getCurrentJoueur() != null){
            alternerPosition(currentJoueur, newCase.getCurrentJoueur());
        }

        currentJoueur.setMyCase(newCase);
        currentJoueur.getMyCase().setCurrentJoueur(currentJoueur);
    }
}