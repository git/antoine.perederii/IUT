package view;

import data.Stub;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import models.Jeu;
import models.Joueur;


public class Fenetre {
    @FXML
    private BorderPane myBorderPane;
    @FXML
    private VBox labelContainer;
    @FXML
    private ListView<Joueur> joueursContainer;
    private final Jeu game = new Stub().loadPions();
    Joueur currentPion = game.getMyAdminPlayer().getCurrentJoueur();
    public void updateButtonLabels() {
        for (int i = 0; i < game.getPlateau().getCaseList().size(); i++) {
            System.out.println((i + 1) + " " + game.getPlateau().getCaseList().size());
            Label label = (Label) labelContainer.getChildren().get(i);

            if (game.getPlateau().getCaseList().get(i).getCurrentJoueur() != null) {
                label.setText("Case " + (i + 1) + "\n" + "Joueur: " + game.getPlateau().getCaseList().get(i).getCurrentJoueur().getIdJoueur());
            } else {
                label.setText("Case " + (i + 1));
            }
        }
    }
    @FXML
    private void onButtonRollClicked() {
        game.lancerDes();
        updateButtonLabels();
    }
    public void initialize() {
        for (int i = 0; i < game.getPlateau().getCaseList().size(); i++) {
            Label label = new Label();
            if (game.getPlateau().getCaseList().get(i).getCurrentJoueur() != null) {
                label.setText("Case " + (i+1) + "\n" + "Joueur: " + game.getPlateau().getCaseList().get(i).getCurrentJoueur().getIdJoueur());
            } else {
                label.setText("Case " + (i+1));
            }
            labelContainer.getChildren().add(label);
        }
        joueursContainer.itemsProperty().bind(game.getMyAdminPlayer().lesJoueursProperty());
        game.getMyAdminPlayer().currentJoueurProperty().addListener(new ChangeListener<Joueur>() {
            @Override
            public void changed(ObservableValue<? extends Joueur> observable, Joueur oldValue, Joueur newValue) {
                joueursContainer.getSelectionModel().select(newValue);
            }
        });
//        == > se transforme en ça :
        game.getMyAdminPlayer().currentJoueurProperty().addListener((__,  ___, newValue) -> joueursContainer.getSelectionModel().select(newValue));


    }
//    public void initialize() {
//        de.setDisable(true);
//        jeu = new Jeu();
//        gestionnaireJoueur = new GestionnaireJoueur();
//        creationPlateau();
//        gestionnaireJoueur.getJoueurList().addListener(new ListChangeListener<Joueur>() {
//            @Override
//            public void onChanged(Change<? extends Joueur> c) {
//                while (c.next()) {
//                    if (c.wasAdded()) {
//                        for (Joueur joueur : c.getAddedSubList()) {
//                            addGridPlayer();
//                        }
//                    }
//                    if (c.wasUpdated()) {
//                        for (Joueur joueur : c.getAddedSubList()) {
//                            changeGridPlayer(joueur);
//                        }
//                    }
//                }
//            }
//        });
//
//
//    }
//
//    public void creationPlateau() {
//        int nombreColonnes = 16;
//
//        for (int i = 0; i < jeu.getPlateau().getSizeplateau(); i++) {
//            Text text = new Text(String.valueOf(i + 1));
//            text.setStyle("-fx-font-size: 35;");
//            int row = i / nombreColonnes;
//            int col = i % nombreColonnes;
//
//            gridPanel.add(text, col, row);
//            gridPanel.setMargin(text, new Insets(30, 25, 50, 0));
//            gridPanel.setAlignment(Pos.CENTER);
//        }
//    }
//
//    public void addPlayer(){
//        gestionnaireJoueur.ajouterJoueur();
//    }
//
//    public void addGridPlayer(){
//        int rowIndex = gestionnaireJoueur.getJoueurList().size();
//        Joueur joueur = gestionnaireJoueur.getJoueurList().get(rowIndex - 1);
//
//        Label idJoueurLabel = new Label("Joueur numéro " + joueur.getId());
//        gridPlayer.add(idJoueurLabel, 0, rowIndex);
//
//        Label caseJoueurLabel = new Label();
//        caseJoueurLabel.setText("Case numéro: N/A");
//
//
//        gridPlayer.add(caseJoueurLabel, 1, rowIndex);
//
//    }
//
//    public void changeGridPlayer(Joueur joueur){
//        Label caseJoueurLabel = (Label) gridPlayer.getChildren().get(joueur.getId());
//        if (joueur.getCurrentCase() != null) {
//            caseJoueurLabel.textProperty().bind(Bindings.concat("Case numéro " + joueur.getCurrentCase().getNum()));
//        } else {
//            caseJoueurLabel.textProperty().bind(Bindings.concat("Case numéro: N/A"));
//        }
//
//    }
//
//    public void start() {
//        addPlay.setDisable(true);
//        de.setDisable(false);
//        btStart.setDisable(true);
//        gestionnaireJoueur.InitaliseJoueur();
//        currentJoueur = gestionnaireJoueur.getCurrentJoueur();
//    }
//
//
//
//    public void lanceDe() {
//        idPlayer.textProperty().bind(currentJoueur.idProperty().asString());
//        deSimple.lancer();
//        idValeur.textProperty().bind(deSimple.valProperty().asString());
//        if (deSimple.getVal() == 0){
//            return;
//        }
//        System.out.println("Current Joueur = " + currentJoueur);
//        jeu.CaseDeplacer(currentJoueur,deSimple.getVal() );
//        currentJoueur = gestionnaireJoueur.switchCurrentPlayer();
//        System.out.println();
//    }
}