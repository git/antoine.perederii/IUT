package models;

import java.util.Random;

public abstract class De {
    private int valeur;
    private int nbFaces;
    Random RANDOM = new Random();
    public De(int nbFaces) {
        this.nbFaces = nbFaces;
    }
    public int getNbFaces() {
        return nbFaces;
    }

    public int getValeur() {
        return valeur;
    }
    public void setValeur(int valeur) {
        this.valeur = valeur;
    }
    public abstract int lancer();
}
