package models;

import managers.DeplaceurJoueur;
import managers.GestionnaireJoueur;

public class Jeu {
    private Plateau myPlateau;
    private De myDe;
    private GestionnaireJoueur myAdminPlayer = new GestionnaireJoueur();
    private DeplaceurJoueur myMovePlayer = new DeplaceurJoueur();
    public Jeu(int nbCases, int nbFaces) {
        myPlateau = new Plateau(nbCases);
        myDe = new DeSimple(nbFaces);
    }
    public Plateau getPlateau() {
        return this.myPlateau;
    }

    public GestionnaireJoueur getMyAdminPlayer() {
        return myAdminPlayer;
    }
    public DeplaceurJoueur getMyMovePlayer() {
        return myMovePlayer;
    }
    public void lancerDes() {
        Joueur joueurCourant = this.myAdminPlayer.getCurrentJoueur();
        int resDe = this.myDe.lancer();
        if (resDe == 0){
            return;
        }
        this.myMovePlayer.deplacerJoueur(myPlateau.rechercheCase(joueurCourant.getMyCase(), resDe), joueurCourant);
        this.myAdminPlayer.nextPlayer();
    }
}
