package models;

import java.util.ArrayList;
import java.util.List;

public class Plateau {
    private List<Case> CaseList = new ArrayList<>();
    public Plateau(int nbCase) {
        for(int i = 1; i <= nbCase; ++i) {
            CaseList.add(new Case(i));
        }
    }
    public List<Case> getCaseList() {
        return this.CaseList;
    }
    public Case rechercheCase(Case caseDepart, int nombreDeplacement){
        if (caseDepart == null){
            return CaseList.get(nombreDeplacement-1);
        }
        int val = caseDepart.getIdCase() + nombreDeplacement;

        for (int i = 0; i < getCaseList().size();i++){
            if(getCaseList().get(i).getIdCase() == val){
                return getCaseList().get(i);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String plateau = "";
        for(int i = 0; i < CaseList.size(); i++) {
            plateau = plateau.concat(CaseList.get(i).toString());
        }
        return plateau;
    }
}
