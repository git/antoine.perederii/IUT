package models;

public class Joueur {
    private static int count = 0;

    private int idJoueur;
    private String color;
    private Case myCase = null;

    public Joueur(String myColor) {
        this.idJoueur = ++count;
        this.color = myColor;
    }

    public int getIdJoueur() {
        return idJoueur;
    }

    public String getColor() {
        return color;
    }

    public Case getMyCase() {
        return this.myCase;
    }

    public void setMyCase(Case myCase) {
        this.myCase = myCase;
    }

    @Override
    public String toString() {
        return "Joueur " + idJoueur + ", couleur " + this.color;
    }
}

