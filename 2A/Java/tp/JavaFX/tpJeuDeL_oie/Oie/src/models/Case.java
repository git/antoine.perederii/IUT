package models;

import java.util.ArrayList;
import java.util.List;

public class Case {
    private int idCase;
    private Joueur myJoueur = null;
    public Case(int idCase) {
        this.myJoueur = null;
        this.idCase = idCase;
    }
    public Joueur getCurrentJoueur() {
        return this.myJoueur;
    }
    public void setCurrentJoueur(Joueur joueur) {
        this.myJoueur = joueur;
    }
    public int getIdCase() {
        return this.idCase;
    }
    @Override
    public String toString() {
        if (this.myJoueur != null) {
            return this.idCase + " " + this.myJoueur.toString() + "\n";
        }
        return this.idCase + " " + "\n";
    }
}
