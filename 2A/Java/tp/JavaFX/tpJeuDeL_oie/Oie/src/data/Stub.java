package data;

import models.Jeu;
import models.Joueur;
import models.Plateau;

public class Stub {
    public Jeu loadPions() {
        Jeu game = new Jeu(15, 6);
        game.getMyAdminPlayer().ajouterJoueur(new Joueur("rouge"));
        game.getMyAdminPlayer().ajouterJoueur(new Joueur("bleu"));
        return game;
    }
}
