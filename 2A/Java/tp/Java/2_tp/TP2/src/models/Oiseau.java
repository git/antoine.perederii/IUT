package models;

public class Oiseau extends Animal{

    public Oiseau(String nomAnimal, int zoneAnimal){
        super(nomAnimal, zoneAnimal);
    }

    public void hiverner() {
        if(this.zone == 0) {
            super.setZone(1);
        }
        else {
            super.setZone(0);
        }
    }

    @Override
    public String toString() {
        if(zone == 0) {
            return "Oiseau : " +
                    "nom='" + nom + '\'' +
                    ", zone= Sud";
        }
        return "Oiseau : " +
                "nom='" + nom + '\'' +
                ", zone= Nord";
    }
}
