package models;

public abstract class Animal {
    protected String nom;
    protected int zone;
    protected Animal(String nomAnimal, int zoneAnimal){
        this.nom = nomAnimal;
        this.zone = zoneAnimal;
    }

    public int getZone() {
        return zone;
    }

    public String getNom() {
        return nom;
    }

    protected void setZone(int zone) {
        this.zone = zone;
    }

    protected abstract void hiverner();

    @Override
    public String toString() {
        return "Animal : " +
                "nom='" + this.nom + '\'' +
                ", zone=" + zone;
    }
}
