package models;

public class AnimalFourrure extends Animal {
    private int densiteFourrure;

    public AnimalFourrure(String nomAnimal, int zoneAnimal, int densite) {
        super(nomAnimal, zoneAnimal);
        this.densiteFourrure = densite;
    }

    public void setFourrure(int densite){
        this.densiteFourrure = densite;
    }

    public int getDensite(){
        return this.densiteFourrure;
    }

    public String getType(){
        return "Fourrure";
    }

    public void hiverner() {
        if(this.densiteFourrure == 1){
            setFourrure(0);
        }
        else {
            setFourrure(1);
        }
    }

    @Override
    public String toString() {
        if(zone == 0) {
            if(densiteFourrure == 0) {
                return "AnimalFourrure :" +
                        " nom='" + nom + '\'' +
                        ", zone= Sud" +
                        ", densiteFourrure= Fine";
            }
            return "AnimalFourrure :" +
                    " nom='" + nom + '\'' +
                    ", zone= Sud" +
                    ", densiteFourrure= Epaisse";
        }
        if(zone == 1) {
            if(densiteFourrure == 0) {
                return "AnimalFourrure :" +
                        " nom='" + nom + '\'' +
                        ", zone= Nord" +
                        ", densiteFourrure= Fine";
            }
            return "AnimalFourrure :" +
                    " nom='" + nom + '\'' +
                    ", zone= Nord" +
                    ", densiteFourrure= Epaisse";
        }
        if(zone == 2) {
            if(densiteFourrure == 0) {
                return "AnimalFourrure :" +
                        " nom='" + nom + '\'' +
                        ", zone= Est" +
                        ", densiteFourrure= Fine";
            }
            return "AnimalFourrure :" +
                    " nom='" + nom + '\'' +
                    ", zone= Est" +
                    ", densiteFourrure= Epaisse";
        } else if (densiteFourrure == 0) {
            return "AnimalFourrure :" +
                    " nom='" + nom + '\'' +
                    ", zone= Ouest"+
                    ", densiteFourrure= Fine";
        }
        return "AnimalFourrure :" +
                " nom='" + nom + '\'' +
                ", zone= Ouest"+
                ", densiteFourrure= Epaisse";
    }
}
