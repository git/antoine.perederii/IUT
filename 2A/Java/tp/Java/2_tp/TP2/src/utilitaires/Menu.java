package utilitaires;

import models.Animal;
import models.AnimalFourrure;
import models.AnimalTheque;
import models.Oiseau;

public class Menu {
    private AnimalTheque animalTheque;
    private Afficheur afficheur;
    private Suppresseur suppresseur;
    private Saisisseur saisisseur;

    public Menu(AnimalTheque animalTheque, Afficheur afficheur, Suppresseur suppresseur, Saisisseur saisisseur) {
        this.animalTheque = animalTheque;
        this.afficheur = afficheur;
        this.suppresseur = suppresseur;
        this.saisisseur = saisisseur;
    }

    public void afficherMenu() {
        int choix;
        do {
            System.out.println("Menu :");
            System.out.println("1. Afficher les animaux");
            System.out.println("2. Ajouter un animal");
            System.out.println("3. Supprimer un animal");
            System.out.println("4. Hiverner un animal");
            System.out.println("5. Quitter");
            System.out.print("Choisissez une option : ");
            choix = saisisseur.saisirEntier();

            switch (choix) {
                case 1:
                    afficheur.afficherAnimaux(animalTheque);
                    break;
                case 2:
                    ajouterAnimal();
                    break;
                case 3:
                    supprimerAnimal();
                    break;
                case 4:
                    hivernerAnimal();
                    break;
                case 5:
                    afficheur.afficherMessage("Au revoir !");
                    break;
                default:
                    afficheur.afficherMessage("Option invalide. Veuillez choisir une option valide.");
            }
        } while (choix != 5);
    }

    private void ajouterAnimal() {
        int choixAjout;
        do {
            System.out.println("Menu ajout animal :");
            System.out.println("1. Aimal à Fourrure");
            System.out.println("2. Oiseau");
            System.out.print("Choisissez une option : ");
            choixAjout = saisisseur.saisirEntier();

            switch (choixAjout) {
                case 1:
                    String nomF = saisisseur.saisirNom();
                    int zoneF = saisisseur.saisirZone();
                    while(zoneF != 0 && zoneF != 1 && zoneF != 3 && zoneF != 4) {
                        System.out.println("Densité de fourrure invalide. Veuillez ressaisir ! (0 Sud, 1 Nord, 2 Est, 4 Ouest)");
                        System.out.println(zoneF);
                        zoneF = saisisseur.saisirZone();
                    }

                    int densiteF = saisisseur.saisirDensiteFourrure();
                    while(densiteF != 0 && densiteF != 1) {
                        System.out.println("Densité de fourrure invalide. Veuillez ressaisir ! (0 Fine ou 1 Epaisse)");
                        System.out.println(densiteF);
                        densiteF = saisisseur.saisirZone();
                    }

                    Animal animalF = new AnimalFourrure(nomF, zoneF, densiteF);
                    animalTheque.lesAnimaux.add(animalF);

                    afficheur.afficherMessage("L'animal à fourrure a été ajouté avec succès !");
                    break;
                case 2:
                    String nomO = saisisseur.saisirNom();
                    int zoneO = saisisseur.saisirZone();
                    while(zoneO != 0 && zoneO != 1) {
                        System.out.println("Zone d'oiseau invalide. Veuillez ressaisir ! (0 Sud, 1 Nord)");
                        System.out.println(zoneO);
                        zoneO = saisisseur.saisirZone();
                    }

                    Animal animalO = new Oiseau(nomO, zoneO);
                    animalTheque.lesAnimaux.add(animalO);

                    afficheur.afficherMessage("L'Oiseau a été ajouté avec succès !");
                    break;
                default:
                    afficheur.afficherMessage("Option invalide. Veuillez choisir une option valide.");
            }
        } while (choixAjout != 2 && choixAjout != 1);
    }

    private void supprimerAnimal() {
        String nomF = saisisseur.saisirNom();
        if (suppresseur.supprimerAnimal(animalTheque, nomF)) {
            afficheur.afficherMessage("L'animal a été supprimé avec succès !");
        } else {
            afficheur.afficherMessage("Aucun animal trouvé avec ce nom.");
        }
    }

    private void hivernerAnimal() {
        for (Animal animal : animalTheque.lesAnimaux) {
            if (animal instanceof AnimalFourrure) {
                ((AnimalFourrure) animal).hiverner();
            } else if (animal instanceof Oiseau) {
                ((Oiseau) animal).hiverner();
            }
            else {
                afficheur.afficherMessage("Aucun animal trouvé avec ce nom ou cet animal ne peut pas hiverner.");
                return;
            }
        }
        afficheur.afficherMessage("Tous les animaux vont hiverner.");
    }
}
