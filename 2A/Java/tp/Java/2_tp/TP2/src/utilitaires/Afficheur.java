package utilitaires;

import models.Animal;
import models.AnimalTheque;

public class Afficheur {
    public void afficherAnimaux(AnimalTheque animalTheque) {
        for (Animal animal : animalTheque.lesAnimaux) {
            System.out.println(animal.toString());
        }
    }

    public void afficherMessage(String message) {
        System.out.println(message);
    }
}
