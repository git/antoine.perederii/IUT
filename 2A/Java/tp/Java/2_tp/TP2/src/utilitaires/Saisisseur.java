package utilitaires;

import java.util.Scanner;

public class Saisisseur {
    private Scanner scanner;

    public Saisisseur() {
        this.scanner = new Scanner(System.in);
    }

    public String saisirNom() {
        System.out.print("Nom de l'animal : ");
        return scanner.next();
    }

    public int saisirZone() {
        System.out.print("Zone de l'animal : ");
        return scanner.nextInt();
    }

    public int saisirDensiteFourrure() {
        System.out.print("Densité de fourrure : ");
        return scanner.nextInt();
    }

    public int saisirEntier() {
        return scanner.nextInt();
    }
}
