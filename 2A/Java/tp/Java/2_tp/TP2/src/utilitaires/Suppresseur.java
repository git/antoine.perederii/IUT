package utilitaires;

import models.Animal;
import models.AnimalTheque;

public class Suppresseur {
    public boolean supprimerAnimal(AnimalTheque animalTheque, String nom) {
        for (Animal animal : animalTheque.lesAnimaux) {
            if (animal.getNom().equalsIgnoreCase(nom)) {
                animalTheque.lesAnimaux.remove(animal);
                return true; // Animal supprimé avec succès
            }
        }
        return false; // Animal non trouvé
    }
}
