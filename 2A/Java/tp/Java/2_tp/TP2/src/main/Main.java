package main;

import models.Animal;
import models.AnimalFourrure;
import models.AnimalTheque;
import models.Oiseau;
import utilitaires.Afficheur;
import utilitaires.Menu;
import utilitaires.Saisisseur;
import utilitaires.Suppresseur;

public class Main {
    public static void testOiseau(){
        Animal oiseau = new Oiseau("sozio", 1);
        System.out.println(oiseau.getNom());
        System.out.println(oiseau.getZone());
        System.out.println(oiseau.getClass());
    }
    public static void testAnimalFourrure(){
        Animal af = new AnimalFourrure("ours", 1, 0);
        System.out.println(af.getNom());
        System.out.println(af.getZone());
        System.out.println(af.getClass());
    }

    public static void testMenu() {
        AnimalTheque animalTheque = new AnimalTheque();
        Afficheur afficheur = new Afficheur();
        Suppresseur suppresseur = new Suppresseur();
        Saisisseur saisisseur = new Saisisseur();

        Menu menu = new Menu(animalTheque, afficheur, suppresseur, saisisseur);

        // Crée quelques animaux pour les tests
        Animal animal1 = new AnimalFourrure("Lion", 1, 0);
        Animal animal2 = new AnimalFourrure("Ours", 2, 1);
        Animal animal3 = new Oiseau("pioupiou", 0);
        animalTheque.lesAnimaux.add(animal1);
        animalTheque.lesAnimaux.add(animal2);
        animalTheque.lesAnimaux.add(animal3);
        menu.afficherMenu();
    }

    public static void main(String[] args){
//        testAnimalFourrure();
        testMenu();
    }
}
