package main;

import models.Patient;
import models.Patientheque;
import utilitaires.Afficheur;

public class Main {

    public static void testAfficherPatient(){
        Patientheque lesPatients = new Patientheque();
        Patient A = new Patient("josette", "morice", 89);
        Patient B = new Patient("Gerard", "lanvin", 59);
        lesPatients.addPatient(A);
        lesPatients.addPatient(B);
        Afficheur.afficherLesPatients(lesPatients.toString());
    }
//    public static void testAnimalFourrure(){
//        Animal af = new AnimalFourrure("ours", 1, 0);
//        System.out.println(af.getNom());
//        System.out.println(af.getZone());
//        System.out.println(af.getClass());
//    }

    public static void main(String[] args){
        testAfficherPatient();
    }
}

