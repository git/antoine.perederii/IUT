package models;

import java.util.ArrayList;
import java.util.List;

public class Patientheque {
    public List<Patient> listDePatients = new ArrayList<>();
    public void addPatient(Patient p){
        listDePatients.add(p);
    }
    public StringBuilder toString(Patient patient) {
        StringBuilder total = new StringBuilder(listDePatients.get(0).toString());
        for (int i = 1; i < listDePatients.size(); ++i) {
            total.append(listDePatients.get(i).toString());
            total.append("\n");
            //= new StringBuilder(listDePatients.get(i).toString());
        }
        return total;
    }
}
