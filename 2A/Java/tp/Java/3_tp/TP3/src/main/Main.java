package main;

import data.ExportCSV;
import data.Serialiser;
import data.Stub;
import models.Vehicule;
import models.VehiculeTheque;
import models.Voiture;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void testVehiculeTheque(){
        VehiculeTheque vehiculeTheque = new VehiculeTheque();
        Vehicule v1 = new Voiture("v1", "111-AB-222");
        Vehicule v2 = new Voiture("v2", "333-GH-865");
        vehiculeTheque.getLesVehicules().add(v1);
        vehiculeTheque.getLesVehicules().add(v2);
        System.out.println(vehiculeTheque.getLesVehicules().toString());
    }
    public static void testStub() {
        Stub monStub = new Stub();
        List<Vehicule> maListe = monStub.getVehiculeList();
        System.out.println(maListe);
    }
    public static void testSerialiser() {
        Serialiser monSerialiser = new Serialiser();
        Vehicule monVehicule;
        monSerialiser.serialiserWriteBin();
        monVehicule = monSerialiser.serialiserReadBin();
        System.out.println(monVehicule);
    }

    public static void testCSV() {
        ExportCSV moncsv = new ExportCSV();
//        Voiture maVoiture = new Voiture("monNom", "maPlaque");
//        moncsv.csvWriter(maVoiture);
        System.out.println(moncsv.csvReader());
    }

    public static void main(String[] args){

//        testVehiculeTheque();
//        testStub();
//        testSerialiser();
        testCSV();
    }
}
