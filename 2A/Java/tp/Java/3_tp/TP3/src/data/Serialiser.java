package data;

import models.Vehicule;
import models.VehiculeTheque;
import models.Voiture;

import java.io.*;

public class Serialiser {
    public void serialiserWriteBin(){
        Voiture john = new Voiture("hello", "maPlaque");
        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("data.ser"))) {
            out.writeObject(john);
        } catch (IOException e) {
// Gestion erreur
        }
    }
    public Vehicule serialiserReadBin() {
        try (ObjectInputStream in = new ObjectInputStream(
                new FileInputStream("data.ser"))) {
            return (Vehicule) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
