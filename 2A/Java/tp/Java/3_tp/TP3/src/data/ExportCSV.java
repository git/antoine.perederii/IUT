package data;

import models.Vehicule;
import models.Voiture;

import java.io.*;

public class ExportCSV {
    public String csvReader() {
        try (BufferedReader in = new BufferedReader(
                        new FileReader("donnees.csv"))) {
            String s;
            StringBuilder sb = new StringBuilder();
            while ((s = in.readLine()) != null) {
                sb.append(s);
                sb.append("\n");
                // Découper la ligne en utilisant le point-virgule comme séparateur

                String[] elements = s.split(";");

                // Faire quelque chose avec les éléments découpés
                for (String element : elements) {
                    System.out.println("Élément : " + element);
                }
//                Lire la ligne puis la decouper au points virgules
            }
            return sb.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void csvWriter(Vehicule monVehicule) {
        try (BufferedWriter out = new BufferedWriter(
                new FileWriter("donnees.csv"))) {
            out.append(monVehicule.getNom());
            out.append("; ");
            out.append(monVehicule.getPlaque());
            out.append("\n");
        } catch (IOException e) {
// Gestion erreur
        }
    }
}
