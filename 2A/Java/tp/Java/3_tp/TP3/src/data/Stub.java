package data;

import models.Vehicule;
import models.VehiculeTheque;
import models.Voiture;

import java.util.ArrayList;
import java.util.List;

public class Stub {
    public List<Vehicule> vehiculeList = new ArrayList<>();
    public Stub(){
        vehiculeList.add(new Voiture("Merco", "ABC-123"));
        vehiculeList.add(new Voiture("Audi", "XYZ-987"));
        vehiculeList.add(new Voiture("Renault", "HIJ-456"));
    }

    public List<Vehicule> getVehiculeList() {
        return this.vehiculeList;
    }
}
