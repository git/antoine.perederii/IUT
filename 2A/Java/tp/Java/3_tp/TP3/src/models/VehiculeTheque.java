package models;

import java.util.ArrayList;
import java.util.List;

public class VehiculeTheque {

    private List<Vehicule> lesVehicules = new ArrayList<>();

    public List<Vehicule> getLesVehicules() {
        return this.lesVehicules;
    }

    @Override
    public String toString() {
        return "VehiculeTheque{" +
                "lesVehicules=" + lesVehicules.toString() +
                '}';
    }
}
