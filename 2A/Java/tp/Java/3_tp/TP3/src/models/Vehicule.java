package models;

import data.Serialiser;

import java.io.Serializable;

public abstract class Vehicule implements Serializable {
    protected String nom;
    protected String plaque;

    public abstract String getNom();

    public abstract String getPlaque();

    @Override
    public String toString() {
        return "Vehicule{" +
                "nom='" + nom + '\'' +
                ", plaque='" + plaque + '\'' +
                '}';
    }
}
