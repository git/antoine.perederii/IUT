package models;

public class Voiture extends Vehicule {

    public Voiture(String nomV, String plaqueV) {
        this.nom = nomV;
        this.plaque = plaqueV;
    }

    public String getNom() {
        return this.nom;
    }

    public String getPlaque() {
        return this.plaque;
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "nom='" + nom + '\'' +
                ", plaque='" + plaque + '\'' +
                "}\n";
    }
}
