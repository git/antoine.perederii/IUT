package models;

import java.util.Date;

public class FichierTexte extends Fichier {

    private StringBuilder monContenu;

    public FichierTexte(String nom, Date dateCreation, int droit, int taille) {
        super(nom, dateCreation, droit, taille);
    }

    private StringBuilder getContenu(){
        return this.monContenu;
    }

    @Override
    public String toString() {
        return "monContenu : ' " + monContenu + "'";
    }
}
