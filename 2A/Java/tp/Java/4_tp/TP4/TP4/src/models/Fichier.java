package models;

import java.util.Date;

public abstract class Fichier {
    private String nom;
    private final Date dateCreation;
    private int droit;
    private int taille;

    public Fichier(String nom, Date dateCreation, int droit, int taille){
        this.nom = nom;
        this.dateCreation = dateCreation;
        this.droit = droit;
        this.taille = taille;
    }
    private String getNom() {
        return this.nom;
    }

    public Date getDateCreation() {
        return this.dateCreation;
    }

    public int getDroit() {
        return this.droit;
    }

    public int getTaille() {
        return this.taille;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDroit(int droit) {
        this.droit = droit;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

//    @Override
//    protected String equals() {
//
//    }
}
