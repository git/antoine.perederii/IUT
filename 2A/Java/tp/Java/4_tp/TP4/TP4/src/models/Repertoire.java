package models;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Repertoire extends Fichier {
    
    List<Fichier> monRepertoire = new ArrayList<>();

    public Repertoire(String nom, Date dateCreation, int droit, int taille) {
        super(nom, dateCreation, droit, taille);
    }
    
    private void ajouterFichier(Fichier monFichier) {
        monRepertoire.add(monFichier);
    }

    private void supprimerFichier(Fichier monFichier) {
        monRepertoire.remove(monFichier);
    }

    private List<Fichier> getFichier(){
        return this.monRepertoire;
    }
}
