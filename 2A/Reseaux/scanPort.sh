#!/bin/bash

i=0
ADDR='iutclinfb1211l'
PORT=0

while [ $i -le 1024 ]; do
	if echo "" | socat STDIN TCP:$ADDR:${i} &> /dev/null  ; then
		PORT=$(cat /etc/services | grep "	$i/tcp" | cut -d "	" -f 1)
		echo "port $i ouvert $PORT"
	fi
	i=$((i+1))
done
