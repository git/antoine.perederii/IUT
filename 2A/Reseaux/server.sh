#!/bin/bash

# set -x

if [ "$1" != '-server' ]; then
	socat TCP-LISTEN:2000,fork,reuseaddr EXEC:"$0 -server"; exit $?
fi

case $@ in 
	'-server')
		read request
		method=$(echo $request | cut -d " " -f 1)
    		fich=$(echo $request | cut -d " " -f 2)
		if [ $fich = "/" ]; then
			fich="/index.html"
		fi

		fich="./www-test/${fich:1}"


		if [ $method != 'GET' ]; then 
			echo -e -n "HTTP/1.1 403 Forbiden\r\n"
			echo -e -n "Content-Length: 14 \r\n"
        		echo -e -n "Connection: close\r\n"
        		echo -e -n "Content-Type: text/plain\r\n"
			echo -e -n "\r\n"
			echo -e -n "403 : Forbiden 2\n";echo -n "Erreur 403" >&2;exit $?

		fi
		if [ ! -f $fich ]; then
			echo -e -n "HTTP/1.1 404 Not found\r\n"
			echo -e -n "Server: Apache/2.2.22 (Debian)\r\n"
			echo -e -n "Accept-Ranges: bytes\r\n"
			echo -e -n "Content-Length: 15\r\n"
			echo -e -n "Content-Type: $mine\r\n"
			echo -e -n "Connection: close\r\n"
			echo -e -n "\r\n"
			echo -n "404 : Not Found 2" #ne sert a rien car ne s'affiche pas !
			echo "Erreur 404" >&2
		fi
		
		mime=$(file -L --brief --mime-type $fich)

		echo -e -n "HTTP/1.1 200 OK\r\n"
		echo -e -n "Server: Apache/2.2.22 (Debian)\r\n"
		echo -e -n "Accept-Ranges:bytes\r\n"
		echo -e -n "Content-Length: $(cat $fich | wc -c)\r\n"
		echo -e -n "Content-Type: $mine\r\n"
		echo -e -n "Connection: close\r\n"
		echo -e -n "\r\n"

		cat $fich

		;;
esac
