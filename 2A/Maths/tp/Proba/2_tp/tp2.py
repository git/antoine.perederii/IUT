import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial,binom


rng = np.random.default_rng()


print(factorial(5))
print(binom(7,3))


# k = np.array(range(10))
# plt.figure("La fonction x^2 sur les entiers de 1 à10")
# plt.plot(k, k**2, 'r')
# plt.show()



# Exercice 1 (Simulation d’une planche).

#? 1. On considère une bille dans la planche, à un certain étage n donné. Notons k la po-
#? sition actuelle de la bille (k = 0 si elle est complètement à gauche, k = n si elle est
#? complètement à droite). Quelles positions la bille peut-elle atteindre à l’étage suivant ?

#* la position suivante de la bille sera k - 1 ou k + 1

#? 2. On définit la variable aléatoire Y telle que
#? position suivante = position actuelle + Y
#? Quelle loi de probabilité suit la variable Y ? Trouvez comment générer en Python des
#? échantillons de cette loi de probabilité.

#* loi de bernoulli, n fois

# Paramètre de la loi de Bernoulli (probabilité de succès)
p = 0.3 

# Générer un échantillon de la loi de Bernoulli
def bernoulli(p):
    if np.random.randint(0,2) <= p :
        y = 0
    else :
        y = 1
    return y



print(bernoulli(0.5))


#? 3. Déduisez-en un moyen simple de simuler la planche de Galton. Écrivez une fonction
#? qui simule le passage d’une bille à travers une planche de Galton, et renvoie la position
#? d’arrivée de la bille. Cette fonction prend en entrée deux paramètres définissant la nature
#? de la planche (sa hauteur N et son paramètre gauche/droite p), et doit renvoyer un
#? nombre entier aléatoire entre 0 et N (position d’arrivée de la bille).

def simul_planche(N, p):
    position = 0  # La position initiale est 0 (au sommet de la planche)
    
    for _ in range(N):
        position += bernoulli(p)
    
    # Assurez-vous que la position reste dans les limites de la planche
    position = max(0, min(N, position))
    
    return position

# Exemple d'utilisation de la fonction
N = 10  # Hauteur de la planche
p = 0.5  # Probabilité de rebondir à gauche ==> quand équilibré
position_finale = simul_planche(N, p)
print("Position d'arrivée de la bille :", position_finale)


#? 4. Simulez un grand nombre de billes (par exemple 10000) passant à travers une planche de
#? Galton de paramètres N = 12 et p = 0.5. Stockez les 10000 positions d’arrivée obtenues.

# Nombre de billes à simuler
nombre_de_billes = 10000

# Liste pour stocker les positions d'arrivée
positions_arrivee = []

# Simulation des billes
for _ in range(nombre_de_billes):
    positions_arrivee.append(simul_planche(N, p)) #.append, ajoute dans la liste


#? Maintenant, positions_arrivee contient les positions d'arrivée de 10 000 billes
#? Vous pouvez les utiliser pour effectuer des analyses ou des calculs statistiques

positions_arrivee = np.array(positions_arrivee)
moyenne = np.mean(positions_arrivee)
variance = np.var(positions_arrivee)

print("Moyenne :", moyenne)
print("Variance :", variance)


#? 5. Tracez l’histogramme des 10000 positions d’arrivée précédentes. Vous pouvez utiliser la
#? fonction hist de la librairie matplotlib, ou encore la fonction histplot de la
#? librairie seaborn.
#? Question en passant : avec une planche de Galton réelle, et de vraies billes, comment
#? pourrait-on obtenir concrètement cet histogramme ?

# Tracer l'histogramme
plt.hist(positions_arrivee, bins=range(N+2), align='left', alpha=0.75, rwidth=0.8)
plt.xticks(range(N+1))
plt.xlabel('Position d\'arrivée')
plt.ylabel('Fréquence')
plt.title(f'Histogramme des positions d\'arrivée pour {nombre_de_billes} billes')
plt.grid(axis='y', alpha=0.75)
plt.show()


#? 6. Reproduisez l’expérience avec différentes autres valeurs possibles pour le paramètre p.
#? Quelles sont les conséquences au niveau de l’histogramme ?

N = 10  # Hauteur de la planche
p = 0.75  # Probabilité de rebondir à gauche ==> quand équilibré
position_finale = simul_planche(N, p)
print("Position d'arrivée de la bille :", position_finale)

positions_arrivee = []

# Simulation des billes
for _ in range(nombre_de_billes):
    positions_arrivee.append(simul_planche(N, p)) #.append, ajoute dans la liste

positions_arrivee = np.array(positions_arrivee)
moyenne = np.mean(positions_arrivee)
variance = np.var(positions_arrivee)

print("Moyenne :", moyenne)
print("Variance :", variance)


# Tracer l'histogramme
plt.hist(positions_arrivee, bins=range(N+2), align='left', alpha=0.75, rwidth=0.8)
plt.xticks(range(N+1))
plt.xlabel('Position d\'arrivée')
plt.ylabel('Fréquence')
plt.title(f'Histogramme des positions d\'arrivée pour {nombre_de_billes} billes')
plt.grid(axis='y', alpha=0.75)
plt.show()

# Exercice 2
from numpy.ma.core import choose
# Exercice 2
# 1) étant doné que l'expérience est répété plusieur fois c'est une lois binomiale
# 2) P(X =k) = (n|k).p^k.(1-p)^n-k

galton3=nr.binomial(12, 0.3, 1000)
plt.hist(galton2,bins=12,range=(0,12),density=True)
plt.show()
def loi(N, p):
    galton3LOI = []
    for i in range(N+1):
        #galton3LOI.append(binom.pmf(i, N, p))

        galton3LOI.append((binom(N,i))*(p**i)*(1-p)**(N-i))

    plt.plot(range(N+1), galton3LOI, 'r')
    plt.show()

loi(12,0.5)


from scipy.stats import norm

# Paramètres
N = 1000
p = 0.5

positions = np.random.binomial(N, p, size=10000)

# Afficher l'histogramme
plt.hist(positions, bins=range(N+2), density=True, alpha=0.6, color='b', label='Histogramme de Galton')

# Superposer la distribution normale
mu = N * p
sigma = np.sqrt(N * p * (1 - p))
x = np.arange(0, N+1)
y = norm.pdf(x, mu, sigma)
plt.plot(x, y, 'r', label='Distribution normale')
plt.show()
