import numpy.random as npr
from collections import Counter

# Chiffre entre 0 et 1
print(npr.rand())

# reset l'instance random de base =1 ==> soit none
print(npr.seed(seed=1))

# random entre 3<= et >5
print(npr.randint(3,5))

# 4- énère les résultats des commandes précédentes.
print(npr.rand())
print(npr.seed(seed=1))
print(npr.randint(3,5))

# 5- Générer un tableau binaire de taille 10 (qui ne contient que 0 ou 1)
print(npr.randint(0,2,10))
tab1=npr.randint(0,2,10)

# 6 - A l’aide de la commande Counter de la classe collections, calculer les occurrences de 1 et de 0.
print(Counter(tab1))

# 7 - Écrire une fonction fct_occ qui prend un tableau d’entiers en entrée, calcule les
# occurrences de chacun de ses éléments et rend un tableau qui contient ces occurrences.
def fct_occ(tab):
    occ=Counter()
    for i in tab:
        occ[i]+=1
    return occ

print(fct_occ(tab1))

# 8 - Générer un tableau aléatoire tab de taille 5 qui ne contient que des entiers entre 1
# et 6.
tab2 = npr.randint(1,7,5)
print(tab2)

# 9 - À l’aide de la fonction fct_occ et de tab, comment détecteriez-vous
# • Un carré ? quattres valeurs identique dans tab1
# • Un full ? Trois valeurs identique et deux autres identiques
# • Une petite suite ? 5 valeurs différentes qui se suivent

# Exo 2

#? 1
# P(carre) = (6*5*5)/6⁵ ==> proba théorique

def fct_carre():
    carre = 0
    for i in range(10000):
        tab = npr.randint(1,7,5)
        res = fct_occ(tab)
        for k in res.values():
            if k == 4:
                carre += 1
    return carre

print("Estimation numérique : ",fct_carre()/10000)
print("Proba théorique : P(carre) = (6*5*5)/6⁵")

#? 2
# P(full) = (6*5*4)/6⁵ ==> proba théorique

def fct_full():
    full = 0
    for i in range(10000):
        tab = npr.randint(1,7,5)
        res = sorted(fct_occ(tab).values())
        if res == [2,3]:
            full += 1
    return full

print("Estimation numérique : ",fct_full()/10000)
print("Proba théorique : P(carre) = (6*5*4)/6⁵")

#? 3
# P(brelan) = (6*5*4)/6⁵ ==> proba théorique

def fct_brelan():
    brelan = 0
    for i in range(10000):
        tab = npr.randint(1,7,5)
        res = sorted(fct_occ(tab).values())
        if res == [1,1,3]:
            brelan += 1
    return brelan

print("Estimation numérique : ",fct_brelan()/10000)
print("Proba théorique : P(carre) = (6*5*4)/6⁵")

#? 4
# P(yam) = (6)/6⁵ ==> proba théorique

def fct_yam():
    yam = 0
    for i in range(10000):
        tab = npr.randint(1,7,5)
        res = fct_occ(tab)
        for k in res.values():
            if res == [2,3]:
                yam += 1
    return yam

print("Estimation numérique : ",fct_yam()/10000)
print("Proba théorique : P(carre) = (6)/6⁵")

#? 5
# P(ps) = (3)/6⁵ ==> proba théorique

def fct_ps():
    ps = 0
    for i in range(10000):
        tab = npr.randint(1,7,5)
        res = sorted(fct_occ(tab).values())
        if res == [2,3]:
            ps += 1
    return ps

print("Estimation numérique : ",fct_ps()/10000)
print("Proba théorique : P(carre) = (3)/6⁵")

# Exo 3

#1 
def jouer_6():
    tab=[0,0,0,0,0]
    tab6=[0,0,0,0,0]
    nombre=0
    n=Counter()
    for i in range(3):
        for i in range(len(tab)):
            tab[i]=npr.randint(1,7)
        for k in range(len(tab6)):
            if (tab6[k]==6):
                tab[k]=0
        for j in range(len(tab)):
            if (tab[j] == 6):
                tab6[j]=6
        for l in range(len(tab6)):
            if (tab6[l]==0):
                tab6[l]=tab[l]
    n = fct_occ(tab6)
    nombre = n[6]
    return nombre

print("")
occ=jouer_6()
print(occ)

#2
x=[]
for i in range(10000):
 npr.seed(seed=i)
 x.append(jouer_6())


#plt.hist(x,orientation='horizontal')
#plt.show()

#3
#a.
lancer1=0
lancer2 =0
lancer3 =0
lancer4= 0
lancer5=0
total=0
for i in x:
    if (i == 1):
        lancer1+=1
    if (i == 2):
        lancer2+=1
    if (i == 3):
        lancer3+=1
    if (i == 4):
        lancer4+=1
    if (i == 5):
        lancer5+=1
    total+=1

print("")
print("")
print("Proba d'avoir 1 le lance d'un 6: ", lancer1/total, "%")
print("Proba d'avoir 2 le lance d'un 6: ", lancer2/total, "%")
print("Proba d'avoir 3 le lance d'un 6: ", lancer3/total, "%")
print("Proba d'avoir 4 le lance d'un 6: ", lancer4/total, "%")
print("Proba d'avoir 5 le lance d'un 6: ", lancer5/total, "%")

#b. 
print("")
print("Pour obtenir un Yam de 6, on aura ", lancer5/total, "% \de chance d'en avoir un sur 10.000 lancers.")

# Ex 4

# P(3lancers) = 1/6 * 5/6 * 5/6
# P(5lancers) = 6/6 * 1/6 * 1/6 * 1/6 * 1/6