MEMBRE
matricule
nom
espece
metier #affectation #planeteNaiss

Planete
code 
denomination coordonnees allegeance population systeme

-- ? Q1). Lister par ordre alphabétique les différentes espèces composant les équipages de vaisseaux dont la base se situe dans le système Corellien.

SELECT espece FROM Mene

-- ? Q2). Trouver le numéro et le modèle du vaisseau transportant le plus de vaisseaux.



-- ? Q3). La requête ci-dessous n’est pas optimale. Trouver une autre requête équivalente qui soit plus
-- ? optimisée.
-- SELECT m.matricule , m.nom
-- FROM Membre m, Planete p1, Vaisseau v, Planete p2 WHERE m.planeteNaiss = p1.code AND v.base = p2.code
-- AND m.affectation = v.numero AND p1.code = p2.code AND m.metier = ’pilote’;



-- ? Q4). Quel index permettrait d’améliorer également les performances de la requête précédente? Donner la requête permettant de créer cet index.



-- ? Q5). Écrire une fonction PL/pgSQL capaciteEvacuation qui prend en paramètre le code d’une planète et retourne le nombre maximal de personnes pouvant être évacuées rapidement.
-- Cela correspond au nombre maximum de passagers pouvant être accueillis par des vaisseaux en état de fonctionnement et basés sur cette planète.
-- Il est possible de répondre à la question suivante même si vous n’avez pas réussi à répondre à la question 5.



-- ? Q6). Utiliser la fonction capaciteEvacuation pour calculer combien d’habitants ne pourront pas être évacués rapidement de la planète dénommée ’Coruscent’ en cas d’évacuation de sa population au complet.
-- ? Q7). Écrire une fonction PL/pgSQL maintenance qui prend en paramètre le matricule d’un membre d’équipage et le code d’une planète. Cette fonction répare si nécessaire (autrement dit passe l’état d’un vaisseau de ’R’ à ’F’) tous les vaisseaux basés sur cette la planète. Des exceptions doivent être levées dans plusieurs cas :
-- ? — Une exception no_data_found est levée si aucun membre d’équipage ne possède le matricule passé en paramètre.
-- ? — Une exception avec le message ’Réparation impossible’ est levée si le membre d’équipage associé au matricule n’est pas un mécanicien.
-- ? Cette fonction devra utiliser un curseur pour trouver les vaisseaux à réparer.
-- ? Q8). Mettre en place un trigger qui, lorsqu’une planète change d’allégeance, de l’empire vers la
-- ? rébellion, détruit tous les vaisseaux stationnés sur cette planète.
-- ? Q9). Supposons que deux sessions S1 et S2 accèdent à la base de données et exécutent la séquence d’instructions ci-dessous. Pour chaque étape, expliquer ce qu’il se passe en indiquant le résultat des requêtes et en détaillant les mécanismes de verrous mis en jeu.
-- ? Nous supposerons que l’autocommit est désactivé dans les deux sessions.