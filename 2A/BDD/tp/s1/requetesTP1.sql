\! clear

--? 2 - Dans quelle ville est située l’équipe surnommée les Grizzlies?
-- SELECT city
-- FROM TEAM 
-- WHERE nickname = 'Grizzlies';

--   city   
-- ---------
--  Memphis
-- (1 row)

--? 3 - Quel est le plus gros score marqué à domicile par une équipe?
-- SELECT max(ptsHome)
-- FROM Game;

--  max 
-- -----
--  168
-- (1 row)

--? 4 - Quel est le plus gros score marqué à l’extérieur par une équipe?
-- SELECT max(ptsAway)
-- FROM Game;

--  max 
-- -----
--  168
-- (1 row)

--? 5 - Le résultat obtenu devrait être surprenant. Justifier que vos réponses aux deux dernières
--? questions sont correctes.
-- Le resultat est le même entre à domicile et à l'exterieur. Les scores ont du être réalisé sur deux match différents mais ils ont le même max.
-- SELECT 'Points max à domicile : ' || max(ptsHome) || 'Avec pour id de game : ' || id
-- FROM Game
-- GROUP BY ptsHome, id;

-- ?column?                          
-- ------------------------------------------------------------
--  Points max à domicile : 93Avec pour id de game : 21200728
--  Points max à domicile : 106Avec pour id de game : 20501075
--  Points max à domicile : 94Avec pour id de game : 22000463
--  Points max à domicile : 94Avec pour id de game : 21600817
--  Points max à domicile : 103Avec pour id de game : 20700669
--  Points max à domicile : 129Avec pour id de game : 20800711
--  Points max à domicile : 106Avec pour id de game : 21000744
--  Points max à domicile : 106Avec pour id de game : 20901056
--  Points max à domicile : 101Avec pour id de game : 20600197

-- SELECT 'Points max à exterieur : ' || max(ptsAway) || 'Avec pour id de game : ' || id
-- FROM Game
-- GROUP BY ptsAway, id;

-- ?column?                           
-- -------------------------------------------------------------
--  Points max à exterieur : 84Avec pour id de game : 21200728
--  Points max à exterieur : 94Avec pour id de game : 20501075
--  Points max à exterieur : 96Avec pour id de game : 22000463
--  Points max à exterieur : 90Avec pour id de game : 21600817
--  Points max à exterieur : 116Avec pour id de game : 20700669

--? 6 - Quelle équipe a marqué le plus de points à domicile, indiquez l’abbréviation, le nickname, la ville, les points marqués et la date du match ?
-- SELECT abbreviation, nickname, city, ptsHome, dateGame
-- FROM Team t, Game g
-- WHERE t.id = g.idHomeTeam AND g.ptsHome = (SELECT max(ptsHome)
--                                            FROM Game);

--  abbreviation | nickname |  city  | ptshome |  dategame  
-- --------------+----------+--------+---------+------------
--  DEN          | Nuggets  | Denver |     168 | 2008-03-16
-- (1 row)

--? 7 - Même pour l'exterieur
-- SELECT abbreviation, nickname, city, ptsAway, dateGame
-- FROM Team t, Game g
-- WHERE t.id = g.idVisitorTeam AND g.ptsAway = (SELECT max(ptsAway)
--                                            FROM Game);

--  abbreviation | nickname |  city   | ptsaway |  dategame  
-- --------------+----------+---------+---------+------------
--  CHI          | Bulls    | Chicago |     168 | 2019-03-01
-- (1 row)

--? 8 - Quels sont les noms des joueurs qui ont le plus gros pourcentage de paniers à trois points dans un match ?
-- SELECT p.name, max(threePointsPrctage)
-- FROM Player p, GameDetail gd 
-- WHERE p.id = gd.idPlayer AND gd.threePointsPrctage = (SELECT max(threePointsPrctage)
--                                FROM GameDetail)
-- GROUP BY name, threePointsPrctage;

-- name           | max 
-- --------------------------+-----
--  Malcolm Hill             |   1
--  Rajon Rondo              |   1
--  Kobi Simmons             |   1
--  Derrick Jones Jr.        |   1
--  Scott Williams           |   1
--  Isaiah Joe               |   1
--  Jarrell Brantley         |   1
--  Tahjere McCall           |   1
--  Jeremy Senglin           |   1

--? 9 - Quels sont les joueurs qui ont la moyenne la plus élevée de pourcentage de paniers à trois points sur tous les matchs ?
-- SELECT p.id, avg(threePointsPrctage)
-- FROM Player p, GameDetail gd 
-- WHERE p.id = gd.idPlayer AND avg(threePointsPrctage) >= ALL(SELECT avg(threePointsPrctage)
--                                                             FROM GameDetail)
-- GROUP BY p.id, threePointsPrctage
-- ORDER BY threePointsPrctage ASC;

-- --? 10 - Pour la saison 2012, calculer quel est le plus grand nombre de paniers à 3 points marqués par un joueur.
-- SELECT max(threePointsMade), g.season, p.name
-- FROM GameDetail gd, Game g, Player p
-- WHERE gd.idGame = g.id AND gd.idPlayer = p.id AND g.season = '2012'
-- GROUP BY threePointsMade, season, p.name
-- ORDER BY threePointsMade DESC;

-- --? 11 - Pour chaque conférence, quelle équipe (nom et ville) est la dernière à avoir rejoint la NBA en indiquant l’année d’arrivée en NBA ?
-- --? (Faire une seule requête pour les deux conférences, et non pas une requête par conférence.)
-- SELECT nickname, city
-- FROM Team t, Game g, GameDetail gd
-- WHERE t.id = gd.idTeam AND gd.idGame = g.id
-- GROUP BY g.season
-- HAVING max(season) - min(season) IN(SELECT max(max(season) - min(season)) FROM Game);

-- --? 12 - Quel est le nom du joueur qui a fait le plus de passes décisives dans un match? Donner la date de son exploit.
-- SELECT p.name, gd.turnovers, g.dateGame
-- FROM Player p, GameDetail gd, Game g
-- WHERE p.id = gd.idPlayer AND gd.idGame = g.id AND gd.turnovers >= ALL(SELECT gd.turnovers
--                                                  FROM GameDetail);

--? 13 - Quel est le joueur qui a fait le plus rapidement 6 fautes personnelles ? Donner la date de cette performance et le nom du joueur.
-- SELECT p.name, gd.playingTime, gd.personnalFoul
-- FROM Player p, GameDetail gd
-- WHERE p.id = gd.idPlayer AND gd.playingTime <= ALL(SELECT playingTime FROM GameDetail WHERE personnalFoul = 6) AND gd.personnalFoul = 6;

-- name      | playingtime | personnalfoul 
-- ---------------+-------------+---------------
--  Sean Williams | 00:02:00    |             6
-- (1 row)


--? 14 - Même question, mais on veut désormais les deux équipes qui se sont affrontées lors de ce match.

--? 15 - Écrire la requête permettant d’afficher la liste des joueurs qui ont fait un triple double dans 
--? leur carrière en donnant leurs statistiques dans les 5 catégories. Un triple double c’est avoir au moins 
--? dans 3 catégories des statistiques à 2 chiffres (points, rebonds, passes, contres, interceptions) dans un même match.

--? 16 - Écrire la requête permettant d’afficher la liste des joueurs qui ont fait plus d’interception que de 
--? balles perdues et qui ont intercepté plus de 1000 balles dans leur carrière.


