
-- Q1). Lister les noms, villes et nombres d’étoiles des adresses répertoriées en Californie (CA) ayant
-- obtenu 5 étoiles, triées par nom par ordre alphabétique.

db.yelp.find({state: 'CA', stars: 5}, ["name", "city", "stars"]).sort({"name": 1})

-- Q2). Faire la même chose sous forme de pipeline d’agrégation.

db.yelp.aggregate({ $match : {"state": 'CA','stars': 5}}, {$project : {"name": 1,"city": 1,"stars": 1}},{ $sort : {"name": 1}})

-- Q3). Faire la même chose via l’interface graphique de Compass en utilisant l’onglet .
-- En rajoutant l’option .explain("executionStats") entre le nom de la collection et le .aggregate,
-- il est possible d’obtenir des informations sur l’exécution de votre pipeline d’agrégation comme par
-- exemple :
-- ...
-- executionStats: {
-- executionSuccess: true, /*Est-ce que l’exécution s’est bien passée ?*/
-- nReturned: 2416, /*Le nombre de documents retournés*/
-- executionTimeMillis: 250, /*Le temps d’exécution en ms*/
-- ...



-- Q4). Utiliser cette option pour obtenir le temps d’exécution du pipeline d’agrégation réalisé pour
-- la question 2.

db.yelp.explain("executionStats").aggregate({ $match : {"state": 'CA','stars': 5}}, {$project : {"name": 1,"city": 1,"stars": 1}},{ $sort : {"name": 1}})


-- Q5). Inverser l’ordre de vos opérateurs dans le pipeline et comparer le temps d’exécution de cette
-- nouvelle version avec l’ancienne. Pourquoi ce résultat ?

db.yelp.explain("executionStats").aggregate({$project : {"name": 1,"city": 1,"stars": 1}}, { $match : {"state": 'CA','stars': 5}},{ $sort : {"name": 1}})
db.yelp.explain("executionStats").aggregate({$project : {"name": 1,"city": 1,"stars": 1}}, { $match : {"state": 'CA','stars': 5}},{ $sort : {"name": 1}})
db.yelp.explain("executionStats").aggregate({$project : {"name": 1,"city": 1,"stars": 1}}, { $sort : {"name": 1}}, { $match : {"state": 'CA','stars': 5}})
db.yelp.explain("executionStats").aggregate({ $sort : {"name": 1}}, {$project : {"name": 1,"city": 1,"stars": 1}}, { $match : {"state": 'CA','stars': 5}})
db.yelp.explain("executionStats").aggregate({ $sort : {"name": 1}}, { $match : {"state": 'CA','stars': 5}}, {$project : {"name": 1,"city": 1,"stars": 1}})



-- Q6). Utiliser un pipeline pour lister les états représentés dans cette base de données.

db.yelp.aggregate({$project : {state : 1}})

-- Q7). Utiliser un pipeline pour lister les villes du Texas (TX) représentées dans cette base de données.

db.yelp.aggregate({$match : {"state" : "TX"}},{$project : {city: 1, _id : 0}})

-- Q8). Compter le nombre d’adresses connues à Santa Barbara.

db.yelp.aggregate({$match : {"city" : "Santa Barbara"}},{$count : "name"})
db.yelp.aggregate({$match : {"city" : "Santa Barbara"}},{$count : "*"})

-- Q9). Combien y a t’il d’adresses référencées qui sont définitivement fermées ?

db.yelp.aggregate({$match : {"is_open" : 0}},{$count : "*"})

-- Q10). Compter le nombre d’adresses référencées pour chaque état, les états en ayant le plus en
-- premier.

db.yelp.aggregate({$group : {_id : "$state", "nombre" : {$count : {}}}}, {$sort : {"nombre" : -1}})

-- Q11). Compter le nombre total de commentaires laissés pour des restaurants dans chaque ville de
-- Californie (CA).

db.yelp.aggregate({$match : {state : "CA", "categories": ["Restaurants"]}}, {$group : {_id : "$city", "nombre" : { $count : {} } } })


-- Q12). Quel est le nombre maximum de commentaires laissés pour des chefs à domicile (Personal
-- Chefs) pour chaque état ?



-- Q13). Quel est le nombre moyen d’étoiles reçues par les bars de chaque ville du Missouri (MO) ?



-- Q14). Récupérer le fichier cities.json sur Moodle. Ce fichier contient des informations sur les
-- 1000 plus grandes villes américaines.



-- Q15). Importer les documents du fichier cities.json dans une nouvelle collection nommée cities.
-- Vous pouvez utiliser la commande mongoimport ou importer via Compass.



-- Q16). Les champs "rank" et "population" sont sous forme de chaîne de caractères. Pour faciliter
-- l’analyse ultérieure de ses données nous allons les transformer en entier. Faire un pipeline d’agrégation
-- faisant la conversion en entier pour ces deux champs et enregistrant le résultat dans une nouvelle
-- collection cities2.



-- Q17). Vérifier que la collection cities2 a bien été créée et afficher quelques documents pour vérifier
-- que la conversion en entier a bien été réalisée.



-- Q18). Quelle est la population moyenne de ces villes ?



-- Q19). Faire une jointure entre la collection yelp et la collection cities2. Le champ servant à stocker
-- le résultat devra s’appeler "city_details".



-- Q20). Plusieurs villes américaines situées dans des états différents portent le même nom. Par
-- exemple, il existe une ville se nommant Madison dans l’Alabama, dans le Tennessee et dans le Wis-
-- consin.
-- Modifier votre pipeline d’agrégation pour que le champ "city_details" ne soit plus un tableau mais
-- un objet unique, et que les informations contenues dans "city_details" correspondent bien à la ville
-- en question. Vous en profiterez pour éliminer les adresses pour lesquelles nous n’avons pas d’information
-- sur la ville.



-- Q21). Lister les glaciers (Gelato) pour lesquels au moins un habitant sur 1000 de sa ville a laissé un
-- commentai