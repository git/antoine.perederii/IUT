-- ? Q4). Combien y a t’il de documents ?

--* 150346

-- ? Q5). Quelle est la taille de la base de données ?

--* DOCUMENTS   150.3k

-- ? Q6). Afficher un unique document contenu dans cette collection. Que représente ce document ? De
-- ? Quelles données dispose-t’on ?

--* un lieu , magasins
--* l'addresse la localisation, ...

-- ? Q7). Utiliser l’interface de Compass pour parcourir quelques documents. Que remarquez-vous ?
-- Ont-ils tous la même structure ?

--* oui à peu près

-- ? Q8). Analyser la collection grâce à Compass. Cliquer plusieurs fois sur le bouton (en
-- n’ayant rien renseigné dans la barre Filter). Que remarquez-vous ? Pourquoi ?

--* il y a des champs avec un seul document parfois

-- ? Q9) Lister les noms des commerces répertoriés dans la ville de Tampa.

-- db.yelp.find({"city": "Tampa"},["name"])

-- ? Q10). Lister les 10 premières adresses qui sont soit en Floride (FL) ou soit dans le Nevada (NV).
-- Proposez deux façons d’écrire cette requête (en utilisant deux opérateurs différents).

--! db.yelp.find({"state": {"FA", "NV"}}},["name"]).limit(10)
db.yelp.find({$or: [{"state": "FL"}, {state: "NV"}]},["name"]).limit(10)

-- ? Q11). Lister les adresses de la ville d’Edmonton ayant une note d’au moins 4 étoiles. Proposez deux
-- façons d’écrire cette requête (l’une avec un opérateur explicite, l’autre avec la version implicite).

db.yelp.find({$and:[{"city": "Edmonton"},{"stars": {$gte:4}}]},["address"])
db.yelp.find({"city": "Edmonton", "stars": {$gte:4}},["address"])

-- ? Q12). Lister les adresses ayant une note d’au plus une étoile avec plus de 400 commentaires
-- (review_count).

db.yelp.find({"stars": {$lte: 1}, review_count: {$gte: 400}},["address"])

[
  {
    _id: ObjectId('63972dd1158ef602a68b7c9b'),
    address: '639 B Gravios Bluffs Blvd'
  },
  {
    _id: ObjectId('63972dd2158ef602a68bdb68'),
    address: '3750 Priority Way South Dr, Ste 200'
  }
]


-- ? Q13). Lister les noms des entreprises et villes des plombiers (Plumbing) d’Arizona (AZ).

db.yelp.find({"state": "AZ", categories: "Plumbing"},["name", "city"])


-- ? Q14). Lister les noms et adresses des restaurants de Philadelphie (Philadelphia) qui possèdent une
-- terrasse (OutdoorSeating).

db.yelp.find({"categories": "Restaurants", "city" : "Philadelphia", "attributes.OutdoorSeating": "true"},["name", "address"])

-- ? Q15). Lister tous les restaurants japonais ayant obtenu une note supérieure à quatre étoiles.

db.yelp.find({"categories": "Restaurants", "categories" : "Japanese", "stars": {$gte: 4}})

-- ? Q16). Lister tous les restaurants japonais ou mexicains du Missouri (MO).

db.yelp.find({$or: [{"categories": ["Restaurants", "Japanese"], state: "MO"}, {"categories": ["Restaurants", "Mexican"], state: "MO"}]})

-- ? Q17). Lister toutes les adresses ayant un nombre d’avis inférieur ou égal à leur nombre d’étoiles.

db.yelp.find({$expr: {$lte: ["$review_count", "$stars"]}}, ["address"])

-- ? Q18). Lister tous les restaurants qui proposent à la fois la livraison et à emporter ou ni l’un ni
-- l’autre.
-- Proposer une requête qui compare les valeurs de deux champs afin de répondre à cette question.

db.yelp.find({$or: [{"attributes.RestaurantsTakeOut": true, "attributes.RestaurantsDelivery": true}, {"attributes.RestaurantsTakeOut": false, "attributes.RestaurantsDelivery": false}]})

-- ? Q19). Lister les pharmacies (drugstores) de la Nouvelle-Orléans dont on connaît les horaires d’ou-
-- verture.

--! db.yelp.find({"categories": "drugstores", state: "NO", hours: {$ne: null}})

-- ? Q20). Lister les parcs des villes de Bristol, Hulmeville, Langhorne, Newtown et Pendell en Pensyl-
-- vannie (PA).



-- ? Q21). Lister les églises de Floride qui ne sont pas dans les plus grandes villes (Miami, Orlando,
-- Tampa et Jacksonville) par ordre alphabétique.



-- ? Q22). Lister les noms des adresses référencées sur Virginia Street ("Virginia St") à Reno.



-- ? Q23). Lister les noms et adresses magasins du Tennessee (TN) dont le nom contient le mot "Tree"
-- ou le mot "Earth", les adresses ayant eu le plus de commentaires en premier.



-- ? Q24). Le restaurant Twin Peaks d’Indianapolis change de propriétaire. Il faut donc mettre à jour
-- certaines informations. Enlevez la catégorie "Bars".



-- ? Q25). Supprimez également les catégories "Sports Bars", "American (New)" et "American (
-- Traditionnal)".



-- ? Q26). Le restaurant propose désormais de la cuisine française. Ajoutez la catégorie "French".



-- ? Q27). Ajoutez également les catégories "Creperies" et "Seafood". Vous devrez n’utiliser qu’une
-- seule instruction.



-- ? Q28). Vérifiez que vos modifications ont bien été prises en compte


