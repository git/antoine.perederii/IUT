package test;
import org.junit.Test;

import model.Pythagore;
import static org.junit.jupiter.api.Assertions.*;


class PythagoreTest {
    @Test
    public void testInstanciation() {
        Pythagore pythagore = new Pythagore();
        assertNotNull(pythagore);
    }

    @Test
    public void testTriangleRectangle() {
        assertTrue(Pythagore.estTriangleRectangle(3, 4, 5));
    }

    @Test
    public void testNonTriangleRectangle() {
        assertFalse(Pythagore.estTriangleRectangle(3, 4, 6));
    }

    @Test
    public void testCotesNegatifs() {
        assertFalse(Pythagore.estTriangleRectangle(-3, 4, 5));
    }

    @Test
    public void testCotesNuls() {
        assertFalse(Pythagore.estTriangleRectangle(0, 4, 5));
    }
}
