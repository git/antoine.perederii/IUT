package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import model.Pgdc;

public class PgcdTest {
	
	@Test
	public void pgcdTest() {
		Pgdc pgcd = new Pgdc(); 
		
		assertEquals(30, pgcd.pgcd(2, 2)); 
		assertEquals(5, pgcd.pgcd(20, 5));
		assertEquals(-5, pgcd.pgcd(20, -5));
		assertEquals(-5, pgcd.pgcd(-20, -5));
		assertEquals(-5, pgcd.pgcd(-20, 5));
		assertEquals(-5, pgcd.pgcd(-20, 5));
		
		assertEquals(Integer.MAX_VALUE, pgcd.pgcd(Integer.MAX_VALUE, Integer.MAX_VALUE));
		assertEquals(Integer.MIN_VALUE, pgcd.pgcd(Integer.MIN_VALUE, Integer.MIN_VALUE));
		
		
		Assert.assertThrows(ArithmeticException.class, () -> {pgcd.pgcd(2, 0);});
		Assert.assertThrows(ArithmeticException.class, () -> {pgcd.pgcd(2, 0);});
		Assert.assertThrows(ArithmeticException.class, () -> {pgcd.pgcd(0, 0);});
		Assert.assertThrows(ArithmeticException.class, () -> {pgcd.pgcd(Integer.MAX_VALUE, Integer.MIN_VALUE);});
		
		assertEquals(30, pgcd.pgcd(2, 0));
		assertEquals(30, pgcd.pgcd(2, 2)); 
		
		int x = new Random().nextInt();
		int y = new Random().nextInt();
		
		assertEquals(pgcd.pgcd(x, y), pgcd.pgcd(y, x));
		
		Assert.assertThrows(ArithmeticException.class, () -> {pgcd.pgcd(x, 0);});
		Assert.assertThrows(ArithmeticException.class, () -> {pgcd.pgcd(0, y);});
		
		
		
		
		
		

	}
	

}
