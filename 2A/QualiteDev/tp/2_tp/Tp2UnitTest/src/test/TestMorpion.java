package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import model.Morpion;
import model.Pgdc;

class TestMorpion {
	private int[][] morpionMatrice = new int[3][3];

	@Test

	void MorpionTestsNominauxPoint() {
		Morpion morpion = new Morpion(); 
		
		assertEquals(true, morpion.AddPoint(0, 0));
		assertEquals(true, morpion.AddPoint(1, 2));
		assertEquals(true, morpion.AddPoint(2, 2));
		assertEquals(false, morpion.AddPoint(3, 2));
		assertEquals(false, morpion.AddPoint(-1, 1));
		assertEquals(false, morpion.AddPoint(1, -1));
		assertEquals(false, morpion.AddPoint(-1, -1));
		assertEquals(false, morpion.AddPoint(3, 3));
		assertEquals(false, morpion.AddPoint(3, 0)); 
		assertEquals(false, morpion.AddPoint(0, 4));
	} 
	
	@Test
	void MorpionTestsValeursLimitesPoint() {
		Morpion morpion = new Morpion(); 
		
		assertEquals(false, morpion.AddPoint(Integer.MAX_VALUE, Integer.MAX_VALUE));
		assertEquals(false, morpion.AddPoint(Integer.MIN_VALUE, Integer.MIN_VALUE));
		assertEquals(false, morpion.AddPoint(Integer.MIN_VALUE, Integer.MAX_VALUE)); 
		assertEquals(false, morpion.AddPoint(Integer.MAX_VALUE, Integer.MIN_VALUE));
		
	} 
	@Test
	@RepeatedTest(value = 100)
	void MorpionTestsAleatoiresPoint() {
		
		Morpion morpion = new Morpion();
		
		int x = new Random().nextInt(-5, 5);
		int y = new Random().nextInt(-5, 5);
		
		
		if( x < 3 && y < 3 && x >= 0 && y >= 0) {
			assertEquals(true, morpion.AddPoint(x, y)); 
		}
		else {
			assertEquals(false, morpion.AddPoint(x, y)); 
		}
	}
//	void MorpionTestsNominauxValidation() {
//		
//	}
//	void MorpionTestsValeursLimitesValidation() {
//		
//	}
//	void MorpionTestsAleatoiresValidation(){
//		
//	}

}
