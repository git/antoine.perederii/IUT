package test;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import model.Operations;

public class OperationsTests {
	
	@Test
	public void AdditionTest() {
		
		Operations operation = new Operations();
		assertEquals(30, operation.additionner(5, 5, 5,5));
		assertEquals(1, operation.additionner(-5, 2, 4));
		assertEquals(-4, operation.additionner(-1, -3, null));
		assertEquals(2, operation.additionner(-1, 3, null));
		assertEquals(3, operation.additionner(0, 3, null));
		assertEquals(3, operation.additionner(3, 0, null));
		assertEquals(3, operation.additionner(0, 0, null));
		assertEquals(-1, operation.additionner(Long.MAX_VALUE, Long.MIN_VALUE, null));
		assertEquals(-2, operation.additionner(Long.MAX_VALUE, Long.MAX_VALUE , null));
		assertEquals(Long.MIN_VALUE, operation.additionner(Long.MAX_VALUE, 1 , null));
		
		long x = new Random().nextLong();
		long y = new Random().nextLong();
		long z = new Random().nextLong();
		
		assertEquals(x+y, operation.additionner(x, y, null));
		assertEquals(y+x, operation.additionner(y, x, null));
		assertEquals(z+x+y, operation.additionner(z, x, y));
		assertEquals(y+z+x, operation.additionner(y, z, x));
		assertEquals(x+y+z, operation.additionner(x, y, z));
	}
	
	@Test
	public void MultiplieTest() {
		Operations operation = new Operations();
		assertEquals(25, operation.multiplier(5, 5, null));
		assertEquals(1, operation.multiplier(Long.MAX_VALUE, Long.MAX_VALUE, null));
		assertEquals(0, operation.multiplier(0, 0, null));
		assertEquals(0, operation.multiplier(0, 5, null));
		assertEquals(0, operation.multiplier(5, 0, null));
		assertEquals(0, operation.multiplier(Long.MAX_VALUE, 0, null));
		assertEquals(0, operation.multiplier(Long.MIN_VALUE, 0, null));
		assertEquals(0, operation.multiplier(Long.MIN_VALUE, Long.MIN_VALUE, null));
		assertEquals(0, operation.multiplier(5, 0, null));
		
		long x = new Random().nextLong();
		long y = new Random().nextLong();
		long z = new Random().nextLong();
		
		assertEquals(x*y, operation.additionner(x, y, null));
		assertEquals(y*x, operation.additionner(y, x, null));
		assertEquals(z*x*y, operation.additionner(z, x, y));
		assertEquals(y*z*x, operation.additionner(y, z, x));
		assertEquals(x*y*z, operation.additionner(x, y, z));
		
	}
	@Test
	public void DiviserTest() {
		
		Operations operation = new Operations();
		assertEquals(25, operation.multiplier(5, 5, null));
		assertEquals(1, operation.multiplier(Long.MAX_VALUE, Long.MAX_VALUE, null));
		assertEquals(0, operation.multiplier(0, 0, null));
		assertEquals(0, operation.multiplier(0, 5, null));
		assertEquals(0, operation.multiplier(5, 0, null));
		assertEquals(0, operation.multiplier(Long.MAX_VALUE, 0, null));
		assertEquals(0, operation.multiplier(Long.MIN_VALUE, 0, null));
		assertEquals(0, operation.multiplier(Long.MIN_VALUE, Long.MIN_VALUE, null));
		assertEquals(0, operation.multiplier(5, 0, null));
		
		long x = new Random().nextLong();
		long y = new Random().nextLong();
		long z = new Random().nextLong();
		
		assertEquals(x/y, operation.additionner(x, y, null));
		assertEquals(y/x, operation.additionner(y, x, null));
		assertEquals((z/x)/y, operation.additionner(z, x, y));
		assertEquals((y/z)/x, operation.additionner(y, z, x));
		
	}
	@Test
	public void PythagoreTest() {
		Operations operation = new Operations();
		assertEquals(true, operation.pythagore(3, 4, 5));
		assertEquals(true, operation.pythagore(4, 5, 3));
		assertEquals(true, operation.pythagore(5, 3, 4));
		
		Assert.assertThrows(ArithmeticException.class, () -> {operation.pythagore(-1, 4, 5);});
		Assert.assertThrows(ArithmeticException.class, () -> {operation.pythagore(3, -1, 5);});
		Assert.assertThrows(ArithmeticException.class, () -> {operation.pythagore(3, 4, -1);});
		
		Assert.assertThrows(ArithmeticException.class, () -> {operation.pythagore(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);});
		Assert.assertThrows(ArithmeticException.class, () -> {operation.pythagore(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);});
		
		Assert.assertThrows(ArithmeticException.class, () -> {operation.pythagore(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);});
	}
	
	
	

}
