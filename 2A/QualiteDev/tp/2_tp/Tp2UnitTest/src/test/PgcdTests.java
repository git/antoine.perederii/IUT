package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.TddPgcd;

class PgcdTests {

	@Test
	void testPgcdNominaux() {
		
		TddPgcd pgcdTdd = new TddPgcd();
				
		
		assertEquals(5, pgcdTdd.pgcdTdd(20, 5));
		assertEquals(-5, pgcdTdd.pgcdTdd(20, -5));
		assertEquals(-5, pgcdTdd.pgcdTdd(-20, -5));
		assertEquals(-5, pgcdTdd.pgcdTdd(-20, 5));
	} 
	@Test
	void testPgcdtestPgcdValeursLimites() {
		TddPgcd pgcdTdd = new TddPgcd();

		assertEquals(Integer.MAX_VALUE, pgcdTdd.pgcdTdd(Integer.MAX_VALUE, Integer.MAX_VALUE));
		assertEquals(Integer.MIN_VALUE, pgcdTdd.pgcdTdd(Integer.MIN_VALUE, Integer.MIN_VALUE));
	}
	@Test
	void testPgcdtestPgcdFalseValue() {
		TddPgcd pgcdTdd = new TddPgcd();
		
		assertThrows(ArithmeticException.class, () -> {pgcdTdd.pgcdTdd(2, 0);});
		assertThrows(ArithmeticException.class, () -> {pgcdTdd.pgcdTdd(0, 2);});
		assertThrows(ArithmeticException.class, () -> {pgcdTdd.pgcdTdd(0, 0);});
		assertThrows(ArithmeticException.class, () -> {pgcdTdd.pgcdTdd(Integer.MAX_VALUE, Integer.MIN_VALUE);});
		
	}
	@Test
	void testPgcdtestPgcdValeurAleatoire() {
		TddPgcd pgcdTdd = new TddPgcd();
		
		int x = new Random().nextInt(); 
		int y = new Random().nextInt();
		
		assertEquals(pgcdTdd.pgcdTdd(x, y), pgcdTdd.pgcdTdd(x, y));
		
		Assert.assertThrows(ArithmeticException.class, () -> {pgcdTdd.pgcdTdd(x, 0);});
		Assert.assertThrows(ArithmeticException.class, () -> {pgcdTdd.pgcdTdd(0, y);});
		
	}
	
	
}
