package test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import model.Loup;
import model.Orientation;

public class TestLoup {

    @Test
    void testConstructeur() {
        Loup leLoup = new Loup();
        Assertions.assertEquals(Orientation.NORD,leLoup.getOrientation());
    }

    @Test
    void testTourner() {
        Loup leLoup = new Loup();
        leLoup.tourner();
        Assertions.assertEquals(Orientation.EST,leLoup.getOrientation());
        leLoup.tourner();
        Assertions.assertEquals(Orientation.SUD,leLoup.getOrientation());
        leLoup.tourner();
        Assertions.assertEquals(Orientation.OUEST,leLoup.getOrientation());
        leLoup.tourner();
        Assertions.assertEquals(Orientation.NORD,leLoup.getOrientation());
    }

}