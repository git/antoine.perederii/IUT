package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import model.Fibonacci;

class FibonacciTest {
	
	Fibonacci fibo = new Fibonacci();
	
	@Test
    public void testInstanciation() {
        Fibonacci fib = new Fibonacci();
        assertNotNull(fib);
    }

	@Test
	public void testFibonacciZero() {
		Assert.assertEquals(0, Fibonacci.fibo(0));
		
	}
	@Test
	public void testFibonacciUn() {
		Assert.assertEquals(1, Fibonacci.fibo(1));  
	}
	@Test
	public void testFibonacci2() {
	    int result = Fibonacci.fibo(2);
	    assertEquals(1, result);
	}

	@Test
	public void testFibonacci3() {
	    int result = Fibonacci.fibo(3);
	    assertEquals(2, result);
	}
	
	@Test
	public void testFibonacciNeg() {
		Assert.assertThrows(ArithmeticException.class, () -> {Fibonacci.fibo(-1);});   
	}
	
	@Test
	public void testFibonacciMIN() {
		Assert.assertThrows(IllegalArgumentException.class, () -> {Fibonacci.fibo(Integer.MIN_VALUE);});  
	}
	
	@Test
	public void testFibonacciMax() {
		Assert.assertThrows(IllegalArgumentException.class, () -> {Fibonacci.fibo(Integer.MAX_VALUE);});  
	}
}
