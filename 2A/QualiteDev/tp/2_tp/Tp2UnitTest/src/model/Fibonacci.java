package model;

public class Fibonacci {
    public static int fibo(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else if (n == Integer.MAX_VALUE || n == Integer.MIN_VALUE){
            throw new IllegalArgumentException("Integer.MIN_VALUE and Integer.MAX_VALUE is not supported");
        } else if (n < 0) {
        	throw new ArithmeticException("Negative input not allowed");
        }
        return fibo(n - 1) + fibo(n - 2);
    }
}