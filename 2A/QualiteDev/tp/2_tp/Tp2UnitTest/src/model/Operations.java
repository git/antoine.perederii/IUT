package model;

public class Operations extends RuntimeException{
	
	 public long additionner(long a, long b, long... param) {
		long resultat = a+b;
		if(param.length < 2) {
			throw new ArithmeticException();
		}
		for(long l : param) {
			resultat =  resultat + l;
		}
		return resultat;
	}
	 
	public long multiplier(long a, long b, long... param) {
		long resultat = a*b;
		if(param.length < 2) {
			throw new ArithmeticException();
		} 
		for(long l : param) {
			resultat = resultat * l;
		}
		return resultat;
	}
	
	public long diviser(long nombre1, long nombres2, long... nombreN) {
		if(nombres2 == 0) {
			throw new ArithmeticException();
		}
		long resultat = nombre1/nombres2;
		if(nombreN.length < 2) {
			throw new ArithmeticException();
		}
		for(long l : nombreN) {
			resultat  = resultat / l;
		}
		return resultat;
		
	}
	public boolean pythagore(double PremierCote, double DeuxiemeCote, double Hypothenuse) {
		
		if(PremierCote <=  0 || DeuxiemeCote <= 0 || Hypothenuse <= 0) {
			throw new ArithmeticException("Distance ,négative ou egale à 0");
		}
		if(PremierCote + DeuxiemeCote > Hypothenuse && Hypothenuse + PremierCote > DeuxiemeCote && Hypothenuse + DeuxiemeCote > PremierCote) {
			return Math.pow(Hypothenuse, 2) == Math.pow(PremierCote, 2) + Math.pow(DeuxiemeCote, 2);
		}
		else if(Math.pow(Hypothenuse, 2) == Math.pow(PremierCote, 2) + Math.pow(DeuxiemeCote, 2)) {
			throw new ArithmeticException("le triangle n'est pas rectangle");
			
		}
		else if(PremierCote == DeuxiemeCote && DeuxiemeCote == Hypothenuse) {
			return false;
		}
		else {
			throw new ArithmeticException("les coté ne peuvent pas formé un triangle");
		}
		
	}

}
