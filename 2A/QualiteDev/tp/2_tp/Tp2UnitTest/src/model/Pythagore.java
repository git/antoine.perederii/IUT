/**
 * 
 */
package model;

/**
 * @author anperederi
 *
 */
public class Pythagore {
	public static boolean estTriangleRectangle(int a, int b, int c) {
        return (a > 0 && b > 0 && c > 0) && (c * c == a * a + b * b || a * a == b * b + c * c || b * b == a * a + c * c);
    }
}