package model;

public class Pgdc {
	
	public int pgcd(int x, int y) {
		 int n = 0;
		 if(x<=0 || y <= 0) {
			 throw new ArithmeticException("x or y = 0"); 
		 }
		 for(int i=1; i <= x && i <= y; i++ ) {
			 
			 if(x%i==0 && y%i==0) {
				 n = i; 
			 }
		 }
		 return n;
	}

}
