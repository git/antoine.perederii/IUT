package test;

import org.junit.Test;
import static org.junit.Assert.*;
import model.Lampe;

public class LampeTest {

    @Test
    public void testMarche() {
        Lampe lampe = new Lampe("Salon");
        lampe.marche();
        assertEquals(100, lampe.getNiveau());
    }

    @Test
    public void testArret() {
        Lampe lampe = new Lampe("Chambre");
        lampe.arret();
        assertEquals(0, lampe.getNiveau());
    }

    @Test
    public void testAttenuer() {
        Lampe lampe = new Lampe("Cuisine");
        
        lampe.attenuer(50);
        assertEquals(50, lampe.getNiveau());

        lampe.attenuer(0);
        assertEquals(0, lampe.getNiveau());
    }
    
    @Test
    public void testNiveauNegatif() {
    	Lampe lampe = new Lampe("Lit");
    	
    	lampe.attenuer(-10);
    	assertEquals(0, lampe.getNiveau());
    }

    @Test
    public void testGetNiveau() {
        Lampe lampe = new Lampe("Salle de bain");
        assertEquals(0, lampe.getNiveau());
    }
}
