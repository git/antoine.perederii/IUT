package test;

import org.junit.Assert;
import org.junit.Test;

import model.Stereo;

public class StereoTest {
	@Test
    public void testStereoChambre1() {
        Stereo stereo = new Stereo("Chambre1");
        Assert.assertEquals(stereo.getLocation(), "Chambre1");
    }
	@Test
    public void testStereoGrenier() {
        Stereo stereo = new Stereo("Grenier");
        Assert.assertNotEquals(stereo.getLocation(), "Chambre1");
        Assert.assertEquals(stereo.getLocation(), "Grenier");
    }
}
