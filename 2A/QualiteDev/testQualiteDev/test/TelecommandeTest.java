package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Commande;
import model.PasDeCommande;
import model.Telecommande;
import static org.junit.Assert.*;

public class TelecommandeTest {

    @Test
    public void testTelecommande() {
        Telecommande telecommande = new Telecommande();
        Commande marcheCommande = new PasDeCommande();
        Commande arretCommande = new PasDeCommande();

        telecommande.setCommande(0, marcheCommande, arretCommande);
        telecommande.boutonMarchePresse(0);

        assertEquals(marcheCommande, telecommande.commandeAnnulation);
    }

//    @Test
//    public void testToString() {
//        // Arrange
//        Telecommande telecommande = new Telecommande();
//        Commande marcheCommande = new PasDeCommande();
//        Commande arretCommande = new PasDeCommande();
//        telecommande.setCommande(0, marcheCommande, arretCommande);
//
//        // Act
//        String result = telecommande.toString();
//
//        // Assert
//        String expected = "\n------ Télécommande -------\n" +
//                "[empt 0] PasDeCommande    PasDeCommande\n" +
//                "[annulation] PasDeCommande\n";
//        assertEquals(expected, result);
//    }
}
