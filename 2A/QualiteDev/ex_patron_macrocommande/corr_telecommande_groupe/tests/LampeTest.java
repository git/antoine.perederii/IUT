import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LampeTest {

    @Test
    public void testMarche() {
        Lampe lampe = new Lampe("Salon");
        lampe.marche();
        assertEquals(100, lampe.getNiveau());
    }

    @Test
    public void testArret() {
        Lampe lampe = new Lampe("Salon");
        lampe.marche();
        lampe.arret();
        assertEquals(0, lampe.getNiveau());
    }

    @Test
    public void testAttenuer() {
        Lampe lampe = new Lampe("Salon");
        lampe.attenuer(50);
        assertEquals(50, lampe.getNiveau());
    }

    @Test
    public void testAttenuerToZero() {
        Lampe lampe = new Lampe("Salon");
        lampe.attenuer(0);
        assertEquals(0, lampe.getNiveau());
    }

    @Test
    public void testAttenuerToZeroAfterOn() {
        Lampe lampe = new Lampe("Salon");
        lampe.marche();
        lampe.attenuer(0);
        assertEquals(0, lampe.getNiveau());
    }

    @Test
    public void testGetNiveau() {
        Lampe lampe = new Lampe("Salon");
        assertEquals(0, lampe.getNiveau());
    }
}
