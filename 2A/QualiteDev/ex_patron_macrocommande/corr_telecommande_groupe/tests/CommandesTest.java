import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CommandesTest {
    @Test
    public void testCommandeAllumerJacuzzi() {
        Jacuzzi jacuzzi = new Jacuzzi();
        Commande commande = new CommandeAllumerJacuzzi(jacuzzi);
        commande.executer();

        assertTrue(jacuzzi.allume);
        assertEquals(40, jacuzzi.temperature);
    }

    @Test
    public void testCommandeEteindreJacuzzi() {
        Jacuzzi jacuzzi = new Jacuzzi();
        Commande commande = new CommandeEteindreJacuzzi(jacuzzi);
        commande.executer();

        assertFalse(jacuzzi.allume);
        assertEquals(36, jacuzzi.temperature);
    }

    @Test
    public void testCommandeAnnulerJacuzzi() {
        Jacuzzi jacuzzi = new Jacuzzi();
        Commande commande = new CommandeAllumerJacuzzi(jacuzzi);
        commande.executer();
        commande.annuler();

        assertFalse(jacuzzi.allume);
        assertEquals(40, jacuzzi.temperature);
    }

    @Test
    public void testPasDeCommande() {
        Commande pasDeCommande = new PasDeCommande();
        pasDeCommande.executer();
        pasDeCommande.annuler();
    }

    @Test
    public void testMacroCommande() {
        Jacuzzi jacuzzi = new Jacuzzi();
        Commande commande1 = new CommandeAllumerJacuzzi(jacuzzi);
        Commande commande2 = new CommandeEteindreJacuzzi(jacuzzi);
        Commande[] commandes = {commande1, commande2};
        MacroCommande macroCommande = new MacroCommande(commandes);
        macroCommande.executer();
        macroCommande.annuler();
    }

    @Test
    public void testCommandeAllumerLampe() {
        Lampe lampe = new Lampe("Salon");
        Commande commande = new CommandeAllumerLampe(lampe);
        commande.executer();
        assertEquals(100, lampe.niveau);
    }

    @Test
    public void testCommandeEteindreLampe() {
        Lampe lampe = new Lampe("Salon");
        Commande commande = new CommandeEteindreLampe(lampe);
        commande.executer();
        assertEquals(0, lampe.niveau);
    }

    @Test
    public void testCommandeAnnulerLampe() {
        Lampe lampe = new Lampe("Salon");
        Commande commande = new CommandeAllumerLampe(lampe);
        commande.executer();
        commande.annuler();
        assertEquals(0, lampe.niveau);
    }

    @Test
    public void testCommandeEteindreLampeAnnuler() {
        Lampe lampe = new Lampe("Salon");
        Commande commande = new CommandeEteindreLampe(lampe);
        commande.executer();
        commande.annuler();
        assertEquals(100, lampe.niveau);
    }

    @Test
    public void testCommandeAllumerLampeSejour() {
        Lampe lampe = new Lampe("Séjour");
        Commande commande = new CommandeAllumerLampeSejour(lampe);
        commande.executer();
        assertEquals(0, lampe.niveau);
    }

    @Test
    public void testCommandeEteindreLampeSejour() {
        Lampe lampe = new Lampe("Séjour");
        Commande commande = new CommandeEteindreLampeSejour(lampe);
        commande.executer();
        assertEquals(100, lampe.niveau);
    }

    @Test
    public void testCommandeAnnulerLampeSejour() {
        Lampe lampe = new Lampe("Séjour");
        Commande commande = new CommandeAllumerLampeSejour(lampe);
        commande.executer();
        commande.annuler();
        assertEquals(100, lampe.niveau);
    }

    @Test
    public void testCommandeAnnulerEteindreLampeSejour() {
        Lampe lampe = new Lampe("Séjour");
        Commande commande = new CommandeEteindreLampeSejour(lampe);
        commande.executer();
        commande.annuler();
        assertEquals(0, lampe.niveau);
    }


    @Test
    public void testCommandeAllumerStereoAvecCD() {
        Stereo stereo = new Stereo("Salon");
        Commande commande = new CommandeAllumerStereoAvecCD(stereo);
        commande.executer();
        assertEquals(11, stereo.volume);
    }

    @Test
    public void testCommandeAnnulerStereoAvecCD() {
        Stereo stereo = new Stereo("Salon");
        Commande commande = new CommandeAllumerStereoAvecCD(stereo);
        commande.executer();
        commande.annuler();
        assertFalse(stereo.allume);
    }

    @Test
    public void testCommandeAllumerTV() {
        TV tv = new TV("Salon");
        Commande commande = new CommandeAllumerTV(tv);
        commande.executer();
        assertEquals(3, tv.canal);
    }

}
