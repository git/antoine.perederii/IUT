import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StereoTest {

    @Test
    public void testSetCDQuandPasAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.arret();
        assertThrows(IllegalStateException.class, stereo::setCD);
    }

    @Test
    public void testSetDVDQuandPasAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.arret();
        assertThrows(IllegalStateException.class, stereo::setDVD);
    }

    @Test
    public void testSetRadioQuandPasAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.arret();
        assertThrows(IllegalStateException.class, stereo::setRadio);
    }

    @Test
    public void testSetCDQuandAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.marche();
        assertDoesNotThrow(stereo::setCD);
    }

    @Test
    public void testSetDVDQuandAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.marche();
        assertDoesNotThrow(stereo::setDVD);
    }

    @Test
    public void testSetRadioQuandAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.marche();
        assertDoesNotThrow(stereo::setRadio);
    }

    @Test
    public void testSetVolumeQuandAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.marche();
        assertDoesNotThrow(() -> stereo.setVolume(5));
    }

    @Test
    public void testSetVolumeQuandPasAllume() {
        Stereo stereo = new Stereo("Salon");
        stereo.arret();
        assertThrows(IllegalStateException.class, () -> stereo.setVolume(5));
    }

    @Test
    public void testSetVolumeOutOfRange() {
        Stereo stereo = new Stereo("Salon");
        stereo.marche();
        assertThrows(IllegalArgumentException.class, () -> stereo.setVolume(20));
    }

    @Test
    public void testSetVolume() {
        Stereo stereo = new Stereo("Salon");
        stereo.marche();
        stereo.setVolume(8);
        assertEquals(8, stereo.getVolume());
    }
}
