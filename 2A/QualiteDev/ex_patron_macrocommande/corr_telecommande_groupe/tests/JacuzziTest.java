import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class JacuzziTest {

    @Test
    public void testAllumer() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.allumer();
        assertTrue(jacuzzi.allume);
    }

    @Test
    public void testEteindre() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.allumer();
        jacuzzi.eteindre();
        assertFalse(jacuzzi.allume);
    }

    @Test
    public void testBouillonnerQuandAllume() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.allumer();
        assertDoesNotThrow(jacuzzi::bouillonner);
    }

    @Test
    public void testBouillonnerQuandPasAllume() {
        Jacuzzi jacuzzi = new Jacuzzi();
        assertThrows(IllegalStateException.class, jacuzzi::bouillonner);
    }

    @Test
    public void testMarcheQuandAllume() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.allumer();
        assertDoesNotThrow(jacuzzi::marche);
    }

    @Test
    public void testMarcheQuandPasAllume() {
        Jacuzzi jacuzzi = new Jacuzzi();
        assertThrows(IllegalStateException.class, jacuzzi::marche);
    }

    @Test
    public void testArretQuandAllume() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.allumer();
        assertDoesNotThrow(jacuzzi::arret);
    }

    @Test
    public void testArretQuandPasAllume() {
        Jacuzzi jacuzzi = new Jacuzzi();
        assertThrows(IllegalStateException.class, jacuzzi::arret);
    }

    @Test
    public void testAugmenterTemp() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.setTemperature(30);
        jacuzzi.setTemperature(35);
        assertEquals(35, jacuzzi.temperature);
    }

    @Test
    public void testBaisserTemp() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.setTemperature(30);
        jacuzzi.setTemperature(25);
        assertEquals(25, jacuzzi.temperature);
    }

}
