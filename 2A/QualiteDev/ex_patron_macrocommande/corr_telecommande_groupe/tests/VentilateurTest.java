import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class VentilateurTest {

    @Test
    public void testRapide() {
        Ventilateur ventilateur = new Ventilateur("Salon");
        ventilateur.rapide();
        assertEquals(Ventilateur.RAPIDE, ventilateur.getVitesse());
    }

    @Test
    public void testMoyen() {
        Ventilateur ventilateur = new Ventilateur("Salon");
        ventilateur.moyen();
        assertEquals(Ventilateur.MOYEN, ventilateur.getVitesse());
    }

    @Test
    public void testLent() {
        Ventilateur ventilateur = new Ventilateur("Salon");
        ventilateur.lent();
        assertEquals(Ventilateur.LENT, ventilateur.getVitesse());
    }

    @Test
    public void testArreter() {
        Ventilateur ventilateur = new Ventilateur("Salon");
        ventilateur.arreter();
        assertEquals(Ventilateur.ARRET, ventilateur.getVitesse());
    }
}
