import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TVTest {

    @Test
    public void testMarche() {
        TV tv = new TV("Salon");
        assertDoesNotThrow(tv::marche);
    }

    @Test
    public void testArret() {
        TV tv = new TV("Salon");
        assertDoesNotThrow(tv::arret);
    }

    @Test
    public void testSelectionnerCanal() {
        TV tv = new TV("Salon");
        tv.selectionnerCanal();
        assertEquals(3, tv.canal);
    }
}
