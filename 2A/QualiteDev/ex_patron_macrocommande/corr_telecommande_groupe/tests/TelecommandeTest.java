import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TelecommandeTest {

    @Test
    public void testSetCommande() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerJacuzzi(new Jacuzzi());
        Commande commandeArret = new CommandeEteindreJacuzzi(new Jacuzzi());
        telecommande.setCommande(0, commandeMarche, commandeArret);

        assertEquals(commandeMarche, telecommande.commandesMarche[0]);
        assertEquals(commandeArret, telecommande.commandesArret[0]);
    }

    @Test
    public void testBoutonMarchePresse() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerLampe(new Lampe("Salon"));
        Commande commandeArret = new CommandeEteindreLampe(new Lampe("Salon"));
        telecommande.setCommande(0, commandeMarche, commandeArret);

        telecommande.boutonMarchePresse(0);
        assertEquals(commandeMarche, telecommande.commandeAnnulation);
    }

    @Test
    public void testBoutonArretPresse() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerTV(new TV("Salon"));
        Commande commandeArret = new CommandeEteindreTV(new TV("Salon"));
        telecommande.setCommande(0, commandeMarche, commandeArret);

        telecommande.boutonArretPresse(0);
        assertEquals(commandeArret, telecommande.commandeAnnulation);
    }

    @Test
    public void testBoutonAnnulPresse() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerStereo(new Stereo("Salon"));
        Commande commandeArret = new CommandeEteindreStereo(new Stereo("Salon"));
        telecommande.setCommande(0, commandeMarche, commandeArret);

        telecommande.boutonMarchePresse(0);
        telecommande.boutonAnnulPresse();
        assertEquals(commandeMarche, telecommande.commandeAnnulation);
    }

    @Test
    public void testMacroCommande() {
        Telecommande telecommande = new Telecommande();
        Commande commande1 = new CommandeEteindreVentilateur(new Ventilateur("Salon"));
        Commande commande2 = new CommandeVentilateurMoyen(new Ventilateur("Salon"));
        Commande commande3 = new CommandeVentilateurRapide(new Ventilateur("Salon"));
        Commande[] commandes = {commande1, commande2, commande3};
        MacroCommande macroCommande = new MacroCommande(commandes);

        telecommande.setCommande(0, macroCommande, new PasDeCommande());
        telecommande.boutonMarchePresse(0);

        assertEquals(commande1, ((MacroCommande) telecommande.commandesMarche[0]).commandes[0]);
        assertEquals(commande2, ((MacroCommande) telecommande.commandesMarche[0]).commandes[1]);
        assertEquals(commande3, ((MacroCommande) telecommande.commandesMarche[0]).commandes[2]);
    }

    @Test
    public void testSetCommandeStereoCD() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerStereoAvecCD(new Stereo("Salon"));
        Commande commandeArret = new CommandeEteindreStereo(new Stereo("Salon"));
        telecommande.setCommande(0, commandeMarche, commandeArret);

        assertEquals(commandeMarche, telecommande.commandesMarche[0]);
        assertEquals(commandeArret, telecommande.commandesArret[0]);
    }



    @Test
    public void testToString() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerLampeSejour(new Lampe("Salon"));
        Commande commandeArret = new CommandeEteindreLampeSejour(new Lampe("Salon"));
        telecommande.setCommande(0, commandeMarche, commandeArret);

        String expected = "\n------ Télécommande -------\n" +
                "[empt 0] CommandeAllumerLampeSejour    CommandeEteindreLampeSejour\n" +
                "[empt 1] PasDeCommande    PasDeCommande\n" +
                "[empt 2] PasDeCommande    PasDeCommande\n" +
                "[empt 3] PasDeCommande    PasDeCommande\n" +
                "[empt 4] PasDeCommande    PasDeCommande\n" +
                "[empt 5] PasDeCommande    PasDeCommande\n" +
                "[empt 6] PasDeCommande    PasDeCommande\n" +
                "[annulation] PasDeCommande\n";

        assertEquals(expected, telecommande.toString());
    }
}
