public class Stereo {
	String localisation;
	boolean allume;
	int volume;

	public Stereo(String localisation) {
		this.localisation = localisation;
	}

	public void marche() {
		this.allume = true;
		System.out.println(localisation + ": stéréo allumée");
	}

	public void arret() {
		this.allume = false;
		System.out.println(localisation + ": stéréo éteinte");
	}

	public void setCD() {
		if (!allume) {
			throw new IllegalStateException("La stéréo est éteinte. Allumez-la d'abord.");
		}
		System.out.println(localisation + ": stéréo réglée pour l'entrée CD");
	}

	public void setDVD() {
		if (!allume) {
			throw new IllegalStateException("La stéréo est éteinte. Allumez-la d'abord.");
		}
		System.out.println(localisation + ": stéréo réglée pour l'entrée DVD");
	}

	public void setRadio() {
		if (!allume) {
			throw new IllegalStateException("La stéréo est éteinte. Allumez-la d'abord.");
		}
		System.out.println(localisation + ": stéréo réglée pour la radio");
	}

	public void setVolume(int volume) {
		if (!allume) {
			throw new IllegalStateException("La stéréo est éteinte. Allumez-la d'abord.");
		}
		if (volume < 0 || volume > 11) {
			throw new IllegalArgumentException("Le volume doit être compris entre 0 et 11.");
		}
		this.volume = volume;
		System.out.println(localisation + ": le volume stéréo est " + volume);
	}

	public int getVolume() {
		return volume;
	}
}
