public class Jacuzzi {
	boolean allume;
	int temperature;

	public Jacuzzi() {
	}

	public void allumer() {
		allume = true;
	}

	public void eteindre() {
		allume = false;
	}

	public void bouillonner() {
		if (allume) {
			System.out.println("Le jaccuzi bouillonne !");
		} else {
			throw new IllegalStateException("Impossible de bouillonner lorsque le jacuzzi est éteint.");
		}
	}

	public void marche() {
		if (allume) {
			System.out.println("Le jaccuzi est en marche");
		} else {
			throw new IllegalStateException("Impossible de démarrer le jacuzzi lorsque celui-ci est éteint.");
		}
	}

	public void arret() {
		if (allume) {
			System.out.println("Le jaccuzi est arrêté");
		} else {
			throw new IllegalStateException("Impossible d'arrêter le jacuzzi car il est déjà éteint.");
		}
	}

	public void setTemperature(int temperature) {
		if (temperature > this.temperature) {
			System.out.println("Le jacuzzi chauffe à " + temperature + "°");
		} else {
			System.out.println("Le jacuzzi refroidit à " + temperature + "°");
		}
		this.temperature = temperature;
	}

}
