//package test;
//
//import static org.junit.Assert.assertEquals;
//
//import org.junit.Test;
//
//import model.Commande;
//import model.PasDeCommande;
//import model.Telecommande;
//import static org.junit.Assert.*;
//
//public class TelecommandeTest {
//
//    @Test
//    public void testTelecommande() {
//        Telecommande telecommande = new Telecommande();
//        Commande marcheCommande = new PasDeCommande();
//        Commande arretCommande = new PasDeCommande();
//
//        telecommande.setCommande(0, marcheCommande, arretCommande);
//        telecommande.boutonMarchePresse(0);
//
//        assertEquals(marcheCommande, telecommande.commandeAnnulation);
//    }
//
////    @Test
////    public void testToString() {
////        // Arrange
////        Telecommande telecommande = new Telecommande();
////        Commande marcheCommande = new PasDeCommande();
////        Commande arretCommande = new PasDeCommande();
////        telecommande.setCommande(0, marcheCommande, arretCommande);
////
////        // Act
////        String result = telecommande.toString();
////
////        // Assert
////        String expected = "\n------ Télécommande -------\n" +
////                "[empt 0] PasDeCommande    PasDeCommande\n" +
////                "[annulation] PasDeCommande\n";
////        assertEquals(expected, result);
////    }
//}

import org.junit.Test;

import model.Commande;
import model.CommandeAllumerLampe;
import model.CommandeEteindreLampe;
import model.Lampe;
import model.PasDeCommande;
import model.Telecommande;

import static org.junit.Assert.*;

public class TelecommandeTest {

    @Test
    public void testSetCommande() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeMarche = new CommandeAllumerLampe(lampe);
        Commande commandeArret = new CommandeEteindreLampe(lampe);

        telecommande.setCommande(0, commandeMarche, commandeArret);

        assertSame(commandeMarche, telecommande.commandesMarche[0]);
        assertSame(commandeArret, telecommande.commandesArret[0]);
    }

    @Test
    public void testBoutonMarchePresse() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeMarche = new CommandeAllumerLampe(lampe);

        telecommande.setCommande(0, commandeMarche, null);

        telecommande.boutonMarchePresse(0);

        assertTrue(lampe.estAllumee());
        assertSame(commandeMarche, telecommande.commandeAnnulation);
    }

    @Test
    public void testBoutonArretPresse() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeArret = new CommandeEteindreLampe(lampe);

        telecommande.setCommande(0, null, commandeArret);

        telecommande.boutonArretPresse(0);

        assertFalse(lampe.estAllumee());
        assertSame(commandeArret, telecommande.commandeAnnulation);
    }

    @Test
    public void testBoutonAnnulPresse() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeMarche = new CommandeAllumerLampe(lampe);

        telecommande.setCommande(0, commandeMarche, null);

        telecommande.boutonMarchePresse(0);
        assertTrue(lampe.estAllumee());

        telecommande.boutonAnnulPresse();

        assertFalse(lampe.estAllumee());
    }

    @Test
    public void testToString() {
        Telecommande telecommande = new Telecommande();
        String expected = "\n------ Télécommande -------\n";
        for (int i = 0; i < 7; i++) {
            expected += "[empt " + i + "] model.PasDeCommande    model.PasDeCommande\n";
        }
        expected += "[annulation] model.PasDeCommande\n";

        assertEquals(expected, telecommande.toString());
    }
    
    @Test
    public void testConstruction() {
        Telecommande telecommande = new Telecommande();

        assertNotNull(telecommande.commandesMarche);
        assertNotNull(telecommande.commandesArret);
        assertNotNull(telecommande.commandeAnnulation);

        assertEquals(7, telecommande.commandesMarche.length);
        assertEquals(7, telecommande.commandesArret.length);

        for (int i = 0; i < 7; i++) {
            assertTrue(telecommande.commandesMarche[i] instanceof PasDeCommande);
            assertTrue(telecommande.commandesArret[i] instanceof PasDeCommande);
        }

        assertTrue(telecommande.commandeAnnulation instanceof PasDeCommande);
    }

    @Test
    public void testSetCommande1() {
        Telecommande telecommande = new Telecommande();
        Commande commandeMarche = new CommandeAllumerLampe(new Lampe("Test"));
        Commande commandeArret = new CommandeEteindreLampe(new Lampe("Test"));

        telecommande.setCommande(0, commandeMarche, commandeArret);

        assertSame(commandeMarche, telecommande.commandesMarche[0]);
        assertSame(commandeArret, telecommande.commandesArret[0]);
    }

    @Test
    public void testBoutonMarchePresse1() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeMarche = new CommandeAllumerLampe(lampe);

        telecommande.setCommande(0, commandeMarche, null);

        telecommande.boutonMarchePresse(0);

        assertTrue(lampe.estAllumee());
        assertSame(commandeMarche, telecommande.commandeAnnulation);
    }

    @Test
    public void testBoutonArretPresse1() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeArret = new CommandeEteindreLampe(lampe);

        telecommande.setCommande(0, null, commandeArret);

        telecommande.boutonArretPresse(0);

        assertFalse(lampe.estAllumee());
        assertSame(commandeArret, telecommande.commandeAnnulation);
    }

    @Test
    public void testBoutonAnnulPresse1() {
        Telecommande telecommande = new Telecommande();
        Lampe lampe = new Lampe("Test");
        Commande commandeMarche = new CommandeAllumerLampe(lampe);

        telecommande.setCommande(0, commandeMarche, null);

        telecommande.boutonMarchePresse(0);
        assertTrue(lampe.estAllumee());

        telecommande.boutonAnnulPresse();

        assertFalse(lampe.estAllumee());
    }

    @Test
    public void testToString1() {
        Telecommande telecommande = new Telecommande();
        String expected = "\n------ Télécommande -------\n";
        for (int i = 0; i < 7; i++) {
            expected += "[empt " + i + "] model.PasDeCommande    model.PasDeCommande\n";
        }
        expected += "[annulation] model.PasDeCommande\n";

        assertEquals(expected, telecommande.toString());
    }
}

