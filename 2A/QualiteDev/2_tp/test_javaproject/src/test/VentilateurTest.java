package test;

import org.junit.Test;
import static org.junit.Assert.*;
import model.Ventilateur;

public class VentilateurTest {

    @Test
    public void testRapide() {
        Ventilateur ventilateur = new Ventilateur("Salon");

        ventilateur.rapide();
        assertEquals(Ventilateur.RAPIDE, ventilateur.getVitesse());
    }

    @Test
    public void testMoyen() {
        Ventilateur ventilateur = new Ventilateur("Chambre");

        ventilateur.moyen();

        assertEquals(Ventilateur.MOYEN, ventilateur.getVitesse());
    }

    @Test
    public void testLent() {
        Ventilateur ventilateur = new Ventilateur("Cuisine");

        ventilateur.lent();

        assertEquals(Ventilateur.LENT, ventilateur.getVitesse());
    }

    @Test
    public void testArreter() {
        Ventilateur ventilateur = new Ventilateur("Salle de bain");

        ventilateur.arreter();

        assertEquals(Ventilateur.ARRET, ventilateur.getVitesse());
    }

    @Test
    public void testGetVitesse() {
        Ventilateur ventilateur = new Ventilateur("Salon");

        int result = ventilateur.getVitesse();

        assertEquals(Ventilateur.ARRET, result);
    }
}
