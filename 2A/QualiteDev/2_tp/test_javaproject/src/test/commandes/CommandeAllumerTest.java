package test.commandes;

import org.junit.Test;

import model.CommandeAllumerJacuzzi;
import model.Jacuzzi;

import static org.junit.Assert.*;

public class CommandeAllumerTest {

    @Test
    public void testCommandeAllumerJacuzzi() {
        // Arrange
        Jacuzzi jacuzzi = new Jacuzzi();
        CommandeAllumerJacuzzi commande = new CommandeAllumerJacuzzi(jacuzzi);

        // Act
        commande.executer();

        // Assert
        assertTrue(jacuzzi.getAllume());
        assertEquals(40, jacuzzi.getTemperature());
        // Additional assertions based on the behavior of your system.
    }

    @Test
    public void testCommandeAnnulerJacuzzi() {
        // Arrange
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.allumer();  // To test the annuler() method, we assume the Jacuzzi is already on.
        CommandeAllumerJacuzzi commande = new CommandeAllumerJacuzzi(jacuzzi);

        // Act
        commande.annuler();

        // Assert
        assertFalse(jacuzzi.getAllume());
        // Additional assertions based on the behavior of your system.
    }
}
