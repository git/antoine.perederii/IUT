package test.commandes;

import org.junit.Test;
import static org.junit.Assert.*;

import model.CommandeAllumerJacuzzi;
import model.Jacuzzi;

public class CommandeAllumerJacuzziTest {

    @Test
    public void testExecuter() {
        // Arrange
        Jacuzzi jacuzzi = new Jacuzzi();
        CommandeAllumerJacuzzi commande = new CommandeAllumerJacuzzi(mockJacuzzi);

        // Act
        commande.executer();

        // Assert
        Mockito.verify(mockJacuzzi, Mockito.times(1)).allumer();
        Mockito.verify(mockJacuzzi, Mockito.times(1)).setTemperature(40);
        Mockito.verify(mockJacuzzi, Mockito.times(1)).bouillonner();
        Mockito.verifyNoMoreInteractions(mockJacuzzi);
    }

    @Test
    public void testAnnuler() {
        // Arrange
        Jacuzzi mockJacuzzi = Mockito.mock(Jacuzzi.class);
        CommandeAllumerJacuzzi commande = new CommandeAllumerJacuzzi(mockJacuzzi);

        // Act
        commande.annuler();

        // Assert
        Mockito.verify(mockJacuzzi, Mockito.times(1)).eteindre();
        Mockito.verifyNoMoreInteractions(mockJacuzzi);
    }
}
