package test;

import org.junit.Assert;
import org.junit.Test;

import model.TV;

public class TVTest {
	@Test
	public void locationTest() {
		TV myTv = new TV("Entree");
		Assert.assertEquals(myTv.getLocation(), "Entree");
		Assert.assertNotEquals(myTv.getLocation(), "Salon");
	}
}
