package test;

import model.Jacuzzi;

import org.junit.Assert;
import org.junit.Test;

public class JacuzziTest {

    @Test
    public void testAllumer() {
        Jacuzzi jacuzzi = new Jacuzzi();

        jacuzzi.allumer();

        Assert.assertTrue(jacuzzi.getAllume());
    }

    @Test
    public void testEteindre() {
        Jacuzzi jacuzzi = new Jacuzzi();
        jacuzzi.eteindre();

        Assert.assertFalse(jacuzzi.getAllume());
    }

    @Test
    public void testSetTemperature() {
        Jacuzzi jacuzzi = new Jacuzzi();

        jacuzzi.setTemperature(40);

        Assert.assertEquals(40, jacuzzi.getTemperature());
        Assert.assertNotEquals(50, jacuzzi.getTemperature());
    }
    
    @Test
    public void testSetTemperatureNegative() {
        Jacuzzi jacuzzi = new Jacuzzi();

        jacuzzi.setTemperature(-10);

        Assert.assertEquals(-10, jacuzzi.getTemperature());
        Assert.assertNotEquals(0, jacuzzi.getTemperature());
    }
}
