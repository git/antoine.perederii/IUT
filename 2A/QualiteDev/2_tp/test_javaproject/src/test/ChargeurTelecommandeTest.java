package test;

import org.junit.Test;

import model.Commande;
import model.CommandeAllumerJacuzzi;
import model.CommandeAllumerLampe;
import model.CommandeAllumerStereo;
import model.CommandeAllumerTV;
import model.CommandeEteindreJacuzzi;
import model.CommandeEteindreLampe;
import model.CommandeEteindreStereo;
import model.CommandeEteindreTV;
import model.Jacuzzi;
import model.Lampe;
import model.MacroCommande;
import model.Stereo;
import model.TV;

import static org.junit.Assert.*;

public class ChargeurTelecommandeTest {

    @Test
    public void testMacroAllumage() {
        // Création des équipements
        Lampe lampe = new Lampe("Séjour");
        TV tv = new TV("Séjour");
        Stereo stereo = new Stereo("Séjour");
        Jacuzzi jacuzzi = new Jacuzzi();

        // Création des commandes pour allumer les équipements
        CommandeAllumerLampe lampeAllumee = new CommandeAllumerLampe(lampe);
        CommandeAllumerStereo stereoAllumee = new CommandeAllumerStereo(stereo);
        CommandeAllumerTV tvAllumee = new CommandeAllumerTV(tv);
        CommandeAllumerJacuzzi jacuzziAllume = new CommandeAllumerJacuzzi(jacuzzi);

        // Création de la macro commande d'allumage
        Commande[] allumageGroupe = {lampeAllumee, stereoAllumee, tvAllumee, jacuzziAllume};
        MacroCommande macroAllumageGroupe = new MacroCommande(allumageGroupe);

        // Vérification que la macro commande d'allumage fonctionne correctement
        assertNotNull(macroAllumageGroupe);
        macroAllumageGroupe.executer();
    }

    @Test
    public void testMacroExtinction() {
        // Création des équipements
        Lampe lampe = new Lampe("Séjour");
        TV tv = new TV("Séjour");
        Stereo stereo = new Stereo("Séjour");
        Jacuzzi jacuzzi = new Jacuzzi();

        // Création des commandes pour éteindre les équipements
        CommandeEteindreLampe lampeEteinte = new CommandeEteindreLampe(lampe);
        CommandeEteindreStereo stereoEteinte = new CommandeEteindreStereo(stereo);
        CommandeEteindreTV tvEteinte = new CommandeEteindreTV(tv);
        CommandeEteindreJacuzzi jacuzziEteint = new CommandeEteindreJacuzzi(jacuzzi);

        // Création de la macro commande d'extinction
        Commande[] extinctionGroupe = {lampeEteinte, stereoEteinte, tvEteinte, jacuzziEteint};
        MacroCommande macroExtinctionGroupe = new MacroCommande(extinctionGroupe);

        // Vérification que la macro commande d'extinction fonctionne correctement
        assertNotNull(macroExtinctionGroupe);
    }
}
