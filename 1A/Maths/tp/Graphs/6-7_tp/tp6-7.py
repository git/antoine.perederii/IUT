import networkx as nx
import matplotlib.pyplot as plt


H=nx.Graph()
H.add_edges_from([(1,2),(1,5),(2,3),(2,5),(3,4),(4,5),(4,6),(5,0),(0,1),
(0,6),(3,6),(3,5),(5,6)])
for s in H.nodes:
    H.nodes[s]["couleur"] = 0


def plus_petite_couleur_disponible(G,s):
    couleurs = [G.nodes[v]["couleur"] for v in G.neighbors(s)]
    for i in range(len(couleurs)+1):
        if i not in couleurs:
            return i
        
print(plus_petite_couleur_disponible(H,1))

def coloration_gloutonne(G):
    for s in G.nodes:
        G.nodes[s]["couleur"] = plus_petite_couleur_disponible(G,s)
    return G

print(coloration_gloutonne(H).nodes.data())

def afficher_graphe_colore(G):
    nx.draw(G,with_labels=True,node_color=[G.nodes[v]["couleur"] for v in G.nodes])
    plt.show()

afficher_graphe_colore(H)

# En option, on pourrait aussi afficher des vraies couleurs, prises au hasard, avec une
# fonction afficher graphe colore vraies couleurs(G,c) o`u G est le graphe d ́ej`a co-
# lori ́e et c est le nombre de couleurs. Pour tirer une couleur au hasard, il faut tirer au
# hasard trois flottants (entre 0 et 1 chacun) repr ́esentant la proportion de rouge, vert, et
# bleu, avec la fonction random.random() (il faut faire au pr ́ealable un import random).
# Il faudra ensuite associer `a chaque num ́ero de couleur (entre 1 et c) de la coloration,
# une couleur diff ́erente.
# Par exemple, supposons que la liste L contient les sommets colori ́es avec une certaine
# couleur (par exemple la couleur 1) et qu’on veut associer la couleur 1 `a la vraie couleur
# d ́efinie par r=0.2, v=0.7, b=1, le code suivant affichera les sommets de L (et uniquement
# ceux-ci) avec cette couleur :
# dico_pos = nx.spring_layout(G)
# nx.draw(G,pos=dico_pos,with_labels=True)
# nx.draw_networkx_nodes(G, dico_pos, nodelist=L, node_color=[[r,v,b]])
# plt.show()

def afficher_graphe_colore_vraies_couleurs(G, c):
    import random
    for s in G.nodes:
        G.nodes[s]["couleur"] = plus_petite_couleur_disponible(G,s)
    dico_pos = nx.spring_layout(G)
    nx.draw(G,pos=dico_pos,with_labels=True)
    for i in range(c):
        r = random.random()
        v = random.random()
        b = random.random()
        nx.draw_networkx_nodes(G, dico_pos, nodelist=[s for s in G.nodes if G.nodes[s]["couleur"] == i], node_color=[[r,v,b]])
    plt.show()

afficher_graphe_colore_vraies_couleurs(H, 4)

# Exercice 2. Algorithme de Welsh-Powell

# 1. Avec combien de couleurs l’algorithme glouton de l’exercice 1 colorie-t-il l’arbre de la
# figure ci-dessus (avec l’ordre donn ́e par les num ́eros) ? Et l’algorithme de Welsh-Powell
# (en suivant l’ordre des degr ́es d ́ecroissants) ? Quel est le nombre optimal de couleurs ?

# 4
# Pour welshpowell c'est 2 differents

# 2. Coder une fonction prochain sommet WP(G) qui,  ́etant donn ́e un graphe partiellement
# color ́e, renvoie le prochain sommet color ́e par l’algorithme de Welsh-Powell (de degr ́e
# maximum parmi les non-colori ́es).

def prochain_sommet_WP(G):
    degre_max = 0
    sommet_max = None
    for s in G.nodes:
        if G.nodes[s]["couleur"] == 0 and G.degree[s] > degre_max:
            degre_max = G.degree[s]
            sommet_max = s
    return sommet_max

print(prochain_sommet_WP(H))

QwwwWWWWW