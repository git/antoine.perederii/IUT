import networkx as nx
import matplotlib.pyplot as plt
import numpy as np


""" H=nx.Graph() #cr ́ee un graphe
H.add_edge(0,1) #ajoute une arˆete entre les sommets 0 et 1
H.add_edges_from([(3,0),(3,4)]) #ajoute les arˆetes d’une liste donn ́ee
H.add_node("toto") #ajoute un sommet nomm ́e "toto"
H.remove_node(s) #supprime le sommet s
H.nodes #sommets du graphe (attention, pour en faire une vraie liste Python,  ́ecrire: list(H.nodes))
H.edges #arˆetes du graphe (attention, pour en faire une vraie liste Python,  ́ecrire: list(H.edges))
H.edges(s) #les arˆetes qui touchent le sommet s
H.neighbors(s) #un it ́erateur sur les voisins du sommet s dans H pour obtenir une liste,  ́ecrire: list(H.neighbors(s))
H.nodes[s]["attri"] #acc`ede `a l’attribut nomm ́e "attri" du sommet s (tant en lecture qu’en  ́ecriture) exemple: H.nodes[s]["attri"]=2 ou alors print(H.nodes[s]["attri"])
 """

# les arbres

def ajouter_file(F, v):
    F.append(v)
    return

def enlever_tete_file(F):
    return F.pop(0)

def file_vide(F):
    return len(F) == 0

def valeur_tete_file(F):
    return F[0]

def ajouter_pile(P, v):
    P.append(v)
    return

def enlever_tete_pile(P):
    return P.pop()

def pile_vide(P):
    return len(P) == 0

def valeur_tete_pile(P):
    return P[-1]


F=[]
P=[]
print(file_vide(F))
print(file_vide(P))
ajouter_file(F, 1)
ajouter_file(F, 2)
enlever_tete_file(F)
print(F)
ajouter_pile(P, 1)
ajouter_pile(P, 2)
enlever_tete_pile(P)
print(P)
valeur_tete_file(F)
valeur_tete_pile(P)

G=nx.Graph()
G.add_edges_from([(1,2),(1,5),(2,3),(2,5),(3,4),(4,5),(4,6),(5,0)])
nx.draw(G,with_labels=True)
plt.show()


def parcours_largeur(G, s):
    V = [s]
    F = [s]
    while F:
        x = F.pop(0)
        for y in G.neighbors(x):
            if y not in V:
                V.append(y)
                F.append(y)
    return V


print(parcours_largeur(G, 0))
print(parcours_largeur(G, 1))
print(parcours_largeur(G, 2))
print(parcours_largeur(G, 3))
print(parcours_largeur(G, 4))
print(parcours_largeur(G, 5))


# ecrire la fonction parcours profondeur(G,s) qui effectue un parcours en profondeur
# du graphe G `a partir du sommet s, et qui retourne la liste des sommets visit ́es.



def parcours_profondeur(G, s):
    V = [s]
    P = [s]
    while P:
        x = P.pop()
        for y in G.neighbors(x):
            if y not in V:
                V.append(y)
                P.append(y)
    return V

