import networkx as nx #pour la gestion des graphes
import matplotlib.pyplot as plt #pour l’affichage
#(on les renomme pour raccourcir le code)

# Exo 1

G1 = nx.Graph() #cr ́ee un graphe non-orient ́e vide
G1.add_edge(0,1) #ajoute une arˆete entre les sommets 0 et 1
G1.add_edges_from([(3,0),(3,4)]) #ajoute les arˆetes d’une liste donn ́ee
nx.add_path(G1,[1,2,3]) #ajoute les arˆetes du chemin 1-2-3

G1.add_node(6) #ajoute un sommet
G1.add_node("toto")
G1.add_edge("toto",6)


G1.remove_node(6) #Supprimez un sommet s de votre choix, puis r ́e-affichez le graphe.
print(G1.nodes)
print(G1.edges)

nx.draw(G1, with_labels=True, pos=nx.circular_layout(G1), node_color='r', edge_color='b')
# ou 
# dico_positions = {0:(0,0),1:(1,0),2:(0,1),3:(1,1),4:(0,-1),6:(-1,-1),"toto":(0,-1)}
# nx.draw(G1, with_labels=True, pos=dico_positions)

# nx.draw(G1,with_labels=True) #on pr ́epare le dessin du graphe

plt.show() #on affiche le dessin

# Exo 2

H = nx.Graph() #cr ́ee un graphe non-orient ́e vide

H.add_edges_from([(1,2),(2,3),(3,4),(4,5),(5,2),(4,6),(1,5)]) #ajoute les arˆetes d’une liste donn ́ee
# nx.add_path(G1,[1,2,3,4,5,2]) #ajoute les arˆetes du chemin 1-2-3

nx.draw(H, with_labels=True, pos=nx.circular_layout(H), node_color='r', edge_color='b')

plt.show() #on affiche le dessin

# 2. Ecrire le code qui permet d’afficher les salles directement connect ́ees `a la salle 5, en utilisant la m ́ethode H.edges qui renvoie la liste des arˆetes de H. (Attention : une arˆete entre i et j peut ˆetre stock ́ee sous forme (i,j) ou (j,i).

def getArete(G, node):
    aretes = G.edges
    aretes = [arete for arete in aretes if node in arete]
    return aretes

print(getArete(H, 5))

H.add_edge(3, 5)

print(getArete(H, 5))

# Exo 3

def nombre_sommets(G):
    return len(G.nodes)

def nombre_aretes(G):
    return len(G.edges)

def existe_arete(G, i, j): # renvoie si le couple i,j existe dans les aretes du graphe G
    return (i, j) in G.edges or (j, i) in G.edges

def voisins(G,s): #renvoie la liste des voisins du sommet s grace à la methode existe_arete
    return [i for i in G.nodes if existe_arete(G, s, i)]

def degre(G,s): # renvoie le degre (nombre de voisins) du sommet s
    return len(voisins(G, s))

def degre_max(G): # renvoie le degre max du graphe G (celui qui à le plus de voisins)
    return max([degre(G, s) for s in G.nodes])

def sommets_de_degre_max(G): # renvoie la liste des sommets de degre max du graphe G
    return [s for s in G.nodes if degre(G, s) == degre_max(G)]


print(nombre_sommets(H))
print(nombre_aretes(H))
print(existe_arete(H, 1, 2))
print(voisins(H, 1))
print(degre(H, 1))
print(degre_max(H))
print(sommets_de_degre_max(H))


# Exo 4

GO = nx.DiGraph() #on cr ́ee un graphe orient ́e
GO.add_edge(0,1)
GO.add_edge(1,0)
GO.add_edge(1,2)
GO.add_edge(3,2)
GO.add_edge(3,1)
nx.draw(GO,with_labels=True) #on pr ́epare le dessin du graphe
plt.show() #on affiche le dessin


def existe_arc(G, i, j): # renvoie si le couple i,j existe dans les aretes du graphe G
    return (i, j) in G.edges

def voisins_entrants(G,s): #renvoie la liste des voisins du sommet s grace à la methode existe_arete
    return [i for i in G.nodes if existe_arc(G, i, s)]

def degre_entrant(G,s): # renvoie le degre (nombre de voisins) du sommet s
    return len(voisins_entrants(G, s))

def est_source(G,s): #renvoie si le sommet s est une source
    return degre_entrant(G, s) == 0


print(existe_arc(GO, 1, 2))
print(voisins_entrants(GO, 1))
print(degre_entrant(GO, 1))
print(est_source(GO, 1))


# Exo 6

F2 = nx.Graph() #cr ́ee un graphe non-orient ́e vide
F2.add_node("0-0")
F2.add_node("0-1")
F2.add_node("0-2")
F2.add_node("0-3")
F2.add_node("0-4")
F2.add_node("0-5")
F2.add_node("1-0")
F2.add_node("1-1")
F2.add_node("1-2")
F2.add_node("1-3")
F2.add_node("1-4")
F2.add_node("1-5")
F2.add_node("2-0")
F2.add_node("2-1")
F2.add_node("2-2")
F2.add_node("2-3")
F2.add_node("2-4")
F2.add_node("2-5")
F2.add_node("3-0")
F2.add_node("3-1")
F2.add_node("3-2")
F2.add_node("3-3")
F2.add_node("3-4")
F2.add_node("3-5")
F2.add_node("4-0")
F2.add_node("4-1")
F2.add_node("4-2")
F2.add_node("4-3")
F2.add_node("4-4")
F2.add_node("4-5")

nx.add_path(F2,["1-0", "0-0", "0-1", "1-1", "1-2", "0-2", "0-3", "0-4"]) #ajoute les arˆetes du chemin 
nx.add_path(F2,["1-2", "1-3", "1-4"])
nx.add_path(F2,["1-3", "2-3", "2-2", "2-1", "3-1", "3-0", "2-0"])
nx.add_path(F2,["2-2", "3-2", "3-3"])
nx.add_path(F2,["3-2", "4-2", "4-1", "4-0"])
nx.add_path(F2,["4-2", "4-3", "4-4", "3-4", "2-4", "2-5", "1-5", "0-5"])
nx.add_path(F2,["2-5", "3-5", "4-5"])

# nx.add_path(F2,["1-0", "1-2", "0-4"])
# nx.add_path(F2,["1-2", "1-3", "1-4"])
# nx.add_path(F2,["1-3", "2-2", "2-0"])
# nx.add_path(F2,["2-2", "3-2", "3-3"])
# nx.add_path(F2,["3-2", "4-2", "4-0"])
# nx.add_path(F2,["4-2", "2-5", "0-5"])
# nx.add_path(F2,["2-5", "4-5"])
# F2.add_edges_from([(3,0),(3,4)]) #ajoute les arˆetes d’une liste donn ́ee

# dico_positions = {0:(0,0),1:(1,0),2:(0,1),3:(1,1),4:(0,-1),6:(-1,-1),"toto":(0,-1)}

nx.draw(F2, with_labels=True)

plt.show() #on affiche le dessin

# c'est un arbre donc un seu chemin possible

# Coder une fonction chemin(s) qui trouve un chemin depuis l’entr ́ee "4-0" vers un sommet s donn ́e (on peut r ́ep ́eter des cases). On affichera les cases parcourues lors de l’exploration. Lors de l’exploration du labyrinthe, on pourra se souvenir des sommets d ́ej`a parcourus. Un it ́erateur sur les voisins d’un sommet s est donn ́e par G.neighbors(s) (on obtient la liste des voisins avec list(G.neighbors(s))).

def chemin(arrivee):
    L = []
    L.append(arrivee)
    while arrivee != (4,0):
        for i in voisins(F2,arrivee):
            if(existe_arete(F2,arrivee,i)):
                arrivee = i
                L.append(arrivee)
                L.reverse()
    return L

print(chemin((4,1)))