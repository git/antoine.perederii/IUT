import matplotlib.pyplot as plt
import numpy as np
import time
# import shape

# On active le "mode interactif" de pyplot. Ainsi, les plt.show() ne sont plus nécessaires.
plt.ion()						

# --------------------------------------------------------------------------------------
# Fonction de "fin temporaire" pendant l'avancée du TP. Attend une frappe Entrée, puis quitte le script.

def stop_ici():
	plt.pause(0.1)	# nécessaire pour que matplotlib ait le temps d'afficher les figures
	input()
	exit()
	
from time import time
debut = time()
def CastelJauUnPoint(t, P):
	P = P.copy()
	N = P.shape[0] - 1
	for i in range(N):
	    for j in range(N-i):
		    P[j] = (1-t)*P[j] + t*P[j+1]
	return P[0]
	

def traceBezierCastelJau(P,K):
	allt = np.linspace(0,1,K)
	allFt = np.array([CastelJauUnPoint(t,P) for t in allt])
	plt.plot(allFt[:,0], allFt[:,1])
# met des valeurs aléatoire a P
P = np.random.rand(4,2)
K = 1000000000
traceBezierCastelJau(P,K)

fin = time()
temps = fin - debut
print("Durée de calcul pour Castel: ", temps)



from time import time
debut = time()
from scipy.special import binom
def BezierFormuleUnPoint(t,P):
	N = P.shape[0]-1
	Ft = np.zeros(2)
	for i in range(N+1):
		Ft += binom(N,i)*t**i*(1-t)**(N-i)*P[i]
	return Ft

def traceBezierFormule(P,K):
	allt = np.linspace(0,1,K)
	allFt = np.array([BezierFormuleUnPoint(t,P) for t in allt])
	plt.plot(allFt[:,0], allFt[:,1])

# met des valeurs aléatoire a P
P = np.random.rand(4,2)
K = 1000000000
traceBezierFormule(P,K)

fin = time()
duree = fin - debut
print("Durée de calcul pour Bezier un point : ", duree)


stop_ici()