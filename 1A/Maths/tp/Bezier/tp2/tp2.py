import numpy as np
import matplotlib.pyplot as plt

# Exercice 1

P = np.array([[-1,3], [2,6],[7,8],[4,1]])
print(P)

assert isinstance(P,np.ndarray) and P.shape==(4,2), "P n'a pas le bon format"
nombre = P[2][0]
print(nombre)
assert nombre == 7, "Le nombre n'est pas bon"

P1 = P[1]
print(P1)
assert np.array_equal(P1,np.array([2,6])), "P1 n'est pas bon"

Psaufdernier = P[:-1]
print(Psaufdernier)
assert Psaufdernier.shape == (3,2), "Psaufdernier n'a pas le bon format"

P_x = P[:,0]
print(P_x)

# plt.figure()
# plt.plot(P_x,P[:,1], 'o')
# plt.plot(P_x,P[:,1])
# # plt.show()


# Exercice 2

def CasteljeuEtape(t,P):
    return (1-t)*P[:-1] + t*P[1:]

def bezierCasteljeu(t,P):
    n = len(P)
    for i in range(n-1):
        P = CasteljeuEtape(t,P)
        plt.plot(P[:,0],P[:,1])
    return P

P = np.array([[-1,3],[2,6],[7,8],[4,1]])
plt.figure()
plt.plot(P[:,0],P[:,1], 'o')
plt.plot(P[:,0],P[:,1])
bezierCasteljeu(0.5, P)
# plt.show()

# Exercice 3

def bezierRec(t,P):
    if P.shape [0]==1:
        return P[0]
    else:
        return (1-t)*bezierRec(t,P[:-1]) + t*bezierRec(t,P[1:])

P = np.array([[0,0],[7,0],[13,5],[10,-2],[12,-7],[9,-10],[11,-13],[11,-13],[11,-22],[10,-24],[12,-27],[10,-29],[8,-25],[-1,-25],[-3,-29],[-5,-27],[-4,-24],[-6,-22],[-6,-13],[-4,-10],[-6,-8],[-3,-2],[-6,4],[0,0]]) 
plt.figure()
plt.plot(P[:,0],P[:,1], 'o')
plt.plot(P[:,0],P[:,1])
bezierCasteljeu(0.5,P)
plt.show()