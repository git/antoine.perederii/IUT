import matplotlib.pyplot as plt
import numpy as np
import time
# import shape

# On active le "mode interactif" de pyplot. Ainsi, les plt.show() ne sont plus nécessaires.
plt.ion()						

# --------------------------------------------------------------------------------------
# Fonction de "fin temporaire" pendant l'avancée du TP. Attend une frappe Entrée, puis quitte le script.

def stop_ici():
	plt.pause(0.1)	# nécessaire pour que matplotlib ait le temps d'afficher les figures
	input()
	exit()

# Exercice 1

# Question 1
    #! F(A,B,C,t) = (1-t)**2*A + 2*t*(1 - t)*B + t**2*C

def formuleBezier2(t,A,B,C):
    return (1-t)**2*A + 2*t*(1 - t)*B + t**2*C

A, B, C = np.array([0,1]),np.array([3,3]),np.array([5,0])
allt = np.linspace(0,1,100)
allFt = np.zeros((len(allt),2))
for i in range(len(allt)):
    allFt[i] = formuleBezier2(allt[i],A,B,C)
plt.figure()
plt.plot(allFt[:,0],allFt[:,1])

# Exercice 2

# Question 1
    #! F(A,B,C,D,t) = (1-t)**3*A + 3*t*(1 - t)**2*B + 3*t**2*(1 - t)*C + t**3*D

def formuleBezier3(t,A,B,C,D):
    return (1-t)**3*A + 3*t*(1 - t)**2*B + 3*t**2*(1 - t)*C + t**3*D

A, B, C, D = np.array([0,1]),np.array([3,3]),np.array([5,0]),np.array([7,1])
allt = np.linspace(0,1,100)
allFt = np.zeros((len(allt),2))
for i in range(len(allt)):
    allFt[i] = formuleBezier3(allt[i],A,B,C,D)
plt.figure()
plt.plot(allFt[:,0],allFt[:,1])

# Exercice 3

# Question 1
def binom(N,k):
    return np.math.factorial(N)/(np.math.factorial(k)*np.math.factorial(N-k))

# Question 2

def formuleBezierGenerale(t,P):
    Ft = np.zeros(2)
    for k in range(len(P)):
        Ft += P[k]*np.math.factorial(len(P)-1)/(np.math.factorial(k)*np.math.factorial(len(P)-1-k))*t**k*(1-t)**(len(P)-1-k)
    return Ft

P = [np.array([0,1]),np.array([3,3]),np.array([5,0]),np.array([7,1])]
allt = np.linspace(0,1,100)
allFt = np.zeros((len(allt),2))
for i in range(len(allt)):
    allFt[i] = formuleBezierGenerale(allt[i],P)
plt.figure()
plt.plot(allFt[:,0],allFt[:,1])


# Exercice 4

A,B = np.array([0,1]),np.array([3,3])
allt = np.linspace(0,1,100)


#! /* version non vectorisée */
#* allFt = np.zeros((len(allt),2))
#* for i in range(len(allt)):
    #* t = allt[i]
    #* allFt[i] = (1-t)*A + t*B

#! /* version vectorisée */
#* allFt = (1-allt[:,None])*As[None,:] + allt[:,None]*B[None,:]


# Question 1

from time import time
debut = time()
allFt = np.zeros((len(allt),2))
for i in range(len(allt)):
    t = allt[i]
    allFt[i] = (1-t)*A + t*B
fin = time()
duree = fin - debut
print("Durée de la boucle for pour la non-vectorisé : ",duree)

debut = time()
allFt = (1-allt[:,None])*A[None,:] + allt[:,None]*B[None,:]
fin = time()
duree = fin - debut
print("Durée de la boucle for pour la vectorisé : ",duree)

# c'est environ 26 fois plus rapide

# Question 2

# Question a
# shape = shape(allt)
# shape2 = (len(allt[:None]),2)
# shape3 = (len(B),2)
# shape4 = (len(B[None,:]),2)
# shape5 = (len(allt[:,None]*B[None,:]),2)

# print(shape(allt))
# print("shape2 = ",shape2)
# print("shape3 = ",shape3)
# print("shape4 = ",shape4)
# print("shape5 = ",shape5)


# Question b
# allFt = (1-allt)*A + allt*B

# Question 3
# def formuleBezierVectorisee(allt, P):
#     allFt = np.zeros((len(allt),2))
#     for k in range(len(P)):
#         allFt += P[k]*np.math.factorial(len(P)-1)/(np.math.factorial(k)*np.math.factorial(len(P)-1-k))*allt**k*(1-allt)**(len(P)-1-k)
#     return allFt

# P = [np.array([0,1]),np.array([3,3]),np.array([5,0]),np.array([7,1])]
# allt = np.linspace(0,1,100)

# debut = time()
# allFt = formuleBezierVectorisee(allt,P)
# fin = time()
# duree = fin - debut
# print("Durée de la formule vectorisée : ",duree)





stop_ici() # pour arrêter le script ici
