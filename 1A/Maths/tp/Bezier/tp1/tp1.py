import numpy as np
import matplotlib.pyplot as plt

# Exercice 1

def bezier1(t,A,B):
    Ft = (1-t)*A + t*B
    return Ft

plt.figure()
A = np.array([-1,3])
B = np.array([-3,2])
T = np.linspace(0,1,10)
Ft=[]
for t in T:
    Ft.append(bezier1(t,A,B))
Ft = np.array(Ft)
plt.plot(A[0],A[1],'r*')
plt.plot(B[0],B[1],'g*')
plt.plot(Ft[:,0],Ft[:,1],marker='x',color='blue')
# plt.show()

# Exercice 2

def bezier2(t,A,B,C):
    D = bezier1(t,A,B)
    E = bezier1(t,B,C)
    return (1-t)*D + t*E

plt.figure()
C = np.array([3,2])
T = np.linspace(0,1,10)
Ft=[]
for t in T:
    Ft.append(bezier2(t,A,B,C))
Ft = np.array(Ft)
plt.plot(A[0],A[1],'r*')
plt.plot(B[0],B[1],'g*')
plt.plot(C[0],C[1],'b*')
plt.plot(Ft[:,0],Ft[:,1],marker='x',color='blue')
plt.plot([A[0],B[0]],[A[1],B[1]],marker='x',color='red')
plt.plot([B[0],C[0]],[B[1],C[1]],marker='x',color='green')
# plt.show()

# Exercice 3

def bezier3(t,P0,P1,P2,P3):
    return (1-t)**3*P0 + 3*(1-t)**2*t*P1 + 3*(1-t)*t**2*P2 + t**3*P3

plt.figure()
P0 = np.array([-1,3])
P1 = np.array([-3,2])
P2 = np.array([3,2])
P3 = np.array([6,2.5])
Q0 = bezier1(0.5,P0,P1)
Q1 = bezier1(0.5,P1,P2)
Q2 = bezier1(0.5,P2,P3)
R0 = bezier1(0.5,Q0,Q1)
R1 = bezier1(0.5,Q1,Q2)
S = bezier1(0.5,R0,R1)
T = np.linspace(0,1,50)
Ft=[]
for t in T:
    Ft.append(bezier3(t,P0,P1,P2,P3))
Ft = np.array(Ft)
plt.plot(P0[0],P0[1],'r*')
plt.plot(P1[0],P1[1],'g*')
plt.plot(P2[0],P2[1],'b*')
plt.plot(P3[0],P3[1],'y*')
plt.plot(Ft[:,0],Ft[:,1],color='red')
plt.plot([P0[0],P1[0]],[P0[1],P1[1]],color='blue')
plt.plot([P0[0],P1[0]],[P0[1],P1[1]],color='black')
plt.plot([P1[0],P2[0]],[P1[1],P2[1]],color='black')
plt.plot([P2[0],P3[0]],[P2[1],P3[1]],color='black')
plt.plot([Q0[0],Q1[0]],[Q0[1],Q1[1]],color='blue')
plt.plot([Q1[0],Q2[0]],[Q1[1],Q2[1]],color='blue')
plt.plot([R0[0],R1[0]],[R0[1],R1[1]],color='green')
plt.plot([S[0]],[S[1]],marker='o',color='red')
plt.show()

# Exercice 4

plt.close('all')
plt.figure()
# Choix de 4 points de controle au hasard (P[0] est le premier point , P[1] le second, etc .)
P = np.random.rand(4,2)
T = np.linspace(0,1,20)
# Liste qui stockera les points F(t) déjà calcul és (afin de les réafficher à chaque it ération)
allFt_x = []
allFt_y = []
for t in T: # 20 valeurs équiréparties entre 0 et 1
    # Nettoie la figure
    plt.gcf().clear()
    # Calcul du nouveau point F(t)
    Ft = np.append(bezier3(t,P[0],P[1],P[2],P[3]),0)
    # Calcul et affichage de tous les polygones de contrôle interm édiaires
    Ft = np.array(Ft)
    # Stocke les coordonnées du nouveau point F(t)
    allFt_x.append(Ft[0])
    allFt_y.append(Ft[1])
    # Affiche l 'ensemble des points construits jusqu ' ici
    plt.plot(allFt_x,allFt_y,color='red',marker='o')
    # Pause (définit la vitesse de l ' animation)
    plt.pause(.2)
plt.show()

# Exercice 4 Mais valeur defini (non aleatoire)

plt.close('all')
plt.figure()
# Choix de 4 points de controle au hasar (P[0] est le premier point , P[1] le second, etc .)
P0 = np.array([0,0])
P1 = np.array([2,1])
P2 = np.array([0,1])
P3 = np.array([2,0])
T = np.linspace(0,1,20)
# Liste qui stockera les points F(t) déjà calcul és (afin de les réafficher à chaque it ération)
allFt_x = []
allFt_y = []
for t in T: # 20 valeurs équiréparties entre 0 et 1
    # Nettoie la figure
    plt.gcf().clear()
    # Calcul du nouveau point F(t)
    Ft = np.append(bezier3(t,P0,P1,P2,P3),0)
    # Calcul et affichage de tous les polygones de contrôle interm édiaires
    Ft = np.array(Ft)
    # Stocke les coordonnées du nouveau point F(t)
    allFt_x.append(Ft[0])
    allFt_y.append(Ft[1])
    # Affiche l 'ensemble des points construits jusqu ' ici
    plt.plot(allFt_x,allFt_y,color='red',marker='o')
    # Pause (définit la vitesse de l ' animation)
    plt.pause(.2)
plt.show()