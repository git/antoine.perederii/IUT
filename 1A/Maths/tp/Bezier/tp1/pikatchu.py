import numpy as np
import matplotlib.pyplot as plt

def bezier1(t,A,B):
    Ft = (1-t)*A + t*B
    return Ft

def bezier2(t,A,B,C):
    D = bezier1(t,A,B)
    E = bezier1(t,B,C)
    return (1-t)*D + t*E

def bezier3(t,P0,P1,P2,P3):
    return (1-t)**3*P0 + 3*(1-t)**2*t*P1 + 3*(1-t)*t**2*P2 + t**3*P3

plt.figure()
P0 = np.array([0,0])
P1 = np.array([7,0])
P2 = np.array([13,5])
P3 = np.array([10,-2])
P4 = np.array([12,-7])
P5 = np.array([9,-10])
P6 = np.array([11,-13])
P7 = np.array([11,-22])
P8 = np.array([10,-24])
P9 = np.array([12,-27])
P10 = np.array([10,-29])
P11 = np.array([8,-25])
P12 = np.array([-1,-25])
P13 = np.array([-3,-29])
P14 = np.array([-5,-27])
P15 = np.array([-4,-24])
P16 = np.array([-6,-22])
P17 = np.array([-6,-13])
P18 = np.array([-4,-10])
P19 = np.array([-6,-8])
P20 = np.array([-3,-2])
P21 = np.array([-6,4])

# T = np.linspace(-10,15,100)
# Ft = []
# Fo = []
# Fp = []
# Fs = []
# for t in T:
#     Ft = bezier2(t,P1,P2,P3)
#     Fo = bezier2(t,P3,P4,P5)
#     Fp = bezier3(t,P5,P6,P7,P8)
#     Fs = bezier3(t,P8,P9,P10,P11)
# F1=np.array(F1)
# F2=np.array(F2)
# F3=np.array(F3)
# F4=np.array(F4)
plt.plot([P0[0],P1[0]],[P0[1],P1[1]],color='black')
plt.plot([P1[0],P2[0]],[P1[1],P2[1]],color='black')
plt.plot([P2[0],P3[0]],[P2[1],P3[1]],color='black')
plt.plot([P3[0],P4[0]],[P3[1],P4[1]],color='black')
plt.plot([P4[0],P5[0]],[P4[1],P5[1]],color='black')
plt.plot([P5[0],P6[0]],[P5[1],P6[1]],color='black')
plt.plot([P6[0],P7[0]],[P6[1],P7[1]],color='black')
plt.plot([P7[0],P8[0]],[P7[1],P8[1]],color='black')
plt.plot([P8[0],P9[0]],[P8[1],P9[1]],color='black')
plt.plot([P9[0],P10[0]],[P9[1],P10[1]],color='black')
plt.plot([P10[0],P11[0]],[P10[1],P11[1]],color='black')
plt.plot([P11[0],P12[0]],[P11[1],P12[1]],color='black')
plt.plot([P12[0],P13[0]],[P12[1],P13[1]],color='black')
plt.plot([P13[0],P14[0]],[P13[1],P14[1]],color='black')
plt.plot([P14[0],P15[0]],[P14[1],P15[1]],color='black')
plt.plot([P15[0],P16[0]],[P15[1],P16[1]],color='black')
plt.plot([P16[0],P17[0]],[P16[1],P17[1]],color='black')
plt.plot([P17[0],P18[0]],[P17[1],P18[1]],color='black')
plt.plot([P18[0],P19[0]],[P18[1],P19[1]],color='black')
plt.plot([P19[0],P20[0]],[P19[1],P20[1]],color='black')
plt.plot([P20[0],P21[0]],[P20[1],P21[1]],color='black')
plt.plot([P21[0],P0[0]],[P21[1],P0[1]],color='black')
# plt.plot(F1[:,0],F1[:,1],color='red')
# plt.plot(F2[:,0],F2[:,1],color='red')
# plt.plot(F3[:,0],F3[:,1],color='red')
# plt.plot(F4[:,0],F4[:,1],color='red')
plt.show()