print("Exo 6")

def char2num(c):
	if c == ' ':
    		return 0
	else:
			return ord(c)-ord('A')+1

print(char2num('A'))
print(char2num('B'))
print(char2num('Z'))
print(char2num(' '))

print("=====================================")
print("Exo 6")

def num2char(n):
	if n == 0:
    			return ' '
	else:
			return chr(n+ord('A')-1)

print(num2char(1))
print(num2char(2))
print(num2char(0))

print("=====================================")
print("Exo 6")

def strlist(s):
    tab = [0]*len(s)
    for i in range(0, len(s)):
        tab[i]=char2num(s[i])
    return tab
  
print(strlist('ABC'))
print(strlist('ABC EFG'))
print(strlist('SALUT LES GARS')) 

print("=====================================")
print("Exo 6")

def strlist(L):
	tab = [0]*len(L)
	for i in range(0, len(L)):
		tab[i]=num2char(L[i])
	return tab

print(strlist([1,2,3]))
print(strlist([1,2,3,0,5,6,7]))
print(strlist([19,1,12,20,21,20,0,12,5,19,0,7,1,18,19]))

print("=====================================")
print("Exo 6")

def charnum(c):
	if c == ' ':
    		return 0
	else:
			return ((ord(c)-ord('A')+1)*7 + 19)

print(char2num('A'))
print(char2num('B'))
print(char2num('Z'))
print(char2num(' '))


print("=====================================")
print("Exo 7")

def strlis(s):
    tab = [0]*len(s)
    for i in range(0, len(s)):
        tab[i]=charnum(s[i])
    return tab

print(strlis('ABC'))
print(strlis('ABC EFG'))
print(strlis('SALUT LES GARS'))