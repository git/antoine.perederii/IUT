def evalue_clause(C, X):
    for clause in C: #passage de la liste C
        indice = abs(clause) - 1 #index = valeur absolue de C
        val = X[indice] #X = indice de X
        if clause < 0: #si la valeur et inferieur à 1 alors enlever 1 à 1
            val = 1 - val
        if val == 1: #Si la valeur et égale à 1 alors retourner 1
            return val
    return 0

print(evalue_clause([1], [1, 1, 0, 1])) #affiche la fonction evalue_clause(C, X) en remplacant C et X
print(evalue_clause([2, 3, 1], [1, 0, 1, 1])) #affiche le resultat total de cette ligne : 1 + 0 + 1 + 1 = 1
print(evalue_clause([1, -2, 4], [0, 1, 1, 0])) # 0 + 0 +  + 0 = 0 le 1 normal dc 0 l'inverse du 2 dc 0 et le 4 normal dc 0
print(evalue_clause([-2, -3, -4], [1, 0, 0, 1])) # 0 +  + 1 + 0 = 1 l'inverse du 2 dc 0 l'inverse du 3 dc 0 l'inverse du 4 dc 0
print(evalue_clause([-1, 3], [0, 0, 0, 0])) # 1 + 0 + 0 + 0 = 1