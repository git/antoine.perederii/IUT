fo = open("f_exemple.cnf", "r") # va chercher le fichier cnf
print ("Name of the file : ", fo.name) #affiche à l'utilisateur le fichier importé

in_data = fo.readlines() #permet de lire le fichier importé

cnf = list() #dire que le fichier cnf est une liste 
cnf.append(list()) #permet d'ajouter un element à la liste

for line in in_data: #
    tokens = line.split()
    if len(tokens) != 0 and tokens[0] not in ("c"):
        if tokens[0] == "p":
            print(tokens[0],tokens[1],tokens[2],tokens[3])
            nbvariables=int(tokens[2])
            nbclauses=int(tokens[3])
        else:
            for tok in tokens:
                lit = int(tok)
                if lit == 0:
                    cnf.append(list())
                else:
                    cnf[-1].append(lit)

assert len(cnf[-1]) == 0
cnf.pop()

print(nbvariables)
print(nbclauses)
print(cnf)
