N = 4

print('p cnf', N*N, 31)

# Lignes
for i in range(1, N*N, N):
    for p in range(i, i+N):
        for n in range(p+1, i+N):
            print('-', p, ' ', n)
    print('-', i, ' ', i+1, ' ', i+2, ' ', i+3, sep='', end=' 0 \n')
    print('-', i+1, ' ', i+2, ' ', i+3, sep='', end=' 0 \n')
    print('-', i+2, ' ', i+3, sep='', end=' 0 \n')

print('c =============')

# Colonnes
for i in range(1, N+1):
    print('-', i, ' ', i+N, ' ', i+2*N, ' ', i+3*N, sep='', end=' 0 \n')
    print('-', i+N, ' ', i+2*N, ' ', i+3*N, sep='', end=' 0 \n')
    print('-', i+2*N, ' ', i+3*N, sep='', end=' 0 \n')

print('c =============')

# Diagonales
print('-1', N+2, N*2+3, N*3+4)
print('-2', N+3, N*2+4)
print('-3', N+4)

# Une à chaque ligne
for l in range(1, N*4, 4):
    for i in range(N):
        print(l + i, end=' ')
    print('0')
