import math

#Ex1
def euclide(a,b):
	while b!=0:
		r = a%b
		a = b
		b = r
	print(a)
	return a

def etranger(a,b):
	if (euclide(a,b) == 1):
		print(a,' et ', b,' sont étrangers')
	else:
		print(a,' et ', b,' ne sont pas étrangers')

def euclide_etendu(a,b):
	r,ru,rv = a,1,0
	s,su,sv = b,0,1
	while s!=0:
		q = r//s
		new_s = r%s
		new_su = ru-su*q
		new_sv = rv-sv*q
		r,ru,rv = s,su,sv 
		s,su,sv = new_s,new_su,new_sv
	return r,ru,rv

def inverse_modulaire(a,b):
	r,u,v = euclide_etendu(a,b)
	if (r == 1):
		return u

def generateur(g,p):
	L = []
	for i in range (1, p):
		test=(g**i)%p
		if (test in L):
			return 0
		else:
			L.append(test)
		print(L)
	return 1

def exp_basique(x,n,p):
	res=(x**n)%p
	return res

def exp_rapide(x,n,p):
	if (n==1):
		return x%p
	else:
		if (n%2==0):
			return exp_rapide((x**2)%p,n/2,p)%p
		else:
			return (x*exp_rapide((x**2)%p,(n-1)/2,p))%p
	


u=exp_rapide(123456789,123456798,987654321)
print(u)








#def test():
#	a = 79
#	b = 22
#	if (euclide(a,b) == 1):
#		print(a,' et ', b,' sont étrangers')
#		print(euclide_etendu(a,b))
#	else:
#		print(a,' et ', b,' ne sont pas étrangers')

#test()
