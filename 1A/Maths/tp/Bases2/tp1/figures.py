import matplotlib.pyplot as plt
from math import cos, sin, tau

points = 10
frequence = tau / points

rosace = True

plt.figure()
X=[cos(n * frequence) for n in range(points + 1)]
Y=[sin(n * frequence) for n in range(points + 1)]

if rosace:
	for i in range(points):
		o = frequence 
	n = ((points // 2) - 1) 
	X.append(cos(n * frequence))
	Y.append(sin(n * frequence))


plt.plot(X,Y,color='blue')
plt.axis('equal')
plt.title('Cercle')
plt.show()
