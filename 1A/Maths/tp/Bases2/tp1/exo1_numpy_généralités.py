### Exercice : tableaux en Numpy (introduction)
###############################################

import numpy as np
from math import log, cos, sin

# Tous les passages indiqués "TODO()" sont à remplacer par vos soins
def TODO():
	print("à vous!")
	exit()

### DÉFINITION D'UN ARRAY NUMPY

print("Définition d'un array Numpy à partir d'une list :")
A = np.array([ -4, -32,  25, -15, -12, -23,   4,  -3, -17])       
print("A=",A)
print("len(A)=",len(A))

print("Définition d'un array Numpy avec la fonction arange:")
# 	np.arange(DEBUT,FIN,PAS)
# Fonctionne comme 'range', mais on peut utiliser des nombres non entiers
B = np.arange(0,1,0.05)
print("B=",B)
print("len(B)=",len(B))

print("Définition d'un array Numpy avec la fonction linspace:")
#	np.linspace(DEBUT,FIN,NOMBRE_DE_POINTS)
C = np.linspace(0,np.pi,20)
print("C=",C)
print("len(C)=",len(C))
# Remarque: linspace est généralement le choix le plus pertinent pour les graphiques (voir plus loin)

### MANIPULATIONS DE BASE : À VOUS !

print("Affichez un tableau contenant la somme de B et C (élément par élément):")
print(B + C)

# Remarque : notez bien la différence de fonctionnement comparé aux listes Python!
# Dans le cadre de Numpy, l'opérateur '+' reprend un sens mathématique d'addition.

print("Définissez un tableau D de 100 nombres (exactement) répartis uniformément de 1 (inclus) à 10 (inclus).")
D = np.linspace(1,10,100)
print("D=",D)
print("len(D)=",len(D))

print("Définissez un tableau E de 100 nombres, contenant le LOGARITHME DÉCIMAL de chaque valeur contenue dans D.")
E = np.log10(D)
print("E=",E)
print("len(E)=",len(E))

print("Définissez un tableau F de 100 nombres, contenant la formule \"cos(x)+2*sin(x)\" appliquée à chaque élément du tableau D.")
F = np.vectorize(lambda x: cos(x)+2*sin(x))(D)
F = np.cos(D) + 2 * np.sin(D)
print("F=",F)
print("len(F)=",len(F))

print("Trouvez la plus grande valeur contenue dans le tableau F:")
print(np.max(F))

print("Créez un tableau contenant la concaténation des tableaux A et B, sur une seule ligne:")
print("A=",A)
print("B=",B)
print(np.concatenate((A, B)))
# (internet autorisé, attention aux parenthèses!)

