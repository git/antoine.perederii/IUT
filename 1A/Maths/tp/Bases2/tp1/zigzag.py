import matplotlib.pyplot as plt

frequence = 14
hauteur = 0.2

n = frequence * 2 + 1

plt.figure()
X=[i/frequence for i in range(n)]
Y=[hauteur if (i % 2) == 1 else 0 for i in range(n)]
plt.plot(X,Y,color='blue')
plt.axis('equal')
plt.title('Zigzag')
plt.show()
