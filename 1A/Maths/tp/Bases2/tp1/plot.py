import matplotlib.pyplot as plt
from matplotlib import collections as mc

plt.figure()

lignes = [[(-1, 0.5), (0, 0)], [(0, -1), (0, 1)], [(0, 0), (1, 0)], [(-1, -2), (0, -1)], [(0, -1), (1, -2)]]
lc = mc.LineCollection(lignes, color='red', linewidths=1)
fig, ax = plt.subplots()
for a, b in lignes:
	ax.scatter(a[0], a[1], 100, color='red')
	ax.scatter(b[0], b[1], 100, color='red')
ax.add_collection(lc)
plt.axis('equal')
plt.title('Bonhomme')
plt.grid()
plt.show()

