### Exercice : indexation des tableaux en Numpy
###############################################

import numpy as np
from math import sqrt

# Tous les passages indiqués "TODO()" sont à remplacer par vos soins
def TODO():
	print("à vous!")
	exit()

# Définition d'un array Numpy
A = np.array([ -4, -32,  25, -15, -12, -23,   4,  -3, -17,  47, -40,  20,  11,
        -6,  44, -47,  42,  26,  -5,  46,  47,  34,  28,  47,  38, -42,
         5, -39, -28, 29])     
print("A=",A)
print("len(A)=",len(A)) 

### SLICING

# Le slicing permet d'accéder à une sous-partie d'une liste ou d'un tuple 
# (objets Python standards) ou d'un array (objet Numpy).
# Il prend la forme générale
#		DEBUT:FIN:PAS
# qui signifie : tous les éléments depuis l'indice DEBUT (inclus) jusqu'à 
#		l'indice FIN (exclus) en prenant un élément tous les PAS.

print("======= Exemple de slicing : A[3:14:2] ")
print(A[3:14:2])

# Si PAS est omis, il vaut 1 (on prend les éléments de 1 en 1)
print("Affichez tous les éléments de l'index 3 (inclus) à 14 (exclus).")
print(A[3:14])

# Si DEBUT est omis, il vaut 0 (on part depuis le début du tableau)
print("Affichez 1 élément sur 2, depuis le début et jusqu'à l'index 12 INCLUS")
print(A[:13:2])

# Si FIN est omis, on va jusqu'au bout du tableau
print("Affichez 1 élément sur 3, depuis l'index 4 (inclus) jusqu'à la fin.")
print(A[4::3])

# Si DEBUT et/ou FIN sont négatifs, ils indiquent une position en partant
# de la fin du tableau (-1 est le dernier élément, -2 l'avant-dernier, etc.)
print("Affichez les 10 derniers éléments de A.")
print(A[-10:])

print("Affichez 1 élément sur 3, en partant du début, et en vous arrêtant 10 éléments avant la fin du tableau.")
print(A[:-10:3])

# Si PAS est négatif, on lit les éléments dans le sens inverse.
print("Affichez le tableau A à l'envers, depuis son dernier élément jusqu'au premier.")
print(A[::-1])

print("Affichez les 10 derniers éléments de A, à l'envers.")
print(A[:-11:-1])


### INDEXATION PAR UNE LISTE D'INDICES 		(uniquement en Numpy)

# En Numpy, on peut utiliser une LISTE (ou un autre ARRAY) de nombres entiers positifs
# indiquant tous les emplacements qu'on souhaite lire dans le tableau.

print("======= Exemple d'indexation par une liste :")
print("A=",A)
print("len(A)=",len(A)) 
indices = [1,3,4,5,10]
print(indices)
print(A[indices])

print("Extrayez le sous-tableau de A donné par les indices qui sont un nombre au carré (0,1,4,9, etc.) :")
print(A[[n*n for n in range(int(sqrt(len(A))))]])


### INDEXATION PAR UN MASQUE BINAIRE 		(uniquement en Numpy)
   
# En Numpy, on peut indexer des éléments de A à l'aide d'un autre array, de la même
# taille que A, mais contenant des booléens indiquant si chaque index doit être 
# conservé (True) ou écarté (False)

print("======= Exemple d'indexation par masque binaire:")
print("A=",A)
print("len(A)=",len(A)) 
test = A>0
print(test)
print(A[test])

print("Créez un tableau B, de la même taille que A, dans lequel les nombres pairs sont conservés, mais les nombres impairs sont remplacés par des 0 :")
B = A.copy()
B[np.mod(B, 2) != 0] = 0
print(B)
#print(np.mod(B, where=test))
# Indice : utilisez la fonction np.mod

