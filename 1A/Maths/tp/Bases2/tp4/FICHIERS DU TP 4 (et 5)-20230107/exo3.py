
import numpy as np
np.set_printoptions(suppress=True)	# (pour mieux arrondir à 0 lors des print)
from numpy.polynomial.polynomial import polyval

import math

import matplotlib.pyplot as plt

# On active le "mode interactif" de pyplot. Ainsi, les plt.show() ne sont plus nécessaires.
plt.ion()						

# --------------------------------------------------------------------------------------
# Fonction de "fin temporaire" pendant l'avancée du TP. Attend une frappe Entrée, puis quitte le script.

def stop_ici():
	plt.pause(0.1)	# nécessaire pour que matplotlib ait le temps d'afficher les figures
	input()
	exit()

# --------------------------------------------------------------------------------------
# Tous les passages indiqués "TODO()" sont à remplacer par vos soins

def TODO():
	print("à vous!")
	stop_ici()


#################################################################################################
###
### EXERCICE 3 : Interpolation polynômiale en 2D
###
#################################################################################################


### Mise en jambes : afficher une courbe paramétrée polynomiale
###############################################################

# Rappel de la séance du TP2 :
#
# Une *courbe paramétrée polynômiale* est une courbe paramétrée
#		(P(t), Q(t))
# dont les 2 fonctions de coordonnées sont polynômiales.
#
# - La variable "t" s'appelle le **paramètre** de la courbe.
# - Chaque valeur de t définit UN point, dont l'abscisse vaut P(t) et l'ordonnée vaut Q(t).
# - La courbe est obtenue en représentant les points associés à TOUTES les valeurs de t.


# Cas particulier : Lorsque P et Q sont des polynômes de degré <=2, la courbe résultante est une parabole:
p = [1,1,1]
q = [-3,2,4]
t = np.linspace(-5,5,200)
Pt = polyval(t,p)
Qt = polyval(t,q)
plt.figure()
plt.plot(Pt,Qt)

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)


# Lorsque le degré est >=3, par contre, la courbe peut ressembler un peu à n'importe quoi :

print('''Représentez la courbe paramétrée polynômiale
	(P(t),Q(t))
associée aux polynômes
	P(X) = 1 -4X + 2X^2 + X^3
	Q(X) = 2 +7X + 2X^2 - X^4
pour le paramètre t dans l'intervalle [-2,2].''')

TODO()

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)


print('''Représentez la courbe paramétrée polynômiale
	(P(t),Q(t))
associée aux polynômes
	P(X) = 7 - 3X + X^3
	Q(X) = 12X -9X^2 + 2X^3
pour le paramètre t dans l'intervalle [-1,3].''')

TODO()

# Nota : Remarquez que cette courbe présente une particularité : un POINT DE REBROUSSEMENT.
# Cette situation survient lorsque les dérivées des 2 fonctions de coordonnées s'annulent *en même temps* : P'(t)=0 et Q'(t)=0.

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)


### Interpolation polynômiale en 2D
###################################

# On se donne un certain nombre de points cibles **en 2D**.
# Pour être plus lisible, on donne maintenant ces points comme des COUPLES successifs de la forme (x,y)

cibles = (1,3), (4,-1), (0,2)
N = len(cibles)					# (ici, N=3, mais votre code doit être robuste si cette valeur change)

print('''Fabriquez 
-une liste "x" contenant les ABSCISSES de tous les points cible,
-une liste "y" contenant les ORDONNÉES de tous les points cible.''')

# (suggestion, utilisez la syntaxe "spéciale python" x = [ ..... for c in cibles ] )

x = TODO()
y = TODO()
print("x=\n",x,"\ny=\n",y)

# On peut maintenant afficher les points cibles:
plt.figure()
plt.plot(x,y,linestyle='none',marker='o')

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)


# L'INTERPOLATION POLYNÔMIALE EN 2D consiste à fabriquer une courbe polynômiale
# 		(P(t),Q(t))
# qui **passe par tous les points cibles**, à certaines valeurs imposées du paramètre t.
# Dans notre cas, on décide que les valeurs imposées du paramètre sont 
# 		t=1, t=2, ..., t=N
# C'est-à-dire que nos polynômes P et Q doivent vérifier
#		t=1 -> point cible (x1,y1) ->  P(1)=x1  et  Q(1)=y1
#		t=2 -> point cible (x2,y2) ->  P(2)=x2  et  Q(2)=y2
#		...
#		t=N -> point cible (xN,yN) ->  P(N)=xN  et  Q(N)=yN

print('''Trouvez 
- l'unique polynôme P de degré N-1 qui interpole les N abscisses x, aux temps t=1,2,...,N.
- l'unique polynôme Q de degré N-1 qui interpole les N ordonnées y, aux temps t=1,2,...,N.
''')

# Indice : utilisez la fonction interpol définie à l'exercice 2

TODO()
p = TODO()
q = TODO()

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite ------------------

# --------------------------------------------------------------------------------------
print("-"*80)


print('''Dans la même figure que précédemment, tracez la courbe paramétrée
	(P(t),Q(t))
lorsque le paramètre t varie sur l'intervalle [1,N].
Vérifiez que vous interpolez correctement les 3 points cibles.
''')

# Remarque : ici, N=3 points cibles s'interpolent par des polynômes P et Q de degré 2.
# La courbe résultante est donc une parabole.

TODO()

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)


print('''Encapsulez votre code des questions précédentes dans une fonction, qui: 
- prend N points en entrée (sous la forme d'une liste "cibles" contenant les N couples (xi,yi) à interpoler)
- calcule l'unique courbe polynômiale interpolatrice de degré N-1 telle que
	P(1)=x1, Q(1)=y1
	...
	P(N)=xN, Q(N)=yN
- TRACE la courbe résultante, pour le pramètre t dans l'intervalle [1,N].
''')

# Remarque: votre fonction doit uniquement tracer la courbe résultante, pas marquer les points à interpoler (car en général on ne veut pas les voir).

def trace_interpol(cibles):
	N = TODO()
	TODO()	# (plusieurs lignes)
	plt.plot(TODO())


### Test de votre fonction :

# N points à interpoler (aux temps t=1,2,...,N)
cibles = (1,3),(4,-1),(0,2),(-1,-3),(0,0)

plt.figure()
# Représentation graphique des points...
plt.plot([c[0] for c in cibles],[c[1] for c in cibles],linestyle='None',marker='o')
# ...et de leur interpolation polynômiale :
trace_interpol(cibles)		# (la fonction que vous venez d'écrire)

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)	

print('''Modifiez votre fonction "trace_interpol" précédente avec l'astuce suivante:

def trace_interpol(cibles,**kwargs)
	......
	......
	plt.plot(....,**kwargs)
	
Cette astuce permet de passer à la fonction "trace interpol" n'importe quel argument clé/valeur (du genre linestyle='truc') et de le transmettre à la fonction plt.plot. Ainsi, vous pourrez par exemple appeler

trace_interpol(cibles, color='red', linestyle='dashed')
''')


### Test de votre fonction modifiée (sur les mêmes cibles que précédemment) :

plt.figure()
plt.plot([c[0] for c in cibles],[c[1] for c in cibles],linestyle='None',marker='o')
# Interpolation polynômiale avec le style modifiable :
trace_interpol(cibles, color='red',linestyle='dashed')

stop_ici() # -------------- Supprimez cette ligne pour passer à la suite --------------

# --------------------------------------------------------------------------------------
print("-"*80)


print('''Et voilà, la partie 1 est presque finie !

Votre fonction "trace_interpol" vous offre maintenant un moyen concret de tracer des courbes en 2D, en les "contrôlant" à l'aide de quelques points de passage dont vous spécifiez les coordonnées.
 
Pour terminer le TP, utilisez votre fonction "trace_interpol" et essayez de reproduire l'une des figures données dans le sujet pdf. (À moins que vous préfériez inventer votre propre dessin!)''')

plt.close('all')

TODO()

stop_ici()

