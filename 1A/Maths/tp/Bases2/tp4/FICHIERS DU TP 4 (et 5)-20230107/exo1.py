import numpy as np
np.set_printoptions(suppress=True)	# (pour mieux arrondir à 0 lors des print)

# --------------------------------------------------------------------------------------
# Fonction de "fin temporaire" pendant l'avancée du TP. Attend une frappe Entrée, puis quitte le script.

def stop_ici():
	input()
	exit()

# --------------------------------------------------------------------------------------
# Tous les passages indiqués "TODO()" sont à remplacer par vos soins

def TODO():
	print("à vous!")
	stop_ici()
	

#################################################################################################
###
### EXERCICE 1 : Résolution de systèmes linéaires avec Numpy
###
#################################################################################################


print("Bonjour et bonne année ! Pour ce TP vous aurez également besoin d'une feuille et d'un stylo.")
print("Merci à vous aussi !!! Ah, il va donc falloir que je le sorte de mon sac ?!?!? Vous vous rendez ompte de cet effort productif !!!")

# stop_ici()	# -------------- Supprimez cette ligne pour passer à la suite ------------------

# --------------------------------------------------------------------------------------
print("-"*80)

print("Veuillez s'il-vous-plaît compléter et signer le texte suivant.")

declaration = '''
Je déclare par la présente disposer d'une feuille et d'un stylo, et décharger mon enseignant de math de toute incompréhension relative à l'absence de l'un ou l'autre de ces deux objets. 

Fait à Aubière le XX/XX [dater] en un exemplaire, et signé par l'étudiant.e
XXX [signature]
'''

print(declaration)
	
# stop_ici()	# -------------- Supprimez cette ligne pour passer à la suite ------------------

# --------------------------------------------------------------------------------------
print("Merci, j'en prends bonne note :)")
print("-"*80)


# Numpy permet de résoudre des systèmes linéaires, par exemple en calculant des MATRICES INVERSES.

A = np.array( [ [1,-4,2] , [3,3,6] , [-2,-1,0] ] )
M = np.linalg.inv(A)		# nota : "linalg" signifie "algèbre linéaire" (le vrai nom de la théorie des matrices)
print("A=\n",A)
print("M=\n",M)

# Vérification :
print("A*M=\n",A.dot(M))
print("M*A=\n",M.dot(A))

# stop_ici()	# -------------- Supprimez cette ligne pour passer à la suite ------------------
	
# --------------------------------------------------------------------------------------
print("-"*80)


# Des agriculteurs conditionnent leurs légumes sous la forme de quatre "paniers" destinés à différentes préparations culinaires.
# Voici les contenus de chaque panier :

# Panier :     "Pot-au-feu" "Potée" "Boeuf bourguignon" "Chou farci"
# Patates (kg)       0         1              2               0  
# Chou (unités)      0        0.5             0               1
# Carottes (kg)      1         1              1               0
# Oignons (kg)      0.25      0.25           0.25            0.25

# Question : Les agriculteurs ont récolté 540 kg de patates, 220 choux, 340 kg de carottes et 130 kg d'oignons. Afin d'écouler toute leur production, combien doivent-ils préparer de panier de chaque type ?

print('''(Question papier) Montrez que le problème des agriculteurs constitue un système linéaire, de la forme
		A * v = b
où
    A est la matrice des coefficients
    b est le vecteur du second membre
    v est le vecteur des inconnues

Précisez le contenu de la matrice A et du vecteur b''')

# stop_ici()	# -------------- Supprimez cette ligne pour passer à la suite ------------------

# --------------------------------------------------------------------------------------
print("-"*80)

# On va à présent résoudre ce système avec Numpy.

print("Rentrez les paramètres du système linéaire (A et b), sous forme de np.array")
A = np.array( [ [0 , 1, 2, 0] , [0, 0.5, 0, 1] , [1, 1, 1, 0] , [0.25, 0.25, 0.25, 0.25]] )
b = np.array( [ [540] , [220] , [340] , [130]] )
print("A=\n",A)
print("b=\n",b.reshape(-1,1))

# stop_ici()	# -------------- Supprimez cette ligne pour passer à la suite ------------------
	
# --------------------------------------------------------------------------------------
print("-"*80)

print("Trouvez avec Numpy la solution du système.")

v = np.linalg.inv(A).dot(b) # .dot() est le produit matriciel
print("v=\n",v.reshape(-1,1))

# Vérification:
if not np.allclose (A.dot(v), b):
 	print('Erreur, v n''est la solution du système A*v=b')

# stop_ici()

print(f'''Conclusion : les agriculteurs doivent préparer:
{v[0]} paniers \"pot-au-feu\"
{v[1]} paniers \"potée\"
{v[2]} paniers \"boeuf bourguignon\"
{v[3]} paniers \"chou farci\"''')


# Voilà pour la mise en jambes !
# Pour la suite, retenez bien comment on résout un système linéaire avec Numpy.

