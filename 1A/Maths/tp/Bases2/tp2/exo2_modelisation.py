import matplotlib.pyplot as plt
import numpy as np
import math

# Tous les passages indiqués "TODO()" sont à remplacer par vos soins
def TODO():
	print("à vous!")
	exit()


#################################################################################################
### EXERCICE 2 : rentabilité d'un fabricant de processeurs
#################################################################################################

print("(1) Définissez un array numpy `N` contenant les entiers de 1 à 5000")
N = np.arange(1,5001)
# N = np.linspace(1,5000,1)
print(N)

# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(2) Construisez un array numpy `PN`, de la même taille que `N`, donnant le coût de production en fonction du nombre d'unités fabriquées")

PN = 2000 + 60 * (N)**(2/3)
print(PN)

# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(3) Construisez un array numpy `AN`, de la même taille que `N`, donnant le chiffre d'affaires, en fonction du nombre d'unités fabriquées (en supposant que toutes les unités fabriquées sont vendues)")
AN = N * 5
print(AN)

# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(4) Représentez la courbe P(N) en bleu, et la courbe A(N), en rouge, dans la même figure")
plt.figure()
plt.plot(N, PN, color='blue', label='coûts de production')
plt.plot(N, AN, color='red', label='chiffre d\'affaires')
plt.legend()		# (utilisera le label associé à chaque courbe au niveau des commandes `plot`)


# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(5) Rajoutez\n",
 "- une grille de coordonnées,\n",
 "- des labels *au nom approprié* sur l'axe des abscisses et l'axe des ordonnées.")
plt.grid()
plt.xlabel('coûts de production')
plt.ylabel('chiffre d\'affaires')
plt.show()

# -------------- Supprimez cette ligne pour passer à la suite ------------------
z = 0

for i in range(len(N)):
	if(PN[i]>AN[i]):
		z = N[i]

print("(6) Estimez le seuil de rentabilité pour le fabricant, à une unité près:")
print("Le seuil de rentabilité est approximativement de", z,"unités journalières")


