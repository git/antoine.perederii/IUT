import matplotlib.pyplot as plt
import numpy as np
from typing import Tuple

markers = np.arange(-3, 4, 0.001)
def droite(T: np.array, a: int, c: int, b: int, d: int) -> Tuple[Tuple[np.array, np.array], Tuple[np.array, np.array]]:
	marques = a + b * markers, c + d * markers
	points = a + b * T, c + d * T
	return points, marques

T = np.linspace(-3, 3, 200)

plt.figure()

#a = droite(T, 3, 1, 5, 3)
#plt.plot(*a[0], color='blue')
#plt.scatter(*a[1])
a = droite(T, -3, -1, 5, 3)
plt.plot(*a[0], color='blue')
a = droite(T, -5, 6, 3, -4)
plt.plot(*a[0], color='green')
a = droite(T, 0, 2, 2, -1)
plt.plot(*a[0], color='red')

plt.grid()

plt.xlim([-5, 5])
plt.ylim([-5, 5])
ax = plt.gca()
ax.set_xticks(range(-5, 6))
ax.set_yticks(range(-5, 6))

plt.show()
