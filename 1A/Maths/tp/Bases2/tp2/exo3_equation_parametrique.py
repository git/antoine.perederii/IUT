import matplotlib.pyplot as plt
import numpy as np
from typing import Tuple

T = np.linspace(-4, 4, 200)

a, b, c = (2, -1, 2)
d, e, f = (-3, -1, -1)

def parabole(T: np.array) -> Tuple[np.array, np.array]:
	XT = a + b * T + c * T * T
	YT = d + e * T + f * T * T
	return XT, YT

plt.figure()
plt.plot(*parabole(T))
plt.grid()

T = np.arange(-4, 5)
plt.scatter(*parabole(T))

plt.show()

