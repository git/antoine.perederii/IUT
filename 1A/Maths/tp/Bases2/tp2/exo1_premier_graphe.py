import matplotlib.pyplot as plt
import numpy as np
import math

# Tous les passages indiqués "TODO()" sont à remplacer par vos soins
def TODO():
	print("à vous!")
	exit()
	
	
#################################################################################################
### EXERCICE 1 : premier exemple de graphe
#################################################################################################


print("(1) Définissez un array numpy `x` contenant 100 nombres, allant de -5 (inclus) à 5 (inclus)")
x = np.linspace(-5,5,100)
print(x)
# VOCABULAIRE: On dit que `x` constitue une DISCRÉTISATION de l'intervalle [-5,5] en 100 nombres.

# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(2) Construisez un array numpy `y`, de la même taille que `x`, contenant l'exponentielle de chaque valeur contenue dans `x`")
y = np.exp(x)
print(y)
# (indice : utilisez les fonctions prédéfinies de Numpy.)

# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(3) Affichez le graphe représentant `x` en abscisses et `y` en ordonnées.")
plt.plot(x,y)


# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(4) Rajoutez une grille de coordonnées.")
plt.grid()

# -------------- Supprimez cette ligne pour passer à la suite ------------------

print("(5) Rajoutez un label \"x\" sur l'axe des abscisses, et \"exp(x)\" sur l'axe des ordonnées")
plt.xlabel("x")
plt.xlabel("exp(x)")
plt.show()

# (indice : cherchez les méthodes de matplotlib permettant de rajouter des labels sur les axes)

#################################################################################################
# Félicitations, vous venez de construire votre premier graphe de fonction sous Matplotlib !


