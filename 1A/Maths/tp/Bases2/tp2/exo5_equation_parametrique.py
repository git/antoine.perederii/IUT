import matplotlib.pyplot as plt
import numpy as np
from typing import Tuple

tau = np.pi * 2
T = np.linspace(0, tau, 16000)

def lissajous(n: int, m: int) -> Tuple[np.array, np.array]:
	C = np.cos(n * T)
	S = np.sin(m * T)
	return C, S

def lemniscate_bernouilli() -> Tuple[np.array, np.array]:
	X = np.sin(T) / (1 + np.cos(T) * np.cos(T))
	Y = np.sin(T) * np.cos(T) / (1 + np.cos(T) * np.cos(T))
	return X, Y

def cardioide() -> Tuple[np.array, np.array]:
	X = np.cos(T) * (1 + np.cos(T))
	Y = np.sin(T) * (1 + np.cos(T))
	return X, Y

plt.plot(*lissajous(5, 4), label='courbe de Lissajous') # équivalent à plt.plot(*lissajous(10, 8))
#plt.plot(*lissajous(7, 3))
#plt.plot(*lissajous(1, 1))
plt.plot(*lemniscate_bernouilli(), label='lemniscate de Bernoulli')
plt.plot(*cardioide(), label='cardioïde')
plt.grid()
plt.legend()
plt.show()

