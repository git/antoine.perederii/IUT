
import numpy as np
np.set_printoptions(suppress=True)	# (pour mieux arrondir à 0 lors des print)

import math

from numpy.polynomial.polynomial import polyval

import matplotlib.pyplot as plt

# On active le "mode interactif" de pyplot. Ainsi, les plt.show() ne sont plus nécessaires.
plt.ion()						

# --------------------------------------------------------------------------------------
# Fonction de "fin temporaire" pendant l'avancée du TP. Attend une frappe Entrée, puis quitte le script.

def stop_ici():
	plt.pause(0.1)	# nécessaire pour que matplotlib ait le temps d'afficher les figures
	input()
	exit()

# --------------------------------------------------------------------------------------
# Tous les passages indiqués "TODO()" sont à remplacer par vos soins

def TODO():
	print("à vous!")
	stop_ici()

# --------------------------------------------------------------------------------------
# Cette petite fonction sert à tracer les flèches avec une syntaxe un peu plus intuitive que le code matplotlib de base.
# - xdepart, ydepart : coordonnées du point d'attache de la flèche
# - xfleche, yfleche : longueurs de la flèche en x et en y

def myfleche(xdepart, ydepart, xfleche, yfleche):
	plt.annotate("", (xdepart+xfleche,ydepart+yfleche) , (xdepart,ydepart),
		arrowprops={'color':'red','shrink': 0,'width':0.02} )



#################################################################################################
###
### EXERCICE 2 : représenter graphiquement l'approximation affine d'une courbe paramétrée
###
#################################################################################################



#################################################################################
# 			 Rappel de la séance du TP2
#################################################################################
#
# Une *courbe paramétrée* est un ensemble de points de la forme
#		Point(t) = (f(t), g(t))
# lorsque la variable t parcourt un certain intervalle donné.
#
# - La variable "t" s'appelle le **paramètre** de la courbe.
# - Chaque valeur de t définit UN point, dont l'abscisse vaut f(t) et l'ordonnée vaut g(t).
# - La courbe est obtenue en représentant les points associés à TOUTES les valeurs de t.
#################################################################################



print('''Représentez la courbe paramétrée
	(f(t),g(t))
associée aux fonctions polynômiales
	f(t) = 1 -4.t + 2.t^2 + 2.t^3
	g(t) = 2 +7.t + t^2   - t^3  - t^4
pour le paramètre t dans l'intervalle [-2,2].''')

# Discrétisation de l'intervalle [-2,2] sur 200 points :
t = np.linspace(-2,2,200)
# Calcul de f(t), g(t) aux points correspondants :
ft = 1 -4*t + 2*t**2 + 2*t**3
gt = 2 +7*t + t**2   - t**3  - t**4
# Représentation de la courbe paramétrée :
plt.figure()
plt.plot(ft,gt)

# ou 
# t = np.linspace(-2,2,200) deja defini en haut
f=[1,-4,2,2]
g=[2,7,1,-1,-1]
ftt=polyval(t,f)
gtt=polyval(t,g)
plt.figure()
plt.plot(ftt,gtt)

stop_ici()  # ---------- Supprimez cette ligne une fois que le code précédent fonctionne ------------------

# --------------------------------------------------------------------------------------
print("-"*80)


#################################################################################
# 			 Approximation affine pour une courbe paramétrée
#################################################################################
#
# L'approximation affine de la courbe paramétrée au point t s'écrit
#
#	f(t+h) ~= f(t) + h.f'(t)		[en abscisses]
#	g(t+h) ~= g(t) + h.g'(t)		[en ordonnées]
#
# On va représenter cette approximation affine par une FLÈCHE.
#
# - La flèche démarre du point "t" sur le graphe
#		Point(t) = ( f(t) , g(t) )
#
# - La flèche a des longueurs (en abscisses et en ordonnées) de valeurs respectives
#		fleche = ( h.f'(t) , h.g'(t) )
#
# Avec ces notations, l'approximation affine ci-dessus peut se réécrire
#		Point(t+h) ~= Point(t) + fleche
#
# c'est-à-dire que "fleche" représente l'évolution (approximative) de la courbe 
# lorsque le point d'intérêt passe du paramètre "t" à "t+h".
#
# Graphiquement, "fleche" est tangente à la courbe au point "t", et sa longueur
# représente la quantité d'évolution de la courbe du point "t" au point "t+h".
#################################################################################


print('''(Question papier) Trouvez l'expression des fonctions dérivées f'(t) et g'(t) de la question précédente.''')

stop_ici()  # ---------- Supprimez cette ligne une fois que le code précédent fonctionne ------------------

# --------------------------------------------------------------------------------------
print("-"*80)

print('''Déduisez-en la valeur des 4 nombres f(1), g(1), f'(1), g'(1), et rentrez-les ci-dessous.''')

f1 = TODO()
g1 = TODO()
fprime1 = TODO()
gprime1 = TODO()

stop_ici()  # ---------- Supprimez cette ligne une fois que le code précédent fonctionne ------------------

# --------------------------------------------------------------------------------------
print("-"*80)

print('''Le code ci-dessous trace la flèche associée aux 4 valeurs que vous avez calculées à la question précédente.

Déterminez graphiquement si la flèche est bien à l'endroit et dans la direction où elle devrait.
Si la flèche n'est pas là où elle devrait, c'est que votre calcul de f(1),g(1),f'(1) ou g'(1) était faux. Dans ce cas, corrigez-le et relancez le code. ''')

plt.plot(f1,g1,marker='o',color='blue')        # marque le point de paramètre "t" avec un cercle
h = 0.5									       # écart "h" de l'approximation affine (fixe la proportionnalité gobale de la flèche)
myfleche(f1, g1, h*fprime1, h*gprime1)         # trace la flèche

stop_ici()  # ---------- Supprimez cette ligne une fois que le code précédent fonctionne ------------------

# --------------------------------------------------------------------------------------
print("-"*80)


print('''Calculez de même les approximations affines pour la courbe aux points
	t = -2, -1.5, -1, ...., 1.5, 2
et représentez-les par autant de flèches.''')

for t in TODO():
	TODO() # (plusieurs lignes)

print('''Vérification: toutes les flèches rouges sont-elles bien *tangentes* à la courbe ?
Si oui, passez à la suite.
Si non, qu'avez-vous oublié ?
''')


