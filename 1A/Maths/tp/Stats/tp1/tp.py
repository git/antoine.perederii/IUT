import numpy as np
import matplotlib.pyplot as plt

# dans python
import random
#générer un nombre aléatoire
random_number = random.random()
print(random_number)
#générer un entier aléatoire
random_int= random.randint(10,20) # doit etre compris entre 10 et 19 et le troisieme pour le nb qu'on veut
print(random_int)
# dans numpy
#générer un tableau de nombres aléatoires : example
random_tab= np.random.random([2,10])
print(random_tab.shape)

import random
Echantillon=random.sample(range(1, 50), 6)
print(Echantillon)

print("-------------------------------------------")

# Exo 1

# Generer une seule ligne de 0 et 1
Ligne1=np.random.randint(0,2,1)
print(Ligne1)

print("-------------------------------------------")

#Genere une seul colone de -1, 1
Colone1=np.random.randint(0,2,1)
Colone1[Colone1==0]=-1
print(Colone1)

print("-------------------------------------------")

# Générer une matrice (un tableau de deux dimensions) de taille de réels positifs et négatifs.
Matrice1=np.random.randint(0,2,[10,10])
Matrice1[Matrice1==0]=-1
print(Matrice1)

print("-------------------------------------------")

# générer deux matrices : A aléatoire de taille [n,m] et B de la même taille que A mais ne contient que 0 et 1. Multiplier A et B (élément par élément) et afficher le résultat
A=np.random.randint(0,2,[10,10])
A[A==0]=-1
print(A)
B=np.random.randint(0,2,[10,10])
print(B)
C=A*B
print(C)


# A l'aide de linspace, générer un tableau ax de 100 ponits començant à 0 et finissant à 1. Générer deux lignes aléatoires x et y de la même taille que ax. Dans une figure, afficher les points qui ont pour abscisse x et ordonnée y.
import numpy as np
import matplotlib.pyplot as plt
ax=np.linspace(0,1,100)
x=np.random.random(100)
y=np.random.random(100)
plt.plot(x,y,'o')
plt.show()


#Choix aléatoire avec python
from random import choice

Villes_européenens = ["Berlin", "Porto", "Madrid", "Amsterdam", "London", "Paris", "Rome", "Paris", "Barcelone", "Bruxelles", "Budapest", "Bucarest", "Lisbonne", "Stockholm", "Prague", "Vienne", "Dublin", "Milan", "Copenhague"]

print('Cette année direction : ',choice(Villes_européenens))

print("-------------------------------------------")

# Exo 2

def de(N):
    return np.random.randint(1,7,N)

T = 100000
y = 6
tirage=de(T)

x= len(tirage[tirage==y])
print("Taux d'apparition du nombre 6 sur le de : ", x/T * 100 ,"%")

print(len(tirage[tirage==y]))

print("-------------------------------------------")

# Exo 3


colone=np.random.randint(0,100,[10,1])
print(colone)
# 1
print("mean, moyenne : ", np.mean(colone))
print("median, mediane : ", np.median(colone))

print("-------------------------------------------")
# 2
print("moyenne : ", np.mean(colone))
print("variance : ", np.var(colone))
print("ecart type : ", np.std(colone))

print("-------------------------------------------")
# 3
matrice=np.random.rand(4,3)
print(matrice)
# print("moyenne : ", np.mean(matrice[10,1]))
# print("variance : ", np.var(matrice[10,1]))
# print("ecart type : ", np.std(matrice[10,1]))
print("moyenne : ", np.mean(matrice))
print("variance : ", np.var(matrice))
print("ecart type : ", np.std(matrice))

print("-------------------------------------------")

# Générer une matrice aléatoire de taille [4, 3]
matrice = np.random.rand(4, 3)

# Calculer la moyenne, la médiane et l'écart-type ligne par ligne
moyennes = np.mean(matrice, axis=1)
medianes = np.median(matrice, axis=1)
ecarts_types = np.std(matrice, axis=1)

# Afficher les résultats
print("Matrice aléatoire:\n", matrice)
print("Moyennes par ligne:", moyennes)
print("Médianes par ligne:", medianes)
print("Écart-types par ligne:", ecarts_types)

print("-------------------------------------------")


# Calculer la moyenne, la médiane et l'écart-type colonne par colonne
moyennes = np.mean(matrice, axis=0)
medianes = np.median(matrice, axis=0)
ecarts_types = np.std(matrice, axis=0)

# Afficher les résultats
print("Matrice aléatoire : ", matrice)
print("Moyennes par colonne : ", moyennes)
print("Médianes par colonne : ", medianes)
print("Écart-types par colonne : ", ecarts_types)

# Exo 4

# 1
def lancer_de():
    return np.random.randint(1, 6)

# 2
IN = [10, 100, 1000]

for n in IN:
    nb_3 = 0
    for i in range(n):
        if lancer_de() == 3:
            nb_3 += 1
    freq_3 = nb_3 / n
    print("Fréquence de 3 après", n, "lancers:", freq_3)

# 4
N = [10, 100, 1000]

for n in N:
    nb_k = 0
    for i in range(n):
        if lancer_de() < 4:
            nb_k += 1
    freq_k = nb_k / n
    print("Fréquence de k < 4 après", n, "lancers:", freq_k)

# 5
import matplotlib.pyplot as plt

probas = [1/6] * 6  # Probabilités théoriques

plt.bar(range(1, 7), probas)
plt.xlabel("Face du dé")
plt.ylabel("Probabilité")
plt.title("Probabilités d'obtenir chaque face du dé")
plt.show()

# 6
N = [10, 100, 1000]

for n in N:
    freq_faces = [0] * 6
    for i in range(n):
        face = lancer_de()
        freq_faces[face-1] += 1
    freq_faces = [f / n for f in freq_faces]
    print("Fréquences de chaque face après", n, "lancers:", freq_faces)


# 7
probas = [1/6] * 6  # Probabilités théoriques
cumul_theo = [sum(probas[:i+1]) for i in range(6)]  # Fonction de répartition théorique

plt.plot(range(1, 7), cumul_theo)
plt.xlabel("Face du dé")
plt.ylabel("Probabilité")
plt.title("Fonction de répartition théorique")
plt.show()


# Exo 5
# 3
line = np.random.randint(0, 11, size=N-1000)
print(line)

# 4
