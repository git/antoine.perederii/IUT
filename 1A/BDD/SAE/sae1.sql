BEGIN;

DROP TABLE IF EXISTS TRANSACTION;
DROP TABLE IF EXISTS COMPOSER;
DROP TABLE IF EXISTS APPARTENIR_CATEGORIE;
DROP TABLE IF EXISTS CLIENT;
DROP TABLE IF EXISTS PAYS;
DROP TABLE IF EXISTS PRODUIT;
DROP TABLE IF EXISTS FOURNISSEUR;
DROP TABLE IF EXISTS CATEGORIE;
DROP TABLE IF EXISTS COMPOSANT;
DROP TABLE IF EXISTS DANGER;

CREATE TABLE pays (
	iso CHAR(3) PRIMARY KEY,
	nom VARCHAR(50) NOT NULL,
	nom_complet VARCHAR(120) NOT NULL,
	code_continent CHAR(2) NOT NULL CHECK (code_continent IN ('AF', 'AN', 'AS', 'EU', 'NA', 'OC', 'SA')),
	UNIQUE (iso)
);

CREATE TABLE fournisseur (
	id CHAR(3) PRIMARY KEY,
	nom VARCHAR(30),
	email VARCHAR(30),
	tel CHAR(12),
	UNIQUE (nom)
);

CREATE TABLE categorie (
	id CHAR(3) PRIMARY KEY,
	nom VARCHAR(30),
	parent CHAR(3) REFERENCES categorie(id)
);

CREATE TABLE client (
	id CHAR(5) PRIMARY KEY,
	organisation VARCHAR(30),
	nom VARCHAR(30),
	prenom VARCHAR(30),
	adresse VARCHAR(50),
	code_postal VARCHAR(10),
	ville VARCHAR(30),
	iso_pays CHAR(3) REFERENCES pays(iso)
);

CREATE TABLE produit (
	id char(5) PRIMARY KEY,
	nom char (30) NOT NULL,
	prix numeric,
	fournisseur char(3) NOT NULL REFERENCES fournisseur(id),
	CHECK (prix > 0)
);

CREATE TABLE appartenir_categorie (
	id_produit CHAR(5) REFERENCES produit(id),
	id_categorie CHAR(3) REFERENCES categorie(id),
	PRIMARY KEY (id_categorie, id_produit)
);

CREATE TABLE transaction (
	id char(5) PRIMARY KEY,
	id_produit char(5) REFERENCES produit(id),
	id_client char(5) REFERENCES client(id),
	date_achat date NOT NULL,
	quantite numeric NOT NULL
);
CREATE TABLE danger (
	id char(5) PRIMARY KEY,
	name char(40) NOT NULL,
	info char (50),
	type char(10) CHECK (type IN ('physical', 'health', 'ecological'))
);
CREATE TABLE composant (
	id char(5) PRIMARY KEY,
	nom varchar(30),
	GHS char(8) REFERENCES danger(id)
);
CREATE TABLE composer (
	id_composant CHAR(5) REFERENCES composant(id),
	id_produit CHAR(5) REFERENCES produit(id),
	PRIMARY KEY(id_composant, id_produit)
);

----------------------------------------------------------------------------------------------------------------

INSERT INTO fournisseur VALUES('F01', 'BlueGrass', 'contact@bluegrass.inc', '330364198672');
INSERT INTO fournisseur VALUES('F02', 'MMMLab', 'contact@mmmlab.org', '370319735692');
INSERT INTO fournisseur VALUES('F03', 'Rasteur', 'contact@rasteur.com', '330378377659');
INSERT INTO fournisseur VALUES('F04', 'Big Pharma', 'contact@Big-Pharma.com', '330378478236');
INSERT INTO fournisseur VALUES('F05', 'Raoult inc', 'contact@Raoult-inc.com', '310563298659');
INSERT INTO fournisseur VALUES('F06', 'Amgen', 'contact@Amgen.com', '340345896359');

/*INSERT INTO client VALUES('C0001', 'Dauga', 'Nathanaël', '54 boulevard Victor Hugo', '63000', 'Clermont-Ferrand', 'France');
INSERT INTO client VALUES('C0002', 'Fréville', 'Clémence', '69 rue du général de Gaulle', '59000', 'Lille', 'France');
INSERT INTO client VALUES('C0003', 'Maiter-Fernandez', 'Mathis', 'Avenue de la Porte de la Chapelle', '75018', 'Paris', 'France');
INSERT INTO client VALUES('C0004', 'Thievet', 'Alberto', '18 Carrer de Balmes', '08007', 'Barcelone', 'Espagne');
INSERT INTO client VALUES('C0005', 'Radovanovic', 'Vladimir', 'Lyapunova Street','119333', 'Moscou', 'Russie');
INSERT INTO client VALUES('C0006', 'Novak', 'Aleksander', 'Nowogrodzka 24-26', '02-006','Varsovie', 'Pologne');
INSERT INTO client VALUES('C0007', 'Wittstock', 'Charlene', '8 Rue Suffren Reymond', '98000', 'Monaco', 'Monaco');
INSERT INTO client VALUES('C0008', 'Hofman', 'Angela', 'Limbecker Str. 35','44388','Dortmund', 'Allemagne');
INSERT INTO client VALUES('C0009', 'Bourbon', 'Louis', 'Rue des Cuirassiers', '69003','Lyon','France');
INSERT INTO client VALUES('C0010', 'Moretti', 'Armando', 'Interno Porto', '80133','Naples','Italie');
INSERT INTO client VALUES('C0011', 'Pas', 'Je commande', '96 Rue de labsence', '12005', 'Salisbury', 'Rhodésie');*/

--pays--
INSERT INTO pays VALUES('AND', 'Andorra', 'Principality of Andorra', 'EU');
INSERT INTO pays VALUES('ARE', 'United Arab Emirates', 'United Arab Emirates', 'AS');
INSERT INTO pays VALUES('AFG', 'Afghanistan', 'Islamic Emirate of Afghanistan', 'AS');
INSERT INTO pays VALUES('ATG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'NA');
INSERT INTO pays VALUES('AIA', 'Anguilla', 'Anguilla', 'NA');
INSERT INTO pays VALUES('ALB', 'Albania', 'Republic of Albania', 'EU');
INSERT INTO pays VALUES('ARM', 'Armenia', 'Republic of Armenia', 'AS');
INSERT INTO pays VALUES('ANT', 'Netherlands Antilles', 'Netherlands Antilles', 'NA');
INSERT INTO pays VALUES('AGO', 'Angola', 'Republic of Angola', 'AF');
INSERT INTO pays VALUES('ATA', 'Antarctica', 'Antarctica (the territory South of 60 deg S)', 'AN');
INSERT INTO pays VALUES('ARG', 'Argentina', 'Argentine Republic', 'SA');
INSERT INTO pays VALUES('ASM', 'American Samoa', 'American Samoa', 'OC');
INSERT INTO pays VALUES('AUT', 'Austria', 'Republic of Austria', 'EU');
INSERT INTO pays VALUES('AUS', 'Australia', 'Commonwealth of Australia', 'OC');
INSERT INTO pays VALUES('ABW', 'Aruba', 'Aruba', 'NA');
INSERT INTO pays VALUES('ALA', 'Åland', 'Åland Islands', 'EU');
INSERT INTO pays VALUES('AZE', 'Azerbaijan', 'Republic of Azerbaijan', 'AS');
INSERT INTO pays VALUES('BIH', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'EU');
INSERT INTO pays VALUES('BRB', 'Barbados', 'Barbados', 'NA');
INSERT INTO pays VALUES('BGD', 'Bangladesh', 'People''s Republic of Bangladesh', 'AS');
INSERT INTO pays VALUES('BEL', 'Belgium', 'Kingdom of Belgium', 'EU');
INSERT INTO pays VALUES('BFA', 'Burkina Faso', 'Burkina Faso', 'AF');
INSERT INTO pays VALUES('BGR', 'Bulgaria', 'Republic of Bulgaria', 'EU');
INSERT INTO pays VALUES('BHR', 'Bahrain', 'Kingdom of Bahrain', 'AS');
INSERT INTO pays VALUES('BDI', 'Burundi', 'Republic of Burundi', 'AF');
INSERT INTO pays VALUES('BEN', 'Benin', 'Republic of Benin', 'AF');
INSERT INTO pays VALUES('BLM', 'Saint Barthélemy', 'Saint Barthelemy', 'NA');
INSERT INTO pays VALUES('BMU', 'Bermuda', 'Bermuda', 'NA');
INSERT INTO pays VALUES('BRN', 'Brunei Darussalam', 'Brunei Darussalam', 'AS');
INSERT INTO pays VALUES('BOL', 'Bolivia', 'Republic of Bolivia', 'SA');
INSERT INTO pays VALUES('BRA', 'Brazil', 'Federative Republic of Brazil', 'SA');
INSERT INTO pays VALUES('BHS', 'Bahamas', 'Commonwealth of the Bahamas', 'NA');
INSERT INTO pays VALUES('BTN', 'Bhutan', 'Kingdom of Bhutan', 'AS');
INSERT INTO pays VALUES('BVT', 'Bouvet Island', 'Bouvet Island (Bouvetoya)', 'AN');
INSERT INTO pays VALUES('BWA', 'Botswana', 'Republic of Botswana', 'AF');
INSERT INTO pays VALUES('BLR', 'Belarus', 'Republic of Belarus', 'EU');
INSERT INTO pays VALUES('BLZ', 'Belize', 'Belize', 'NA');
INSERT INTO pays VALUES('CAN', 'Canada', 'Canada', 'NA');
INSERT INTO pays VALUES('CCK', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'AS');
INSERT INTO pays VALUES('COD', 'Congo (Kinshasa)', 'Democratic Republic of the Congo', 'AF');
INSERT INTO pays VALUES('CAF', 'Central African Republic', 'Central African Republic', 'AF');
INSERT INTO pays VALUES('COG', 'Congo (Brazzaville)', 'Republic of the Congo', 'AF');
INSERT INTO pays VALUES('CHE', 'Switzerland', 'Swiss Confederation', 'EU');
INSERT INTO pays VALUES('CIV', 'Côte d''Ivoire', 'Republic of Cote d''Ivoire', 'AF');
INSERT INTO pays VALUES('COK', 'Cook Islands', 'Cook Islands', 'OC');
INSERT INTO pays VALUES('CHL', 'Chile', 'Republic of Chile', 'SA');
INSERT INTO pays VALUES('CMR', 'Cameroon', 'Republic of Cameroon', 'AF');
INSERT INTO pays VALUES('CHN', 'China', 'People''s Republic of China', 'AS');
INSERT INTO pays VALUES('COL', 'Colombia', 'Republic of Colombia', 'SA');
INSERT INTO pays VALUES('CRI', 'Costa Rica', 'Republic of Costa Rica', 'NA');
INSERT INTO pays VALUES('CUB', 'Cuba', 'Republic of Cuba', 'NA');
INSERT INTO pays VALUES('CPV', 'Cape Verde', 'Republic of Cape Verde', 'AF');
INSERT INTO pays VALUES('CXR', 'Christmas Island', 'Christmas Island', 'AS');
INSERT INTO pays VALUES('CYP', 'Cyprus', 'Republic of Cyprus', 'AS');
INSERT INTO pays VALUES('CZE', 'Czech Republic', 'Czech Republic', 'EU');
INSERT INTO pays VALUES('DEU', 'Germany', 'Federal Republic of Germany', 'EU');
INSERT INTO pays VALUES('DJI', 'Djibouti', 'Republic of Djibouti', 'AF');
INSERT INTO pays VALUES('DNK', 'Denmark', 'Kingdom of Denmark', 'EU');
INSERT INTO pays VALUES('DMA', 'Dominica', 'Commonwealth of Dominica', 'NA');
INSERT INTO pays VALUES('DOM', 'Dominican Republic', 'Dominican Republic', 'NA');
INSERT INTO pays VALUES('DZA', 'Algeria', 'People''s Democratic Republic of Algeria', 'AF');
INSERT INTO pays VALUES('ECU', 'Ecuador', 'Republic of Ecuador', 'SA');
INSERT INTO pays VALUES('EST', 'Estonia', 'Republic of Estonia', 'EU');
INSERT INTO pays VALUES('EGY', 'Egypt', 'Arab Republic of Egypt', 'AF');
INSERT INTO pays VALUES('ESH', 'Western Sahara', 'Western Sahara', 'AF');
INSERT INTO pays VALUES('ERI', 'Eritrea', 'State of Eritrea', 'AF');
INSERT INTO pays VALUES('ESP', 'Spain', 'Kingdom of Spain', 'EU');
INSERT INTO pays VALUES('ETH', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'AF');
INSERT INTO pays VALUES('FIN', 'Finland', 'Republic of Finland', 'EU');
INSERT INTO pays VALUES('FJI', 'Fiji', 'Republic of the Fiji Islands', 'OC');
INSERT INTO pays VALUES('FLK', 'Falkland Islands', 'Falkland Islands (Malvinas)', 'SA');
INSERT INTO pays VALUES('FSM', 'Micronesia', 'Federated States of Micronesia', 'OC');
INSERT INTO pays VALUES('FRO', 'Faroe Islands', 'Faroe Islands', 'EU');
INSERT INTO pays VALUES('FRA', 'France', 'French Republic', 'EU');
INSERT INTO pays VALUES('GAB', 'Gabon', 'Gabonese Republic', 'AF');
INSERT INTO pays VALUES('GBR', 'United Kingdom', 'United Kingdom of Great Britain & Northern Ireland', 'EU');
INSERT INTO pays VALUES('GRD', 'Grenada', 'Grenada', 'NA');
INSERT INTO pays VALUES('GEO', 'Georgia', 'Georgia', 'AS');
INSERT INTO pays VALUES('GUF', 'French Guiana', 'French Guiana', 'SA');
INSERT INTO pays VALUES('GGY', 'Guernsey', 'Bailiwick of Guernsey', 'EU');
INSERT INTO pays VALUES('GHA', 'Ghana', 'Republic of Ghana', 'AF');
INSERT INTO pays VALUES('GIB', 'Gibraltar', 'Gibraltar', 'EU');
INSERT INTO pays VALUES('GRL', 'Greenland', 'Greenland', 'NA');
INSERT INTO pays VALUES('GMB', 'Gambia', 'Republic of the Gambia', 'AF');
INSERT INTO pays VALUES('GIN', 'Guinea', 'Republic of Guinea', 'AF');
INSERT INTO pays VALUES('GLP', 'Guadeloupe', 'Guadeloupe', 'NA');
INSERT INTO pays VALUES('GNQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'AF');
INSERT INTO pays VALUES('GRC', 'Greece', 'Hellenic Republic Greece', 'EU');
INSERT INTO pays VALUES('SGS', 'South Georgia and South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'AN');
INSERT INTO pays VALUES('GTM', 'Guatemala', 'Republic of Guatemala', 'NA');
INSERT INTO pays VALUES('GUM', 'Guam', 'Guam', 'OC');
INSERT INTO pays VALUES('GNB', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'AF');
INSERT INTO pays VALUES('GUY', 'Guyana', 'Co-operative Republic of Guyana', 'SA');
INSERT INTO pays VALUES('HKG', 'Hong Kong', 'Hong Kong Special Administrative Region of China', 'AS');
INSERT INTO pays VALUES('HMD', 'Heard and McDonald Islands', 'Heard Island and McDonald Islands', 'AN');
INSERT INTO pays VALUES('HND', 'Honduras', 'Republic of Honduras', 'NA');
INSERT INTO pays VALUES('HRV', 'Croatia', 'Republic of Croatia', 'EU');
INSERT INTO pays VALUES('HTI', 'Haiti', 'Republic of Haiti', 'NA');
INSERT INTO pays VALUES('HUN', 'Hungary', 'Republic of Hungary', 'EU');
INSERT INTO pays VALUES('IDN', 'Indonesia', 'Republic of Indonesia', 'AS');
INSERT INTO pays VALUES('IRL', 'Ireland', 'Ireland', 'EU');
INSERT INTO pays VALUES('ISR', 'Israel', 'State of Israel', 'AS');
INSERT INTO pays VALUES('IMN', 'Isle of Man', 'Isle of Man', 'EU');
INSERT INTO pays VALUES('IND', 'India', 'Republic of India', 'AS');
INSERT INTO pays VALUES('IOT', 'British Indian Ocean Territory', 'British Indian Ocean Territory (Chagos Archipelago)', 'AS');
INSERT INTO pays VALUES('IRQ', 'Iraq', 'Republic of Iraq', 'AS');
INSERT INTO pays VALUES('IRN', 'Iran', 'Islamic Republic of Iran', 'AS');
INSERT INTO pays VALUES('ISL', 'Iceland', 'Republic of Iceland', 'EU');
INSERT INTO pays VALUES('ITA', 'Italy', 'Italian Republic', 'EU');
INSERT INTO pays VALUES('JEY', 'Jersey', 'Bailiwick of Jersey', 'EU');
INSERT INTO pays VALUES('JAM', 'Jamaica', 'Jamaica', 'NA');
INSERT INTO pays VALUES('JOR', 'Jordan', 'Hashemite Kingdom of Jordan', 'AS');
INSERT INTO pays VALUES('JPN', 'Japan', 'Japan', 'AS');
INSERT INTO pays VALUES('KEN', 'Kenya', 'Republic of Kenya', 'AF');
INSERT INTO pays VALUES('KGZ', 'Kyrgyzstan', 'Kyrgyz Republic', 'AS');
INSERT INTO pays VALUES('KHM', 'Cambodia', 'Kingdom of Cambodia', 'AS');
INSERT INTO pays VALUES('KIR', 'Kiribati', 'Republic of Kiribati', 'OC');
INSERT INTO pays VALUES('COM', 'Comoros', 'Union of the Comoros', 'AF');
INSERT INTO pays VALUES('KNA', 'Saint Kitts and Nevis', 'Federation of Saint Kitts and Nevis', 'NA');
INSERT INTO pays VALUES('PRK', 'North Korea', 'Democratic People''s Republic of Korea', 'AS');
INSERT INTO pays VALUES('KOR', 'South Korea', 'Republic of Korea', 'AS');
INSERT INTO pays VALUES('KWT', 'Kuwait', 'State of Kuwait', 'AS');
INSERT INTO pays VALUES('CYM', 'Cayman Islands', 'Cayman Islands', 'NA');
INSERT INTO pays VALUES('KAZ', 'Kazakhstan', 'Republic of Kazakhstan', 'AS');
INSERT INTO pays VALUES('LAO', 'Laos', 'Lao People''s Democratic Republic', 'AS');
INSERT INTO pays VALUES('LBN', 'Lebanon', 'Lebanese Republic', 'AS');
INSERT INTO pays VALUES('LCA', 'Saint Lucia', 'Saint Lucia', 'NA');
INSERT INTO pays VALUES('LIE', 'Liechtenstein', 'Principality of Liechtenstein', 'EU');
INSERT INTO pays VALUES('LKA', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'AS');
INSERT INTO pays VALUES('LBR', 'Liberia', 'Republic of Liberia', 'AF');
INSERT INTO pays VALUES('LSO', 'Lesotho', 'Kingdom of Lesotho', 'AF');
INSERT INTO pays VALUES('LTU', 'Lithuania', 'Republic of Lithuania', 'EU');
INSERT INTO pays VALUES('LUX', 'Luxembourg', 'Grand Duchy of Luxembourg', 'EU');
INSERT INTO pays VALUES('LVA', 'Latvia', 'Republic of Latvia', 'EU');
INSERT INTO pays VALUES('LBY', 'Libya', 'Libyan Arab Jamahiriya', 'AF');
INSERT INTO pays VALUES('MAR', 'Morocco', 'Kingdom of Morocco', 'AF');
INSERT INTO pays VALUES('MCO', 'Monaco', 'Principality of Monaco', 'EU');
INSERT INTO pays VALUES('MDA', 'Moldova', 'Republic of Moldova', 'EU');
INSERT INTO pays VALUES('MNE', 'Montenegro', 'Republic of Montenegro', 'EU');
INSERT INTO pays VALUES('MAF', 'Saint Martin (French part)', 'Saint Martin', 'NA');
INSERT INTO pays VALUES('MDG', 'Madagascar', 'Republic of Madagascar', 'AF');
INSERT INTO pays VALUES('MHL', 'Marshall Islands', 'Republic of the Marshall Islands', 'OC');
INSERT INTO pays VALUES('MKD', 'Macedonia', 'Republic of Macedonia', 'EU');
INSERT INTO pays VALUES('MLI', 'Mali', 'Republic of Mali', 'AF');
INSERT INTO pays VALUES('MMR', 'Myanmar', 'Union of Myanmar', 'AS');
INSERT INTO pays VALUES('MNG', 'Mongolia', 'Mongolia', 'AS');
INSERT INTO pays VALUES('MAC', 'Macau', 'Macao Special Administrative Region of China', 'AS');
INSERT INTO pays VALUES('MNP', 'Northern Mariana Islands', 'Commonwealth of the Northern Mariana Islands', 'OC');
INSERT INTO pays VALUES('MTQ', 'Martinique', 'Martinique', 'NA');
INSERT INTO pays VALUES('MRT', 'Mauritania', 'Islamic Republic of Mauritania', 'AF');
INSERT INTO pays VALUES('MSR', 'Montserrat', 'Montserrat', 'NA');
INSERT INTO pays VALUES('MLT', 'Malta', 'Republic of Malta', 'EU');
INSERT INTO pays VALUES('MUS', 'Mauritius', 'Republic of Mauritius', 'AF');
INSERT INTO pays VALUES('MDV', 'Maldives', 'Republic of Maldives', 'AS');
INSERT INTO pays VALUES('MWI', 'Malawi', 'Republic of Malawi', 'AF');
INSERT INTO pays VALUES('MEX', 'Mexico', 'United Mexican States', 'NA');
INSERT INTO pays VALUES('MYS', 'Malaysia', 'Malaysia', 'AS');
INSERT INTO pays VALUES('MOZ', 'Mozambique', 'Republic of Mozambique', 'AF');
INSERT INTO pays VALUES('NAM', 'Namibia', 'Republic of Namibia', 'AF');
INSERT INTO pays VALUES('NCL', 'New Caledonia', 'New Caledonia', 'OC');
INSERT INTO pays VALUES('NER', 'Niger', 'Republic of Niger', 'AF');
INSERT INTO pays VALUES('NFK', 'Norfolk Island', 'Norfolk Island', 'OC');
INSERT INTO pays VALUES('NGA', 'Nigeria', 'Federal Republic of Nigeria', 'AF');
INSERT INTO pays VALUES('NIC', 'Nicaragua', 'Republic of Nicaragua', 'NA');
INSERT INTO pays VALUES('NLD', 'Netherlands', 'Kingdom of the Netherlands', 'EU');
INSERT INTO pays VALUES('NOR', 'Norway', 'Kingdom of Norway', 'EU');
INSERT INTO pays VALUES('NPL', 'Nepal', 'State of Nepal', 'AS');
INSERT INTO pays VALUES('NRU', 'Nauru', 'Republic of Nauru', 'OC');
INSERT INTO pays VALUES('NIU', 'Niue', 'Niue', 'OC');
INSERT INTO pays VALUES('NZL', 'New Zealand', 'New Zealand', 'OC');
INSERT INTO pays VALUES('OMN', 'Oman', 'Sultanate of Oman', 'AS');
INSERT INTO pays VALUES('PAN', 'Panama', 'Republic of Panama', 'NA');
INSERT INTO pays VALUES('PER', 'Peru', 'Republic of Peru', 'SA');
INSERT INTO pays VALUES('PYF', 'French Polynesia', 'French Polynesia', 'OC');
INSERT INTO pays VALUES('PNG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'OC');
INSERT INTO pays VALUES('PHL', 'Philippines', 'Republic of the Philippines', 'AS');
INSERT INTO pays VALUES('PAK', 'Pakistan', 'Islamic Republic of Pakistan', 'AS');
INSERT INTO pays VALUES('POL', 'Poland', 'Republic of Poland', 'EU');
INSERT INTO pays VALUES('SPM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'NA');
INSERT INTO pays VALUES('PCN', 'Pitcairn', 'Pitcairn Islands', 'OC');
INSERT INTO pays VALUES('PRI', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'NA');
INSERT INTO pays VALUES('PSE', 'Palestine', 'Occupied Palestinian Territory', 'AS');
INSERT INTO pays VALUES('PRT', 'Portugal', 'Portuguese Republic', 'EU');
INSERT INTO pays VALUES('PLW', 'Palau', 'Republic of Palau', 'OC');
INSERT INTO pays VALUES('PRY', 'Paraguay', 'Republic of Paraguay', 'SA');
INSERT INTO pays VALUES('QAT', 'Qatar', 'State of Qatar', 'AS');
INSERT INTO pays VALUES('REU', 'Reunion', 'Reunion', 'AF');
INSERT INTO pays VALUES('ROU', 'Romania', 'Romania', 'EU');
INSERT INTO pays VALUES('SRB', 'Serbia', 'Republic of Serbia', 'EU');
INSERT INTO pays VALUES('RUS', 'Russian Federation', 'Russian Federation', 'EU');
INSERT INTO pays VALUES('RWA', 'Rwanda', 'Republic of Rwanda', 'AF');
INSERT INTO pays VALUES('SAU', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'AS');
INSERT INTO pays VALUES('SLB', 'Solomon Islands', 'Solomon Islands', 'OC');
INSERT INTO pays VALUES('SYC', 'Seychelles', 'Republic of Seychelles', 'AF');
INSERT INTO pays VALUES('SDN', 'Sudan', 'Republic of Sudan', 'AF');
INSERT INTO pays VALUES('SWE', 'Sweden', 'Kingdom of Sweden', 'EU');
INSERT INTO pays VALUES('SGP', 'Singapore', 'Republic of Singapore', 'AS');
INSERT INTO pays VALUES('SHN', 'Saint Helena', 'Saint Helena', 'AF');
INSERT INTO pays VALUES('SVN', 'Slovenia', 'Republic of Slovenia', 'EU');
INSERT INTO pays VALUES('SJM', 'Svalbard and Jan Mayen Islands', 'Svalbard & Jan Mayen Islands', 'EU');
INSERT INTO pays VALUES('SVK', 'Slovakia', 'Slovakia (Slovak Republic)', 'EU');
INSERT INTO pays VALUES('SLE', 'Sierra Leone', 'Republic of Sierra Leone', 'AF');
INSERT INTO pays VALUES('SMR', 'San Marino', 'Republic of San Marino', 'EU');
INSERT INTO pays VALUES('SEN', 'Senegal', 'Republic of Senegal', 'AF');
INSERT INTO pays VALUES('SOM', 'Somalia', 'Somali Republic', 'AF');
INSERT INTO pays VALUES('SUR', 'Suriname', 'Republic of Suriname', 'SA');
INSERT INTO pays VALUES('STP', 'Sao Tome and Principe', 'Democratic Republic of Sao Tome and Principe', 'AF');
INSERT INTO pays VALUES('SLV', 'El Salvador', 'Republic of El Salvador', 'NA');
INSERT INTO pays VALUES('SYR', 'Syria', 'Syrian Arab Republic', 'AS');
INSERT INTO pays VALUES('SWZ', 'Swaziland', 'Kingdom of Swaziland', 'AF');
INSERT INTO pays VALUES('TCA', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'NA');
INSERT INTO pays VALUES('TCD', 'Chad', 'Republic of Chad', 'AF');
INSERT INTO pays VALUES('ATF', 'French Southern Lands', 'French Southern Territories', 'AN');
INSERT INTO pays VALUES('TGO', 'Togo', 'Togolese Republic', 'AF');
INSERT INTO pays VALUES('THA', 'Thailand', 'Kingdom of Thailand', 'AS');
INSERT INTO pays VALUES('TJK', 'Tajikistan', 'Republic of Tajikistan', 'AS');
INSERT INTO pays VALUES('TKL', 'Tokelau', 'Tokelau', 'OC');
INSERT INTO pays VALUES('TLS', 'Timor-Leste', 'Democratic Republic of Timor-Leste', 'AS');
INSERT INTO pays VALUES('TKM', 'Turkmenistan', 'Turkmenistan', 'AS');
INSERT INTO pays VALUES('TUN', 'Tunisia', 'Tunisian Republic', 'AF');
INSERT INTO pays VALUES('TON', 'Tonga', 'Kingdom of Tonga', 'OC');
INSERT INTO pays VALUES('TUR', 'Turkey', 'Republic of Turkey', 'AS');
INSERT INTO pays VALUES('TTO', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'NA');
INSERT INTO pays VALUES('TUV', 'Tuvalu', 'Tuvalu', 'OC');
INSERT INTO pays VALUES('TWN', 'Taiwan', 'Republic of China', 'AS');
INSERT INTO pays VALUES('TZA', 'Tanzania', 'United Republic of Tanzania', 'AF');
INSERT INTO pays VALUES('UKR', 'Ukraine', 'Ukraine', 'EU');
INSERT INTO pays VALUES('UGA', 'Uganda', 'Republic of Uganda', 'AF');
INSERT INTO pays VALUES('UMI', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'OC');
INSERT INTO pays VALUES('USA', 'United States of America', 'United States of America', 'NA');
INSERT INTO pays VALUES('URY', 'Uruguay', 'Eastern Republic of Uruguay', 'SA');
INSERT INTO pays VALUES('UZB', 'Uzbekistan', 'Republic of Uzbekistan', 'AS');
INSERT INTO pays VALUES('VAT', 'Vatican City', 'Holy See (Vatican City State)', 'EU');
INSERT INTO pays VALUES('VCT', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'NA');
INSERT INTO pays VALUES('VEN', 'Venezuela', 'Bolivarian Republic of Venezuela', 'SA');
INSERT INTO pays VALUES('VG', 'British Virgin Islands', 'Virgin Islands, British', 'NA');
INSERT INTO pays VALUES('VI', 'United States Virgin Islands', 'Virgin Islands, U.S.', 'NA');
INSERT INTO pays VALUES('VNM', 'Vietnam', 'Socialist Republic of Vietnam', 'AS');
INSERT INTO pays VALUES('VUT', 'Vanuatu', 'Republic of Vanuatu', 'OC');
INSERT INTO pays VALUES('WLF', 'Wallis and Futuna Islands', 'Wallis and Futuna', 'OC');
INSERT INTO pays VALUES('WSM', 'Samoa', 'Independent State of Samoa', 'OC');
INSERT INTO pays VALUES('YEM', 'Yemen', 'Yemen', 'AS');
INSERT INTO pays VALUES('MYT', 'Mayotte', 'Mayotte', 'AF');
INSERT INTO pays VALUES('ZAF', 'South Africa', 'Republic of South Africa', 'AF');
INSERT INTO pays VALUES('ZMB', 'Zambia', 'Republic of Zambia', 'AF');
INSERT INTO pays VALUES('ZWE', 'Zimbabwe', 'Republic of Zimbabwe', 'AF');

--pays--

INSERT INTO client VALUES('C0001', 'Cyan Vamp', 'Dauga', 'Nathanaël', '54 boulevard Victor Hugo', '63000', 'Clermont-Ferrand', 'FRA');
INSERT INTO client VALUES('C0002', 'Carré Science', 'Fréville', 'Clémence', '69 rue du général de Gaulle', '59000', 'Lille', 'FRA');
INSERT INTO client VALUES('C0003', 'Apoteck', 'Maiter-Fernandez', 'Mathis', 'Avenue de la Porte de la Chapelle', '75018', 'Paris',  'FRA');
INSERT INTO client VALUES('C0004', 'Ravimeid', 'Thievet', 'Alberto', '18 Carrer de Balmes', '08007', 'Barcelone', 'ESP');
INSERT INTO client VALUES('C0005', 'Léky', 'Radovanovic', 'Vladimir', 'Lyapunova Street','119333', 'Moscou', 'RUS');
INSERT INTO client VALUES('C0006', 'Choroba', 'Novak', 'Aleksander', 'Nowogrodzka 24-26', '02-006','Varsovie', 'POL');
INSERT INTO client VALUES('C0007', 'Aretina', 'Wittstock', 'Charlene', '8 Rue Suffren Reymond', '98000', 'Monaco', 'MCO');
INSERT INTO client VALUES('C0008', 'Krankheit', 'Hofman', 'Angela', 'Limbecker Str. 35','44388','Dortmund', 'DEU');
INSERT INTO client VALUES('C0009', 'Malakoff', 'Bourbon', 'Louis', 'Rue des Cuirassiers', '69003','Lyon', 'FRA');
INSERT INTO client VALUES('C0010', 'Medoca', 'Moretti', 'Armando', 'Interno Porto', '80133','Naples', 'ITA');
INSERT INTO client VALUES('C0011', 'ChemFiles', 'Pas', 'Je commande', '96 Rue de labsence', '12005', 'Salisbury', NULL);
INSERT INTO client VALUES('C0012', 'HealthAlert', 'Clinton', 'Donald', '699 W Ocean Blvd','90831', 'Long Beach', 'USA');
INSERT INTO client VALUES('C0013', 'Las Cinco Llagas', 'Da Silva', 'Neymar', '355 Rue Fernando de Azevedo','21920-430', ' Rio de Janeiro','BRA');
INSERT INTO client VALUES('C0014', 'Welcia', 'Midoriya', 'Izuku', ' Kojiyamachi, 2−6 １Ｆ 麹屋町ビル','850-0871','Nagasaki','JPN');
INSERT INTO client VALUES('C0015', 'Loxton Moppa', 'Sparrow', 'Jack', '54 Wellington Street', '3182', 'Melbourne','AUS');
INSERT INTO client VALUES('C0016', 'Selouane', 'Bismillah', 'Mohammed', '1 Rue Bani Marine', '40000', 'Marrakesh', 'MAR');
INSERT INTO client VALUES('C0017', 'CVS', 'Monroe', 'Marylin', '1301 5th Street Santa Monica','90401','Los Angeles', 'USA');
INSERT INTO client VALUES('C0018', 'Ohridski', 'Morreau', 'Charles', 'Artigas Base', NULL,NULL,'ATA');



INSERT INTO categorie VALUES('001', 'Métaux', NULL);
INSERT INTO categorie VALUES('002', 'Acides', NULL);
INSERT INTO categorie VALUES('003', 'Bicarbonates', '002');
INSERT INTO categorie VALUES('004', 'Sucres', NULL);
INSERT INTO categorie VALUES('005', 'Neurologie', NULL);
INSERT INTO categorie VALUES('006', 'Antimigraineux', '005');
INSERT INTO categorie VALUES('007', 'analgésique', NULL);
INSERT INTO categorie VALUES('008', 'antipaludique', NULL);

INSERT INTO produit VALUES('P0001', 'Bicarbonate de sodium', 10, 'F01');
INSERT INTO produit VALUES('P0002', 'Alliage de métaux', 20, 'F02');
--INSERT INTO produit VALUES('P0003', 'Sulfate de Cuivre', 20, 'F03');

INSERT INTO appartenir_categorie VALUES('P0001', '003');
INSERT INTO appartenir_categorie VALUES('P0002', '001');
--INSERT INTO appartenir_categorie VALUES('P0003', '002');

INSERT INTO produit VALUES('P0003', 'Fantanyl', 100, 'F04');
INSERT INTO appartenir_categorie VALUES('P0003', '007');

INSERT INTO produit VALUES('P0004', 'Phénytoïne', 60, 'F03');
INSERT INTO appartenir_categorie VALUES('P0004', '005');

INSERT INTO produit VALUES('P0005', 'Doliprane', 5, 'F06');
INSERT INTO appartenir_categorie VALUES('P0005', '007');

INSERT INTO produit VALUES('P0006', 'Advil', 15, 'F03');
INSERT INTO appartenir_categorie VALUES('P0006', '006');

INSERT INTO produit VALUES('P0007', 'Chloroquine', 120, 'F05');
INSERT INTO appartenir_categorie VALUES('P0007', '008');

INSERT INTO produit VALUES('P0008', 'Chocolat Therapeutique', 160, 'F02');
INSERT INTO appartenir_categorie VALUES('P0008', '004');

INSERT INTO produit VALUES('P0009', 'Toplexil', 20, 'F03');
INSERT INTO appartenir_categorie VALUES('P0009', '002');

INSERT INTO produit VALUES('P0010', 'Quercetine Anhydre', 60, 'F01');
INSERT INTO appartenir_categorie VALUES('P0010', '001');

INSERT INTO produit VALUES('P0011', 'Phénytoïne', 60, 'F03');
INSERT INTO appartenir_categorie VALUES('P0011', '005');

INSERT INTO transaction VALUES
    ('T0000', 'P0006', 'C0001', '2021-09-01', 268),
    ('T0001', 'P0003', 'C0012', '2021-09-04', 75),
    ('T0002', 'P0003', 'C0016', '2021-09-10', 27),
    ('T0003', 'P0005', 'C0018', '2021-09-11', 326),
    ('T0004', 'P0006', 'C0001', '2021-09-12', 303),
    ('T0005', 'P0003', 'C0018', '2021-09-12', 244),
    ('T0006', 'P0004', 'C0002', '2021-09-15', 29),
    ('T0007', 'P0004', 'C0016', '2021-09-15', 290),
    ('T0008', 'P0008', 'C0004', '2021-09-16', 393),
    ('T0009', 'P0007', 'C0016', '2021-09-18', 342),
    ('T0010', 'P0004', 'C0002', '2021-09-20', 263),
    ('T0011', 'P0006', 'C0007', '2021-09-24', 43),
    ('T0012', 'P0008', 'C0014', '2021-09-26', 120),
    ('T0013', 'P0005', 'C0017', '2021-09-27', 345),
    ('T0014', 'P0006', 'C0016', '2021-09-28', 357),
    ('T0015', 'P0001', 'C0014', '2021-09-28', 111),
    ('T0016', 'P0008', 'C0010', '2021-09-30', 137),
    ('T0017', 'P0008', 'C0013', '2021-09-30', 69),
    ('T0018', 'P0001', 'C0018', '2021-10-02', 104),
    ('T0019', 'P0006', 'C0009', '2021-10-03', 328),
    ('T0020', 'P0004', 'C0013', '2021-10-03', 335),
    ('T0021', 'P0009', 'C0002', '2021-10-04', 365),
    ('T0022', 'P0001', 'C0014', '2021-10-05', 304),
    ('T0023', 'P0005', 'C0016', '2021-10-06', 328),
    ('T0024', 'P0007', 'C0013', '2021-10-08', 189),
    ('T0025', 'P0001', 'C0001', '2021-10-11', 327),
    ('T0026', 'P0006', 'C0013', '2021-10-12', 230),
    ('T0028', 'P0004', 'C0001', '2021-10-16', 313),
    ('T0029', 'P0004', 'C0012', '2021-10-22', 55),
    ('T0030', 'P0005', 'C0005', '2021-10-24', 76),
    ('T0031', 'P0002', 'C0003', '2021-10-25', 346),
    ('T0032', 'P0006', 'C0004', '2021-10-26', 140),
    ('T0033', 'P0008', 'C0008', '2021-10-27', 373),
    ('T0034', 'P0002', 'C0017', '2021-10-28', 238),
    ('T0035', 'P0008', 'C0007', '2021-10-29', 27),
    ('T0036', 'P0008', 'C0018', '2021-10-30', 238),
    ('T0037', 'P0009', 'C0015', '2021-11-01', 70),
    ('T0038', 'P0007', 'C0018', '2021-11-09', 28),
    ('T0039', 'P0004', 'C0018', '2021-11-10', 127),
    ('T0040', 'P0004', 'C0006', '2021-11-15', 137),
    ('T0041', 'P0003', 'C0004', '2021-11-16', 139),
    ('T0042', 'P0002', 'C0003', '2021-11-16', 294),
    ('T0043', 'P0002', 'C0004', '2021-11-16', 35),
    ('T0044', 'P0004', 'C0017', '2021-11-16', 284),
    ('T0045', 'P0004', 'C0014', '2021-11-17', 216),
    ('T0046', 'P0002', 'C0004', '2021-11-18', 59),
    ('T0048', 'P0008', 'C0004', '2021-11-22', 165),
    ('T0049', 'P0001', 'C0001', '2021-11-24', 116),
    ('T0050', 'P0002', 'C0012', '2021-11-24', 275),
    ('T0051', 'P0004', 'C0008', '2021-11-26', 391),
    ('T0052', 'P0004', 'C0007', '2021-11-29', 390),
    ('T0053', 'P0004', 'C0004', '2021-11-30', 301),
    ('T0054', 'P0008', 'C0017', '2021-11-30', 127),
    ('T0055', 'P0004', 'C0014', '2021-12-01', 399),
    ('T0057', 'P0004', 'C0014', '2021-12-01', 316),
    ('T0058', 'P0001', 'C0008', '2021-12-02', 281),
    ('T0059', 'P0009', 'C0012', '2021-12-08', 95),
    ('T0060', 'P0007', 'C0016', '2021-12-10', 70),
    ('T0061', 'P0002', 'C0008', '2021-12-10', 21),
    ('T0062', 'P0002', 'C0009', '2021-12-10', 109),
    ('T0063', 'P0008', 'C0013', '2021-12-17', 250),
    ('T0064', 'P0003', 'C0008', '2021-12-18', 185),
    ('T0065', 'P0009', 'C0006', '2021-12-19', 361),
    ('T0066', 'P0006', 'C0009', '2021-12-21', 355),
    ('T0067', 'P0007', 'C0015', '2021-12-21', 118),
    ('T0068', 'P0008', 'C0008', '2021-12-25', 132),
    ('T0069', 'P0006', 'C0004', '2021-12-31', 8)
;

COMMIT;

-- Les produits qui n'ont jamais été commandés
SELECT p.id, p.nom
FROM produit p
LEFT JOIN transaction t ON p.id = t.id_produit
WHERE t.id_produit IS NULL;

SELECT id, nom
FROM produit
WHERE id NOT IN (
	SELECT DISTINCT id_produit
	FROM transaction
);

-- Compte le nombre de transactions la semaine dernière
SELECT COUNT(*) FROM transaction WHERE date_achat BETWEEN '2021-10-18'::date AND '2021-10-18'::date+7;
SELECT EXTRACT(YEAR FROM date_achat) annee, EXTRACT(WEEK FROM date_achat) semaine, COUNT(*) total, SUM(quantite) total_quantite FROM transaction t INNER JOIN produit p ON p.id = t.id_produit GROUP BY annee, semaine ORDER BY annee DESC, semaine DESC;

-- Les clients qui n'ont jamais commandé
SELECT client.id id_client_sans_commande, client.prenom, client.nom
FROM client
WHERE NOT EXISTS (
    SELECT *
    FROM transaction
    WHERE transaction.id_client = client.id
);

SELECT id id_client_sans_commande, nom, prenom
FROM client
WHERE id NOT IN (
	SELECT DISTINCT id_client
	FROM transaction
);

-- Ventes en France, Europe et hors Europe
SELECT SUM(t.quantite * p.prix) ventes_france FROM transaction t INNER JOIN client c ON t.id_client = c.id INNER JOIN produit p ON t.id_produit = p.id WHERE c.iso_pays = 'FRA';
SELECT SUM(t.quantite * p.prix) ventes_europe FROM transaction t INNER JOIN client c ON t.id_client = c.id INNER JOIN produit p ON t.id_produit = p.id INNER JOIN pays ON c.iso_pays = pays.iso WHERE pays.code_continent = 'EU';
SELECT SUM(t.quantite * p.prix) ventes_hors_eu FROM transaction t INNER JOIN client c ON t.id_client = c.id INNER JOIN produit p ON t.id_produit = p.id INNER JOIN pays ON c.iso_pays = pays.iso WHERE pays.code_continent != 'EU';

-- Ventes en Europe, version sans INNER JOIN
SELECT SUM(t.quantite*pr.prix) ventes_europe
FROM transaction t, client c,pays p,produit pr
Where t.id_client=c.id and c.iso_pays=p.iso and pr.id=t.id_produit
And code_continent like 'EU';


-- Ventes en hors Europe, version sans INNER JOIN
SELECT SUM(t.quantite*pr.prix) ventes_hors_eu
FROM transaction t, client c,pays p,produit pr
Where t.id_client=c.id and c.iso_pays=p.iso and pr.id=t.id_produit
And code_continent != 'EU';

