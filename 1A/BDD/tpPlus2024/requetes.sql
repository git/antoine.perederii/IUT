-- ? Q1). Lister les candidat(e)s (noms, prénoms) qui fêtent leur anniversaire ce mois-ci ? 
-- ?(Assurez-vous que la requête fonctionne, peu importe la date à laquelle elle est exécutée.)

SELECT nom, prenom FROM Candidat WHERE date_trunc('month', dateNaiss) - date_trunc('year', dateNaiss) = date_trunc('month', CURRENT_DATE) - date_trunc('year', CURRENT_DATE);


--? Q2). Pour chaque élection, afficher les dates du premier et du deuxième tour afin d’obtenir le format suivant :
--?        election       |               dates
--? ----------------------+-----------------------------------
--?  Présidentielles 2022 | 1er tour : 10/04, 2e tour : 24/04
--? Les élections devront être affichées par ordre chronologique inverse.

SELECT 'Présidentiels ' || annee, '1er tour : ' || to_char(t1.dateT, 'DD/MM') || ', 2e tour : ' || to_char(t1.dateT, 'DD/MM') AS dates
FROM Election, Tour t1, Tour t2
WHERE annee = t1.election AND t1.election = t2.election AND t1.tour = 1 AND t2.tour = 2
ORDER BY t1.dateT DESC;



--? Q3). Y a t’il déjà eu un tour d’élection qui s’est produit un jour autre que le dimanche?
--? Votre requête doit afficher la date à laquelle cela s’est produit le cas échéant, sinon elle devra ne retourner aucune ligne.
SELECT dateT 
FROM Tour 
WHERE to_char(dateT, 'FMDAY') != 'SUNDAY';


--? Q4). Lister les candidats (noms, prénoms) dont le parti contient le mot France ou Français(e).
SELECT c.nom, c.prenom
FROM Candidat c, SePresente s, Parti p
WHERE c.noCandidat = s.noCandidat AND s.parti = p.tag AND p.tag LIKE '%France%' OR p.tag LIKE '%Français%' OR p.tag LIKE '%Français%';


--? Q5). Pour chaque élection, quel département (code et nom) est celui ayant le plus petit nombre
--? d’inscrits sur les listes électorales ?

SELECT d.code, d.nom  
FROM Resultats r, Departement d, Tour t
WHERE r.dpt = d.code AND r.tour = t.dateT AND r.inscrits <= ALL(SELECT r1.inscrits
                                                                FROM Resultats r1  
                                                                WHERE r1.tour = t.dateT);


--? Q6). Y a t’il un écart dans le nombre d’inscrits sur les listes électorales entre le premier et le second tour d’une élection ? 
--? Afficher le résultat pour chaque élection.
SELECT 'tour 1 : ' || r1.inscrits, 'tour 2 : ' || r2.inscrits, t1.election
FROM Resultats r1, Tour t1, Resultats r2, tour t2
WHERE r1.tour = t1.dateT AND r2.tour = t2.dateT AND t1.election = t2.election;


--? Q7). Quel a été le plus haut score (en pourcentage de voix exprimées) réalisé lors d’un premier tour ? De quel(le) candidat(e) s’agit-il ?

SELECT r.votants/r.inscrits as pourcentage, c.nom, c.prenom
FROM Tour t, Resultats r, DetailVoix d, Candidat c
WHERE r.tour = t.dateT AND t.dateT = d.tour AND d.noCandidat = c.noCandidat AND t.tour = 1 AND r.votants >= ALL(SELECT r1.votants 
                                                                                                FROM Resultats r1
                                                                                                WHERE r1.tour = t.dateT);

-- Q8). Rajouter une colonne position à la table Parti qui peut prendre les valeurs Ga, Dr, EG, ED et Ce (pour Gauche, Droite, Extrême Gauche, Extrême Droite et Centre).
-- Rappel :
-- —
-- — —
-- ALTER TABLE <nom_table> ADD <nom_colonne> <type> : permet d’ajouter une colonne à une table
-- ALTER TABLE <nom_table> ADD CONSTRAINT <contrainte> : permet d’ajouter une contrainte ALTER TABLE <nom_table> ADD <nom_colonne> <type> <contrainte>:permetd’ajouterune colonne à une table avec une contrainte.

