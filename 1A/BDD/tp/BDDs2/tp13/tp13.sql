-- Question 1
SELECT * FROM PG_ROLES;

-- Question 2
CREATE TABLE Test_anperederii(id char(4), msg varchar(30));

INSERT INTO Test_anperederii VALUES('0001', 'Hello tout le monde');
INSERT INTO Test_anperederii VALUES('0002', 'Hello le monde');
INSERT INTO Test_anperederii VALUES('0003', 'Hello tous');
INSERT INTO Test_anperederii VALUES('0004', 'lo tout le monde');

-- Question 3
GRANT UPDATE, DELETE ON Test_anperederii TO kychabanon1;

-- Question 4
-- UPDATE TABLE Test_anperederii(id CHAR(4), msg varchar(30), nb numeric);
ALTER TABLE Test_anperederii ADD nb numeric;

-- ou UPDATE TABLE Test... ADD nb numeric; on a pas les privileges

-- Question 5


-- Question 6
GRANT SELECT ON Test_anperederii TO PUBLIC;

-- Question 7
INSERT INTO Test_anperederii VALUES('0004', 'lo tout le monde');
-- ca l'insert

-- Question 8
-- il faut qu'il me donne les droits

-- Question 9
REVOKE SELECT ON Test_anperederii FROM PUBLIC;




