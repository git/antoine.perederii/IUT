-- Question 1
SELECT d.*
FROM Discipline d, Epreuve e
WHERE d.code=e.discipl AND e.dateE < '2021-08-01' AND e.dateE > '2021-06-30'
GROUP BY d.code
HAVING count(*) >=ALL (SELECT count(*)
FROM Epreuve
WHERE dateE >= '2021-08-01' AND dateE <= '2021-08-31' AND d.code=discipl);

-- code |          nom          
-- ------+-----------------------
--  FEN  | Fencing
--  JUD  | Judo
--  TKW  | Taekwondo
--  GTR  | Trampoline Gymnastics
--  TRI  | Triathlon
--  TTE  | Table Tennis
--  CRD  | Cycling Road
--  SKB  | Skateboarding
--  SWM  | Swimming
--  BK3  | 3x3 Basketball
--  SRF  | Surfing
--  ROW  | Rowing
--  RUG  | Rugby Sevens
--  BSB  | Baseball/Softball
--  DIV  | Diving
--  SHO  | Shooting
--  BMX  | Cycling BMX Racing
--  CSL  | Canoe Slalom
--  ARC  | Archery
--  TEN  | Tennis
--  WLF  | Weightlifting


-- Question 2  3l
SELECT p.nom FROM Pays p WHERE (SELECT COUNT(*) FROM Athlete a WHERE a.pays = p.code) * 0.5 <= (SELECT COUNT(DISTINCT r.athlete) FROM Resultat r 
WHERE r.medaille IS NOT NULL AND r.athlete IN (SELECT a.code FROM Athlete a WHERE a.pays = p.code));
/*        nom         
--------------------
 Bermuda
 San Marino
 Fiji
 Russian Federation
 URSS
(5 lignes)
*/

20l
/*a faire sans left join*/
SELECT Pays.nom FROM Pays JOIN Athlete ON Athlete.pays = Pays.code JOIN Resultat ON Resultat.athlete = Athlete.code 
JOIN Medaille ON Medaille.place = Resultat.medaille AND Medaille.couleur = 'Or' 
LEFT JOIN Medaille MedailleArgent ON MedailleArgent.place = Resultat.medaille AND MedailleArgent.couleur = 'Argent'
LEFT JOIN Medaille MedailleBronze ON MedailleBronze.place = Resultat.medaille AND MedailleBronze.couleur = 'Bronze' GROUP BY Pays.nom
HAVING COUNT(Medaille.place) > COUNT(MedailleArgent.place) + COUNT(MedailleBronze.place);
/*           nom             
----------------------------
 Australia
 Austria
 Bahamas
 Belarus
 Belgium
 Bermuda
 Brazil
 Bulgaria
 Canada
 Chinese Taipei
 Croatia
 Cuba
 Czech Republic
 Denmark
 Ecuador
*/


SELECT a.nom, a.prenom FROM Athlete a WHERE a.sexe = 'F' AND a.taille > ALL ( SELECT a2.taille FROM Athlete a2 WHERE a2.sexe = 'M' AND a2.pays = a.pays);
/*Marche pas ou retourne rien*/

-- Question 5
SELECT a.nom, a.prenom, a.taille, t.max_taille FROM Athlete a JOIN (SELECT pays, MAX(taille) AS max_taille FROM Athlete WHERE sexe = 'M' AND pays = 'FRA' 
GROUP BY pays) t ON a.pays = t.pays WHERE a.sexe = 'M' AND a.pays = 'FRA';

--       nom        |     prenom      | taille | max_taille 
-- ------------------+-----------------+--------+------------
--  ABALO            | Luc             |   1.83 |       2.18
--  AIT SAID         | Samir           |   1.68 |       2.18
--  ALBICY           | Andrew          |   1.78 |       2.18
--  ALIEV            | Mourad          |        |       2.18
--  AMDOUNI          | Morhad          |        |       2.18
--  AMOROS           | Emile           |   1.78 |       2.18
--  ANDRE            | Sylvain         |        |       2.18
--  ANDRODIAS        | Matthieu        |   1.94 |       2.18
--  ANNE             | Mame-Ibra       |        |       2.18
--  APITHY           | Bolade          |        |       2.18
--  ATSU             | Jonathan        |        |       2.18
--  AUBRY            | David           |        |       2.18
--  AYACHE           | Alexandre       |        |       2.18
--  BAJIC            | Stefan          |   1.91 |       2.18
--  BARD             | Melvin          |   1.73 |       2.18
--  BART             | Adrien          |   1.84 |       2.18
--  BATUM            | Nicolas         |   2.03 |       2.18
--  BEAUMONT         | Maxime          |   1.90 |       2.18
--  BEDRANI          | Djilali         |        |       2.18
--  BEKA BEKA        | Alexis          |   1.75 |       2.18
--  BELAUD           | Valentin        |        |       2.18
--  BELOCIAN         | Wilhem          |        |       2.18
--  BENNAMA          | Billal          |   1.75 |       2.18
--  BERGERE          | Leo             |        |       2.18
--  BERNARDONI       | Paul            |   1.90 |       2.18
--  BERNAZ           | Jean Baptiste   |   1.90 |       2.18
--  BERTAUD          | Dimitry         |   1.80 |       2.18
--  BESSAGUET        | Clement         |   1.81 |       2.18
--  BEY              | Augustin        |        |       2.18
--  BIGOT            | Quentin         |        |       2.18
--  BILLOT           | Mathieu         |        |       2.18
--  BIRON            | Gilles          |   1.81 |       2.18
--  BORDIER          | Gabriel         |        |       2.18
--  BOREL            | Yannick         |        |       2.18
--  BOSSE            | Pierre-Ambroise |        |       2.18
--  BOUCHERON        | Hugo            |   1.95 |       2.18
--  BOUREZ           | Michel          |        |       2.18
--  BOYER            | Stephen         |   1.96 |       2.18
--  BRIZARD          | Antoine         |   1.96 |       2.18
--  BULTOR           | Daryl           |   1.97 |       2.18
--  BURGER           | Guillaume       |   1.81 |       2.18
--  BUSSIERE         | Theo            |        |       2.18
--  CACI             | Anthony         |   1.84 |       2.18


-- Question 6
SELECT e.code, e.discipl, e.nom, e.dateE, MIN(e2.dateE) AS datemin, MAX(e2.dateE) AS datemax FROM Epreuve e JOIN Epreuve e2 ON e.discipl = e2.discipl 
GROUP BY e.code, e.discipl, e.nom, e.dateE ORDER BY e.dateE;

-- code | discipl |                     nom                      |   datee    |  datemin   |  datemax   
-- ------+---------+----------------------------------------------+------------+------------+------------
--  E002 | CRD     | Men's Road Race                              | 2021-07-24 | 2021-07-24 | 2021-07-28
--  E004 | FEN     | Women's Epée Individual                      | 2021-07-24 | 2021-07-24 | 2021-08-01
--  E007 | SHO     | 10m Air Pistol Men                           | 2021-07-24 | 2021-07-24 | 2021-08-02
--  E009 | TKW     | Men -58kg                                    | 2021-07-24 | 2021-07-24 | 2021-07-27
--  E008 | SHO     | 10m Air Rifle Women                          | 2021-07-24 | 2021-07-24 | 2021-08-02
--  E003 | FEN     | Men's Sabre Individual                       | 2021-07-24 | 2021-07-24 | 2021-08-01
--  E011 | WLF     | Women's 49kg                                 | 2021-07-24 | 2021-07-24 | 2021-08-04
--  E010 | TKW     | Women -49kg                                  | 2021-07-24 | 2021-07-24 | 2021-07-27
--  E006 | JUD     | Women -48 kg                                 | 2021-07-24 | 2021-07-24 | 2021-07-31
--  E001 | ARC     | Mixed Team                                   | 2021-07-24 | 2021-07-24 | 2021-07-31
--  E005 | JUD     | Men -60 kg                                   | 2021-07-24 | 2021-07-24 | 2021-07-31
--  E025 | SWM     | Women's 400m Individual Medley               | 2021-07-25 | 2021-07-25 | 2021-08-01
--  E015 | FEN     | Men's Epée Individual                        | 2021-07-25 | 2021-07-24 | 2021-08-01
--  E026 | TKW     | Men -68kg                                    | 2021-07-25 | 2021-07-24 | 2021-07-27
--  E020 | SHO     | 10m Air Rifle Men                            | 2021-07-25 | 2021-07-24 | 2021-08-02
--  E023 | SWM     | Men's 400m Individual Medley                 | 2021-07-25 | 2021-07-25 | 2021-08-01
--  E028 | WLF     | Men's 61kg                                   | 2021-07-25 | 2021-07-24 | 2021-08-04
--  E014 | DIV     | Women's Synchronised 3m Springboard          | 2021-07-25 | 2021-07-25 | 2021-08-07
--  E029 | WLF     | Men's 67kg                                   | 2021-07-25 | 2021-07-24 | 2021-08-04
--  E016 | FEN     | Women's Foil Individual                      | 2021-07-25 | 2021-07-24 | 2021-08-01
--  E021 | SKB     | Men's Street                                 | 2021-07-25 | 2021-07-25 | 2021-08-05
--  E027 | TKW     | Women -57kg                                  | 2021-07-25 | 2021-07-24 | 2021-07-27
--  E013 | CRD     | Women's Road Race                            | 2021-07-25 | 2021-07-24 | 2021-07-28
--  E024 | SWM     | Women's 4 x 100m Freestyle Relay             | 2021-07-25 | 2021-07-25 | 2021-08-01
--  E017 | JUD     | Men -66 kg                                   | 2021-07-25 | 2021-07-24 | 2021-07-31
--  E022 | SWM     | Men's 400m Freestyle                         | 2021-07-25 | 2021-07-25 | 2021-08-01
--  E012 | ARC     | Women's Team                                 | 2021-07-25 | 2021-07-24 | 2021-07-31
--  E018 | JUD     | Women -52 kg                                 | 2021-07-25 | 2021-07-24 | 2021-07-31
--  E019 | SHO     | 10m Air Pistol Women                         | 2021-07-25 | 2021-07-24 | 2021-08-02
--  E044 | SWM     | Women's 100m Butterfly                       | 2021-07-26 | 2021-07-25 | 2021-08-01
--  E050 | WLF     | Women's 55kg                                 | 2021-07-26 | 2021-07-24 | 2021-08-04
--  E038 | JUD     | Women -57 kg                                 | 2021-07-26 | 2021-07-24 | 2021-07-31
--  E040 | SHO     | Skeet Women                                  | 2021-07-26 | 2021-07-24 | 2021-08-02
--  E031 | GAR     | Men's Team                                   | 2021-07-26 | 2021-07-26 | 2021-08-03
--  E039 | SHO     | Skeet Men                                    | 2021-07-26 | 2021-07-24 | 2021-08-02
--  E049 | TRI     | Men's Individual                             | 2021-07-26 | 2021-07-26 | 2021-07-31

-- Question 7
UPDATE Epreuve SET dateE = dateE + INTERVAL '7' DAY WHERE dateE = (SELECT MAX(dateE) FROM Epreuve);
/*UPDATE 13*/


-- Question 8
SELECT a.pays 
FROM athlete a, resultat r, epreuve e, discipline d
WHERE a.code = r.athlete AND e.code = r.epreuve AND d.code = e.discipl AND d.nom = 'Weightlifting'
GROUP BY a.pays;

-- Question 9
INSERT INTO Epreuve (code, discipl, nom, dateE)
VALUES ('E356', 'GLF', 'Women''s team', '2021-08-09');
/*INSERT 0 1*/
