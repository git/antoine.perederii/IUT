-- 1. Écrire une requête permettant de lister les codes de pays et le nombre de médailles d’or qu’ils ont remporté
SELECT a.pays code_pays,
    COUNT(DISTINCT r.epreuve) medailles_or
FROM resultat r
INNER JOIN athlete a ON a.code = r.athlete
WHERE r.medaille = (
    SELECT place FROM medaille WHERE couleur = 'Or'
)
GROUP BY a.pays;

-- 2. Transformer la requête précédente en vue nommée MedaillesOr.
CREATE VIEW MedaillesOr AS SELECT a.pays code_pays,
    COUNT(DISTINCT r.epreuve) medailles_or
FROM resultat r
INNER JOIN athlete a ON a.code = r.athlete
WHERE r.medaille = (
    SELECT place FROM medaille WHERE couleur = 'Or'
)
GROUP BY a.pays;

-- 3. Utiliser la vue créée pour trouver le nombre de médailles d’or remportées par la France.
SELECT medailles_or FROM MedaillesOr WHERE code_pays = 'FRA';

/*CREATE OR REPLACE FUNCTION create_view_medailles(couleur_medaille medaille.couleur%TYPE) RETURNS void AS $$
BEGIN
    -- TODO nom de la vue
    CREATE VIEW MedaillesArgent AS SELECT a.pays code_pays,
        COUNT(DISTINCT (r.epreuve, r.medaille)) medailles_or
    FROM resultat r
    INNER JOIN athlete a ON a.code = r.athlete
    WHERE r.medaille = (
        SELECT place FROM medaille WHERE couleur = couleur_medaille
    )
    GROUP BY a.pays;
END;
$$ LANGUAGE plpgsql;*/
CREATE VIEW MedaillesArgent AS SELECT a.pays code_pays,
    COUNT(DISTINCT r.epreuve) medailles_argent
FROM resultat r
INNER JOIN athlete a ON a.code = r.athlete
WHERE r.medaille = (
    SELECT place FROM medaille WHERE couleur = 'Argent'
)
GROUP BY a.pays;

CREATE VIEW MedaillesBronze AS SELECT a.pays code_pays,
    COUNT(DISTINCT r.epreuve) medailles_bronze
FROM resultat r
INNER JOIN athlete a ON a.code = r.athlete
WHERE r.medaille = (
    SELECT place FROM medaille WHERE couleur = 'Bronze'
)
GROUP BY a.pays;

-- 5. Créer une vue TableauMedailles listant le nombre de médailles d’or, d’argent, de bronze et le total.
CREATE VIEW TableauMedailles AS
SELECT p.nom pays,
    COALESCE(o.medailles_or, 0) medailles_or, COALESCE(a.medailles_argent, 0) medailles_argent, COALESCE(b.medailles_bronze, 0) medailles_bronze, 
    (o.medailles_or + a.medailles_argent + b.medailles_bronze) total_medailles
FROM pays p
LEFT JOIN MedaillesOr o ON o.code_pays = p.code
LEFT JOIN MedaillesArgent a ON a.code_pays = p.code
LEFT JOIN MedaillesBronze b ON b.code_pays = p.code
WHERE medailles_or IS NOT NULL
    OR medailles_argent IS NOT NULL
    OR medailles_bronze IS NOT NULL -- Retire les pays qui ont 0 médaille peu importe la couleur
ORDER BY 2 DESC, 3 DESC, 4 DESC;

-- 6. Afficher le tableau des médailles.
SELECT * FROM tableaumedailles;

-- 7. Un organe de presse souhaite faire son propre classement, en ordonnant les pays en fonction ...
SELECT * FROM tableaumedailles ORDER BY total_medailles DESC, pays;

-- 1. Écrire une requête SQL calculant le nombre d’athlètes pratiquants par discipline et par pays
SELECT p.discipl code_discipline, a.pays code_pays, COUNT(*) athletes FROM pratiquer p
INNER JOIN athlete a ON a.code = p.athlete
GROUP BY p.discipl, a.pays;

-- 2. Transformer la requête précédente en vue Pratiquants_v1
CREATE VIEW Pratiquants_v1 AS
SELECT p.discipl code_discipline, a.pays code_pays, COUNT(*) athletes FROM pratiquer p
INNER JOIN athlete a ON a.code = p.athlete
GROUP BY p.discipl, a.pays;

-- 3. Même chose pour Pratiquants_v2 mais ce sera une vue matérialisée.
CREATE MATERIALIZED VIEW Pratiquants_v2 AS
SELECT p.discipl code_discipline, a.pays code_pays, COUNT(*) athletes FROM pratiquer p
INNER JOIN athlete a ON a.code = p.athlete
GROUP BY p.discipl, a.pays;

--  5. Utiliser les vues créées précédemment pour afficher le nombre d’athlètes français pratiquant le judo
SELECT athletes francais_judokas FROM Pratiquants_v1
WHERE code_pays = 'FRA' AND code_discipline = 'JUD';

SELECT athletes francais_judokas FROM Pratiquants_v2
WHERE code_pays = 'FRA' AND code_discipline = 'JUD';

/*SELECT * FROM pratiquer p
INNER JOIN athlete a ON a.code = p.athlete
WHERE p.discipl = 'JUD' AND a.pays = 'FRA';*/

-- 6. Teddy RINER ne pratique plus le judo mais le skateboard (code ’SKB’). Faire les modifications nécessaires.
/*DELETE FROM pratiquer WHERE athlete = (
    SELECT code FROM athlete WHERE prenom = 'Teddy' AND nom = 'RINER'
) AND discipl = 'JUD';
INSERT INTO pratiquer VALUES (
    (
        SELECT code FROM athlete WHERE prenom = 'Teddy' AND nom = 'RINER'
    ),
    'SKB'
);*/
UPDATE pratiquer SET discipl = 'SKB' WHERE athlete = (
    SELECT code FROM athlete WHERE prenom = 'Teddy' AND nom = 'RINER'
) AND discipl = 'JUD';

-- 7 et 8.
-- Teddy RINNER est toujours référence dans la version 2 comme judoka puisque c'est une vue matérisalisée et qu'il faut la rafraîchir :
REFRESH MATERIALIZED VIEW Pratiquants_v2;
