DROP TABLE IF EXISTS Present;
DROP TABLE IF EXISTS Resultat_FR;
DROP TABLE IF EXISTS Resultat;
DROP TABLE IF EXISTS Medaille;
DROP TABLE IF EXISTS Epreuve;
DROP TABLE IF EXISTS Pratiquer;
DROP TABLE IF EXISTS Athlete;
DROP TABLE IF EXISTS Discipline;
DROP TABLE IF EXISTS Pays;

CREATE TABLE Pays(
       code char(3) PRIMARY KEY,
       nom  varchar(40) UNIQUE NOT NULL
);

CREATE TABLE Discipline(
       code char(3) PRIMARY KEY,
       nom  varchar(40) UNIQUE NOT NULL
);

CREATE TABLE Athlete(
       code      char(6) PRIMARY KEY,
       nom       varchar(40) NOT NULL,
       prenom    varchar(40) NOT NULL,
       sexe      char(1) CHECK(sexe IN('F','M')),
       dateNaiss date,
       paysNaiss char(3) REFERENCES Pays,
       pays      char(3) NOT NULL REFERENCES Pays,
       taille    numeric(3,2)
);


CREATE TABLE Pratiquer(
       athlete char(6) REFERENCES Athlete,
       discipl char(3) REFERENCES Discipline,
       PRIMARY KEY(athlete, discipl)
);

CREATE TABLE Epreuve(
       code    char(4) PRIMARY KEY,
       discipl char(3) NOT NULL REFERENCES Discipline,
       nom     varchar(55) NOT NULL,
       dateE   date NOT NULL
);


CREATE TABLE Medaille(
       place   numeric(1) PRIMARY KEY,
       couleur varchar(6)
);

CREATE TABLE Resultat(
       epreuve  char(4) REFERENCES Epreuve,
       athlete  char(6) REFERENCES Athlete,
       medaille numeric(1) REFERENCES Medaille,
       PRIMARY KEY(epreuve,athlete)
);

INSERT INTO Medaille VALUES(1,'Or');
INSERT INTO Medaille VALUES(2,'Argent');
INSERT INTO Medaille VALUES(3,'Bronze');


/*
disciplrel2, 0N Discipline, 11 Epreuve:
Discipline: code, nom
Pratiquer, 0N Athlete, 0N Discipline:
:

Epreuve: code, nom, dateE
Resultat, 0N Athlete, 0N Epreuve, 0N Medaille:
Athlete: code, nom, prenom, sexe, dateNaiss, taille
Resider, 0N Pays, 11 Athlete:

:
Medaille: place, couleur
Etre né, 0N Pays, 01 Athlete:
Pays: code, nom
*/
