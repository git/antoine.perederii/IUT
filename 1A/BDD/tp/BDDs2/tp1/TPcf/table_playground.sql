DROP TABLE IF EXISTS suivre;
DROP TABLE IF EXISTS louer;
DROP TABLE IF EXISTS instrument;
DROP TABLE IF EXISTS eleve;
DROP TABLE IF EXISTS professeur;
DROP TABLE IF EXISTS niveau;

CREATE TABLE niveau
(
	niveau SERIAL PRIMARY KEY,
	tarifHoraire INT NOT NULL
);

CREATE TABLE professeur
(
	num SERIAL PRIMARY KEY,
	nom VARCHAR(255) NOT NULL,
	prenom VARCHAR(255) NOT NULL,
	noTel CHAR(10) NOT NULL
);

CREATE TABLE eleve
(
	num SERIAL PRIMARY KEY,
	nom VARCHAR(255) NOT NULL,
	prenom VARCHAR(255) NOT NULL,
	age INT NOT NULL,
	rue VARCHAR(255) NOT NULL,
	codePostal VARCHAR(255) NOT NULL,
	ville VARCHAR(255) NOT NULL
);

CREATE TABLE instrument
(
	id SERIAL PRIMARY KEY,
	designation VARCHAR(255),
	tarifLocation INT NOT NULL,
	prof INT NOT NULL REFERENCES professeur (num)
);

CREATE TABLE suivre
(
	eleve INT NOT NULL REFERENCES eleve (num),
	instrument INT NOT NULL REFERENCES instrument (id),
	niveau INT NOT NULL REFERENCES niveau (niveau)
);

CREATE TABLE louer
(
	eleve INT NOT NULL REFERENCES eleve (num),
	instrument INT NOT NULL REFERENCES instrument (id)
);

