-- Recalcule les lignes avec des ventes totales incohérentes
UPDATE plain_jeu
SET ventes_total = ROUND((ventes_na + ventes_ue + ventes_jp + ventes_autre)::numeric, 2)
WHERE ABS(ventes_na + ventes_ue + ventes_jp + ventes_autre - ventes_total) > 0.1

SELECT annee_sortie, AVG(ventes_total) ventes_moyennes, SUM(ventes_total) ventes_totales FROM plain_jeu
WHERE genre = 'Adventure' AND annee_sortie IS NOT NULL
GROUP BY annee_sortie;

SELECT SUM(ventes_ue) FROM ventes
GROUP BY plateforme;

SELECT * FROM plain_jeu
WHERE nom = 'Mario Kart 64';

SELECT genre, SUM(ventes_total) total_ventes FROM plain_jeu
GROUP BY genre;

SELECT genre, ROUND((SUM(ventes_total) / (SELECT SUM(ventes_total) FROM plain_jeu)) * 100, 2) pourcentage_ventes FROM plain_jeu
GROUP BY genre;

SELECT CASE WHEN annee_sortie < 1990 THEN '< 1990'
            WHEN annee_sortie BETWEEN 1990 AND 1999 THEN '1990-1999' 
            WHEN annee_sortie BETWEEN 2000 AND 2009 THEN '2000-2009'
            WHEN annee_sortie >= 2010 THEN '>= 2010'
       END annee,
       genre,
       SUM(ventes_total) ventes_totales
FROM plain_jeu
WHERE annee_sortie IS NOT NULL
GROUP BY genre, annee
ORDER BY annee;

