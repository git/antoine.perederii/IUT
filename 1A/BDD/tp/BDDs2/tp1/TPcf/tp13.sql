-- 1
SELECT rolname FROM pg_roles;

-- 2
CREATE TABLE test_clfreville2
(
    id CHAR(4) PRIMARY KEY,
    msg VARCHAR(60)
);

INSERT INTO test_clfreville2 VALUES
('AAAA', 'Félicitations vous avez découvert le premier message'),
('BBBB', 'Saviez-vous que l''ornithorynque est un mammifère?'),
('CCCC', 'Un tiens vaut mieux que deux tu l''auras'),
('DDDD', 'Le boutisme (endianness) c''est le genre des entiers');

-- 3
--CREATE ROLE test_clfreville2_lodufour1 LOGIN
--VALID UNTIL '2022-05-09';
--ALTER TABLE test_clfreville2 OWNER TO test_clfreville2_lodufour1;

-- 4
ALTER TABLE test_clfreville2
ADD nb NUMERIC;

-- 5
ALTER TABLE test_lodufour1 OWNER TO lodufour1;

-- 6
GRANT SELECT ON test_clfreville2 TO public;

--SELECT * FROM dblink('dbname=dblodufour1', 'SELECT * FROM test_lodufour1');

-- 7
INSERT INTO test_lodufour1 VALUES ('DMRT', 'Jeu de l''oie');

-- 8
REVOKE SELECT ON test_clfreville2 FROM public;

-- Connexion à une autre db depuis une connexion courante :
GRANT CONNECT ON DATABASE dbclfreville2 TO lodufour1; -- Autorise la connexion
GRANT INSERT ON test_clfreville2 TO lodufour1; -- Autorise l'insertion à un utilisateur
\connect dblodufour1
SELECT * FROM test_lodufour1;
