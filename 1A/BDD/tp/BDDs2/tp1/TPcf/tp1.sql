-- Combien d’athlètes y a t’il ?
SELECT COUNT(*) FROM athlete;

-- Afficher les épreuves ayant eu lieu le 29 juillet 2021.
SELECT * FROM epreuve WHERE dateE = '2021-07-29';

-- Afficher le nom des disciplines ayant une épreuve le 29 juillet 2021.
SELECT DISTINCT e.nom FROM epreuve e
INNER JOIN discipline d ON e.discipl = d.code 
WHERE dateE = '2021-07-29';

SELECT nom, prenom, dateNaiss FROM athlete
WHERE pays = (SELECT code FROM pays WHERE nom = 'France')
ORDER BY nom, prenom;

SELECT COUNT(*) medailles FROM resultat
WHERE athlete = (
	SELECT code FROM athlete
	WHERE nom = 'LEDECKY'
	AND prenom = 'Kathleen'
);

SELECT a.nom nom_vainqueure, a.prenom prenom_vainqueure, p.nom pays_vainqueur, e.nom epreuve, d.nom discipline
FROM epreuve e
INNER JOIN discipline d ON d.code = e.discipl
INNER JOIN resultat r ON r.epreuve = e.code
INNER JOIN athlete a ON a.code = r.athlete
INNER JOIN pays p ON p.code = a.pays
WHERE e.code = 'E059'
	AND medaille = (SELECT place FROM medaille WHERE couleur = 'Or');

-- Qui est l’athlète le ou la plus jeune, parmi ceux dont on connaît la date de naissance ?
SELECT * FROM athlete
WHERE dateNaiss = (
	SELECT MAX(dateNaiss) FROM athlete
	WHERE dateNaiss IS NOT NULL
);

-- Qui est l’athlète le ou la plus agée et quel était son âge à l’ouverture des jeux (le 23 juillet2021) ?
SELECT *, ROUND(('2021-07-23'-dateNaiss) / 365.2422, 1) age FROM athlete
WHERE dateNaiss = (
	SELECT MIN(dateNaiss) FROM athlete
	WHERE dateNaiss IS NOT NULL
);

-- Lister les pays dont le nom commence par ’A’ et qui n’ont remporté aucune médaille.
SELECT * FROM pays
WHERE code NOT IN (
	SELECT a.pays FROM resultat r
	INNER JOIN athlete a ON a.code = r.athlete
) AND nom LIKE 'A%';

