-- https://www.kaggle.com/datasets/ngshiheng/michelin-guide-restaurants-2021
-- https://docs.openexchangerates.org/docs/currencies-json

DROP TABLE IF EXISTS cuisine_restaurant;
DROP TABLE IF EXISTS restaurant;
DROP TABLE IF EXISTS currency;
DROP TABLE IF EXISTS cuisine;

CREATE TABLE IF NOT EXISTS currency (
    iso CHAR(3) PRIMARY KEY, -- ISO 4217
    full_name VARCHAR(40) NOT NULL,
    UNIQUE(full_name)
);

CREATE TABLE IF NOT EXISTS cuisine (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30),
    UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS restaurant (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    location VARCHAR(150) NOT NULL,
    min_price NUMERIC(9, 2) NOT NULL,
    max_price NUMERIC(9, 2) NOT NULL,
    currency CHAR(3) REFERENCES currency(iso),
    latitude FLOAT,
    longitude FLOAT,
    phone_number VARCHAR(16),
    url VARCHAR(255) NOT NULL,
    website_url VARCHAR(255) NOT NULL,
    stars INT NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS cuisine_restaurant (
    restaurant INT REFERENCES restaurant(id),
    cuisine INT REFERENCES cuisine(id),
    PRIMARY KEY(restaurant, cuisine)
);