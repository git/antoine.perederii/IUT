SELECT c.iso, c.full_name, COUNT(*) restaurants
FROM restaurant r
INNER JOIN currency c ON c.iso = r.currency
GROUP BY c.iso
ORDER BY 3 DESC
LIMIT 10;

SELECT min_price
FROM restaurant
WHERE currency = 'EUR'
ORDER BY min_price
GROUP BY '[3,7)'::int4range;

/*WITH series AS (
    SELECT generate_series(
        (SELECT MIN(min_price) FROM restaurant),
        (SELECT MAX(min_price) FROM restaurant WHERE min_price != 'NaN'::NUMERIC),
        10
    ) low_bound_price
)
SELECT '[' || s.low_bound_price || ',' || (s.low_bound_price+10) || '[', COUNT(*) restaurants
FROM series s
LEFT JOIN restaurant r
    ON r.min_price >= s.low_bound_price
    AND r.min_price < (s.low_bound_price + 10)
GROUP BY low_bound_price;*/

-- COUNT(*) FILTER ?
SELECT CASE WHEN min_price < 5 THEN '< 5'
            WHEN min_price >= 5 AND min_price < 10 THEN '[5;10[' 
            WHEN min_price >= 10 AND min_price < 15 THEN '[10;15[' 
            WHEN min_price >= 15 AND min_price < 20 THEN '[15;20[' 
            WHEN min_price >= 20 AND min_price < 30 THEN '[20;30[' 
            WHEN min_price >= 30 AND min_price < 50 THEN '[30;50[' 
            WHEN min_price >= 50 AND min_price < 100 THEN '[50;100[' 
            WHEN min_price >= 100 AND min_price < 200 THEN '[100;200[' 
            WHEN min_price >= 200 AND min_price < 500 THEN '[200;500[' 
            WHEN min_price >= 500 AND min_price < 1000 THEN '[500;1000[' 
            WHEN min_price >= 1000 THEN '>= 1000' 
        END min_price_range,
    COUNT(*) nb
FROM restaurant
GROUP BY min_price_range;
