#!/bin/bash

sed -r 's/: psycopg2\.(connection|cursor)//' load.py > /tmp/patched_load.py
python3 /tmp/patched_load.py
