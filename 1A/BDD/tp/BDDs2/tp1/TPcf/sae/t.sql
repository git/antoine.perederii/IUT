CREATE TABLE currency (
    iso CHAR(3) PRIMARY KEY,
    full_name VARCHAR(40) NOT NULL
);

CREATE TABLE cuisine (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30)
);

CREATE TABLE restaurant (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    location VARCHAR(150) NOT NULL,
    min_price NUMERIC(9, 2) NOT NULL,
    max_price NUMERIC(9, 2) NOT NULL,
    currency CHAR(3) REFERENCES currency(iso),
    latitude FLOAT,
    longitude FLOAT,
    phone_number VARCHAR(16),
    url VARCHAR(255) NOT NULL,
    website_url VARCHAR(255) NOT NULL
);

CREATE TABLE cuisine_restaurant (
    restaurant INT REFERENCES restaurant(id),
    cuisine INT REFERENCES cuisine(id),
    PRIMARY KEY(restaurant, cuisine)
);
-- TODO : pourquoi sqlship ne gère pas les if not exists ???????