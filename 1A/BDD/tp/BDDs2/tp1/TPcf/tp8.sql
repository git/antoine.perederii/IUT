-- 1
SELECT * FROM aeroport WHERE pays = 'France' ORDER BY ville;

-- 2
SELECT v.* FROM vol v
INNER JOIN avion a ON a.immat = v.avion
WHERE v.compagnie != a.compagnie;

-- 3
SELECT DISTINCT a.nom, a.ville, a.pays
FROM vol v
INNER JOIN aeroport a ON v.aeroportArr = a.code
WHERE v.aeroportDep = (
	SELECT code FROM aeroport WHERE ville = 'Clermont-Ferrand'
);

-- 4
SELECT MAX(m.nbPassagers) FROM avion a
INNER JOIN modele m ON m.code = a.modele
WHERE a.compagnie = (
	SELECT numero FROM compagnie WHERE nom = 'Air France'
);

-- 5
SELECT m.nom, m.marque, a.matricule, MAX(a.dateMiseService)
FROM avion a
INNER JOIN modele m ON m.code = a.modele
GROUP BY a.modele;

-- 6
SELECT a.* FROM vol v
INNER JOIN avion a ON a.immat = v.avion
WHERE v.aeroportDep = 'CDG'
AND v.aeroportDep = 'JFK'
ORDER BY a.dateMiseService
LIMIT 1;

-- 7 (ou jointure)
SELECT a.*, (
	SELECT COUNT(*) FROM vol WHERE aeroportDep = a.code OR aeroportArr = a.code
) nbVols
FROM aeroport a
ORDER BY a.nbVols DESC;

-- 8
SELECT a.*, COUNT(*)
FROM vol v
INNER JOIN avion a ON a.immat = v.numero
GROUP BY a.immat
HAVING COUNT(*) >= 20;

-- 9
SELECT a.compagnie
FROM avion a
INNER JOIN modele m ON m.code = a.modele
GROUP BY a.compagnie
HAVING COUNT(DISTINCT m.marque) = (
	SELECT COUNT(DISTINCT m.marque) FROM modele
);

-- 10
SELECT compagnie
FROM avion
GROUP BY compagnie
HAVING COUNT(*) >= ALL (
	SELECT COUNT(*) c
	FROM avion
	ORDER BY c DESC
);

-- 11
SELECT a.nom
FROM vol v
INNER JOIN aeroport a ON a.code = v.aeroportDep
WHERE a.pays = 'France'
GROUP BY a.code
HAVING COUNT(*) FILTER (WHERE v.compagnie = (
	SELECT numero FROM compagnie WHERE nom = 'Air France'
)) >= (COUNT(*) * 3) / 4
);

-- 12
SELECT m.nom, ROUND((COUNT(*) * 100) / (
	SELECT COUNT(*) FROM avion
	WHERE compagnie = (SELECT numero FROM compagnie = 'Air France')
), 2) pourcentage
FROM avion a
INNER JOIN modele m ON m.code = a.modele
GROUP BY a.modele;

-- 13
DELETE vol v
USING avion a
WHERE v.avion = a.immat AND v.aeroportDep > (a.dateMiseService + 10000);

SELECT *
FROM vol v
INNER JOIN aeroport depart ON depart.code = v.aeroportDep
INNER JOIN aeroport arrive ON arrive.code = v.aeroportArr
INNER JOIN avion a ON a.immat = v.avion
INNER JOIN modele m ON m.code = a.modele
WHERE depart.pays IN ('France', 'USA') AND arrive.pays IN ('France', 'USA')
	AND ((depart.pays = 'France' AND depart.ville LIKE 'C%')
	OR (depart.pays = 'USA' AND arrive.ville LIKE 'L%'))
	AND ((arrive.pays = 'France' AND arrive.ville LIKE 'C%')
	OR (arrive.pays = 'USA' AND arrive.ville LIKE 'L%'))
	AND m.nbPassagers % 3 = 0
	AND a.compagnie != v.compagnie
	AND (
		SELECT COUNT(*) FROM vol
		INNER JOIN co
	)

