-- 1. Discipline proposant plus d’épreuves en juillet qu’en août.
SELECT d.*
FROM Discipline d, Epreuve e
WHERE d.code=e.discipl AND e.dateE < '2021-08-01' AND e.dateE > '2021-06-30'
GROUP BY d.code
HAVING count(*) >=ALL (SELECT count(*)
FROM Epreuve
WHERE dateE >= '2021-08-01' AND dateE <= '2021-08-31' AND d.code=discipl);

-- code |          nom          
-- ------+-----------------------
--  FEN  | Fencing
--  JUD  | Judo
--  TKW  | Taekwondo
--  GTR  | Trampoline Gymnastics
--  TRI  | Triathlon
--  TTE  | Table Tennis
--  CRD  | Cycling Road
--  SKB  | Skateboarding
--  SWM  | Swimming
--  BK3  | 3x3 Basketball
--  SRF  | Surfing
--  ROW  | Rowing
--  RUG  | Rugby Sevens
--  BSB  | Baseball/Softball
--  DIV  | Diving
--  SHO  | Shooting
--  BMX  | Cycling BMX Racing
--  CSL  | Canoe Slalom
--  ARC  | Archery
--  TEN  | Tennis
--  WLF  | Weightlifting
--  MTB  | Cycling Mountain Bike
-- (22 lignes)


-- 2. Pays dont au moins la moitié des athlètes ont obtenu une médaille.
SELECT p.nom FROM Pays p WHERE (SELECT COUNT(*) FROM Athlete a WHERE a.pays = p.code) * 0.5 <= (SELECT COUNT(DISTINCT r.athlete) FROM Resultat r 
WHERE r.medaille IS NOT NULL AND r.athlete IN (SELECT a.code FROM Athlete a WHERE a.pays = p.code));
/*        nom         
--------------------
 Bermuda
 San Marino
 Fiji
 Russian Federation
 URSS
(5 lignes)
*/

-- 3. Pays ayant remporté plus de médailles d’or que de médailles d’argent et de bronze cumulées.
SELECT pays, COUNT(r.medaille) FILTER (WHERE r.medaille = 1) medailles_or, 
	COUNT(r.medaille) FILTER (WHERE r.medaille != 1) autres_medailles
FROM resultat r
INNER JOIN athlete a on a.code = r.athlete
GROUP BY pays
HAVING COUNT(r.medaille) FILTER (WHERE r.medaille = 1) >=
    COUNT(r.medaille) FILTER (WHERE r.medaille != 1);
/*
 pays | medailles_or | autres_medailles 
------+--------------+------------------
 CUB  |            8 |                8
 KOS  |            2 |                0
 EST  |            4 |                1
 BEL  |           20 |                6
 SLO  |            3 |                2
 TUN  |            1 |                1
 BER  |            1 |                0
 LAT  |            4 |                1
 THA  |            1 |                1
 FIJ  |           13 |               13
 BUL  |            7 |                3
 PUR  |            1 |                0
 MAR  |            1 |                0
 UGA  |            2 |                2
 BRA  |           29 |               26
 ECU  |            2 |                1
 BAH  |            2 |                0
 JPN  |           68 |               63
 QAT  |            2 |                2
 UZB  |            3 |                2
(20 lignes)
*/

-- 4. Nom et prénom des athlètes féminines plus grandes que tous leurs compatriotes masculins.
SELECT nom, prenom 
FROM athlete a
WHERE sexe = 'F'
	AND taille IS NOT NULL
	AND taille >= ALL (
						SELECT taille FROM athlete
						WHERE pays = a.pays AND sexe = 'M' AND taille IS NOT NULL);

/*
          nom          |        prenom        
-----------------------+----------------------
 ABDUL HADI            | Farah Ann
 AL-KAABI              | Fatimah Abbas Waheeb
 ALVARADO              | Luciana
 ALVES LOPES           | Marcia
 AYIVON                | Claire
 BABOVIC               | Anastasija
 BALADIN               | Hande
 BANDA                 | Babra
 BAYASGALAN            | Solongo
 BELEMU                | Margaret
 BENITEZ               | Jackie
 BOZ                   | Meryem
 BRNOVIC               | Tatjana
 BYLON                 | Atheyna
[...]
(86 lignes)
*/

-- 5. Lister chaque athlète masculin français (nom, prénom, taille) et comparer leur taille avec celle du plus grand d’entre eux.
SELECT nom, prenom, taille, taille - (
	SELECT MAX(taille) FROM athlete
	WHERE sexe = 'M' AND pays = 'FRA') diff 
FROM athlete
WHERE sexe = 'M' AND pays = 'FRA';
/*
       nom        |     prenom      | taille | diff
------------------+-----------------+--------+-------
 ABALO            | Luc             |   1.83 | -0.35
 AIT SAID         | Samir           |   1.68 | -0.50
 ALBICY           | Andrew          |   1.78 | -0.40
 ALIEV            | Mourad          |        |
 AMDOUNI          | Morhad          |        |
 AMOROS           | Emile           |   1.78 | -0.40
 ANDRE            | Sylvain         |        |
 ANDRODIAS        | Matthieu        |   1.94 | -0.24
[...]
(220 lignes)
*/

-- 6. Pour chaque épreuve, indiquer sa date et les dates des premières et dernières épreuves de la discipline.
SELECT *,
	(SELECT MIN(dateE) FROM epreuve WHERE discipl = e.discipl) datemin,
	(SELECT MAX(dateE) FROM epreuve WHERE discipl = e.discipl) datemax
FROM epreuve e;
/*
 code | discipl |                     nom                      |   datee    |  datemin   |  datemax
------+---------+----------------------------------------------+------------+------------+------------
 E001 | ARC     | Mixed Team                                   | 2021-07-24 | 2021-07-24 | 2021-07-31
 E002 | CRD     | Men's Road Race                              | 2021-07-24 | 2021-07-24 | 2021-07-28
 E003 | FEN     | Men's Sabre Individual                       | 2021-07-24 | 2021-07-24 | 2021-08-01
 E004 | FEN     | Women's Epée Individual                      | 2021-07-24 | 2021-07-24 | 2021-08-01
 E005 | JUD     | Men -60 kg                                   | 2021-07-24 | 2021-07-24 | 2021-07-31
 E006 | JUD     | Women -48 kg                                 | 2021-07-24 | 2021-07-24 | 2021-07-31
 E007 | SHO     | 10m Air Pistol Men                           | 2021-07-24 | 2021-07-24 | 2021-08-02
[...]
(355 lignes)
*/

-- 7. Un typhon est annoncé. Les épreuves de la dernière journée doivent être reportée d’une semaine. Quelle commande permet de faire cette modification sans connaître la date de la dernière journée ?
UPDATE epreuve
SET dateE = dateE + 7
WHERE dateE = (SELECT MAX(dateE) FROM epreuve);
/*
UPDATE 13
*/

-- 8. Suspicion de dopage dans les épreuves d’haltérophilie (Weightlifting). Supprimer tous les résultats aux épreuves d’haltérophilie pour le pays ayant remporté le plus de médailles dans cette discipline.
DELETE FROM resultat r
USING epreuve e
WHERE e.code = r.epreuve AND e.discipl = (
	SELECT code FROM discipline WHERE nom = 'Weightlifting'
) AND r.athlete IN (
					SELECT code FROM athlete WHERE pays IN (
					SELECT a.pays FROM resultat r
					INNER JOIN epreuve e ON e.code = r.epreuve
					INNER JOIN athlete a ON a.code = r.athlete
					WHERE e.discipl = (
						SELECT code FROM discipline WHERE nom = 'Weightlifting'
					)
					GROUP BY a.pays
					ORDER BY COUNT(*) DESC
					LIMIT 1));
/*
DELETE 8
*/

-- 9. Ajouter une nouvelle épreuve :’Women’’s team’ pour le golf ayant lieu le 09/08/2021 (code de l’épreuve :’E356’).
INSERT INTO Epreuve (code, discipl, nom, dateE)
VALUES ('E356', 'GLF', 'Women''s team', '2021-08-09');
/*
INSERT 0 1
*/

-- 10. Écrire une requête permettant de donner une médaille d’or lors de l’épreuve crée à la question précédente à toutes les golfeuses des Etats-unis.
INSERT INTO resultat
SELECT 'E356', a.code, (SELECT place FROM medaille WHERE couleur = 'Or')
FROM athlete a
INNER JOIN pratiquer p ON p.athlete = a.code
WHERE a.pays = (SELECT code FROM pays WHERE nom = 'United States of America')
	AND p.discipl = (SELECT code FROM discipline WHERE nom = 'Golf')
	AND a.sexe = 'F';
/*
INSERT 0 4
*/

-- 11. Même question mais pour donner une médaille d’argent à toutes les golfeuses sud-coréennes.
INSERT INTO resultat
SELECT 'E356', a.code, (SELECT place FROM medaille WHERE couleur = 'Argent')
FROM athlete a
INNER JOIN pratiquer p ON p.athlete = a.code
WHERE a.pays = (SELECT code FROM pays WHERE nom = 'Republic of Korea')
	AND p.discipl = (SELECT code FROM discipline WHERE nom = 'Golf')
	AND a.sexe = 'F';
/*
INSERT 0 4
*/

-- 12. Vérifier que vos deux requêtes précédentes ont bien fonctionnées en affichant les résultats de l’épreuve 'E356' (y compris nom, prénom et pays des médaillées).
SELECT p.nom pays, a.sexe, a.nom, a.prenom, m.couleur FROM resultat r
INNER JOIN medaille m ON m.place = r.medaille
INNER JOIN athlete a ON a.code = r.athlete
INNER JOIN pays p ON p.code = a.pays
WHERE r.epreuve = 'E356'
ORDER BY r.medaille;
/*
           pays           | sexe |   nom    |  prenom   | couleur 
--------------------------+------+----------+-----------+---------
 United States of America | F    | THOMPSON | Lexi      | Or
 United States of America | F    | KORDA    | Jessica   | Or
 United States of America | F    | KORDA    | Nelly     | Or
 United States of America | F    | KANG     | Danielle  | Or
 Republic of Korea        | F    | PARK     | Inbee     | Argent
 Republic of Korea        | F    | KIM      | Hyojoo    | Argent
 Republic of Korea        | F    | KIM      | Sei Young | Argent
 Republic of Korea        | F    | KO       | Jin Young | Argent
(8 lignes)
*/

-- 13. Réinitialiser votre base de données avec le script jo.sql pour annuler les modifications faites sur ces deux dernières questions.
\i jo.sql
