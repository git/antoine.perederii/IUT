-- Question 1
SELECT count(athlete) 
FROM Athlete
GROUP BY pays;

-- count 
-- -------
--     37
--     10
--    108
--      2
--    283
--      3
--      8
--      5
--      3
--      8
--     41
--      5
--     43
--     69
--    172
--    116
--     76
--    125
--     90
--    392
--      5
--     11
--      2
--     33
--      2
--     44
--      3
--      6
--    130
--     35
--     20
--     53
--      3
--    115
--     13
--      2
--      6
--     42
--     82
--     31
--      5
--      6
--     43
--      5
--      6


-- Question 2
SELECT count(epreuve)
FROM Epreuve
GROUP BY discipl;

-- count 
-- -------
--     12
--      2
--     35
--      2
--      2
--      2
--     48
--      2
--      2
--      3
--      2
--      2
--      2
--      2
--      4
--      2
--     14
--      5
--     12
--      6
--     14
--     10
--      2
--     26
--     18
--      2
--      2
--      2
--      5
--      2
--     12
--      8
--      5
--      8
--      2
--     15
--     14
--      8
--     15
--      8
--      2
--      2
--      4
--      2
--      2

-- Question 3
SELECT MAX(e.dateE), d.nom
FROM Discipline d, Epreuve e
WHERE e.discipl = d.code
GROUP BY e.discipl, d.nom;

-- max     |          nom          
-- ------------+-----------------------
--  2021-07-27 | Surfing
--  2021-07-30 | Canoe Slalom
--  2021-08-01 | Cycling BMX Freestyle
--  2021-08-07 | Diving
--  2021-07-27 | Taekwondo
--  2021-07-28 | Cycling Road
--  2021-08-08 | Boxing
--  2021-08-03 | Artistic Gymnastics
--  2021-08-01 | Swimming
--  2021-07-31 | Triathlon
--  2021-08-08 | Basketball
--  2021-08-02 | Shooting
--  2021-08-06 | Hockey
--  2021-08-05 | Marathon Swimming
--  2021-08-07 | Artistic Swimming
--  2021-08-04 | Sailing
--  2021-08-07 | Karate
--  2021-08-04 | Weightlifting
--  2021-08-07 | Baseball/Softball
--  2021-08-07 | Football
--  2021-07-27 | Cycling Mountain Bike
--  2021-07-31 | Rugby Sevens
--  2021-08-07 | Beach Volleyball
--  2021-08-01 | Tennis
--  2021-08-07 | Canoe Sprint
--  2021-08-06 | Table Tennis
--  2021-08-07 | Wrestling
--  2021-07-30 | Cycling BMX Racing
--  2021-08-07 | Equestrian
--  2021-07-28 | 3x3 Basketball
--  2021-08-08 | Cycling Track
--  2021-08-08 | Volleyball
--  2021-08-01 | Fencing
--  2021-07-31 | Judo
--  2021-08-05 | Skateboarding
--  2021-07-31 | Archery
--  2021-08-07 | Modern Pentathlon
--  2021-08-06 | Sport Climbing
--  2021-08-07 | Golf
--  2021-08-02 | Badminton
--  2021-08-08 | Handball

-- Question 4
SELECT max(a.dateNaiss), a.sexe
FROM Athlete a, Pratiquer p, Discipline d
WHERE a.code = p.athlete AND p.discipl = d.code AND a.sexe IS NOT NULL
GROUP BY a.sexe, p.discipl;

-- max     | sexe 
-- ------------+------
--  2003-01-28 | F
--  2002-03-14 | M
--  2002-05-11 | F
--  2001-06-29 | M
--  2004-04-07 | F
--  2001-11-25 | M
--  2003-07-24 | M
--  2003-08-10 | M
--  2002-09-18 | M
--  2004-05-05 | M
--  2000-11-20 | M
--  2001-01-01 | M
--  2000-07-08 | M
--  2007-03-28 | F
--  2002-10-20 | F
--  2002-03-09 | M
--  2005-10-11 | F
--  2003-04-12 | M
--  2001-10-03 | F
--  1999-02-25 | F
--  2002-01-29 | F
--  2002-03-19 | M
--  2004-11-29 | F
--  2004-01-29 | M
--  2001-02-01 | M
--  2002-03-03 | M
--  2000-01-25 | M
--  2001-03-12 | M
--  2003-01-15 | M
--  2004-01-26 | F
--  1999-10-07 | M
--  2001-09-03 | F
--  2004-01-02 | F
--  2001-07-06 | M
--  2001-12-05 | M
--  2004-04-18 | F
--  2000-09-01 | M
--  2001-03-28 | M
--  2002-01-04 | F

-- Question 5
SELECT count(m.place)
FROM Medaille m, Athlete a, Resultat r
WHERE m.place = r.medaille AND r.athlete = a.code
GROUP BY a.pays
ORDER BY count(m.place) DESC;

-- count 
-- -------
--    295
--    147
--    140
--    137
--    131
--    130
--    129
--     82
--     77
--     72
--     70
--     69
--     65
--     55
--     51
--     43
--     38
--     35
--     33
--     32
--     32
--     31
--     29
--     27
--     26
--     26
--     24
--     23
--     19
--     16
--     16
--     16
--     15
--     14
--     13
--     13
--     11
--     10
--     10
--     10
--      9
--      8
--      8
--      8
--      8

-- Question 6
SELECT count(m.place)
FROM Medaille m, Athlete a, Resultat r
WHERE m.place = r.medaille AND r.athlete = a.code AND m.couleur = 'Or'
GROUP BY a.pays
ORDER BY count(m.place) DESC;

-- count 
-- -------
--    112
--     68
--     65
--     58
--     40
--     39
--     36
--     36
--     31
--     29
--     29
--     20
--     18
--     18
--     15
--     14
--     13
--     10
--      9
--      9
--      8
--      7
--      5
--      5
--      5
--      5
--      4
--      4
--      4
--      4
--      4
--      3
--      3
--      3
--      3
--      3
--      3
--      2
--      2
--      2
--      2
--      2
--      2
--      2
--      2

-- Question 7
SELECT count(m.place)
FROM Medaille m, Athlete a, Resultat r
WHERE m.place = r.medaille AND r.athlete = a.code AND m.couleur = 'Or'
GROUP BY a.pays
HAVING count(m.place) > 30
ORDER BY count(m.place) DESC;

-- count 
-- -------
--    112
--     68
--     65
--     58
--     40
--     39
--     36
--     36
--     31


-- Question 8
SELECT a.code, a.nom, a.prenom, a.pays
FROM Athlete a, Pratiquer p
WHERE a.code = p.athlete
GROUP BY a.code, a.nom, a.prenom, a.pays
HAVING count(p.*) > 1;

-- code  |      nom       |   prenom   | pays 
-- --------+----------------+------------+------
--  A05315 | KOPECKY        | Lotte      | BEL
--  A01327 | BRENNAUER      | Lisa       | GER
--  A07786 | PALTRINIERI    | Gregorio   | ITA
--  A05148 | KIRPICHNIKOVA  | Anastasiia | ROC
--  A02686 | DYGERT         | Chloe      | USA
--  A10627 | VAN ROUWENDAAL | Sharon     | NED
--  A00622 | AUBRY          | David      | FRA
--  A05189 | KLEIN          | Lisa       | GER
--  A03293 | GANNA          | Filippo    | ITA
--  A11041 | WELLBROCK      | Florian    | GER
--  A03991 | HAVIK          | Yoeri      | NED

-- Question 9
SELECT a.code, a.nom, a.prenom, a.pays
FROM Athlete a, Resultat r
WHERE a.code = r.athlete
GROUP BY a.code, a.nom, a.prenom, a.pays
HAVING count(r.athlete) = 4;

-- code  |   nom   |  prenom  | pays 
-- --------+---------+----------+------
--  A11582 | ZHANG   | Yufei    | CHN
--  A09215 | SCOTT   | Duncan   | GBR
--  A10225 | TITMUS  | Ariarne  | AUS
--  A06581 | McKEOWN | Kaylee   | AUS
--  A05668 | LEDECKY | Kathleen | USA

-- Question 10
SELECT e.discipl, count(athlete)
FROM Athlete a, Resultat r, Epreuve e
WHERE a.code = r.athlete AND r.epreuve = e.code AND '2021-07-23' - a.dateNaiss < 18 * 365
GROUP BY e.discipl
HAVING count(a.*) >= 3;

-- discipl | count 
-- ---------+-------
--  GAR     |     5
--  DIV     |     4
--  SKB     |     5
--  SWM     |     9

-- Question 11
SELECT p.nom, count(a.*)
FROM pays p, Athlete a
WHERE p.code = a.pays
GROUP BY p.nom
HAVING count(a.*) >= ALL(SELECT count(a.*) FROM pays p, Athlete a WHERE p.code = a.pays GROUP BY p.nom);

-- nom            | count 
-- --------------------------+-------
--  United States of America |   633

-- Question 12
SELECT a.nom, a.prenom, a.pays, count(m.place)
FROM Athlete a, Resultat r, Medaille m 
WHERE a.code = r.athlete AND r.medaille = m.place
GROUP BY a.nom, a.prenom, a.pays
HAVING count(m.place) >= ALL(SELECT count(m.place) FROM Athlete a, Resultat r, Medaille m WHERE a.code = r.athlete AND r.medaille = m.place GROUP BY a.nom, a.prenom, a.pays);

-- nom   | prenom | pays | count 
-- --------+--------+------+-------
--  McKEON | Emma   | AUS  |     7