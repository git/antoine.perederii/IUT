-- 1 Ecrire une requête permettant de lister les codes de pays et le nombre de médailles d’or qu’ils ont remporté.
-- Attention, il ne faut pas compter plusieurs médailles remportées lors d’épreuves par équipes, autrement
-- dit lorsque plusieurs athlètes d’un même pays remporte la même médaille lors d’une épreuve.

SELECT pays, COUNT(*) AS nb_medailles_or
FROM Medailles
WHERE medaille = 'Or'
GROUP BY pays
ORDER BY nb_medailles_or DESC;


-- 2  Transformer la requête précédente en vue nommée MedaillesOr

CREATE VIEW MedaillesOr AS
SELECT pays, COUNT(*) AS nb_medailles_or
FROM Medailles
WHERE medaille = 'Or'
GROUP BY pays
ORDER BY nb_medailles_or DESC;
