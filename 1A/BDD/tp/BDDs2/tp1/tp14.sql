-- 1. Écrire une requête permettant de lister les codes de pays et le nombre de médailles d’or qu’ils ont remporté
SELECT a.pays code_pays,
    COUNT(DISTINCT r.epreuve) medailles_or
FROM resultat r
INNER JOIN athlete a ON a.code = r.athlete
WHERE r.medaille = (
    SELECT place FROM medaille WHERE couleur = 'Or'
)
GROUP BY a.Pays
ORDER BY medailles_or DESC;

-- 2. Transformer la requête précédente en vue nommée MedaillesOr.
CREATE VIEW MedaillesOr AS SELECT a.pays code_pays, COUNT(DISTINCT r.epreuve) medailles_or
                            FROM resultat r, athlete a 
                            WHERE a.code = r.athlete AND r.medaille = (
                                                                        SELECT place 
                                                                        FROM medaille 
                                                                        WHERE couleur = 'Or')
                            GROUP BY a.pays;

-- 3. Utiliser la vue créée pour trouver le nombre de médailles d’or remportées par la France.
SELECT medailles_or FROM MedaillesOr WHERE code_pays = 'FRA';

-- 4. Comme pour MedaillesOr, créer deux vues MedaillesArgent et MedaillesBronze associant le code des pays au nombre de médailles d’argent et de bronze remportées, respectivement.

CREATE VIEW MedaillesArgent AS SELECT a.pays code_pays, COUNT(DISTINCT r.epreuve) medailles_argent
                            FROM resultat r, athlete a 
                            WHERE a.code = r.athlete AND r.medaille = (
                                                                        SELECT place 
                                                                        FROM medaille 
                                                                        WHERE couleur = 'Argent')
                            GROUP BY a.pays;

CREATE VIEW MedaillesBronze AS SELECT a.pays code_pays, COUNT(DISTINCT r.epreuve) medailles_bronze
        FROM resultat r, athlete a 
        WHERE a.code = r.athlete AND r.medaille = (
                                                    SELECT place 
                                                    FROM medaille 
                                                    WHERE couleur = 'Bronze')
        GROUP BY a.pays;        

-- 5. Créer une vue TableauMedailles listant le nombre de médailles d’or, d’argent, de bronze et
-- le total de médailles remportées par chaque pays. Les pays ayant remporté le plus de médailles d’or
-- apparaitront en premier, puis ceux ayant remporté le plus de médailles d’argent et enfin ceux ayant
-- remporté le plus de médailles de bronze.

CREATE VIEW TableauMedailles AS
    SELECT p.nom pays,
        o.medailles_or, a.medailles_argent, b.medailles_bronze, 
        (o.medailles_or + a.medailles_argent + b.medailles_bronze) total_medailles
    FROM pays p, MedaillesOr o, MedaillesArgent a, MedaillesBronze b
    WHERE o.code_pays = p.code AND a.code_pays = p.code AND b.code_pays = p.code AND 
        medailles_or IS NOT NULL
        OR medailles_argent IS NOT NULL
        OR medailles_bronze IS NOT NULL 
    ORDER by total_medailles DESC;-- Retire les pays qui ont 0 médaille peu importe la couleur

SELECT * FROM TableauMedailles WHERE pays = 'France';

-- 6. Afficher le tableau des médailles
SELECT * FROM tableaumedailles;

-- 7. Un organe de presse souhaite faire son propre classement, en ordonnant les pays en fonction
-- du nombre total de médailles remportées, et par ordre alphabétique des noms de pays en cas d’égalité.
-- Sans modifier votre vue TableauMedailles, afficher ce nouveau classement.

SELECT * 
FROM TableauMedailles 
ORDER BY (total_medailles) DESC, (pays) ASC;


--! Partie 2

-- 1. Ecrire une requête SQL calculant le nombre d’athlètes pratiquants par discipline et par pays
-- (trié par discipline, puis pays).

SELECT pa.nom AS pays, d.code AS discipline, count(*) AS nb_athletes
FROM Athlete a, Pays pa, Pratiquer pr, Discipline d
WHERE a.pays = pa.code AND pr.athlete = a.code AND d.code = pr.discipl
GROUP BY pa.nom, d.code;

-- 2. Transformer la requête précédente en vue Pratiquants_v1.
DROP VIEW Pratiquants_v1 cascade;
CREATE VIEW Pratiquants_v1 AS SELECT pa.nom AS pays, d.code AS discipline, count(*) AS nb_athletes  
                                FROM Athlete a, Pays pa, Pratiquer pr, Discipline d
                                WHERE a.pays = pa.code AND pr.athlete = a.code AND d.code = pr.discipl
                                GROUP BY pa.nom, d.code;

-- 3. Même chose pour Pratiquants_v2 mais ce sera une vue matérialisée

CREATE MATERIALIZED VIEW Pratiquants_v2 AS SELECT pa.nom AS pays, d.code AS discipline, count(*) AS nb_athletes  
                                            FROM Athlete a, Pays pa, Pratiquer pr, Discipline d
                                            WHERE a.pays = pa.code AND pr.athlete = a.code AND d.code = pr.discipl
                                            GROUP BY pa.nom, d.code;

-- 4. Utiliser les vues créées précédemment pour afficher le nombre d’athlètes français pratiquant le
-- judo (code ’JUD’)

SELECT * 
FROM Pratiquants_v1
WHERE pays = 'France' AND discipline = 'JUD';

SELECT * 
FROM Pratiquants_v2
WHERE pays = 'France' AND discipline = 'JUD';

-- 5.Teddy RINER ne pratique plus le judo mais le skateboard (code ’SKB’). Faire les modifications
-- nécessaires.
-- UPDATE discipl
-- SET discipl = 'SKB'
-- FROM Pratiquer
-- WHERE athlete = (a.code FROM Athlete a WHERE a.nom = 'Riner');

UPDATE pratiquer 
SET discipl = 'SKB' 
WHERE athlete = (
                SELECT code 
                FROM Athlete
                WHERE nom = 'RINER');

-- 6. Réafficher le nombre d’athlètes français pratiquant le judo (code ’JUD’).
-- Comparer le résultat obtenu avec les 2 vues. Pourquoi cette différence ?

SELECT * 
FROM Pratiquants_v2
WHERE pays = 'France' AND discipl = 'JUD';

SELECT * 
FROM Pratiquants_v1
WHERE pays = 'France' AND discipl = 'JUD';

-- car elle est pas update

-- 7. Que faut-il faire pour que les deux vues affichent le même résultat ? Vérifier

-- mettre 
-- REFRESH MATERIALIZED VIEW Pratiquants_v2;