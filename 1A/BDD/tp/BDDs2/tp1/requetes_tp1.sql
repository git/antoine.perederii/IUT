-- Question 3
SELECT COUNT(code) 
FROM Athlete;

--  count 
-- -------
--  11647
-- (1 ligne)


-- Question 4
SELECT * 
FROM Epreuve 
WHERE dateE = '2021-07-29';

--  code | discipl |                nom                |   datee    
-- ------+---------+-----------------------------------+------------
--  E096 | GAR     | Women's All-Around                | 2021-07-29
--  E097 | CSL     | Women's Canoe                     | 2021-07-29
--  E098 | FEN     | Women's Foil Team                 | 2021-07-29
--  E099 | JUD     | Men -100 kg                       | 2021-07-29
--  E100 | JUD     | Women -78 kg                      | 2021-07-29
--  E101 | ROW     | Lightweight Men's Double Sculls   | 2021-07-29
--  E102 | ROW     | Lightweight Women's Double Sculls | 2021-07-29
--  E103 | ROW     | Men's Pair                        | 2021-07-29
--  E104 | ROW     | Women's Pair                      | 2021-07-29
--  E105 | SHO     | Trap Men                          | 2021-07-29
--  E106 | SHO     | Trap Women                        | 2021-07-29
--  E107 | SWM     | Men's 100m Freestyle              | 2021-07-29
--  E108 | SWM     | Men's 200m Breaststroke           | 2021-07-29
--  E109 | SWM     | Men's 800m Freestyle              | 2021-07-29
--  E110 | SWM     | Women's 200m Butterfly            | 2021-07-29
--  E111 | SWM     | Women's 4 x 200m Freestyle Relay  | 2021-07-29
--  E112 | TTE     | Women's Singles                   | 2021-07-29
-- (17 lignes)

-- Question 5
SELECT d.nom 
FROM Discipline d, Epreuve e 
WHERE e.discipl = d.code AND e.dateE = '2021-07-29';

--          nom         
-- ---------------------
--  Artistic Gymnastics
--  Canoe Slalom
--  Fencing
--  Judo
--  Judo
--  Rowing
--  Rowing
--  Rowing
--  Rowing
--  Shooting
--  Shooting
--  Swimming
--  Swimming
--  Swimming
--  Swimming
--  Swimming
--  Table Tennis
-- (17 lignes)

-- Question 6
SELECT a.nom, a.prenom, a.dateNaiss 
FROM Athlete a, Pays p
WHERE a.pays = p.code AND p.nom = 'France'
ORDER BY a.nom, a.prenom;

--          nom         |       prenom       | datenaiss  
-- ---------------------+--------------------+------------
--  ABALO               | Luc                | 1984-09-06
--  ADO                 | Pauline            | 1991-02-14
--  AERNOUDTS           | Violaine           | 1999-10-23
--  AGBEGNENOU          | Clarisse           | 1992-10-25
--  AIT SAID            | Samir              | 1989-11-01
--  ALBICY              | Andrew             | 1990-03-21
--  ALIEV               | Mourad             | 1995-07-31

-- Question 7
SELECT COUNT(m.place) 
FROM Medaille m, Athlete a, Resultat r 
WHERE m.place = r.medaille AND r.athlete = a.code AND a.nom = 'LEDECKY' AND a.prenom = 'Kathleen';

--  count 
-- -------
--      4
-- (1 ligne)

-- Question 8
SELECT a.nom, a.prenom, a.pays, e.nom, d.nom
FROM Athlete a, Resultat r, Epreuve e, Discipline d, Medaille m
WHERE a.code = r.athlete AND r.epreuve = e.code AND e.discipl = d.code AND r.medaille = m.place AND e.code = 'E059' AND m.couleur = 'Or';

--     nom     |  prenom  | pays |     nom      | nom  
-- ------------+----------+------+--------------+------
--  AGBEGNENOU | Clarisse | FRA  | Women -63 kg | Judo

-- Question 9
SELECT * 
FROM Athlete
WHERE dateNaiss IN(SELECT MAX(dateNaiss) FROM Athlete);

--   code  | nom  | prenom | sexe | datenaiss  | paysnaiss | pays | taille 
-- --------+------+--------+------+------------+-----------+------+--------
--  A11515 | ZAZA | Hend   | F    | 2009-01-01 | SYR       | SYR  |       

-- Question 10
SELECT Athlete, ROUND(('2021-07-23'-dateNaiss)/365.25 -0.5) AS age
FROM Athlete
WHERE dateNaiss IN(SELECT MIN(dateNaiss) FROM Athlete);

--                   athlete                  | ?column? 
-- -------------------------------------------+----------
--  (A03900,HANNA,Mary,F,1954-12-01,AUS,AUS,) |       66

-- Question 11
SELECT p.nom
FROM Pays p
WHERE p.code NOT IN(SELECT Pays FROM Athlete WHERE code IN(SELECT Athlete FROM Resultat)) AND nom LIKE 'A%';

--          nom         
-- ---------------------
--  Angola
--  Algeria
--  Afghanistan
--  Albania
--  American Samoa
--  Andorra
--  Aruba
--  Antigua and Barbuda