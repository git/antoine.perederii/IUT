import matplotlib.pyplot as plt

--Q5 La colonne Global_Sales est redondante car elle peut être calculée grâce aux autres colonnes.
--Cela pose d’ailleurs problème puisque certaines données sont incohérentes. Écrire une requête SQL
--permettant de détecter les lignes incohérentes 
SELECT COUNT(global_sales) FROM VGSales WHERE global_sales != (na_sales + eu_sales + jp_sales + other_sales);
/*
count
-------
   4548
*/


--Q6 Écrire une requête SQL permettant de recalculer la colonne Global_sales en fonction des
--autres colonnes pour ces lignes problématiques
Update VGSales SET global_sales=(na_sales + eu_sales + jp_sales + other_sales) WHERE global_sales != (na_sales + eu_sales + jp_sales + other_sales);
/*
UPDATE 4548
*/

--2
--Q1 Écrire la requête SQL permettant de calculer les ventes moyennes par année de sortie des jeux, pour les jeux du genre ’Adventure’
SELECT ROUND(AVG(global_sales),2), year 
FROM VGSales 
WHERE genre = 'Adventure' and year != 'NaN'
GROUP BY year
ORDER by year;
/*
=========================RESULTAT=========================
 round | year 
-------+------
  0.40 | 1983
  4.38 | 1987
  1.12 | 1991
  3.06 | 1992
  0.07 | 1993
  0.94 | 1994
  0.05 | 1995
  0.25 | 1996
  0.36 | 1997
  0.39 | 1998
  0.40 | 1999
  0.19 | 2000
  0.44 | 2001
  0.26 | 2002
  0.18 | 2003
  0.22 | 2004
  0.20 | 2005
  0.16 | 2006
  0.29 | 2007
  0.15 | 2008
  0.15 | 2009
  0.11 | 2010
  0.15 | 2011
  0.10 | 2012
  0.10 | 2013
  0.08 | 2014
  0.15 | 2015
  0.05 | 2016
(28 lignes)
*/

--Q2 Afficher le résultat de la requête précédente sous forme de courbe.

--Q3 Même question pour le total des ventes par année de sortie des jeux
SELECT SUM(global_sales) as total_vente, year 
FROM VGSales 
WHERE genre = 'Adventure' and year != 'NaN'
GROUP BY year
ORDER by year;
/*
=========================RESULTAT=========================
 total_vente | year 
-------------+------
        0.40 | 1983
        4.38 | 1987
        2.24 | 1991
       12.24 | 1992
        0.07 | 1993
        3.74 | 1994
        0.71 | 1995
        4.18 | 1996
        4.97 | 1997
        9.04 | 1998
        7.61 | 1999
        2.96 | 2000
*/

--Q4 Même question pour l’ensemble des jeux.
SELECT SUM(global_sales) as total_vente, year 
FROM VGSales 
WHERE year != 'NaN'
GROUP BY year
ORDER by year;
/*
=========================RESULTAT=========================
 total_vente | year 
-------------+------
       11.38 | 1980
       35.33 | 1981
       28.88 | 1982
       16.80 | 1983
       50.35 | 1984
       53.49 | 1985
       37.08 | 1986
       21.70 | 1987
       47.21 | 1988
       73.45 | 1989
       49.37 | 1990
       32.23 | 1991
*/

--PARTIE3
--Q1 Écrire la requête SQL permettant de calculer le total des ventes en Europe par plateforme.
SELECT platform, SUM(eu_sales) as total_vente
FROM VGSales 
GROUP BY platform;
/*
=========================RESULTAT=========================
  platform | total_vente 
----------+-------------
 TG16     |        0.00
 PSP      |       67.83
 2600     |        5.47
 PS4      |      123.70
 N64      |       41.06
 GBA      |       74.60
 GG       |        0.00
 PSV      |       16.33
 XB       |       60.38
 DC       |        1.69
 XOne     |       44.77
 PS2      |      339.11
*/

--Q4 Écrire la requête SQL permettant de calculer le total des ventes aux Etats-Unis, en Europe, 
--au Japon et ailleurs par plateforme.
SELECT platform, SUM(na_sales) as total_vente_na, SUM(eu_sales) as total_vente_eu, SUM(jp_sales) as total_vente_jp, SUM(other_sales) as total_vente_autre
FROM VGSales
GROUP BY platform;
/*
=========================RESULTAT=========================
 platform | total_vente_na | total_vente_eu | total_vente_jp | total_vente_autre
----------+----------------+----------------+----------------+-------------------
 TG16     |           0.00 |           0.00 |           0.16 |              0.00
 PSP      |         108.53 |          67.83 |          76.79 |             42.19
 2600     |          90.25 |           5.47 |           0.00 |              0.91
 PS4      |          96.80 |         123.70 |          14.30 |             43.36
 N64      |         137.82 |          41.06 |          34.22 |              4.38
 GBA      |         187.54 |          74.60 |          47.33 |              7.73
 GG       |           0.00 |           0.00 |           0.04 |              0.00
 PSV      |          16.20 |          16.33 |          20.96 |              8.45
 XB       |         186.69 |          60.38 |           1.38 |              8.72
 DC       |           5.43 |           1.69 |           8.56 |              0.27
 XOne     |          83.19 |          44.77 |           0.34 |             11.92
 PS2      |         581.97 |         339.11 |         139.64 |            193.44
*/

--Q5 Afficher sous forme de graphique les résultats de la requête précédente avec une courbe par
--zone géographique. Cette fois-ci la légende devra apparaître.

--PARTIE4
--Q1 Écrire la requête SQL de récupérer les ventes (Etats-unis, Europe, Japon, Autre) du jeu Mario Kart 64.
SELECT SUM(na_sales) as total_vente_na, SUM(eu_sales) as total_vente_eu, SUM(jp_sales) as total_vente_jp, SUM(other_sales) as total_vente_autre
FROM VGSales
WHERE name = 'Mario Kart 64';
/*
=========================RESULTAT=========================
 total_vente_na | total_vente_eu | total_vente_jp | total_vente_autre 
----------------+----------------+----------------+-------------------
           5.55 |           1.94 |           2.23 |              0.15
*/

--PARTIE5
--Q1 Écrire la requête SQL permettant de calculer le total des ventes de jeux dans le monde par genre.
SELECT round(sum(global_sales),2) as total_vente, genre 
FROM VGSales
GROUP BY genre
ORDER BY total_vente DESC;