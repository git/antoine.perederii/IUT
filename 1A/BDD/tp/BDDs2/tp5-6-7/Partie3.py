import pandas as pd
import psycopg2 as psy
import getpass
import matplotlib.pyplot as plt

data = pd. read_csv (r'vgsales.csv')
df = pd.DataFrame(data)
df = df.drop_duplicates()

co = None

try:
    co = psy. connect(host='londres',
                        database ='dbanperederi',
                        user='anperederi',
                        password = getpass.getpass("Mot de passe:"))

#Q2 Afficher sous forme de courbe les résultats de la requête précédente. 
    datafr = pd.read_sql('''SELECT platform, SUM(eu_sales) as total_vente
                            FROM VGSales 
                            GROUP BY platform;''', con=co)
    
    fig = datafr.plot(x='platform', y='total_vente', kind='line', title='Ventes par plateforme en europe', figsize=(10, 5), legend=True, fontsize=12)
    fig.set_xlabel('Platforme')
    fig.set_ylabel('Ventes (en millions)')
    fig.set_xticks(datafr.index)
    fig.set_xticklabels(datafr['platform'], rotation=45)
    plt.show()

#Q4
    datafr = pd.read_sql('''SELECT platform, SUM(eu_sales) as total_vente
                            FROM VGSales 
                            GROUP BY platform;''', con=co)
    
    fig = datafr.plot(x='platform', y='total_vente', kind='bar', title='Ventes par plateforme en europe', figsize=(10, 5), legend=True, fontsize=12)
    fig.set_xlabel('Platforme')
    fig.set_ylabel('Ventes (en millions)')
    fig.set_xticks(datafr.index)
    fig.set_xticklabels(datafr['platform'], rotation=45)
    plt.show()


#Q5
    datafr = pd.read_sql('''SELECT platform, SUM(na_sales) as total_vente_na, SUM(eu_sales) as total_vente_eu, SUM(jp_sales) as total_vente_jp, SUM(other_sales) as total_vente_autre
                            FROM VGSales
                            GROUP BY platform;''', con=co)
    
    fig = datafr.plot(x='platform', y=['total_vente_na', 'total_vente_eu', 'total_vente_jp', 'total_vente_autre'], kind='line', title='Ventes par plateforme', figsize=(10, 5), legend=True, fontsize=12)
    fig.set_xlabel('Platforme')
    fig.set_ylabel('Ventes (en millions)')
    fig.set_xticks(datafr.index)
    fig.set_xticklabels(datafr['platform'], rotation=45)
    plt.show()
#Q6
    datafr = pd.read_sql('''SELECT platform, SUM(na_sales) as total_vente_na, SUM(eu_sales) as total_vente_eu, SUM(jp_sales) as total_vente_jp, SUM(other_sales) as total_vente_autre
                            FROM VGSales
                            GROUP BY platform;''', con=co)
    
    fig = datafr.plot(x='platform', y=['total_vente_na', 'total_vente_eu', 'total_vente_jp', 'total_vente_autre'], kind='line', title='Ventes par plateforme', figsize=(10, 5), legend=True, fontsize=12, style=['-', '--', '-.', ':'])
    fig.set_xlabel('Platforme')
    fig.set_ylabel('Ventes (en millions)')
    fig.set_xticks(datafr.index)
    fig.set_xticklabels(datafr['platform'], rotation=45)
    plt.show()

#Q7
    datafr = pd.read_sql('''SELECT platform, SUM(na_sales) as total_vente_na, SUM(eu_sales) as total_vente_eu, SUM(jp_sales) as total_vente_jp, SUM(other_sales) as total_vente_autre
                            FROM VGSales
                            GROUP BY platform;''', con=co)
    
    fig = datafr.plot(x='platform', y=['total_vente_na', 'total_vente_eu', 'total_vente_jp', 'total_vente_autre'], kind='bar', title='Ventes par plateforme', figsize=(10, 5), legend=True, fontsize=12, style=['b', 'g', 'r', 'c'])
    fig.set_xlabel('Platforme')
    fig.set_ylabel('Ventes (en millions)')
    fig.set_xticks(datafr.index)
    fig.set_xticklabels(datafr['platform'], rotation=45)
    plt.show()

except (Exception , psy.DatabaseError ) as error :
    print ( error )
finally :
    if co is not None:
        co.close ()