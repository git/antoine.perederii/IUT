import pandas as pd
import psycopg2 as psy
import getpass
import matplotlib.pyplot as plt

data = pd. read_csv (r'vgsales.csv')
df = pd.DataFrame(data)
df = df.drop_duplicates()

co = None

try:
    co = psy. connect(host='londres',
                        database ='dbanperederi',
                        user='anperederi',
                        password = getpass.getpass("Mot de passe:"))

    datafr = pd.read_sql('''SELECT ROUND(AVG(global_sales),2) as vente_moyennes, year 
                            FROM VGSales 
                            WHERE genre = 'Adventure' and year != 'NaN'
                            GROUP BY year
                            ORDER by year;''', con=co)
    
    fig = datafr.plot(x='year', y='vente_moyennes', kind='line', title='Evolution des ventes globales des jeux daventure', figsize=(10, 5), legend=True, fontsize=12)
    plt.show()

    datafr = pd.read_sql('''SELECT SUM(global_sales) as total_vente, year 
                            FROM VGSales 
                            WHERE genre = 'Adventure' AND year != 'NaN'
                            GROUP BY year
                            ORDER by year;''', con=co)
    
    fig = datafr.plot(x='year', y='total_vente', kind='line', title='Evolution des ventes globales des jeux', figsize=(10, 5), legend=True, fontsize=12)
    plt.show()

    datafr = pd.read_sql('''SELECT SUM(global_sales) as total_vente, year 
                            FROM VGSales 
                            WHERE year != 'NaN'
                            GROUP BY year
                            ORDER by year;''', con=co)
    
    fig = datafr.plot(x='year', y='total_vente', kind='line', title='Evolution des ventes globales des jeux', figsize=(10, 5), legend=True, fontsize=12)
    plt.show()

except (Exception , psy.DatabaseError ) as error :
    print ( error )
finally :
    if co is not None:
        co.close ()