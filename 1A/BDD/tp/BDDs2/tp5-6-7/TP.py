import pandas as pd
import psycopg2 as psy
import getpass

data = pd. read_csv (r'vgsales.csv')
df = pd.DataFrame(data)
df = df.drop_duplicates()

co = None

try:
    co = psy. connect(host='londres',
                        database ='dbanperederi',
                        user='anperederi',
                        password = getpass.getpass("Mot de passe:"))

    curs = co.cursor()

    curs. execute ('''DROP TABLE IF EXISTS VGSales ;''')

    curs. execute ('''CREATE TABLE VGSales (
                    name varchar (160) ,
                    platform varchar(160) ,
                    year numeric (4),
                    genre varchar (160) ,
                    publisher varchar (160) ,
                    na_sales numeric (5,2) DEFAULT 0,
                    eu_sales numeric (5,2) DEFAULT 0,
                    jp_sales numeric (5,2) DEFAULT 0,
                    other_sales numeric (5,2) DEFAULT 0,
                    global_sales numeric (5,2) DEFAULT 0 ,
                    PRIMARY KEY (name, platform, year)
                    );''')

    for row in df.itertuples ():
        curs. execute ('''INSERT INTO VGSales VALUES (%s ,%s ,%s ,%s, %s ,%s ,%s ,%s, %s ,%s);''',
                        (row.Name , row.Platform , row.Year , row.Genre , row.Publisher , row.NA_Sales , row.EU_Sales , row.JP_Sales , row.Other_Sales , row.Global_Sales ))

    #curs.execute('''UPDATE''')

    co.commit ()
    curs.close ()
except (Exception , psy.DatabaseError ) as error :
    print ( error )
finally :
    if co is not None:
        co.close ()


"""
Evolution du script:

Ajout de la fonction drop_duplicates() pour supprimer les doublons dans le fichier csv.

"""

