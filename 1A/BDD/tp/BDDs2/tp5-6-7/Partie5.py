import pandas as pd
import psycopg2 as psy
import getpass
import matplotlib.pyplot as plt

data = pd. read_csv (r'vgsales.csv')
df = pd.DataFrame(data)
df = df.drop_duplicates()

co = None

try:
    co = psy. connect(host='londres',
                        database ='dbanperederi',
                        user='anperederi',
                        password = getpass.getpass("Mot de passe:"))

#Q1 Écrire la requête SQL permettant de calculer le total des ventes de jeux dans le monde par genre.
    datafr = pd.read_sql('''SELECT round(sum(global_sales),2) as total_vente, genre 
                            FROM VGSales
                            GROUP BY genre
                            ORDER BY total_vente DESC;''', con=co)
    
    fig = datafr .plot.pie(y='total_vente', labels=datafr['genre'], autopct='%1.1f%%', figsize=(5, 5), legend=False) # Generation du graphique
    plt.show () # Affichage

#Q2 Écrire la requête SQL permettant de calculer e pourcentage des ventes dans le monde par genre.
    datafr = pd.read_sql('''SELECT (sum(global_sales)/(SELECT sum(global_sales) FROM VGSales))*100 as pourcentage
                            FROM VGSales
                            GROUP BY genre
                            ORDER BY pourcentage DESC;''', con=co)
    
    fig = datafr.plot(kind='pie', legend=False ,title='Ventes par plateforme', figsize=(5, 5), fontsize=12, subplots=True)
    plt.show()
    
    # fig = datafr .plot.pie(y='pourcentage', labels=datafr['genre'], autopct='%1.1f%%', figsize=(5, 5), legend=False) # Generation du graphique
    # plt.show () # Affichage

#Q3 Afficher les résultats sous forme de diagramme camembert.
    datafr = pd.read_sql('''SELECT (sum(global_sales)/(SELECT sum(global_sales) FROM VGSales))*100 as pourcentage 
                            FROM VGSales
                            GROUP BY genre
                            ORDER BY pourcentage DESC;''', con=co)
    
    fig = datafr.plot(kind='pie', legend=False ,title='Ventes par plateforme', figsize=(5, 5), fontsize=12, subplots=True)
    plt.show()
    
    # fig = datafr .plot.pie(y='pourcentage', labels=datafr['genre'], autopct='%1.1f%%', figsize=(5, 5), legend=False) # Generation du graphique
    # plt.show () # Affichage
    
#Q4 Afficher le total des ventes par genre sous forme de diagrammes camembert de façon à voir sur la même figure :
# un camembert représentant les ventes avant l’année 1990 (exclue),
# un camembert représentant les ventes entre 1990 et 1999,
# un camembert représentant les ventes entre 2000 et 2009,
# un camembert représentant les ventes à partir de 2010.
    datafr1 = pd.read_sql('''SELECT (sum(global_sales)/(SELECT sum(global_sales) FROM VGSales WHERE year < 1990))*100 as pourcentage
                            FROM VGSales
                            WHERE year < 1990
                            GROUP BY genre
                            ORDER BY pourcentage DESC;''', con=co)
    
    datafr2 = pd.read_sql('''SELECT (sum(global_sales)/(SELECT sum(global_sales) FROM VGSales WHERE year >= 1990 AND year < 2000))*100 as pourcentage
                            FROM VGSales
                            WHERE year >= 1990 AND year <= 1999
                            GROUP BY genre
                            ORDER BY pourcentage DESC;''', con=co)
    
    datafr3 = pd.read_sql('''SELECT (sum(global_sales)/(SELECT sum(global_sales) FROM VGSales WHERE year >= 2000 AND year < 2010))*100 as pourcentage
                            FROM VGSales
                            WHERE year >= 2000 AND year <= 2009
                            GROUP BY genre
                            ORDER BY pourcentage DESC;''', con=co)
    
    datafr4 = pd.read_sql('''SELECT (sum(global_sales)/(SELECT sum(global_sales) FROM VGSales WHERE year >= 2010))*100 as pourcentage
                            FROM VGSales
                            WHERE year >= 2010
                            GROUP BY genre
                            ORDER BY pourcentage DESC;''', con=co)
    
    _, axes = plt.subplots(nrows=2, ncols=2)
    
    fig = datafr1.plot(y=0, kind='pie', legend=False ,title='Ventes par plateforme', ax=axes[0,0])
    fig = datafr2.plot(y=0, kind='pie', legend=False ,title='Ventes par plateforme', ax=axes[0,1])
    fig = datafr3.plot(y=0, kind='pie', legend=False ,title='Ventes par plateforme', ax=axes[1,0])
    fig = datafr4.plot(y=0, kind='pie', legend=False ,title='Ventes par plateforme', ax=axes[1,1])
    plt.show()
    
    # fig = datafr .plot.pie(y='pourcentage', labels=datafr['genre'], autopct='%1.1f%%', figsize=(5, 5), legend=False) # Generation du graphique
    # plt.show () # Affichage

except (Exception , psy.DatabaseError ) as error :
    print ( error )
finally :
    if co is not None:
        co.close ()