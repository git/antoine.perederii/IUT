import pandas as pd
import psycopg2 as psy
import getpass
import matplotlib.pyplot as plt

data = pd. read_csv (r'vgsales.csv')
df = pd.DataFrame(data)
df = df.drop_duplicates()

co = None

try:
    co = psy. connect(host='londres',
                        database ='dbanperederi',
                        user='anperederi',
                        password = getpass.getpass("Mot de passe:"))

#Q2 Afficher sous forme de courbe les résultats de la requête précédente. 
    datafr = pd.read_sql('''SELECT na_sales as total_vente_na, eu_sales as total_vente_eu, jp_sales as total_vente_jp, other_sales as total_vente_autre
                            FROM VGSales
                            WHERE name = 'Mario Kart 64';''', con=co)
    
    fig = datafr.transpose().plot(kind='bar', title='Ventes par plateforme', figsize=(10, 5), legend=True, fontsize=12)
    fig.set_xticklabels(['NA', 'EU', 'JP', 'OTHER'], rotation=45)
    plt.show()

#Q3 Même question sous forme de diagramme “camembert”. Les noms des zones devront être à côté
#de la tranche concernée.
    datafr = pd.read_sql('''SELECT na_sales as total_vente_na, eu_sales as total_vente_eu, jp_sales as total_vente_jp, other_sales as total_vente_autre
                            FROM VGSales
                            WHERE name = 'Mario Kart 64';''', con=co)
    
    fig = datafr.transpose().plot(kind='pie', legend=False ,title='Ventes par plateforme', figsize=(10, 5), fontsize=12, subplots=True)
    plt.show()

#Q4 Même question mais le pourcentage de ventes dans chaque zone doit apparaître à côté de la
#tranche concernée et le nom de la zone doit apparaître dans la légende
    datafr = pd.read_sql('''SELECT na_sales as total_vente_na, eu_sales as total_vente_eu, jp_sales as total_vente_jp, other_sales as total_vente_autre
                            FROM VGSales
                            WHERE name = 'Mario Kart 64';''', con=co)
    
    fig = datafr.transpose().plot(kind='pie',title='Ventes par plateforme', figsize=(10, 5), fontsize=12, subplots=True, autopct='%1.1f%%', legend=False)
    plt.show()
    

except (Exception , psy.DatabaseError ) as error :
    print ( error )
finally :
    if co is not None:
        co.close ()