import pandas as pd
import psycopg2 as psy
import getpass

co = None

try:
    co = psy.connect (host='londres',
                      database ='dbanperederi',
                      user='anperederi',
                      password = getpass.getpass ("Mot de passe:"))
    df = pd.read_sql('''SELECT *
                    FROM VGSales ;''', con=co)
    print(df)
except (Exception , psy. DatabaseError ) as error :
    print ( error )
finally :
    if co is not None:
        co. close ()

#======================RESULTAT======================#
"""
                                                   name platform    year       genre  ... eu_sales  jp_sales  other_sales  global_sales
0                                            Wii Sports      Wii  2006.0      Sports  ...    29.02      3.77         8.46         82.74
1                                     Super Mario Bros.      NES  1985.0    Platform  ...     3.58      6.81         0.77         40.24
2                                     Wii Sports Resort      Wii  2009.0      Sports  ...    11.01      3.28         2.96         33.00
3                                                Tetris       GB  1989.0      Puzzle  ...     2.26      4.22         0.58         30.26
4                                 New Super Mario Bros.       DS  2006.0    Platform  ...     9.23      6.50         2.90         30.01
..                                                 ...      ...     ...         ...  ...      ...       ...          ...           ...
16591                                    K-1 Grand Prix       PS  1999.0    Fighting  ...     0.01      0.00         0.00          0.02
16592                           Carmageddon: Max Damage     XOne  2016.0      Action  ...     0.01      0.00         0.00          0.02
16593                  The Ultimate Battle of the Sexes      Wii  2010.0        Misc  ...     0.01      0.00         0.00          0.02
16594             Help Wanted: 50 Wacky Jobs (jp sales)      Wii  2008.0  Simulation  ...     0.01      0.01         0.00          0.02
16595  SCORE International Baja 1000: The Official Game      PS2  2008.0      Racing  ...     0.00      0.00         0.00          0.00

[16596 rows x 10 columns]

"""