DROP TABLE REPAS;
DROP TABLE AMOUR;
DROP TABLE DRAGON;
DROP TABLE NOURRITURE;
DROP TABLE TERRITOIRE;

\! clear

CREATE TABLE TERRITOIRE(
    num_terr char(3) PRIMARY KEY,
    nom varchar(30) UNIQUE,
    longitude numeric NOT NULL CHECK (longitude <= 180 AND longitude >= -180),
    latitude1 numeric NOT NULL CHECK (latitude1 <= 90),
    latitude2 char(1) NOT NULL CHECK (latitude2 ='S' OR latitude2 ='N')
);
CREATE TABLE DRAGON(
    numD char(5) PRIMARY KEY,
    nom varchar(30) UNIQUE,
    longeur numeric NOT NULL CHECK (longeur > 0),
    sexe char(1) NOT NULL CHECK (sexe ='F' OR sexe ='M'),
    nb_ecailles numeric CHECK (nb_ecailles > 0),
    date_naissance date,
    en_amour varchar(20) NOT NULL CHECK (en_amour ='macho' AND sexe='M' OR en_amour='timide' OR en_amour='sincere' OR en_amour='volage'),
    crache_feu char(1) CHECK (crache_feu ='O' OR crache_feu ='N'),
    num_terr char(4) REFERENCES TERRITOIRE
);

CREATE TABLE NOURRITURE(
    numN char(5)PRIMARY KEY,
    nom varchar(30) UNIQUE,
    calories numeric NOT NULL CHECK (calories >0)

);
CREATE TABLE AMOUR (
    force varchar(20) CHECK (force='un peu' OR force='beaucoup' OR force='passionnement' OR force='a la folie'),
    numDragon1 char(5) REFERENCES DRAGON,
    numDragon2 char(5) REFERENCES DRAGON,
    PRIMARY KEY (numDragon1,numDragon2)
);

CREATE TABLE REPAS(
    date_repas date,
    qte  numeric NOT NULL CHECK (qte >0),
    numD char(5) REFERENCES DRAGON,
    numN char(5) REFERENCES NOURRITURE,
    PRIMARY KEY (numD,numN)
);

INSERT INTO TERRITOIRE VALUES('T01', 'terre brûlées', 92, 40, 'S');
INSERT INTO TERRITOIRE VALUES ('T02', 'Terre de fleurs', 98, 48, 'S');
INSERT INTO TERRITOIRE VALUES ('T03', 'Fleur de naiges', 100, 8, 'N');

INSERT INTO DRAGON VALUES('D0001', 'Smeagol', 152, 'M', 1857,'14/06/1985', 'macho', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0002', 'Birduth', 258, 'M', 4787, '05/05/1989', 'timide', 'N', 'T01');
INSERT INTO DRAGON VALUES ('D0003', 'Negueth', 128,'F',1582,'08/08/1992', 'sincere', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0004', 'Miss Toc', 183,'F',2781,'04/07/2020', 'volage', NULL, 'T01');
INSERT INTO DRAGON VALUES ('D0005', 'Bolong', 213,'M',754,'06/05/2010', 'macho', 'N', 'T01');
INSERT INTO DRAGON VALUES ('D0006', 'Miloch', 83,'M',718,'29/04/2015', 'timide', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0007', 'Nessie', 168,'M',1721,'12/12/2005', 'macho', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0008', 'Tarak', 123,'F',851,'15/04/2009', 'timide', 'N', 'T03');
INSERT INTO DRAGON VALUES ('D0009', 'Solong', 173,'M',1481,'04/08/2021', 'timide', NULL, 'T01');

INSERT INTO NOURRITURE VALUES ('P0001', 'Pomme', '7');
INSERT INTO NOURRITURE VALUES ('P0002', 'Cacahuète', '10');
INSERT INTO NOURRITURE VALUES ('P0003', 'Orange', '25');
INSERT INTO NOURRITURE VALUES ('P0004', 'Oeuf', '15');
INSERT INTO NOURRITURE VALUES ('P0005', 'Ver', '3');
INSERT INTO NOURRITURE VALUES ('P0006', 'Poisson', '35');

INSERT INTO AMOUR VALUES ('passionnement', 'D0001', 'D0008');
INSERT INTO AMOUR VALUES ('beaucoup', 'D0002', 'D0003');
INSERT INTO AMOUR VALUES ('a la folie', 'D0003', 'D0006');
INSERT INTO AMOUR VALUES ('a la folie', 'D0006', 'D0003');
INSERT INTO AMOUR VALUES ('un peu', 'D0008', 'D0005');
INSERT INTO AMOUR VALUES ('beaucoup', 'D0005', 'D0008');
INSERT INTO AMOUR VALUES ('un peu', 'D0007', 'D0008');

INSERT INTO REPAS VALUES ('10/09/2021', 1000, 'D0001', 'P0002');
INSERT INTO REPAS VALUES ('10/09/2021', 16, 'D0001', 'P0001');
INSERT INTO REPAS VALUES ('11/09/2021', 4, 'D0005', 'P0004');
INSERT INTO REPAS VALUES ('10/09/2021', 6, 'D0003', 'P0003');
INSERT INTO REPAS VALUES ('11/09/2021', 1, 'D0003', 'P0004');
INSERT INTO REPAS VALUES ('10/09/2021', 53, 'D0006', 'P0005');
INSERT INTO REPAS VALUES ('11/09/2021', 100, 'D0006', 'P0002');
INSERT INTO REPAS VALUES ('10/09/2021', 20, 'D0007', 'P0006');
INSERT INTO REPAS VALUES ('10/09/2021', 10, 'D0008', 'P0001');
INSERT INTO REPAS VALUES ('11/09/2021', 10, 'D0008', 'P0003');
INSERT INTO REPAS VALUES ('10/09/2021', 1, 'D0009', 'P0006');
INSERT INTO REPAS VALUES ('11/09/2021', 2, 'D0009', 'P0003');
INSERT INTO REPAS VALUES ('12/09/2021', 8, 'D0009', 'P0004');
INSERT INTO REPAS VALUES ('25/09/2021', 20, 'D0006', 'P0003');

/*      TP9 */

INSERT INTO AMOUR VALUES('un peu', 'D0008', 'D0002');

/*  Question 1  */
SELECT Dragon.nom FROM Dragon WHERE num NOT IN (SELECT num_dragon FROM Repas WHERE num_nourriture = (SELECT num FROM Nourriture WHERE nom = 'Pomme'));

-- Question 2
SELECT Dragon.nom FROM Dragon WHERE num NOT IN (SELECT num_dragon1 FROM Amour);

-- Question 3
SELECT Dragon.nom FROM Dragon WHERE num NOT IN (SELECT num_dragon FROM Repas);

-- Question 4
SELECT Dragon.nom FROM Dragon WHERE longueur > (SELECT longueur FROM Dragon WHERE nom = 'Smeagol');

-- Question 5
SELECT Dragon.nom FROM Dragon WHERE num NOT IN (SELECT num_dragon1 FROM Amour) AND num NOT IN (SELECT num_dragon FROM Repas);

-- Question 6
SELECT Dragon.nom FROM Dragon WHERE sexe=0 AND num IN (SELECT num_dragon2 FROM Amour WHERE num_dragon1 NOT IN (SELECT num_dragon FROM Repas));

-- Question 7
SELECT Nourriture.nom FROM Nourriture WHERE num NOT IN (SELECT num_nourriture FROM Repas WHERE num_dragon IN (SELECT num FROM Dragon WHERE crache_feu = 'N'));

-- Question 8
SELECT nom, nb_ecailles FROM Dragon WHERE EXISTS (SELECT * FROM Dragon WHERE nb_ecailles > 500);

-- Question 9
SELECT nom, nb_ecailles FROM Dragon WHERE nb_ecailles > 500 AND sexe = 1 AND EXISTS (SELECT * FROM Dragon WHERE nb_ecailles > 5000);

-- Question 10
SELECT nom FROM Dragon WHERE NOT EXISTS (SELECT * FROM Amour WHERE num_dragon2 = num);

-- Question 11
SELECT Dragon.nom FROM Dragon, Territoire WHERE Dragon.num_terr = Territoire.num_terr AND latitude2 = 'S';

-- Question 12
SELECT Dragon.nom FROM Dragon WHERE num IN (SELECT num_dragon1 FROM Amour WHERE force = 'un peu');

-- Question 13
SELECT Dragon.nom FROM Dragon WHERE num IN (SELECT num_dragon FROM Amour) AND crache_feu = 'O' AND num_terr IN (SELECT num_terr FROM Territoire WHERE longitude > 85 AND longitude < 95);

-- Question 14
SELECT Nourriture.nom FROM Nourriture WHERE num IN (SELECT num_nourriture FROM Repas WHERE num_dragon IN (SELECT num FROM Dragon WHERE longueur > 200
AND num IN (SELECT num_dragon2 FROM Amour WHERE force = 'un peu')));

-- Question 15
SELECT Dragon.nom FROM Dragon WHERE crache_feu='O' AND num IN (SELECT num_dragon FROM Repas WHERE num_nourriture IN (SELECT num FROM Nourriture WHERE nom = 'Oeuf' OR nom = 'Orange'));

-- Question 16
SELECT Dragon.nom FROM Dragon WHERE crache_feu='O' AND num IN (SELECT num_dragon FROM Repas WHERE num_nourriture IN (SELECT num FROM Nourriture WHERE nom = 'Oeuf' OR nom = 'Orange')) 
AND num_terr='T02';

\d
