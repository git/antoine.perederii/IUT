DROP TABLE IF EXISTS Medecin, Patient, Consultation, Medicament, Laboratoire ,Posologie ,Fournir;

\! clear;

CREATE TABLE Medecin (
	id_medecin numeric PRIMARY KEY,
	nom varchar NOT NULL,
	prenom varchar NOT NULL
);

CREATE TABLE Patient (
	num_pat numeric PRIMARY KEY,
	nom varchar NOT NULL,
	prenom varchar NOT NULL,
	age numeric CHECK (age is NULL OR age > 0)
);

CREATE TABLE Consultation (
	num_cons varchar PRIMARY KEY CHECK (num_cons LIKE 'C%'),
	date date,
	heure time,
	id_medecin numeric, 
	num_pat numeric,
	FOREIGN KEY (id_medecin) REFERENCES Medecin,
	FOREIGN KEY (num_pat) REFERENCES Patient
);


CREATE TABLE Medicament (
	num_med numeric PRIMARY KEY,
	nom varchar NOT NULL UNIQUE,
	prix numeric CHECK (prix > 0) 
);

CREATE TABLE Posologie (
	matin numeric CHECK (matin is NULL or matin > 0),
	midi numeric CHECK (midi is NULL or midi > 0),
	soir numeric CHECK (soir is NULL or soir > 0),
	num_cons varchar,
	num_med numeric,
	PRIMARY KEY(num_cons, num_med),
	FOREIGN KEY (num_med) REFERENCES Medicament,
	FOREIGN KEY (num_cons) REFERENCES Consultation
);

CREATE TABLE Laboratoire (
	num_lab numeric PRIMARY KEY,
	nom varchar NOT NULL UNIQUE,
	rue numeric,
	code_postal numeric(5) CHECK ((code_postal IS NOT NULL) = (pays = 'France')), /*<=> (code_post IS NOT NULL AND pays IS 'France') OR (code_postal IS NULL AND Pays IS NOT 'France')*/
	ville varchar,
	pays varchar DEFAULT ('France')
);

CREATE TABLE Fournir (
	num_lab numeric,
	num_med numeric,
	PRIMARY KEY (num_lab, num_med),
	FOREIGN KEY (num_lab) REFERENCES Laboratoire,
	FOREIGN KEY (num_med) REFERENCES Medicament
);

INSERT INTO Medecin VALUES (1, 'DUPONT', 'Jean');
INSERT INTO Medecin VALUES (2, 'DURAND', 'Pierre');
INSERT INTO Medecin VALUES (3, 'DUPOND', 'Paul');
INSERT INTO Medecin VALUES (4, 'DUPUIS', 'Jacques');
INSERT INTO Medecin VALUES (5, 'DUPONT', 'Pierre');

INSERT INTO Patient VALUES(1, 'Bertrand', 'François', 25);
INSERT INTO Patient VALUES(2, 'Diner', 'Jinette', 18);
INSERT INTO Patient VALUES(3, 'Bouhours', 'Patrick', 30);
INSERT INTO Patient VALUES(4, 'Framabour', 'Gerard', 28);
INSERT INTO Patient VALUES(5, 'Delobel', 'Matthieu', 50);
INSERT INTO Patient VALUES(6, 'Dufour', 'Jean', 15);
INSERT INTO Patient VALUES(7, 'Dufour', 'Pierre', 12);

INSERT INTO Consultation VALUES('C1', '2015-01-01', '10:00', 1, 1);
INSERT INTO Consultation VALUES('C2', '2015-01-01', '10:00', 1, 2);
INSERT INTO Consultation VALUES('C3', '2015-01-01', '10:00', 2, 3);
INSERT INTO Consultation VALUES('C4', '2015-01-01', '10:00', 3, 4);
INSERT INTO Consultation VALUES('C5', '2015-01-01', '10:00', 2, 5);

INSERT INTO Medicament VALUES(1, 'Aspirine', 10);
INSERT INTO Medicament VALUES(2, 'Paracétamol', 25);
INSERT INTO Medicament VALUES(6, 'Antistase', 48);
INSERT INTO Medicament VALUES(7, 'Marimosane', 98);
INSERT INTO Medicament VALUES(3, 'Doliprane', 115);
INSERT INTO Medicament VALUES(4, 'Maxilase', 100);
INSERT INTO Medicament VALUES(5, 'Amoxiciline', 125);

INSERT INTO Posologie VALUES(1, 1, 1, 'C1', 1);
INSERT INTO Posologie VALUES(2, 2, 0, 'C2', 2);
INSERT INTO Posologie VALUES(3, 3, 3, 'C3', 3);
INSERT INTO Posologie VALUES(4, 4, 4, 'C4', 4);
INSERT INTO Posologie VALUES(15, 5, 5, 'C5', 5);

INSERT INTO Laboratoire VALUES(1, 'Laboratoire A', 1, 75001, 'Paris', 'France');
INSERT INTO Laboratoire VALUES(2, 'Laboratoire B', 2, 75002, 'Paris', 'France');
INSERT INTO Laboratoire VALUES(3, 'Laboratoire C', 3, NULL, 'Paris', 'France');
INSERT INTO Laboratoire VALUES(4, 'Laboratoire D', 4, 69400, 'Lyon', 'France');
INSERT INTO Laboratoire VALUES(5, 'Laboratoire E', 5, 15000, 'Aurillac', 'France');
INSERT INTO Laboratoire VALUES(6, 'Laboratoire F', 6, NULL, 'Barcelone', 'Espagne');
INSERT INTO Laboratoire VALUES(7, 'Laboratoire G', 7, 63500, 'Bioude', 'France');
INSERT INTO Laboratoire VALUES(8, 'Laboratoire RU', 9, NULL, 'Londres', 'Royaume-Unis');


-- Question 3
SELECT * FROM Medecin ORDER BY nom, prenom ASC;

-- Question 4
SELECT * FROM Patient WHERE age < 18 ORDER BY nom, prenom DESC;

-- Question 5
SELECT Consultation.* FROM Consultation JOIN Posologie ON Consultation.num_cons = Posologie.num_cons WHERE soir IS NOT NULL;

-- Question 6
SELECT Consultation.* FROM Consultation JOIN Posologie ON Consultation.num_cons = Posologie.num_cons WHERE matin > (midi + soir);

-- Question 7
SELECT * FROM Laboratoire WHERE pays = 'France' AND code_postal IS NOT NULL;

-- Question 8-9  a revoir  c'est les meme question ???!!!
-- Requette sql pour afficher les laboratoires qui ne sont pas en France ou dont le code postal ne commence pas par 63
SELECT * FROM Laboratoire WHERE pays != 'France' OR code_postal NOT LIKE '63%';

-- Question 10
SELECT * FROM Medicament WHERE prix > 50;

-- Question 11
SELECT * FROM Medicament WHERE prix * 1.1 > 50;

-- Question 12  à revoir c'est pas possible comme le dit la question !!
SELECT * FROM Medicament WHERE prix < 100 AND prix*1.2 > 125;

-- Question 13


-- Question 14  is good ??
SELECT DISTINCT pays FROM Laboratoire;

\d
