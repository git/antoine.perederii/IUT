DROP TABLE IF EXISTS TERRITOIRE, DRAGON, NOURRITURE, REPAS, AMOUR;
CREATE TABLE TERRITOIRE(
    num_terr        char(3) PRIMARY KEY,
    nom             char(20),
    longitude       numeric,
    latitude1        numeric,
    latitude2        char(2)
);
CREATE TABLE DRAGON(
    num             char(5) PRIMARY KEY,
    nom             char(30),
    longueur        numeric,
    sexe            numeric,
    nb_ecailles     numeric,
    date_naissance  date,
    en_amour        char(20),
    crache_feu      char(1),
    num_terr char(4) REFERENCES TERRITOIRE
);
CREATE TABLE NOURRITURE(
    num              char(3) PRIMARY KEY,
    nom              varchar(20),
    calories         numeric
);
CREATE TABLE AMOUR(
    force           varchar(20),
    num_dragon1     char(5),
    num_dragon2     char(5),
    PRIMARY KEY (num_dragon1, num_dragon2),
    FOREIGN KEY (num_dragon1) REFERENCES DRAGON(num),
    FOREIGN KEY (num_dragon2) REFERENCES DRAGON(num)
);
CREATE TABLE REPAS(
    qte             numeric,
    date_repas      date,
    num_nourr       char(5),
    num_dragon      char(5),
    PRIMARY KEY (num_nourr, num_dragon),
    FOREIGN KEY (num_nourr) REFERENCES NOURRITURE(num),
    FOREIGN KEY (num_dragon) REFERENCES DRAGON(num)
);

\d

/*SELECT * FROM TERRITOIRE;
SELECT * FROM DRAGON;
SELECT * FROM NOURRITURE;
SELECT * FROM AMOUR;
SELECT * FROM REPAS;*/