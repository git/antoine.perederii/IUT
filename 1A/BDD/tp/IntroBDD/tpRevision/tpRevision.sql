-- DROP TABLE IF EXISTS FAIRE, INTERVENTION, REPARATION, TECHNICIEN, CLIENT CASCADE;
DROP TABLE FAIRE;
DROP TABLE INTERVENTION;
DROP TABLE REPARATION;
DROP TABLE TECHNICIEN;
DROP TABLE CLIENT;


CREATE TABLE CLIENT (
    noClient numeric PRIMARY KEY,
    nom varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL,
    rue varchar(30) NOT NULL,
    codePostal char(5) NOT NULL,
    ville varchar(30) NOT NULL,
    noTelephone char(10) NOT NULL
);

CREATE TABLE TECHNICIEN (
    noEmploye numeric PRIMARY KEY,
    nom varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL
);

CREATE TABLE INTERVENTION (
    noIntervention numeric PRIMARY KEY,
    date date NOT NULL,
    noClient numeric NOT NULL REFERENCES CLIENT(noClient),
    noEmploye numeric NOT NULL REFERENCES TECHNICIEN(noEmploye)
);

CREATE TABLE REPARATION (
    codeReparation numeric PRIMARY KEY,
    designation varchar(30) NOT NULL,
    prix numeric(6,2) NOT NULL
);

CREATE TABLE FAIRE (
    noIntervention numeric NOT NULL REFERENCES INTERVENTION(noIntervention),
    codeReparation numeric NOT NULL REFERENCES REPARATION(codeReparation),
    PRIMARY KEY (noIntervention, codeReparation)
);

INSERT INTO CLIENT VALUES (1, 'DUPONT', 'Jean', 'Rue des Lilas', '75001', 'Paris', '0123456789');
INSERT INTO CLIENT VALUES (2, 'DURAND', 'Marie', 'Rue des Roses', '63002', 'Clermont-Ferrand', '0234567890');
INSERT INTO CLIENT VALUES (3, 'DUPOND', 'Pierre', 'Rue des Marguerites', '63003', 'Clermont-Ferrand', '0345678901');

SELECT * FROM CLIENT WHERE ville='Clermont-Ferrand' ORDER BY noTelephone;