DROP TABLE CONTENIR;
DROP TABLE POTION;
DROP TABLE OBTENIR;
DROP TABLE INGREDIENT;
DROP TABLE SOURCES;

\! clear

/*      fAIRE ATTENTION À L'ORDRE ET PAS TROP FAIRE LES DROP TABLE ENSEMBLE !!! */

CREATE TABLE SOURCES(
    idSources       char(4) PRIMARY KEY,
    nom             varchar(20) NOT NULL UNIQUE,
    lieu            varchar(50),
    dateCollecte    date
);

CREATE TABLE INGREDIENT(
    idIngredient    char(4) PRIMARY KEY,
    designation     varchar(50) NOT NULL UNIQUE,
    idSOURCES char(4) REFERENCES SOURCES
);

CREATE TABLE POTION(
    idPotion char(4) PRIMARY KEY,
    effet    varchar(50) DEFAULT 'INCONNU'
);

CREATE TABLE CONTENIR(
    idPotion    char(4) REFERENCES POTION,
    idIngredient char(4) REFERENCES INGREDIENT,
    quantite    numeric(3) CHECK(quantite > 0),
    unite       varchar(2)
);

CREATE TABLE OBTENIR(
    transformation      char(10) DEFAULT 'Issues',
    idIngredientBase        char(4) REFERENCES INGREDIENT,
    idIngredientObtenue     char(4) REFERENCES INGREDIENT
);

INSERT INTO SOURCES VALUES ('S001','Abeille sauvage','plaines verdoyantes','22/09/2021');
INSERT INTO SOURCES VALUES ('S002','Branchiflore','marais sombre','08/09/2021');
INSERT INTO SOURCES VALUES ('S003','Licorne','montagne dorée','16/04/2020');
INSERT INTO SOURCES VALUES ('S004','Mandragore','forêt de l Est','12/10/2021');
INSERT INTO SOURCES VALUES ('S005','Pin bleu','forêt de l Est','10/10/2021');
INSERT INTO SOURCES VALUES ('S006','Sirène','mer d opale','03/02/2021');
INSERT INTO SOURCES VALUES ('S007','Tortue géante','mer d opale','03/02/2021');

INSERT INTO INGREDIENT VALUES('I001', 'poudre de Mandragore', 'S004');
INSERT INTO INGREDIENT VALUES('I002', 'larmes de Sirene', 'S006');
INSERT INTO INGREDIENT VALUES('I003', 'Miel', 'S001');
INSERT INTO INGREDIENT VALUES('I004', 'poils de Licorne', 'S003');
INSERT INTO INGREDIENT VALUES('I005', 'seve de Pin', 'S005');
INSERT INTO INGREDIENT VALUES('I006', 'graines de Branchiflore', 'S002');
INSERT INTO INGREDIENT VALUES('I007', 'ecailles de tortues', 'S007');
INSERT INTO INGREDIENT VALUES('I008', 'racine de Mandragore', 'S004');
INSERT INTO INGREDIENT VALUES('I009', 'rayon de miel d Abeille sauvage', 'S001');
INSERT INTO INGREDIENT VALUES('I010', 'Pins bleus', 'S005');
INSERT INTO INGREDIENT VALUES('I011', 'Branchiflores', 'S002');
INSERT INTO INGREDIENT VALUES('I012', 'Tortues géantes', 'S007');
INSERT INTO INGREDIENT VALUES('I013', 'Mandragore', 'S004');
INSERT INTO INGREDIENT VALUES('I014', 'Sirene', 'S006');
INSERT INTO INGREDIENT VALUES('I015', 'Licorne', 'S003');


INSERT INTO POTION VALUES ('P001','Invisibilité');
INSERT INTO POTION VALUES ('P002','Force');

INSERT INTO CONTENIR VALUES ('P001','I001',20,'g');
INSERT INTO CONTENIR VALUES ('P001','I002',2,'u');
INSERT INTO CONTENIR VALUES ('P001','I003',20,'cl');
INSERT INTO CONTENIR VALUES ('P001','I004',5,'g');
INSERT INTO CONTENIR VALUES ('P002','I005',10,'cl');
INSERT INTO CONTENIR VALUES ('P002','I006',3,'u');
INSERT INTO CONTENIR VALUES ('P002','I007',12,'g');
INSERT INTO CONTENIR VALUES ('P002','I008',1,'u');

INSERT INTO OBTENIR VALUES ('Broyage', 'I008','I001');
INSERT INTO OBTENIR VALUES ('Extraction', 'I008', 'I003');
INSERT INTO OBTENIR VALUES (NULL, 'I013', 'I008');
INSERT INTO OBTENIR VALUES (NULL, 'I014', 'I002');
INSERT INTO OBTENIR VALUES (NULL, 'I015', 'I004');
INSERT INTO OBTENIR VALUES (NULL, 'I010', 'I005');
INSERT INTO OBTENIR VALUES (NULL, 'I011', 'I006');
INSERT INTO OBTENIR VALUES (NULL, 'I012', 'I007');



SELECT * FROM CONTENIR;
SELECT * FROM POTION;
SELECT * FROM OBTENIR;
SELECT * FROM INGREDIENT;
SELECT * FROM SOURCES;

/*  Question 3  */
SELECT * FROM POTION ORDER BY effet;

/*  Question 4  */
SELECT DISTINCT lieu FROM SOURCES;

/*  Question 5  */
/*  */

SELECT * FROM Potion ORDER BY effet ASC;
SELECT DISTINCT lieu FROM Source JOIN Ingredient ON Source.idSource=Ingredient.idSource WHERE EXISTS(SELECT idSource from Ingredient WHERE Source.idSource = Ingredient.idSource);
SELECT idIngredient2, transformation FROM Transformation WHERE idIngredient='I009';
SELECT idSource, nom FROM Source WHERE (current_date-dateCollecte > 45);
SELECT round(quantite, 2) || ' ' || upper(unite) || ' ' || idIngredient AS tkt FROM Contenir WHERE idPotion='P001';
SELECT idIngredient, designation FROM Ingredient WHERE designation LIKE '%Poudre%';


\d