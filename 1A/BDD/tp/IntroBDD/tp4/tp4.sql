DROP TABLE REPAS;
DROP TABLE AMOUR;
DROP TABLE DRAGON;
DROP TABLE NOURRITURE;
DROP TABLE TERRITOIRE;

\! clear

CREATE TABLE TERRITOIRE(
    num_terr char(3) PRIMARY KEY,
    nom varchar(30) UNIQUE,
    longitude numeric NOT NULL CHECK (longitude <= 180 AND longitude >= -180),
    latitude1 numeric NOT NULL CHECK (latitude1 <= 90),
    latitude2 char(1) NOT NULL CHECK (latitude2 ='S' OR latitude2 ='N')
);
CREATE TABLE DRAGON(
    numD char(5) PRIMARY KEY,
    nom varchar(30) UNIQUE,
    longeur numeric NOT NULL CHECK (longeur > 0),
    sexe char(1) NOT NULL CHECK (sexe ='F' OR sexe ='M'),
    nb_ecailles numeric CHECK (nb_ecailles > 0),
    date_naissance date,
    en_amour varchar(20) NOT NULL CHECK (en_amour ='macho' AND sexe='M' OR en_amour='timide' OR en_amour='sincere' OR en_amour='volage'),
    crache_feu char(1) CHECK (crache_feu ='O' OR crache_feu ='N'),
    num_terr char(4) REFERENCES TERRITOIRE
);

CREATE TABLE NOURRITURE(
    numN char(5)PRIMARY KEY,
    nom varchar(30) UNIQUE,
    calories numeric NOT NULL CHECK (calories >0)

);
CREATE TABLE AMOUR (
    force varchar(20) CHECK (force='un peu' OR force='beaucoup' OR force='passionnement' OR force='a la folie'),
    numDragon1 char(5) REFERENCES DRAGON,
    numDragon2 char(5) REFERENCES DRAGON,
    PRIMARY KEY (numDragon1,numDragon2)
    

);

CREATE TABLE REPAS(
    date_repas date,
    qte  numeric NOT NULL CHECK (qte >0),
    numD char(5) REFERENCES DRAGON,
    numN char(5) REFERENCES NOURRITURE,
    PRIMARY KEY (numD,numN)
);

INSERT INTO TERRITOIRE VALUES('T01', 'terre brûlées', 92, 40, 'S');
INSERT INTO Territoire VALUES ('T02', 'Terre de fleurs', 98, 48, 'S');
INSERT INTO Territoire VALUES ('T03', 'Fleur de naiges', 100, 8, 'N');

INSERT INTO dragon VALUES('D0001', 'Smeagol', 152, 'M', 1857,'14/06/1985', 'macho', 'O', 'T02');
INSERT INTO dragon VALUES ('D0002', 'Birduth', 258, 'M', 4787, '05/05/1989', 'timide', 'N', 'T01');
INSERT INTO dragon VALUES ('D0003', 'Negueth', 128,'F',1582,'08/08/1992', 'sincere', 'O', 'T02');
INSERT INTO dragon VALUES ('D0004', 'Miss Toc', 183,'F',2781,'04/07/2020', 'volage', NULL, 'T01');
INSERT INTO dragon VALUES ('D0005', 'Bolong', 213,'M',754,'06/05/2010', 'macho', 'N', 'T01');
INSERT INTO dragon VALUES ('D0006', 'Miloch', 83,'M',718,'29/04/2015', 'timide', 'O', 'T02');
INSERT INTO dragon VALUES ('D0007', 'Nessie', 168,'M',1721,'12/12/2005', 'macho', 'O', 'T02');
INSERT INTO dragon VALUES ('D0008', 'Tarak', 123,'F',851,'15/04/2009', 'timide', 'N', 'T01');
INSERT INTO dragon VALUES ('D0009', 'Solong', 173,'M',1481,'04/08/2021', 'timide', NULL, 'T01');

INSERT INTO nourriture VALUES ('P0001', 'Pomme', '7');
INSERT INTO nourriture VALUES ('P0002', 'Cacahuète', '10');
INSERT INTO nourriture VALUES ('P0003', 'Orange', '25');
INSERT INTO nourriture VALUES ('P0004', 'Oeuf', '15');
INSERT INTO nourriture VALUES ('P0005', 'Ver', '3');
INSERT INTO nourriture VALUES ('P0006', 'Poisson', '35');

INSERT INTO Amour VALUES ('passionnement', 'D0001', 'D0008');
INSERT INTO Amour VALUES ('beaucoup', 'D0002', 'D0003');
INSERT INTO Amour VALUES ('a la folie', 'D0003', 'D0006');
INSERT INTO Amour VALUES ('a la folie', 'D0006', 'D0003');
INSERT INTO Amour VALUES ('un peu', 'D0008', 'D0005');
INSERT INTO Amour VALUES ('beaucoup', 'D0005', 'D0008');
INSERT INTO Amour VALUES ('un peu', 'D0007', 'D0008');

INSERT INTO Repas VALUES ('10/09/2021', 1000, 'D0001', 'P0002');
INSERT INTO Repas VALUES ('10/09/2021', 16, 'D0001', 'P0001');
INSERT INTO Repas VALUES ('11/09/2021', 4, 'D0005', 'P0004');
INSERT INTO Repas VALUES ('10/09/2021', 6, 'D0003', 'P0003');
INSERT INTO Repas VALUES ('11/09/2021', 1, 'D0003', 'P0004');
INSERT INTO Repas VALUES ('10/09/2021', 53, 'D0006', 'P0005');
INSERT INTO Repas VALUES ('11/09/2021', 100, 'D0006', 'P0002');
INSERT INTO Repas VALUES ('10/09/2021', 20, 'D0007', 'P0006');
INSERT INTO Repas VALUES ('10/09/2021', 10, 'D0008', 'P0001');
INSERT INTO Repas VALUES ('11/09/2021', 10, 'D0008', 'P0003');
INSERT INTO Repas VALUES ('09/09/2021', 6, 'D0009', 'P0004');
INSERT INTO Repas VALUES ('10/09/2021', 1, 'D0009', 'P0006');
INSERT INTO Repas VALUES ('11/09/2021', 2, 'D0009', 'P0003');

/*
INSERT INTO TERRITOIRE VALUES ('T01','terres_brulees',92,42,'S');
INSERT INTO TERRITOIRE VALUES ('T02','terre_des_fleurs',98,48,'S');
INSERT INTO TERRITOIRE VALUES ('T03','terre_des_neiges',100,8,'N');


INSERT INTO DRAGON VALUES ('D0001','Smeagol',152,'M',1857,'14/06/1985','macho','O','T02');
INSERT INTO DRAGON VALUES ('D0002','Birdurh',258,'M',4787,'05/05/1989','timide','N','T01');
INSERT INTO DRAGON VALUES ('D0003','Negueth',128,'M',1587,'08/09/2018','sincere','O','T02');
INSERT INTO DRAGON VALUES ('D0004','Miss Toc',183,'M',2781,'04/07/2020','volage',NULL,'T01');
INSERT INTO DRAGON VALUES ('D0005','Bolong',213,'M',754,'06/05/2010','macho','N','T01');
INSERT INTO DRAGON VALUES ('D0006','Miloch',83,'M',718,'29/04/2015','timide','O','T02');
INSERT INTO DRAGON VALUES ('D0007','Nessie',168,'M',1721,'12/12/2000','macho','O','T02');
INSERT INTO DRAGON VALUES ('D0008','Tarak',123,'F',851,'15/04/2009','timide','N','T01');
INSERT INTO DRAGON VALUES ('D0009','Solong',173,'M',1481,'04/08/2021','timide',NULL,'T01');


INSERT INTO NOURRITURE VALUES ('P0001','pomme',7);
INSERT INTO NOURRITURE VALUES ('P0002','cacahuete',10);
INSERT INTO NOURRITURE VALUES ('P0003','orange',25);
INSERT INTO NOURRITURE VALUES ('P0004','oeuf',15);
INSERT INTO NOURRITURE VALUES ('P0005','ver',3);
INSERT INTO NOURRITURE VALUES ('P0006','poisson',35);

INSERT INTO AMOUR VALUES ('passionnement','D0001','D0008');
INSERT INTO AMOUR VALUES ('beaucoup','D0002','D0003');
INSERT INTO AMOUR VALUES ('a la folie','D0003','D0006');
INSERT INTO AMOUR VALUES ('a la folie','D0006','D0003');
INSERT INTO AMOUR VALUES ('un peu','D0008','D0005');
INSERT INTO AMOUR VALUES ('beaucoup','D0005','D0008');
INSERT INTO AMOUR VALUES ('un peu','D0007','D0008');

INSERT INTO REPAS VALUES (1000,'10/09/2021','D0001','P0002');
INSERT INTO REPAS VALUES (16,'10/09/2021','D0001','P0001');
INSERT INTO REPAS VALUES (4,'11/09/2021','D0005','P0004');
INSERT INTO REPAS VALUES (6,'10/03/2021','D0003','P0003');
INSERT INTO REPAS VALUES (1,'11/09/2021','D0003','P0004');
INSERT INTO REPAS VALUES (53,'10/09/2021','D0006','P0005');
INSERT INTO REPAS VALUES (100,'11/09/2021','D0006','P0002');
INSERT INTO REPAS VALUES (20,'10/09/2021','D0007','P0006');
INSERT INTO REPAS VALUES (10,'10/09/2021','D0008','P0001');
INSERT INTO REPAS VALUES (10,'11/09/2021','D0008','P0003');
INSERT INTO REPAS VALUES (6,'09/09/2021','D0009','P0004');
INSERT INTO REPAS VALUES (1,'10/09/2021','D0009','P0006');
INSERT INTO REPAS VALUES (2,'11/09/2021','D0009','P0003');
*/

SELECT * FROM TERRITOIRE;
SELECT * FROM DRAGON;
SELECT * FROM NOURRITURE;
SELECT * FROM AMOUR;
SELECT * FROM REPAS;

\d
