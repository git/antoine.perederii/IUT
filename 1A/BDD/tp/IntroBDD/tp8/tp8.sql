DROP TABLE REPAS;
DROP TABLE AMOUR;
DROP TABLE DRAGON;
DROP TABLE NOURRITURE;
DROP TABLE TERRITOIRE;

\! clear

CREATE TABLE TERRITOIRE(
    num_terr char(3) PRIMARY KEY,
    nom varchar(30) UNIQUE,
    longitude numeric NOT NULL CHECK (longitude <= 180 AND longitude >= -180),
    latitude1 numeric NOT NULL CHECK (latitude1 <= 90),
    latitude2 char(1) NOT NULL CHECK (latitude2 ='S' OR latitude2 ='N')
);
CREATE TABLE DRAGON(
    numD char(5) PRIMARY KEY,
    nom varchar(30) UNIQUE,
    longeur numeric NOT NULL CHECK (longeur > 0),
    sexe char(1) NOT NULL CHECK (sexe ='F' OR sexe ='M'),
    nb_ecailles numeric CHECK (nb_ecailles > 0),
    date_naissance date,
    en_amour varchar(20) NOT NULL CHECK (en_amour ='macho' AND sexe='M' OR en_amour='timide' OR en_amour='sincere' OR en_amour='volage'),
    crache_feu char(1) CHECK (crache_feu ='O' OR crache_feu ='N'),
    num_terr char(4) REFERENCES TERRITOIRE
);

CREATE TABLE NOURRITURE(
    numN char(5)PRIMARY KEY,
    nom varchar(30) UNIQUE,
    calories numeric NOT NULL CHECK (calories >0)

);
CREATE TABLE AMOUR (
    force varchar(20) CHECK (force='un peu' OR force='beaucoup' OR force='passionnement' OR force='a la folie'),
    numDragon1 char(5) REFERENCES DRAGON,
    numDragon2 char(5) REFERENCES DRAGON,
    PRIMARY KEY (numDragon1,numDragon2)
);

CREATE TABLE REPAS(
    date_repas date,
    qte  numeric NOT NULL CHECK (qte >0),
    numD char(5) REFERENCES DRAGON,
    numN char(5) REFERENCES NOURRITURE,
    PRIMARY KEY (numD,numN)
);

INSERT INTO TERRITOIRE VALUES('T01', 'terre brûlées', 92, 40, 'S');
INSERT INTO TERRITOIRE VALUES ('T02', 'Terre de fleurs', 98, 48, 'S');
INSERT INTO TERRITOIRE VALUES ('T03', 'Fleur de naiges', 100, 8, 'N');

INSERT INTO DRAGON VALUES('D0001', 'Smeagol', 152, 'M', 1857,'14/06/1985', 'macho', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0002', 'Birduth', 258, 'M', 4787, '05/05/1989', 'timide', 'N', 'T01');
INSERT INTO DRAGON VALUES ('D0003', 'Negueth', 128,'F',1582,'08/08/1992', 'sincere', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0004', 'Miss Toc', 183,'F',2781,'04/07/2020', 'volage', NULL, 'T01');
INSERT INTO DRAGON VALUES ('D0005', 'Bolong', 213,'M',754,'06/05/2010', 'macho', 'O', 'T01');
INSERT INTO DRAGON VALUES ('D0006', 'Miloch', 83,'M',718,'29/04/2015', 'timide', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0007', 'Nessie', 168,'M',1721,'12/12/2005', 'macho', 'O', 'T02');
INSERT INTO DRAGON VALUES ('D0008', 'Tarak', 123,'F',851,'15/04/2009', 'timide', 'N', 'T03');
INSERT INTO DRAGON VALUES ('D0009', 'Solong', 173,'M',1481,'04/08/2021', 'timide', NULL, 'T01');

INSERT INTO NOURRITURE VALUES ('P0001', 'Pomme', '7');
INSERT INTO NOURRITURE VALUES ('P0002', 'Cacahuète', '10');
INSERT INTO NOURRITURE VALUES ('P0003', 'Orange', '25');
INSERT INTO NOURRITURE VALUES ('P0004', 'Oeuf', '15');
INSERT INTO NOURRITURE VALUES ('P0005', 'Ver', '3');
INSERT INTO NOURRITURE VALUES ('P0006', 'Poisson', '35');

INSERT INTO AMOUR VALUES ('passionnement', 'D0001', 'D0008');
INSERT INTO AMOUR VALUES ('beaucoup', 'D0002', 'D0003');
INSERT INTO AMOUR VALUES ('a la folie', 'D0003', 'D0006');
INSERT INTO AMOUR VALUES ('a la folie', 'D0006', 'D0003');
INSERT INTO AMOUR VALUES ('un peu', 'D0008', 'D0005');
INSERT INTO AMOUR VALUES ('beaucoup', 'D0005', 'D0008');
INSERT INTO AMOUR VALUES ('un peu', 'D0007', 'D0008');

INSERT INTO REPAS VALUES ('10/09/2021', 1000, 'D0001', 'P0002');
INSERT INTO REPAS VALUES ('10/09/2021', 16, 'D0001', 'P0001');
INSERT INTO REPAS VALUES ('11/09/2021', 4, 'D0005', 'P0004');
INSERT INTO REPAS VALUES ('10/09/2021', 6, 'D0003', 'P0003');
INSERT INTO REPAS VALUES ('11/09/2021', 1, 'D0003', 'P0004');
INSERT INTO REPAS VALUES ('10/09/2021', 53, 'D0006', 'P0005');
INSERT INTO REPAS VALUES ('11/09/2021', 100, 'D0006', 'P0002');
INSERT INTO REPAS VALUES ('10/09/2021', 20, 'D0007', 'P0006');
INSERT INTO REPAS VALUES ('10/09/2021', 10, 'D0008', 'P0001');
INSERT INTO REPAS VALUES ('11/09/2021', 10, 'D0008', 'P0003');
INSERT INTO REPAS VALUES ('10/09/2021', 1, 'D0009', 'P0006');
INSERT INTO REPAS VALUES ('11/09/2021', 2, 'D0009', 'P0003');
INSERT INTO REPAS VALUES ('12/09/2021', 8, 'D0009', 'P0004');
INSERT INTO REPAS VALUES ('25/09/2021', 20, 'D0006', 'P0003');

SELECT * FROM TERRITOIRE;
SELECT * FROM DRAGON;
SELECT * FROM NOURRITURE;
SELECT * FROM AMOUR;
SELECT * FROM REPAS;

/*      TP8*/

UPDATE DRAGON SET num_terr = 'T03' WHERE numD = 'D0008';

/*  Question 1  */
SELECT DRAGON.nom FROM DRAGON JOIN TERRITOIRE ON DRAGON.num_terr = TERRITOIRE.num_terr WHERE TERRITOIRE.latitude2 = 'S';

/*  Question 2  */
SELECT DRAGON.nom FROM DRAGON JOIN AMOUR ON DRAGON.numD = AMOUR.numDragon1 WHERE AMOUR.force = 'un peu';

/*  Question 3  */
SELECT DRAGON.nom FROM DRAGON JOIN REPAS ON DRAGON.numD = REPAS.numD JOIN NOURRITURE ON REPAS.numN = NOURRITURE.numN WHERE NOURRITURE.nom = 'Oeuf' AND sexe = 'M';

/*  Question 4  */
SELECT DISTINCT TERRITOIRE.NOM, TERRITOIRE.longitude, TERRITOIRE.latitude1 FROM TERRITOIRE JOIN DRAGON ON TERRITOIRE.num_terr = DRAGON.num_terr JOIN REPAS ON DRAGON.numD = REPAS.numD JOIN NOURRITURE ON REPAS.numN = NOURRITURE.numN WHERE NOURRITURE.nom = 'Orange';

/*  Question 5  */
SELECT REPAS.date_repas, REPAS.qte, NOURRITURE.nom, NOURRITURE.calories FROM REPAS JOIN NOURRITURE ON REPAS.numN = NOURRITURE.numN WHERE REPAS.numD = 'D0008' ORDER BY REPAS.date_repas;

/*  Question 6  */
SELECT NOURRITURE.numn, NOURRITURE.nom, TERRITOIRE.num_terr FROM NOURRITURE JOIN REPAS ON NOURRITURE.numn = REPAS.numn JOIN DRAGON ON REPAS.numd = DRAGON.numd JOIN TERRITOIRE ON DRAGON.num_terr = TERRITOIRE.num_terr ORDER BY REPAS.date_repas;

/*  Question 7  fonctionne pas !!!  pas de territoire !!??*/
SELECT DISTINCT d.nom
FROM Dragon d, Territoire t, Repas r
WHERE d.num = r.num_Drag AND d.num_terr = t.num_terr AND t.num_terr = 'T02' AND d.crache_feu = 'O' AND (r.num_Nourriture ='P0004' or r.num_Nourriture='P0003');

SELECT DISTINCT DRAGON.nom FROM DRAGON JOIN TERRITOIRE ON DRAGON.num_terr = TERRITOIRE.num_terr JOIN REPAS ON DRAGON.numD = REPAS.numD JOIN NOURRITURE ON REPAS.numN = NOURRITURE.numN WHERE DRAGON.crache_feu = 'O' AND TERRITOIRE.num_terr = 'T02' AND NOURRITURE.nom = 'Oeuf' OR NOURRITURE.nom = 'Orange' ;

/*  Question 8  */
SELECT AMOUR.force, DRAGON.nom FROM DRAGON JOIN AMOUR ON DRAGON.numD = AMOUR.numDragon1;

/*  Question 9  */
SELECT DRAGON.nom FROM DRAGON JOIN AMOUR ON DRAGON.numD = AMOUR.numDragon1 JOIN TERRITOIRE ON TERRITOIRE.num_terr = DRAGON.num_terr WHERE DRAGON.crache_feu='O' AND 85<TERRITOIRE.longitude AND TERRITOIRE.longitude<95;

/*  Question 10 */
SELECT DRAGON.nom FROM DRAGON JOIN AMOUR ON DRAGON.numD = AMOUR.numDragon1 WHERE AMOUR.force = 'passionnement' AND DRAGON.en_amour='macho';

/*  Question 11 Ne fonctionne pas !!!! */
SELECT d.nom
FROM Dragon d, Amour a , Repas r, Nourriture n
WHERE d.num = a.num_Drag2 AND d.num = r.num_Drag AND n.num = r.num_Nourriture AND  d.longueur>=2 AND n.num IN (r.num_Nourriture) AND a.force = 'un peu';

SELECT NOURRITURE.nom FROM NOURRITURE JOIN REPAS ON NOURRITURE.numn = REPAS.numN JOIN DRAGON ON REPAS.numD = DRAGON.numD JOIN AMOUR ON DRAGON.numD = AMOUR.numDragon1 WHERE DRAGON.longeur > 200 AND AMOUR.force = 'un peu';

/*  Question 12 Je sais pas, je comprend pas !!! */
SELECT  *
FROM Amour a1, Amour a2
WHERE a1.num_Drag1=a2.num_Drag2 AND a1.num_Drag2=a2.num_Drag1;

SELECT aimant.nom || ' est en couple avec ' || aime.nom FROM DRAGON AS aimant JOIN AMOUR ON aimant.numD = AMOUR.numDragon1 JOIN DRAGON AS aime ON AMOUR.numDragon2 = aime.numD;

/*  Question 13 */


/*  Question 14 */
SELECT DISTINCT DRAGON.nom FROM DRAGON JOIN REPAS ON DRAGON.numD = REPAS.numD JOIN NOURRITURE ON REPAS.numN = NOURRITURE.numN WHERE NOURRITURE.nom = 'Oeuf' AND DRAGON.numD NOT IN (SELECT DRAGON.numD FROM DRAGON JOIN REPAS ON DRAGON.numD = REPAS.numD JOIN NOURRITURE ON REPAS.numN = NOURRITURE.numN WHERE NOURRITURE.nom = 'Orange');


\d
