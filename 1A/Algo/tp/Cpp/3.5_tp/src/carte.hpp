#ifndef CARTE_HPP
#define CARTE_HPP

#include <iostream>

enum class Couleur {
	BLEU,
	ROUGE, 
	VERT,
	ROSE,
	JAUNE,
	ORANGE,
	NOIR
};

enum class Nom {
	ROI,
	RENNE,
	CAVALIER,
	DRAGON,
	ENCHANTEUR,
	MOINE,
	PAYSAN,
	SORCIER
};

class Carte {

	Nom nom;
	Couleur couleur;

public:

	Carte(Nom nom, Couleur couleur);

};

#endif // CARTE_HPP