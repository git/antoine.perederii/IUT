#ifndef JOUEUR_HPP
#define JOUEUR_HPP

#include <iostream>
#include <list>
#include "carte.hpp"

class Joueur {

    std::string pseudo;
    list<Carte> lesCartes; 

public:

	Joueur(std::string pseudo);

    std::string getPseudo();

    Carte donnerCarte(int num);
    bool recevoirCarte(Carte c);
    void prendreCarte(Joueur j);

	~Joueur()=default;

};

#endif // JOUEUR_HPP