#ifndef INDIEN_HPP
#define INDIEN_HPP

#include <iostream>
#include "humain.hpp"

namespace personnage {

    class Dame;

    class Brigand;

    class Cowboy;

    class Indien : public Humain {

        int nbPlumes;
        std::string totem;

    public :
        
        Indien(const std::string &nom, const std::string &boisson = "jus de racines", const int nbPlumes = 0, const std::string &totem = "Coyote");

        std::string getNom() const override;

        void parler(const std::string &texte) const override;
        void sePresenter() const override;
        void scalper(Dame &dame);
        void scalper(Brigand &brigand);
        void scalper(Cowboy &cowboy);
    };
    
}

#endif // INDIEN_HPP