#ifndef HUMAIN_HPP
#define HUMAIN_HPP

#include <iostream>

namespace personnage {

	class Humain {

		std::string nom;
		std::string boisson;

	public:

		Humain(const std::string &nom, const std::string &boisson);
		Humain(const std::string &nom);

		virtual std::string getNom() const;
		std::string getBoisson() const;

		void setBoisson(const std::string &boisson);

		virtual void parler(const std::string &texte) const;
		virtual void sePresenter() const;
	  //	void narrateur(const std::string &texte) const;
		void boire() const;
	};

}

#endif // HUMAIN_HPP
