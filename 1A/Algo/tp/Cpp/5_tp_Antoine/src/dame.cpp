#include "dame.hpp"
#include "brigand.hpp"  // pour connaitre les méthodes de cette classe
#include "cowboy.hpp"  // pour connaitre les méthodes de cette classe
#include "indien.hpp" // pour avoir accer au infos de l'indien

#include <iostream>


using namespace std;

namespace personnage {

    Dame::Dame(const string &nom, const string &boisson, const string &couleurRobe)
        : Humain(nom, boisson), couleurRobe{couleurRobe}
    {}

    Dame::Dame(const string &nom)
        : Humain(nom, "lait"), couleurRobe{"blanche"}
    {}

    string Dame::getNom() const {
        return "Miss " + Humain::getNom();
    }

    string Dame::getCouleurRobe() const {
        return couleurRobe;
    }

    bool Dame::isCaptive() const {
        return captive;
    }

    void Dame::setCouleurRobe(const string &couleur) {
        this->couleurRobe = couleur;
	parler("Regardez ma nouvelle robe "+ couleurRobe);   /////// ne pas oublier cette ligne pour respecter le sujet
    } 

    void Dame::seFaireKidnapper(const Brigand &brigand) {
        if(brigand.getNom() == this->getNom()) {
            this->parler("Mais enfin ! Je ne peux pas me kidnapper toute seule !!!");
            return;
        }
        this->parler("Au secour, je me fait kidnapper par " + brigand.getNom() + ". Aidez moi !!!");
        this->captive = true;
    }

    bool Dame::seFaireLiberer(const Cowboy &cowboy) {
        if(captive == false) {
            cout << "Ah, Ah, Ah ! Mais pour qui vous prenez vous ?! Je suis une femme libre enfin !" << endl;
            return false;
        }
        else {
            cout << "Merci " << cowboy.getNom() << ", mon valeureux cowboy, je t'en suis reconnaissante !" << endl;
	    captive = false;       ////// ne pas oublier cette ligne
	    return true;           ////// ni celle-ci
        }
    }

    void Dame::seFaireScalper(const Indien &indien) {
        this->parler("Oh non " + indien.getNom() + ", vous avez tâché ma belle robe " + this->getCouleurRobe() + " ... Je me meurs !!");
    }

    void Dame::sePresenter() const {
        Humain::sePresenter();
        this->parler( "Et j'ai une robe de couleur " + this->couleurRobe + " qui est magnifique, en plus de me mettre en valeur !");
    }

}
