#ifndef RIPOU_HPP
#define RIPOU_HPP

#include <iostream>

#include "sherif.hpp"
#include "brigand.hpp"

namespace personnage {

    class Ripou : public Sherif, public Brigand {

        std::string nom;

    public :

        Ripou(const std::string &nom, const std::string &boisson = "gin", const std::string &attitude = "silencieux", const std::string &comportement = "sournois", const float &recompense = 1000);

        std::string getNom() const override;

        void sePresenter() const override;

    };

}



#endif // RIPOU_HPP