#ifndef SCALPABLE_HPP
#define SCALPABLE_HPP

#include <iostream>

namespace personnage {

    class Indien;

    class Scalpable {

    public :

        virtual void seFaireScalper(const Indien &indien) = 0;

    };

}

#endif // SCALPABLE_HPP