#include "ripou.hpp"

using namespace std;

namespace personnage {

    Ripou::Ripou(const string &nom, const string &boisson, const string &attitude, const string &comportement, const float &recompense)
        : Humain(nom, boisson), Sherif(nom, boisson, attitude), Brigand(nom, boisson, comportement, recompense)
    {}

    string Ripou::getNom() const {
        return Sherif::getNom() + "(aka " + Brigand::getNom() + ")";
    }

    void Ripou::sePresenter() const {
        Sherif::sePresenter();
        this->parler("Personne ne doit le savoir, mais j'ai kidnappé " + to_string(this->nbDamesEnlevees) + " dames, je suis un vrai " + this->comportement + " !");
        this->parler("Celui qui me démasque pourra gagner " + to_string(this->getMiseAPrix()) + "$.");
    }

}