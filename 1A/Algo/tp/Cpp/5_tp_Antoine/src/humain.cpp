#include "humain.hpp"

#include <iostream>


using namespace std;

namespace personnage {

	Humain::Humain(const string &nom, const string &boisson)
		: nom{nom}, boisson{boisson}
	{}

	Humain::Humain(const string &nom)
		: nom{nom}, boisson{"eau"}
	{}

	string Humain::getNom() const {
		return nom;
	}

	string Humain::getBoisson() const {
		return boisson;
	}

	void Humain::setBoisson(const string &boisson) {
		this->boisson = boisson;
	}

	void Humain::parler(const string &texte) const {
		cout << "(" << this->nom << ") --- " << texte << endl; 
	}

	void Humain::sePresenter() const {
		this->parler("Bonjour, je m'appelle " + this->nom + ", ma boisson favorite est le " + this->boisson);
	}
  /*
	void Humain::narrateur(const string &texte) const {
		cout << "(narrateur) --- " << texte << endl;
	}
  */
	void Humain::boire() const {
	  //	cout << "Ah ! Un bon verre de " << this->boisson << " ! GLOUPS !" << endl;
	  parler("Ah ! Un bon verre de " + boisson + " ! GLOUPS !" );   // comme ça 
	}

}

