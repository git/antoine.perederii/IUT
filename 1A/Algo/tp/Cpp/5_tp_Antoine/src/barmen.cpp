#include "barmen.hpp"

using namespace std;

namespace personnage {

    Barmen::Barmen(const string &nom, const string &nomBar)
        : Humain(nom, "bière"), nomBar{nomBar}
    {}

    Barmen::Barmen(const string &nom)
    //      : Humain(nom, "bière"), nomBar{"Coffee Place"}
      : Humain(nom, "bière"), nomBar{"Chez "+nom}     ///// comme ça d'après le sujet
    {}

    string Barmen::getNomBar() const {
        return this->nomBar;
    }

    void Barmen::parler(const string &texte) const {
      ///cout << "(" << this->getNom() << ") --- " << texte << " mon gars." << endl;
      //
      Humain::parler(texte + " mon gars.");   // comme ça
      
    }

    void Barmen::sePresenter() const {
        Humain::sePresenter();
        this->parler("De plus, je possede un bar qui se nomme le " + this->getNomBar());
    }

  /*
    void Barmen::servirVerre(const Cowboy &cowboy) const {
        this->parler("Et voilà pour toi " + cowboy.getNom() +"ton verre favori")
    }

    void Barmen::servirVerre(const Dame &dame) const {
        this->parler("Et voilà pour toi " + dame.getNom() +"ton verre favori")
    }

    void Barmen::servirVerre(const Sherif &sherif) const {
        this->parler("Et voilà pour toi " + sherif.getNom() +"ton verre favori")
    }

    void Barmen::servirVerre(const Brigand &brigand) const {
        this->parler("Et voilà pour toi " + brigand.getNom() +"ton verre favori")
    }
  */


  //////// voici comment il fallait faire : 
  void Barmen::servirVerre(const Humain & humain) const {
    this->parler("Et voilà pour toi " + humain.getNom() +"un verre de "
		 + humain.getBoisson());
     humain.boire();
  }

}
