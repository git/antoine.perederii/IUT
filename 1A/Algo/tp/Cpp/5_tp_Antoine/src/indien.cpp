#include "indien.hpp"
#include "dame.hpp"
#include "brigand.hpp"
#include "cowboy.hpp"

using namespace std;

namespace personnage {
    /**
     * @brief Construct a new Indien:: Indien object
     * 
     * @param nom 
     * @param boisson 
     * @param nbPlumes 
     * @param totem 
     */
    Indien::Indien(const string &nom, const string &boisson, const int nbPlumes, const string &totem)
        :Humain(nom, boisson), nbPlumes{nbPlumes}, totem{totem}
    {}

    /**
     * @brief 
     * 
     * @return string 
     */
    string Indien::getNom() const {
        return Humain::getNom();
    }

    /**
     * @brief 
     * 
     * @param texte 
     */
    void Indien::parler(const string &texte) const {
        Humain::parler(texte + " Ugh !");
    }

    /**
     * @brief 
     * 
     */
    void Indien::sePresenter() const {
        this->parler("Oyh, moi être " + this->getNom() + " et aimer " + this->getBoisson() + ".");
        this->parler("Moi vénérer totem " + this->totem + ". Moi guerrier. Moi avoir " + to_string(this->nbPlumes) + " plumes.");
    }

    /**
     * @brief 
     * 
     * @param dame 
     */
    void Indien::scalper(Dame &dame) {
        dame.seFaireScalper(*this);
        nbPlumes += 1;
    }

    /**
     * @brief 
     * 
     * @param brigand 
     */
    void Indien::scalper(Brigand &brigand) {
        brigand.seFaireScalper(*this);
        nbPlumes += 1;
    }

    /**
     * @brief 
     * 
     * @param cowboy 
     */
    void Indien::scalper(Cowboy &cowboy) {
        cowboy.seFaireScalper(*this);
        nbPlumes+= 1;
    }

}