#ifndef COWBOY_HPP
#define COWBOY_HPP

#include <ostream>
#include "humain.hpp"
#include "scalpable.hpp"

namespace personnage {

    class Dame;

    class Brigand;

    class Cowboy : virtual public Humain, public Scalpable {
    
    protected :

        int popularite = 0;
        std::string attitude;

    public:

        Cowboy(const std::string &nom, const std::string &boisson, const std::string &attitude);
        Cowboy(const std::string &nom);

        //////  void liberer(const Dame &dame);
        void liberer(Dame &dame);   ///// comme ça
        /////    void tirerSur(const Brigand &brigand);
        void tirerSur(const Brigand &brigand)const;

        void seFaireScalper(const Indien &indien) override;
      
        void sePresenter() const override;
    };

}

#endif // COWBOY_HPP
