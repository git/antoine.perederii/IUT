#ifndef FEMMEBRIGAND_HPP
#define FEMMEBRIGAND_HPP

#include "dame.hpp"
#include "brigand.hpp"

namespace personnage {

    class Sherif;

    class FemmeBrigand : public Dame, public Brigand{

    public :

        FemmeBrigand(const std::string &nom, const std::string &boisson = "lait", const std::string &couleurRobe = "blanche", const std::string &comportement = "discrète", const float &recompense = 50000);

        std::string getNom() const override;

        void kidnapper(Dame &dame) override;
        bool seFaireEmprisonnerPar(const Sherif &sherif) override;
        void sePresenter() const override;

    };
}

#endif // FEMMEBRIGAND_HPP