#ifndef SHERIF_HPP
#define SHERIF_HPP

#include <iostream>
#include "cowboy.hpp"

namespace personnage {

    class Brigand;

    class Sherif : public Cowboy {

        std::string nom;
        int nbBrigands = 0;

    public :

        Sherif(const std::string &nom);
        Sherif(const std::string &nom, const std::string &boisson, const std::string &attitude);

        std::string getNom() const override;

        ///        void coffrerBrigand(const Brigand &brigand);
        void coffrerBrigand(Brigand &brigand);     ///// comme ça
        ///        void rechercherBrigand(const Brigand &brigand);
        void rechercherBrigand(const Brigand &brigand)const;  // comme ça
        void sePresenter() const override;

    };

}


#endif // SHERIF_HPP
