/**
 * @file main.cpp
 * @author PEREDERII Antoine
 * @brief 
 * @version 0.1
 * @date 2023-03-22
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "dame.hpp"
#include "brigand.hpp"
#include "cowboy.hpp"
#include "barmen.hpp"
#include "sherif.hpp"
#include "ripou.hpp"
#include "femmeBrigand.hpp"
#include "indien.hpp"

#include <vector>
#include <algorithm>

using namespace std;

/**
 * @brief 
 * 
 */
void testHumain() {
    personnage::Humain jacques("Jacques", "biere");
    personnage::Humain pierre("Pierre");
    pierre.setBoisson("binouse");
    jacques.sePresenter();
    pierre.sePresenter();
    jacques.boire();
    pierre.boire();
    pierre.parler("C'était bien sympa Jacques, rentrons à la maison maintenant !");
}

/**
 * @brief 
 * 
 */
void testDame() {
    personnage::Dame rose("Rose");
    personnage::Dame ginette("Ginette", "ricard", "noir");
    rose.sePresenter();
    ginette.sePresenter();
    rose.setCouleurRobe("verte");
    rose.sePresenter();
    rose.parler("J'aime les beaux cowboy !");
}

/**
 * @brief 
 * 
 */
void testBrigand() {
    personnage::Brigand robert("Robert");
    personnage::Brigand bernard("Bernard", "aveze", "ronchon", 200);
    robert.sePresenter();
    bernard.sePresenter();
}

/**
 * @brief cette methode permet de tester la classe cowboy en créant 
 * deux cowboy et en les faisant se présenter
 * 
 */
void testCowboy() {
    personnage::Cowboy danny("Danny", "whisky", "magnifique");
    personnage::Cowboy george("George");
    danny.sePresenter();
    george.sePresenter();
}

void testDBHumain() {
    personnage::Dame rose("Rose");
    personnage::Brigand marc("marc");
    const personnage::Humain &vilainHumain = marc;
    const personnage::Humain &gentilleHumain = rose;
    cout << "Je suis " << vilainHumain.getNom() << endl;
    cout << "Je suis " << gentilleHumain.getNom() << endl;
}

void testBarmen() {
    personnage::Barmen jose("Jose");
    personnage::Barmen franck("Franck", "The Franchy Bar");
    jose.sePresenter();
    franck.sePresenter();
    jose.parler("J'aime les frites !!!");
}

void testSherif() {
    personnage::Brigand robert("Robert");
    personnage::Cowboy *clint = new personnage::Sherif{"Clint"};   
    clint->sePresenter();
    clint->parler("Je suis magnifique aujourd'hui. Mais comme toujours c'est vraie !");
    //   clint->coffrerBrigand(robert);       ///// pas possible. Lors de la compilation on aura erreur car la méthode coffrerBrigand n'existe pas pour Cowboy (type du pointeur)
}

void testRipou() {
    personnage::Ripou michel("Michel");
    personnage::Ripou morice("Morice", "Jet", "Malicieux", "Cruel", 2000);
    michel.sePresenter();
    morice.sePresenter();
    michel.parler("Hello boy !!!");
    michel.tirerSur(morice);
    michel.coffrerBrigand(michel);
}

void testFemmeBrigand() {
    personnage::FemmeBrigand monique{"Monique"};
    personnage::FemmeBrigand yvonne{"Yvonne", "Thé", "noir", "grogneuse", 75000};
    monique.sePresenter();
    yvonne.sePresenter();
    monique.parler("Mais que tu est belle, Yvonne !");
    monique.kidnapper(yvonne);
    monique.kidnapper(monique);
}

void testIndien() {
    personnage::Indien ventDoux{"VentDoux"};
    personnage::Indien ventFort{"VentFort", "jus de pieds", 4, "Fouine"};
    personnage::Brigand robert("Robert");
    ventDoux.sePresenter();
    ventFort.sePresenter();
    robert.sePresenter();
    ventFort.parler("Oeh ventDoux ! Scalpons robert !");
    ventFort.scalper(robert);
    ventFort.sePresenter();
}

void testGeneral1() {
    personnage::Humain pierre("Pierre");
    personnage::Dame rose("Rose");
    personnage::Dame ginette("Ginette", "ricard", "noir");
    personnage::Brigand robert("Robert");
    personnage::Brigand bernard("Bernard", "aveze", "ronchon", 200);
    personnage::Cowboy george("George");
    personnage::Barmen franck("Franck", "The Franchy Bar");
    personnage::Sherif clint("Clint");
    cout << "Il était une fois, une jeune et belle dame qui se promenait dans les bois avec une amie.\n";
    rose.sePresenter();
    ginette.sePresenter();
    rose.parler("Qu'il fait beau aujourd'hui !!");
    cout << "Tout allait bien dans le meilleur des mondes, jusqu'à ce que deux brigands apparurent.\n";
    robert.sePresenter();
    bernard.sePresenter();
    robert.parler("Nous allons vous kidnapper pauvres dames !!");
    bernard.kidnapper(rose);
    robert.kidnapper(ginette);
    cout <<"Les pauvres Rose et Ginette se firent kidnapper par ses deux truans. Mais un vieux cowboy n'est pas loin\n";
    george.sePresenter();
    george.parler("Que faites vous bande de mal autrus !!!");
    george.tirerSur(robert);
    george.tirerSur(bernard);
    bernard.parler("Tu nous a loupé veillard !!");
    george.liberer(ginette);
    robert.parler("À bientôt l'ancien !!");
    cout <<"Et les deux brigands pure s'en aller paisiblement car la vieux cowboy ne tiré plus tout droit.\n";
    cout <<"Surtout avec tout l'alcool qu'il s'était enpiffré. Mais il réussi quand même à liberer une des deux filles.\n";
    cout <<"Et notre cowboy retourna au bar pour se remettre de ses émotions.\n";
    george.parler("Hey Franck, un verre stp pour moi et cette dame !");
    franck.servirVerre(george);
    franck.servirVerre(ginette);
    franck.parler("Alors ces brigands ?!");
    george.parler("Ils se sont enfuit, j'ai plus la forme comme avant ! Mais j'ai réussi à liberer cette dame.");
    cout <<"Pendant ce temps là, les brigands s'échappe toujours. Notre cowboy décide donc d'en informer le Shérif.\n";
    george.parler("Sherif !! J'ai deux brigands qui m'on échapés.");
    clint.parler("Très bien george. Repose toi ! Je prends le relais");
    clint.rechercherBrigand(bernard);
    clint.rechercherBrigand(robert);
    cout <<"Cela s'annonce compliqué pour notre Shérif !!\n";
    clint.parler("Où on-t-il bien pu partir ???");
    pierre.sePresenter();
    pierre.parler("Sherif ! J'ai vu ces deux gangster avec une très jolie femme avec eux !");
    clint.parler("Et où ça ?");
    pierre.parler("Ils sont près de la gare !!!");
    clint.parler("En avant mon fidèle destrier pilepoil !!");
    cout <<"Notre Shérif entama donc son périple vers la gare avec son cheval pile-poil.\n";
    cout <<"Une fois arrivé, il se cacha et s'approcha discrètement de ses deux brigands avant de les intercepter.\n";
    clint.coffrerBrigand(robert);
    clint.tirerSur(bernard);
    clint.coffrerBrigand(bernard);
    clint.liberer(rose);
    rose.parler("Mais que vous êtes merveuilleux mon shérif !!");
    clint.parler("Pour vous servir Miss rose.");
    cout <<"Nos deux personnage partir tout deux heureux.\n";
}

void testGeneral2() {
    vector<personnage::Humain *> vHumain{
        new personnage::Dame("Rose"),
        new personnage::Dame("Ginette", "ricard", "noir"),
        new personnage::Brigand("Robert"),
        new personnage::Brigand("Bernard", "aveze", "ronchon", 200),
        new personnage::Cowboy("George"),
        new personnage::Barmen("Franck", "The Franchy Bar"),
        new personnage::Sherif("Clint"),
        new personnage::Sherif("Marc", "Morce", "Compétent"),
        new personnage::Ripou("Morice", "Jet", "Malicieux", "Cruel", 2000),
        new personnage::FemmeBrigand{"Monique"},
        new personnage::FemmeBrigand{"Yvonne", "Thé", "noir", "grogneuse", 75000},
        new personnage::Indien("Géronimo")
    };
    for(personnage::Humain *pth : vHumain) {
        pth->sePresenter();
    } // personnage

    cout << "\nToute cette histoire commence au saloon de Buddy. C'est la tournée générale du patron :\n" << endl;

    for(vector<personnage::Humain *>::iterator it1 = vHumain.begin(); it1 != vHumain.end(); ++it1) {
        personnage::Barmen*ptb = dynamic_cast<personnage::Barmen*>(vHumain.at(5));
        if(ptb != nullptr) {
            ptb->servirVerre(**it1);
        }
    }

    cout << "Ivre mort, Clint se met à tirer sur tous les hors la loi de la pièce." << endl;

    for(vector<personnage::Humain *>::iterator it2 = vHumain.begin(); it2 != vHumain.end(); ++it2) {
        personnage::Sherif*pts = dynamic_cast<personnage::Sherif*>(*it2);
        if(pts != nullptr) {
            for(auto it3 = vHumain.begin(); it3 != vHumain.end(); ++it3) {
                personnage::Brigand*ptb = dynamic_cast<personnage::Brigand*>(*it3);
                if(ptb != nullptr) {
                    pts->tirerSur(*ptb);
                }
            }
        }
    }

    cout << "Pris de panique, Buck le méchant, qui a réussi à éviter les balles," << endl;
    cout << "s'enfuit en kidnappant toutes les femmes à porté de main." << endl;

    for(vector<personnage::Humain *>::iterator it4 = vHumain.begin(); it4 != vHumain.end(); ++it4) {
        personnage::Brigand*ptb = dynamic_cast<personnage::Brigand*>(*it4);
        if(ptb != nullptr) {
            for(auto it5 = vHumain.begin(); it5 != vHumain.end(); ++it5) {
                personnage::Dame*ptd = dynamic_cast<personnage::Dame*>(*it5);
                if(ptd != nullptr) {
                    ptb->kidnapper(*ptd);
                }
            }
        }
    }

    cout << "Heureusement, Shérif Marc parvient à sauver Ginette." << endl;

    for(vector<personnage::Humain *>::iterator it6 = vHumain.begin(); it6 != vHumain.end(); ++it6) {
        personnage::Sherif*pts = dynamic_cast<personnage::Sherif*>(*it6);
        if(pts != nullptr) {
            for(auto it7 = vHumain.begin(); it7 != vHumain.end(); ++it7) {
                personnage::Dame*ptd = dynamic_cast<personnage::Dame*>(*it7);
                if(ptd != nullptr) {
                    pts->liberer(*ptd);
                }
            }
        }
    }

    cout << "\nTellement ivre, Shérif Duke essaye d'ailleurs de la sauver plusieurs fois.\n" << endl;

    for(vector<personnage::Humain *>::iterator it6 = vHumain.begin(); it6 != vHumain.end(); ++it6) {
        personnage::Sherif*pts = dynamic_cast<personnage::Sherif*>(*it6);
        if(pts != nullptr) {
            for(auto it7 = vHumain.begin(); it7 != vHumain.end(); ++it7) {
                personnage::Dame*ptd = dynamic_cast<personnage::Dame*>(*it7);
                if(ptd != nullptr) {
                    pts->liberer(*ptd);
                }
            }
        }
    }

    cout << "\nSuite à cela, Shérif Clint décide de coffrer tout les brigands." << endl;
    cout << "Mais tellement ivre, il décide de coffrer tout le monde.\n" << endl;

    // for(vector<personnage::Humain *>::iterator it8 = vHumain.begin(); it8 != vHumain.end(); ++it8) {
    //     if(*it8->getNom() == "Shérif Clint") { //*it8 = truc pointé par it8
    //         for(auto it9 = vHumain.begin(); it9 != vHumain.end(); ++it9) {

    //             *it8->coffrerBrigand(**it9);
    //         }
    //     }
    // }

    // cout << "Géronimo commancant à en avoir marre, décide de scalper tout le monde." << endl;

    // for(vector<personnage::Humain *>::iterator it8 = vHumain.begin(); it8 != vHumain.end(); ++it8) {
    //     if(*it8->getNom() == "Géronimo") {
    //         for(auto it9 = vHumain.begin(); it9 != vHumain.end(); ++it9) {
    //                 *it8->sclper(**it9);
    //         }
    //     }
    // }
}


int main() {
    // testHumain();
    // testDame();
    // testBrigand();
    // testCowboy();
    // testDBHumain();
    // testBarmen();
    // testSherif();
    // testRipou();
    // testFemmeBrigand();
    // testIndien();
    // testGeneral1();
    testGeneral2();
    return 0;
}
