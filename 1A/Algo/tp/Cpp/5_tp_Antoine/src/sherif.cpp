#include "sherif.hpp"
#include "brigand.hpp" // pour connaître les méthodes de cette classe

using namespace std;

namespace personnage {

    Sherif::Sherif(const string &nom)
        : Humain(nom, "Tekila"), Cowboy(nom, "Tekila", "honnête")
    {}

    Sherif::Sherif(const string &nom, const string &boisson, 
    const string &attitude)
        : Humain(nom, boisson), Cowboy(nom, boisson, attitude)
    {}

    string Sherif::getNom() const {
        return "Shérif " + Cowboy::getNom();
    }

  /*
    void Sherif::coffrerBrigand(const Brigand &brigand) {
        if() { //? Comment verifier s'il est en prison ?
            this->parler(brigand.getNom() + " au nom de la loi, 
            je vous arrête !!")
            brigand.seFaireEmprisonerPar(this);
        }
    }
  */

  void Sherif::coffrerBrigand(Brigand &brigand) { // comme ça
    if(brigand.getNom() == this->getNom()) {
      this->parler("Je ne peux pas m'emprisonner moi-même enfin !!");
      return;
    }
    else {
      this->parler(brigand.getNom() + " au nom de la loi, je vous arrête !!");
	    bool rep = brigand.seFaireEmprisonnerPar(*this); // comme ça
	    if(rep == true) nbBrigands++;    ///// comme ça  
    }
  }


  /*
    void Sherif::rechercherBrigand(const Brigand &brigand) {
        //? Comment l'écrire sans utiliser de find a chaque fois ?
        Humain::narrateur("OYEZ OYEZ BRAVE GENS !! " + 
        brigand.getMiseAPrix() + " $ à qui arrêtera " + brigand.getNom()
         + "mort ou vif !!");
    }
  */


  /// c'est comme ça : 
  void Sherif::rechercherBrigand(const Brigand &brigand)const { 
    if(brigand.getNom() == this->getNom()) {
      this->parler("Je ne peut pas me revchercher moi-même enfin !!!");
      return;
    }
    parler("OYEZ OYEZ BRAVE GENS !! " + to_string(brigand.getMiseAPrix()) + " $ à qui arrêtera " + brigand.getNom() + "mort ou vif !!");
  }

  void Sherif::sePresenter() const {
    Cowboy::sePresenter();
    this->parler("Et j'ai à mon actif " + to_string(this->nbBrigands) + " capturé.");
  }
    
  
}
