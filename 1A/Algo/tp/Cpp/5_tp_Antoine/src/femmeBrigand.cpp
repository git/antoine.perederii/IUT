#include "femmeBrigand.hpp"
#include "sherif.hpp"
#include "dame.hpp"

using namespace std;

namespace personnage {

    FemmeBrigand::FemmeBrigand(const string &nom, const string &boisson, const string &couleurRobe, const string &comportement, const float &recompense)
        : Humain(nom, boisson), Dame(nom, boisson, couleurRobe), Brigand(nom, boisson, comportement, recompense)
    {}

    string FemmeBrigand::getNom() const {
        return Dame::getNom();
    }

    void FemmeBrigand::sePresenter() const {
        Dame::sePresenter();
        this->parler("La récompense pour ma capture est de " + to_string(this->recompense) + " car je suis " + this->comportement + " !!");
    }

    void FemmeBrigand::kidnapper(Dame &dame) { // comme ça
        if(this->getNom() == dame.getNom()) {
            this->parler("Mais enfin, je ne peut pas me kidnapper toute seule !!!");
            return;
        }
        this->parler("Hi hi ! Désolé " + dame.getNom() + "... encore UNE de moins qui me feras de l'ombre.");
	    /////// dame.seFaireKidnapper(this);
	    dame.seFaireKidnapper(*this);      // comme ça
        nbDamesEnlevees += 1;
    }

    bool FemmeBrigand::seFaireEmprisonnerPar(const Sherif &sherif) {
      if(enPrison){          ////// ne pas oublier ce cas !!
	    parler ("Tu arrives trop tard, je suis déjà en prison !"); //
	    return false;           //////
      }
      else{
        this->parler(sherif.getNom() + " ?!! Une lady comme moi ... je vous promet que je suis inocente !");
        enPrison = true;
	    return true;       //// ne pas oublier cette ligne
      }
    }

}