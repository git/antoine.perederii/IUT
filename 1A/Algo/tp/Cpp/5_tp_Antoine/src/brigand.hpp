#ifndef BRIGAND_HPP
#define BRIGAND_HPP

#include <iostream>
#include "humain.hpp"
#include "scalpable.hpp"



namespace personnage {

    class Dame;

    class Sherif;

    class Brigand : virtual public Humain, public Scalpable {
    
    protected :

        std::string comportement;
        int nbDamesEnlevees = 0;
        float recompense;
        bool enPrison = false;

    public:

        Brigand(const std::string &nom, const std::string &boisson, const std::string &comportement, const float &recompense);
        Brigand(const std::string &nom);

        std::string getNom() const override;
        float getMiseAPrix() const;

        /////////  void kidnapper(const Dame &dame);
        virtual void kidnapper(Dame &dame);      // comme ça
        virtual bool seFaireEmprisonnerPar(const Sherif &sherif);

        void seFaireScalper(const Indien &indien) override;

        void sePresenter() const override;
    };

}

#endif // BRIGAND_HPP
