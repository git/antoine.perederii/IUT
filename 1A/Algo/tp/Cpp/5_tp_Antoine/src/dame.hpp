#ifndef DAME_HPP
#define DAME_HPP

#include <iostream>
#include "humain.hpp"
#include "scalpable.hpp"

namespace personnage {

    class Brigand;

    class Cowboy;

    class Dame : virtual public Humain, public Scalpable {

    protected :

        std::string couleurRobe;
        bool captive = false;

    public:

        Dame(const std::string &nom, const std::string &boisson, const std::string &couleurRobe);
        Dame(const std::string &nom);

        std::string getNom() const override;
        std::string getCouleurRobe() const;
        bool isCaptive() const;
        
        void setCouleurRobe(const std::string &couleur);

        void seFaireKidnapper(const Brigand &brigand);
        bool seFaireLiberer(const Cowboy &cowboy);

        void seFaireScalper(const Indien &indien) override;

        void sePresenter() const override;

    };

}

#endif // DAME_HPP