#include "cowboy.hpp"

#include <ostream>


using namespace std;

namespace personnage {

    Cowboy::Cowboy(const string &nom, const string &boisson, const string &attitude)
        : Humain(nom, boisson), attitude{attitude}
    {}

    Cowboy::Cowboy(const string &nom)
        : Humain(nom, "whisky"), attitude{"vaillant"}
    {}

    void Cowboy::liberer(const Dame &dame) {
        dame.seFaireLiberer(this);
        this->parler("Que vous avez une belle robe " + dame.getCouleurRobe() + ". Elle vous vas à ravir ! ;)");
    } 

    void Cowboy::tirerSur(const Brigand &brigand) {
        this->narrateur("Le " + this->attitude + " " + this->getNom() + " tire sur " + brigand.getNom() + ". PAN !");
        cout << "(Le narrateur) --- Le " << this->attitude << " " << this->getNom() << " tire sur " << brigand.getNom() << ". PAN !" << endl;
        this->parler("Prend ça, rascal !");
        this->liberer(); //? Comment savoir qui on doit liberer ?
    }

    void Cowboy::sePresenter() const {
        Humain::sePresenter();
        if(this->popularite > 0) {
            this->parler("Je suis un " + this->attitude + " cowboy. De plus, j'ai une popularité grimpante de " + to_string(this->popularite) + "."); //! Quand c'est des nb, ne pas oublier de les passer en to_string !
        }
        else {
            this->parler("Je suis un " + this->attitude + " cowboy, mais ma popularité est encore nulle. Un jour viendras, je serais le plus grand des cowboy du comté !!");
        }
    }

}