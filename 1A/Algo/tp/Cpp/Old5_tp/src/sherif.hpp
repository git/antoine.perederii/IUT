#ifndef SHERIF_HPP
#define SHERIF_HPP

#include <iostream>
#include "cowboy.hpp"

namespace personnage {

    class Brigand;

    class Sherif : public Cowboy {

        std::string nom;
        int nbBrigands = 0;

    public :

        Sherif(const std::string &nom);

        std::string getNom() const override;

        void coffrerBrigand(const Brigand &brigand);
        void rechercherBrigand(const Brigand &brigand);

    };

}


#endif // SHERIF_HPP