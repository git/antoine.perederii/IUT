#include "sherif.hpp"

using namespace std;

namespace personnage {

    Sherif::Sherif(const string &nom)
        : Cowboy(nom, "Tekila", "honnête")
    {}

    string Sherif::getNom() const {
        return "Shérif " + Cowboy::getNom();
    }

    void Sherif::coffrerBrigand(const Brigand &brigand) {
        if() { //? Comment verifier s'il est en prison ?
            this->parler(brigand.getNom() + " au nom de la loi, je vous arrête !!")
            brigand.seFaireEmprisonerPar(this);
        }
    }

    void Sherif::rechercherBrigand(const Brigand &brigand) {
        //? Comment l'écrire sans utiliser de find a chaque fois ?
        Humain::narrateur("OYEZ OYEZ BRAVE GENS !! " + brigand.getMiseAPrix() + " $ à qui arrêtera " + brigand.getNom() + "mort ou vif !!");
    }

}