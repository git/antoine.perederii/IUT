#include "humain.hpp"
#include "dame.hpp"
#include "brigand.hpp"
#include "cowboy.hpp"
#include "barmen.hpp"
#include "sherif.hpp"


using namespace std;

void testHumain() {
    personnage::Humain jacques("Jacques", "biere");
    personnage::Humain pierre("Pierre");
    pierre.setBoisson("binouse");
    jacques.sePresenter();
    pierre.sePresenter();
    jacques.boire();
    pierre.boire();
    pierre.parler("C'était bien sympa Jacques, rentrons à la maison maintenant !");
}

void testDame() {
    personnage::Dame rose("Rose");
    personnage::Dame ginette("Ginette", "ricard", "noir");
    rose.sePresenter();
    ginette.sePresenter();
    rose.setCouleurRobe("verte");
    rose.sePresenter();
    rose.parler("J'aime les beaux cowboy !");
}

void testBrigand() {
    personnage::Brigand robert("Robert");
    personnage::Brigand bernard("Bernard", "aveze", "ronchon", 200);
    robert.sePresenter();
    bernard.sePresenter();
}

void testCowboy() {
    personnage::Cowboy danny("Danny", "whisky", "magnifique");
    personnage::Cowboy george("George");
    danny.sePresenter();
    george.sePresenter();
}

void testDBHumain() {
    personnage::Dame rose("Rose");
    personnage::Brigand marc("marc");
    const personnage::Humain &vilainHumain = marc;
    const personnage::Humain &gentilleHumain = rose;
    cout << "Je suis " << vilainHumain.getNom() << endl;
    cout << "Je suis " << gentilleHumain.getNom() << endl;
}

void testBarmen() {
    personnage::Barmen jose("Jose");
    personnage::Barmen franck("Franck", "The Franchy Bar");
    jose.sePresenter();
    franck.sePresenter();
    jose.parler("J'aime les frites !!!");
}

void testSherif() {
    personnage::Brigand robert("Robert");
    personnage::Cowboy *clint = new personnage::Sherif{"Clint"};
    clint->sePresenter();
    clint->parler("Je suis magnifique aujourd'hui. Mais comme toujours c'est vraie !");
    clint->coffrerBrigand(robert);
}

void testGeneral() {
    personnage::Humain pierre("Pierre");
    personnage::Dame rose("Rose");
    personnage::Dame ginette("Ginette", "ricard", "noir");
    personnage::Brigand robert("Robert");
    personnage::Brigand bernard("Bernard", "aveze", "ronchon", 200);
    personnage::Cowboy george("George");
    personnage::Barmen franck("Franck", "The Franchy Bar");
    personnage::Sherif clint("Clint");
    personnage::Humain::narrateur("Il était une fois, une jeune et belle dame qui se promenait dans les bois avec une amie.");
    rose.sePresenter();
    ginette.sePresenter();
    rose.parler("Qu'il fait beau aujourd'hui !!");
    personnage::Humain::narrateur("Tout allait bien dans le meilleur des mondes, jusqu'à ce que deux brigands apparurent.");
    robert.sePresenter();
    bernard.sePresenter();
    robert.parler("Nous allons vous kidnapper pauvres dames !!");
    bernard.kidnapper(rose);
    robert.kidnapper(ginette);
    personnage::Humain::narrateur("Les pauvres Rose et Ginette se firent kidnapper par ses deux truans. Mais un vieux cowboy n'est pas loin");
    george.sePresenter();
    george.parler("Que faites vous bande de mal autrus !!!");
    george.tirerSur(robert);
    george.tirerSur(bernard);
    bernard.parler("Tu nous a loupé veillard !!");
    george.liberer(ginette);
    robert.parler("À bientôt l'ancien !!");
    personnage::Humain::narrateur("Et les deux brigands pure s'en aller paisiblement car la vieux cowboy ne tiré plus tout droit.");
    personnage::Humain::narrateur("Surtout avec tout l'alcool qu'il s'était enpiffré. Mais il réussi quand même à liberer une des deux filles.");
    personnage::Humain::narrateur("Et notre cowboy retourna au bar pour se remettre de ses émotions.");
    george.parler("Hey Franck, un verre stp pour moi et cette dame !");
    franck.servirVerre(george);
    franck.servirVerre(ginette);
    franck.parler("Alors ces brigands ??!!");
    george.parler("Ils se sont enfuit, j'ai plus la forme comme avant ! Mais j'ai réussi à liberer cette dame.");
    personnage::Humain::narrateur("Pendant ce temps là, les brigands s'échappe toujours. Notre cowboy décide donc d'en informer le Shérif.");
    george.parler("Sherif !! J'ai deux brigands qui m'on échapés.");
    clint.parler("Très bien george. Repose toi ! Je prends le relais");
    clint.rechercherBrigand(bernard);
    clint.rechercherBrigand(robert);
    personnage::Humain::narrateur("Cela s'annonce compliqué pour notre Shérif !!");
    clint.parler("Où on-t-il bien pu partir ???");
    pierre.sePresenter();
    pierre.parler("Sherif ! J'ai vu ces deux gangster avec une très jolie femme avec eux !");
    clint.parler("Et où ça ?");
    pierre.parler("Ils sont près de la gare !!!");
    clint.parler("En avant mon fidèle destrier pilepoil !!");
    personnage::Humain::narrateur("Notre Shérif entama donc son périple vers la gare avec son cheval pile-poil.");
    personnage::Humain::narrateur("Une fois arrivé, il se cacha et s'approcha discrètement de ses deux brigands avant de les intercepter.");
    clint.coffrerBrigand(robert);
    clint.tirerSur(bernard);
    clint.coffrerBrigand(bernard);
    clint.liberer(rose);
    rose.parler("Mais que vous êtes merveuilleux mon shérif !!");
    clint.parler("Pour vous servir Miss rose.");
    personnage::Humain::narrateur("Nos deux personnage partir tout deux heureux.");
}


int main() {
    // testHumain();
    // testDame();
    // testBrigand();
    // testCowboy();
    // testDBHumain();
    // testBarmen();
    // testSherif();
    // testGeneral();
    return 0;
}