#ifndef BRIGAND_HPP
#define BRIGAND_HPP

#include <iostream>
#include "humain.hpp"



namespace personnage {

    class Dame;

    class Sherif;

    class Brigand : public Humain{

        std::string comportement;
        int nbDamesEnlevees = 0;
        float recompense;
        bool enPrison = false;

    public:

        Brigand(const std::string &nom, const std::string &boisson, const std::string &comportement, const float &recompense);
        Brigand(const std::string &nom);

        std::string getNom() const override;
        float getMiseAPrix() const;

        void kidnapper(const Dame &dame);
        bool seFaireEmprisonnerPar(const Sherif &sherif);

        void sePresenter() const override;
    };

}

#endif // BRIGAND_HPP