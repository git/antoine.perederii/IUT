#ifndef BARMEN_HPP
#define BARMEN_HPP

#include <iostream>
#include "humain.hpp"


namespace personnage {

    class Dame;

    class Sherif;

    class Brigand;

    class Cowboy;

    class Barmen : public Humain {

        std::string nom;
        std::string nomBar;

    public :

        Barmen(const std::string &nom);
        Barmen(const std::string &nom, const std::string &nomBar);

        std::string getNomBar() const;

        void sePresenter() const override;
        void parler(const std::string &texte) const override;
        void servirVerre(const Cowboy &cowboy) const;
        void servirVerre(const Dame &dame) const;
        void servirVerre(const Sherif &sherif) const;
        void servirVerre(const Brigand &brigand) const;

    };

}

#endif // BARMEN_HPP