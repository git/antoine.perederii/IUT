#include "dame.hpp"

#include <iostream>


using namespace std;

namespace personnage {

    Dame::Dame(const string &nom, const string &boisson, const string &couleurRobe)
        : Humain(nom, boisson), couleurRobe{couleurRobe}
    {}

    Dame::Dame(const string &nom)
        : Humain(nom, "lait"), couleurRobe{"blanche"}
    {}

    string Dame::getNom() const {
        return "Miss " + Humain::getNom();
    }

    string Dame::getCouleurRobe() const {
        return couleurRobe;
    }

    bool Dame::isCaptive() const {
        return captive;
    }

    void Dame::setCouleurRobe(const string &couleur) {
        this->couleurRobe = couleur;
    } 

    void Dame::seFaireKidnapper(const Brigand &brigand) {
        cout << "Au secour, je me fait kidnapper par " << brigand.getNom() << ". Aidez moi !!!" << endl;
        this->captive = true;
    }

    bool Dame::seFaireLiberer(const Cowboy &cowboy) {
        if(captive == false) {
            cout << "Ah, Ah, Ah ! Mais pour qui vous prenez vous ?! Je suis une femme libre enfin !" << endl;
            return false;
        }
        else {
            cout << "Merci " << cowboy.getNom() << ", mon valeureux cowboy, je t'en suis reconnaissante !" << endl; 
        }
    }

    void Dame::sePresenter() const {
        Humain::sePresenter();
        this->parler( "Et j'ai une robe de couleur " + this->couleurRobe + " qui est magnifique, en plus de me mettre en valeur !");
    }

}