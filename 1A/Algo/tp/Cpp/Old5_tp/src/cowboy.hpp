#ifndef COWBOY_HPP
#define COWBOY_HPP

#include <ostream>
#include "humain.hpp"

namespace personnage {

    class Dame;

    class Brigand;

    class Cowboy : public Humain {

        int popularite = 0;
        std::string attitude;

    public:

        Cowboy(const std::string &nom, const std::string &boisson, const std::string &attitude);
        Cowboy(const std::string &nom);

        void liberer(const Dame &dame);
        void tirerSur(const Brigand &brigand);

        void sePresenter() const override;
    };

}

#endif // COWBOY_HPP