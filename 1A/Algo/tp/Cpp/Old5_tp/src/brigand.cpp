#include "brigand.hpp"

#include <iostream>


using namespace std;

namespace personnage {
        
    Brigand::Brigand(const string &nom, const string &boisson, const string &comportement, const float &recompense)
        : Humain(nom, boisson), comportement{comportement}, recompense{recompense}
    {}

    Brigand::Brigand(const string &nom)
        : Humain(nom, "tord-boyaux"), comportement{"méchant"}, recompense{100}
    {}

    string Brigand::getNom() const {
        return Humain::getNom() + " le " + this->comportement;
    }

    float Brigand::getMiseAPrix() const {
        return recompense;
    }

    void Brigand::kidnapper(const Dame &dame) {
        this->parler("Ah ah !" + dame.getNom() + ", tu est mienne désormais !");
        dame.seFaireKidnapper(this);
        nbDamesEnlevees += 1;
    }

    bool Brigand::seFaireEmprisonnerPar(const Sherif &sherif) {
        this->parler("Damned, je suis fait ! " + sherif.getNom() + ", tu m'as eu !");
        enPrison = true;
    }

    void Brigand::sePresenter() const {
        Humain::sePresenter();
        if(this->nbDamesEnlevees > 0) {
            if(this->enPrison == false) {
            this->parler("Je suis " + this->comportement + ", j'ai capturé " + to_string(this->nbDamesEnlevees) + " femmes au total."); //? Comment arrondir au centieme ?
            this->parler("La récompense pour ma capture est de " + to_string(this->recompense) + "$");
            }
            else {
                this->parler("Je suis " + this->comportement + ", j'ai capturé " + to_string(this->nbDamesEnlevees) + " femmes au total, avant d'arriver en prison.");
            }
        }
        else {
            if(this->enPrison == false) {
            this->parler("Je suis " + this->comportement + ", mais je n'ai toujours pas capturé de femme à ce jour ! "); //? Comment arrondir au centieme ?
            this->parler("La récompense pour ma capture est de " + to_string(this->recompense) + "$");
            }
            else {
                this->parler("Je suis " + this->comportement + ", mais je suis allé en prison sans avoir le temps de capturé une seule femme !");
            }
        }
    }



}