#include "monstre.hpp"
#include "chevalier.hpp"
#include <iostream> // cout, endl

using namespace std;

Monstre::Monstre(string nom, int nbPtsVie, int force)
    : nom{nom}, nbPtsVie{nbPtsVie}, force{force} 
{
    cout << "Monstre " << nom << " créé" << endl;
}

void Monstre::rugir(string message) {
    cout << "--" << nom << "(" << nbPtsVie << ")" << "-- : " << message << endl;
}

Monstre::~Monstre() {
    cout << "destruction du Monstre" << nom << endl;
}

void Monstre::attaquerSournoisement(Chevalier *chevalier) {
    rugir("Je vais te tuer !");
    chevalier->subirAttaque(force);
}

void Monstre::attaquerFranchement(Chevalier *chevalier) {
    rugir("Prends ça " + chevalier->titre);
    chevalier->subirEtContreAttaquer(this);
}

void Monstre::subirContreEtAttaquer(Chevalier *chevalier) {
    nbPtsVie = nbPtsVie - chevalier->puissArme;
    rugir("Je subit l'attaque !!! Aie");
    if(nbPtsVie > 0)
    {
        attaquerFranchement(chevalier);
    }
    else rugir("Je me meurs !! heurg heurg heurg !");
}

Monstre::Monstre()
    : Monstre{"Mr Inconnu", 15,18}
{
    cout << "Monstre " << nom << " créé" << endl;
}