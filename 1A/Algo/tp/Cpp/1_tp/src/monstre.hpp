#ifndef MONSTRE_HPP
#define MONSTRE_HPP

#include <string> // string

class Chevalier;

class Monstre {
public:
    std::string nom;
    int nbPtsVie;
    int force;
    
    Monstre(std::string nom, int nbPtsVie=10, int force=1);
    Monstre();
    void rugir(std::string message);
    void attaquerSournoisement(Chevalier *chevalier);
    void attaquerFranchement(Chevalier *chevalier);
    void subirContreEtAttaquer(Chevalier *chevalier);
    ~Monstre();
};

#endif // MONSTRE_HPP