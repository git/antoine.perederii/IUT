#include "chevalier.hpp"
#include "monstre.hpp"
#include <iostream> //Sert aux cout 

using namespace std;

Chevalier::Chevalier(string titre, int bouclier, int arme)
    : titre{titre}, ptsDeVie{10}, puissArme{arme}, puissBouclier{bouclier}
{
    cout << "Naissance de " << titre << endl;
}

void Chevalier::parler(string message) {
    cout << "**" << titre << "(" << ptsDeVie << "," << puissArme << "," << puissBouclier << ")** : " << message << endl;
}

Chevalier::~Chevalier() {
    cout << "disparition de " << titre << endl;
}

Chevalier::Chevalier(int bouclier, int arme)
    : Chevalier{"Mr Incoonu", bouclier, arme}
{
    cout << "Naissance de " << titre << endl;
}

void Chevalier::subirAttaque(int degats) {
    if(degats > puissBouclier)
    {
        ptsDeVie = ptsDeVie -(degats - puissBouclier);
        parler("Je suis touché");
    }
    else parler("Tu m'as manqué !");
}

void Chevalier::subirEtContreAttaquer(Monstre *monstre) {
    subirAttaque(monstre->force);
    if(ptsDeVie > 0)
    {
        parler(monstre->nom + " tu crois que je vais me laisser faire !");
        monstre->subirContreEtAttaquer(this);
    }
    else parler("Aie ! Je me meurs !!!");
}