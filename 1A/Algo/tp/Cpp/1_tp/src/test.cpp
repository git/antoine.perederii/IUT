#include "monstre.hpp"
#include "chevalier.hpp"

void testMonstre(void) {
    Monstre globulus{"Globulus", 15, 5};
    Monstre nemo{"Nemo"};
    globulus.rugir("Tremblez!");
    nemo.rugir("Vous allez trépasser!");
}

void testChevalier(void) {
    Chevalier arthur{"Sir Arthur", 160, 90};
    Chevalier inconnu{16,50};
    arthur.parler("Hors de ma vu !!!");
    inconnu.parler("Je suis un inconnu !!!");
}

void testP4(void)
{
    Chevalier sirArthur{"Sir Arthur",3,5};
    Monstre megadino{"Megadino",15,5};
    Monstre darkman{"Darkman",2,2};

    sirArthur.parler("Quelle belle journée");

    megadino.attaquerSournoisement(&sirArthur);
    sirArthur.parler(" eh bien je ne l'avais pas vu venir");

    darkman.attaquerSournoisement(&sirArthur);
    sirArthur.parler("décidement ...");
}

void testP5(void)
{
    Chevalier sirArthur{"Sir Arthur",3,5};
    Monstre megadino{"Megadino",15,5};

    sirArthur.parler("Quelle belle journée");
    megadino.attaquerFranchement(& sirArthur);
    if(sirArthur.ptsDeVie > 0){
        sirArthur.parler("J'ai eu chaud");
    }
}

void testValgrind(void){
    int *pt;
    pt = new int[100];
    delete[] pt;
    
    Monstre *monstre[5];
    monstre[0] = new Monstre();
    monstre[1] = new Monstre();
    monstre[2]->rugir("RAOOO pas content !!!");
    // monstre[10]->rugir("Mais ca va aller");
    // delete[] monstre;
}

int main(void){
    // test();
    // testChevalier();
    // testP4();
    testP5();
    // testValgrind();
    return 0;
}
