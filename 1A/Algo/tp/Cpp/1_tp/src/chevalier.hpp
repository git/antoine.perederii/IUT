#ifndef CHEVALIER_HPP
#define CHEVALIER_HPP

#include <string>

class Monstre;

class Chevalier {
public:   
    std::string titre;
    int ptsDeVie;
    int puissArme;
    int puissBouclier;

    Chevalier(std::string titre, int bouclier, int arme);
    Chevalier(int bouclier, int arme);
    void subirAttaque(int degats);
    void parler(std::string message);
    void subirEtContreAttaquer(Monstre *monstre);
    ~Chevalier();
};

#endif // CHEVALIER_HPP