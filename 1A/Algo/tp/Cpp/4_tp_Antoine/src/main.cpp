#include <iostream>
#include <map>
#include <unordered_set>
#include <list>

#include "unite.hpp"
#include "quantite.hpp"
#include "ingredient.hpp"
#include "recette.hpp"
#include "recetteAffichage.hpp"
#include "livreRecettes.hpp"

using namespace std;

void testUnite(){
    cout << "------------------------------" << endl;
    cout << "         testUnite()          " << endl;
    cout << "------------------------------" << endl;

    cout << "Résultat attendu => résultat" << endl;
    cout << "kg => " << appli::Unite::KG << endl;
    cout << "g => " << appli::Unite::G << endl;
    cout << "mL => " << appli::Unite::ML << endl;
    cout << "cL => " << appli::Unite::CL << endl;
    cout << "dL => " << appli::Unite::DL << endl;
    cout << "L => " << appli::Unite::L << endl;
    cout << " => " << appli::Unite::UNITE << endl;
}

void testQuantite(){
    cout << "------------------------------" << endl;
    cout << "       testQuantite()         " << endl;
    cout << "------------------------------" << endl;

    cout << "Résultat attendu => résultat" << endl;
    cout << "1.0 => " << appli::Quantite{} << endl;
    cout << "2.5 => " << appli::Quantite{2.5} << endl;
    cout << "1.5L => " << appli::Quantite{1.5, appli::Unite::L} << endl;

    // Partie 3:
    cout << "15L => " << appli::Quantite{15, appli::Unite::L}.normaliser() << endl;
    cout << "1.5L => " << appli::Quantite{15, appli::Unite::DL}.normaliser() << endl;
    cout << "0.15L => " << appli::Quantite{15, appli::Unite::CL}.normaliser() << endl;
    cout << "0.015L => " << appli::Quantite{15, appli::Unite::ML}.normaliser() << endl;
    cout << "15L => " << appli::Quantite{15, appli::Unite::L}.normaliser() << endl;
    cout << "0.8kg => " << appli::Quantite{0.8, appli::Unite::KG}.normaliser() << endl;
    cout << "0.5kg => " << appli::Quantite{500, appli::Unite::G}.normaliser() << endl;
    cout << "2 => " << appli::Quantite{2, appli::Unite::UNITE}.normaliser() << endl;

    cout << "1.5kg => " << appli::Quantite{1, appli::Unite::KG}+appli::Quantite{500, appli::Unite::G} << endl;
    cout << "0.75L => " << appli::Quantite{5, appli::Unite::DL}+appli::Quantite{250, appli::Unite::ML} << endl;
    cout << "Détection d'erreur ?" << endl;
    cout << appli::Quantite{5, appli::Unite::DL}+appli::Quantite{250, appli::Unite::G} << endl;
    
}

void testIngredient(){
    cout << "------------------------------" << endl;
    cout << "     testIngredient()         " << endl;
    cout << "------------------------------" << endl;

    appli::Ingredient i1{"poireaux", appli::Quantite{2.0, appli::Unite::UNITE}};
    appli::Ingredient i2{"pommes de terre", appli::Quantite{300, appli::Unite::G}};
    appli::Ingredient i3{"eau", appli::Quantite{50, appli::Unite::CL}};

    cout << "Résultat attendu => résultat" << endl;
    cout << "2 poireaux => " << i1 << endl;
    cout << "300g pommes de terre => " << i2 << endl;
    cout << "50cL eau => " << i3 << endl;
    
    // à compléter
    
}

void testRecette(){
    cout << "------------------------------" << endl;
    cout << "        testRecette()         " << endl;
    cout << "------------------------------" << endl;

    appli::Recette r{"Soupe de poireaux", "Faire cuire les légumes puis mixer."};

    unordered_set<appli::Ingredient> ing;
    ing.insert(appli::Ingredient{"poireaux", appli::Quantite{2, appli::Unite::UNITE}});
    ing.insert(appli::Ingredient{"pommes de terre", appli::Quantite{300, appli::Unite::G}});
    ing.insert(appli::Ingredient{"eau", appli::Quantite{50, appli::Unite::CL}});

    cout << "Résultat attendu" << endl << "=>" << endl << "résultat" << endl << endl;
    cout << "Soupe de poireaux\n\t300g pommes de terre\n\t50cL eau\n\t2 poireaux" << endl << "=>" << endl;
    afficherNomEtIngredients(r, ing);
    
    cout << endl;
    cout << "Soupe de poireaux\n\t300g pommes de terre\n\t50cL eau\n\t2 poireaux\ndescription :\nFaire cuire les légumes puis mixer." << endl << "=>" << endl;
    afficherTout(r, ing);
		
    appli::Recette r2{"Crêpes", "Faire fondre le beurre. Mélanger la farine, le sel et le sucre. Y ajouter les oeufs puis le lait et enfin le beurre fondu. Faire cuire à la poêle quelques minutes de chaque côté."};

    unordered_set<appli::Ingredient> ing2;
    ing2.insert(appli::Ingredient{"farine", appli::Quantite{250, appli::Unite::G}});
    ing2.insert(appli::Ingredient{"lait", appli::Quantite{0.5, appli::Unite::L}});
    ing2.insert(appli::Ingredient{"sucre", appli::Quantite{30, appli::Unite::G}});
    ing2.insert(appli::Ingredient{"oeufs", appli::Quantite{4, appli::Unite::UNITE}});
    ing2.insert(appli::Ingredient{"sel", appli::Quantite{3, appli::Unite::G}});
    ing2.insert(appli::Ingredient{"beurre", appli::Quantite{50, appli::Unite::G}});

    map<appli::Recette, unordered_set<appli::Ingredient>> recettes;
    recettes.insert(pair{r,ing});
    recettes.insert(pair{r2,ing2});
    
    cout << endl;
    cout << "Crêpes\n\t50g beurre\n\t3g sel\n\t4 oeufs\n\t30g sucre\n\t0.5L lait\n\t250g farine\ndescription :\nFaire fondre le beurre. Mélanger la farine, le sel et le sucre. Y ajouter les oeufs puis le lait et enfin le beurre fondu. Faire cuire à la poêle quelques minutes de chaque côté.\nSoupe de poireaux\n\t300g pommes de terre\n\t50cL eau\n\t2 poireaux\ndescription :\nFaire cuire les légumes puis mixer." << endl << "=>" << endl;
    afficherTout(recettes);
    

    cout << endl;
    cout << "Crêpes\nSoupe de poireaux" << endl << "=>" << endl;
    afficherNoms(recettes);
    

}


void testLivreRecettes(){
    cout << "------------------------------" << endl;
    cout << "    testLivreRecettes()       " << endl;
    cout << "------------------------------" << endl;

    appli::Recette r{"Soupe de poireaux", "Faire cuire les légumes puis mixer."};
    list<appli::Ingredient> ing;
    ing.push_back(appli::Ingredient{"poireaux", appli::Quantite{2, appli::Unite::UNITE}});
    ing.push_back(appli::Ingredient{"pommes de terre", appli::Quantite{300, appli::Unite::G}});
    ing.push_back(appli::Ingredient{"eau", appli::Quantite{50, appli::Unite::CL}});

    appli::Recette r1{"Soupe de poireaux", "Faire cuire les légumes et lentilles puis mixer."};
    list<appli::Ingredient> ing1;
    ing1.push_back(appli::Ingredient{"poireaux", appli::Quantite{3, appli::Unite::UNITE}});
    ing1.push_back(appli::Ingredient{"lentilles corail", appli::Quantite{200, appli::Unite::G}});
    ing1.push_back(appli::Ingredient{"carottes", appli::Quantite{200, appli::Unite::G}});

    appli::Recette r2{"Crêpes", "Faire fondre le beurre. Mélanger la farine, le sel et le sucre. Y ajouter les oeufs puis le lait et enfin le beurre fondu. Faire cuire à la poêle quelques minutes de chaque côté."};
    list<appli::Ingredient> ing2;
    ing2.push_back(appli::Ingredient{"farine", appli::Quantite{250, appli::Unite::G}});
    ing2.push_back(appli::Ingredient{"lait", appli::Quantite{0.5, appli::Unite::L}});
    ing2.push_back(appli::Ingredient{"sucre", appli::Quantite{30, appli::Unite::G}});
    ing2.push_back(appli::Ingredient{"oeufs", appli::Quantite{4, appli::Unite::UNITE}});
    ing2.push_back(appli::Ingredient{"sel", appli::Quantite{3, appli::Unite::G}});
    ing2.push_back(appli::Ingredient{"beurre", appli::Quantite{50, appli::Unite::G}});

    appli::LivreRecettes livre;

    livre.ajouter(r, ing);
    livre.ajouter(r1, ing1);
    livre.ajouter(r2, ing2);

    livre.afficherNomRecette();

    cout << "--------------------------------\n";
    list<appli::Recette> listeRecettes;
    listeRecettes.push_back(r);
    listeRecettes.push_back(r1);
    map<string,appli::Quantite> listeDeCourses = livre.faireListeCourses(listeRecettes);
    cout << "liste de courses nécessaires : \n";
    for(auto p : listeDeCourses){
      cout <<"\t" <<  p.first << " : " << p.second << endl;
    }
      

}

int main(){
  //    testUnite();
  // testQuantite();
  // testIngredient();
  // testRecette();
  testLivreRecettes();
    
    return 0;
}
