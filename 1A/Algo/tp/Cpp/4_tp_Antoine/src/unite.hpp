#ifndef UNITE_HPP
#define UNITE_HPP

#include <iostream>

namespace appli {

    enum class Unite{KG, G, L, DL, CL, ML, UNITE};

    std::ostream &operator<<(std::ostream &os, const Unite &u);

}

#endif // UNITE_HPP