#include "livreRecettes.hpp"
#include "recetteAffichage.hpp"
#include "ingredient.hpp"
#include <map>
#include <algorithm>

using namespace std;

namespace appli {

    map<Recette, unordered_set<Ingredient>> LivreRecettes::rechercherRecette(const string &nomRecette) {
        map<Recette, unordered_set<Ingredient>> rech;
        for(auto it = livre.cbegin(); it != livre.cend(); ++it) {
            if(it->first.getNom() == nomRecette) {
                rech.insert({it->first, it->second});
            }
        }
        return rech;
    }

  /*
    map<string, Quantite> LivreRecettes::faireListeCourses(list<Recette> l) {
        string nomRecette;
        int rep;

        cout << "Quelle recette voulez vous ? " << endl;
        cin >> nomRecette;
        map<Recette, unordered_set<Ingredient>>::iterator it = livre.find(nomRecette);
        // it = this->rechercherRecette(nomRecette);

        for(const Ingredient &ing : it->second) {
            cout << "avez vous du, des " << ing << " ? (0 : non/1 : oui)" << endl;
            cin >> rep;
            if(rep == 0) {
                l.push_back(ing);
            }
        }
    }
  */

    // comme ça : on fait la liste de tous les ingrédients nécessaire pour faire les recettes choisies
    //    (sans prendre en compte si on possède déjà des aliments)
    map<string, Quantite> LivreRecettes::faireListeCourses(list<Recette> l) {
    	map<string, Quantite> listeDeCourses;
	
	// pour chaque recette de la liste de recettes que l'on souhaite réaliser : 
	for(const Recette & r : l){
	  map<Recette, unordered_set<Ingredient>>::iterator it = livre.find(r);
	  if(it == livre.end()) cout << "recette inconnue\n";
	  else{
	    for(const Ingredient &ing : it->second) {
	      // si l'ingrédient est déjà dans la liste de courses on met à jour sa quantité
	      map<string,Quantite>::iterator itIng = listeDeCourses.find(ing.getAliment());
	      if(itIng !=listeDeCourses.end()){
		itIng->second = itIng->second + ing.getQuantite();
	      }
	      else{
		// sinon on l'ajoute dans la liste de courses
		listeDeCourses[ing.getAliment()] = ing.getQuantite();
	      }
	    }
	  }
	}

	return listeDeCourses;
    }


  
    void LivreRecettes::ajouter(const Recette &r, list<Ingredient> l) {
        unordered_set<Ingredient> ing;
        if(livre.find(r) != livre.end()) {
            cout << "recette déjà présente" << endl;
            return;
        }
        for(Ingredient i : l) {
            unordered_set<Ingredient>::iterator it2 = ing.find(i);
            if(it2 == ing.end()) {
                ing.insert(i);
            }
            else {
                Quantite q = i.getQuantite() + it2->getQuantite();
                ing.erase(it2);
                ing.insert(Ingredient{i.getAliment(), q});
            }
        }
        livre.insert({r, ing});
    }

    void LivreRecettes::afficherNomRecette() {
        afficherNoms(livre);
    }

    void LivreRecettes::afficher() {
        afficherTout(livre);
    }

}
