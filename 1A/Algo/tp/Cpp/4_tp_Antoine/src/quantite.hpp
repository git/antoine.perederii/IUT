#ifndef QUANTITE_HPP
#define QUANTITE_HPP

#include <iostream>
#include "unite.hpp"


namespace appli {

    class Quantite {

        double nombre;
        Unite unite;

    public:

        Quantite(double nombre=1, const Unite &unite=Unite::UNITE);

        Quantite normaliser() const;

        double getNombre() const;
        Unite getUnite() const;
    };

    std::ostream &operator<<(std::ostream &os, const Quantite &q);
    Quantite operator+(const Quantite &q1, const Quantite &q2);

}


#endif // QUANTITE_HPP
