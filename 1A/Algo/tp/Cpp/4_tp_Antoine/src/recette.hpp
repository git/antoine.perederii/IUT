#ifndef RECETTE_HPP
#define RECETTE_HPP

#include <iostream>
#include "ingredient.hpp"


namespace appli {

    class Recette {

        std::string nom;
        std::string description;

    public:

        Recette(const std::string &nom, const std::string &description);
        
        std::string getNom() const;
        std::string getDescription() const;
    };

    bool operator<(const Recette &r1, const Recette &r2);

}



#endif //RECETTE_HPP