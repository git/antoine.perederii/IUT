#ifndef RECETTEAFFICHAGE_HPP
#define RECETTEAFFICHAGE_HPP

#include "recette.hpp"
#include "ingredient.hpp"
#include <unordered_set>
#include <map>


void afficherNomEtIngredients(const appli::Recette &r, const std::unordered_set<appli::Ingredient> &ingred);
void afficherTout(const appli::Recette &r, const std::unordered_set<appli::Ingredient> &ingredients);
void afficherTout(const std::map<appli::Recette, std::unordered_set<appli::Ingredient>> &recettes);
void afficherNoms(const std::map<appli::Recette, std::unordered_set<appli::Ingredient>> &recettes);

#endif // RECETTEAFFICHAGE_HPP