#ifndef LIVRERECETTES_HPP
#define LIVRERECETTES_HPP

#include "recette.hpp"
#include "ingredient.hpp"
#include <map>
#include <unordered_set>
#include <list>

namespace appli {

    class LivreRecettes {

            std::map<Recette, std::unordered_set<Ingredient>> livre;

        public:

            std::map<Recette, std::unordered_set<Ingredient>> rechercherRecette(const std::string &nomRecette);
            std::map<std::string, Quantite> faireListeCourses(std::list<Recette> l);

            void ajouter(const Recette &r, std::list<Ingredient> l);
            void afficherNomRecette();
            void afficher();
    };

}


#endif // LIVRERECETTES_HPP