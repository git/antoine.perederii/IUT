#ifndef PASSAGER_HPP
#define PASSAGER_HPP

#include <string>
#include <iostream>

#include "train.hpp"   //// pour que l'on connaisse les méthode de train

class Wagon;

class Passager {
private :
    
    std::string nom;
    std::string prenom;

    Wagon *wagonActuel;

public :

    Passager(const std::string &nom, const std::string &prenom);

    Wagon* getWagonActuel() const;

    friend int Train::monterDansLeTrain(int numeroWagon, Passager& lePassager);         ///// ne pas oublier Train::
    friend int Train::descendreDuTrain(Passager& lePassager);                           ///// idem
    friend int Train::deplacerAuWagonSuivant(Passager& lePassager);                     ///// idem
    friend int Train::deplacerAuWagonPrecedent(Passager& lePassager);                   ///// idem

    friend std::ostream &operator<<(std::ostream &s, const Passager &p);
    
    ~Passager()=default;          ///////// =default pour dire que celui par défaut suffit => pas besoin de le définir dans le .cpp  
};


#endif // PASSAGER_HPP
