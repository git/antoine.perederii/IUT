#ifndef TRAIN_HPP
#define TRAIN_HPP

#include <string>
#include <vector>

#include "wagon.hpp"

class Passager;   //// nécessaire pour pouvoir en prendre par ref en param des méthodes

class Train {
private :
    
    bool roule;

  //     std::list<Wagon*> lesWagons;
  std::vector<Wagon> lesWagons;          //// on avait dit en tp que ce serait un vector de wagon

public :
        
        Train(int nombreDeWagons);
        
        void demarrer();
        void arreter();
        bool isRoule() const;
        
        int monterDansLeTrain(int numeroWagon, Passager& lePassager);
        int descendreDuTrain(Passager& lePassager);
        int deplacerAuWagonSuivant(Passager& lePassager);
        int deplacerAuWagonPrecedent(Passager& lePassager);

        int monterDansLeTrainAPartirDe(int numeroWagon, Passager& lePassager);

        friend std::ostream &operator<<(std::ostream &s, const Train &t);       /// ne pas oublier cette ligne !!!
        ~Train() = default;
};




#endif // TRAIN_HPP
