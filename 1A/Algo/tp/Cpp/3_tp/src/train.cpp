#include "train.hpp"
#include "wagon.hpp"  // nécessaire pour connaitre tout (seulement ce qui est public) ce qui compose un wagon
#include "passager.hpp"


using namespace std;

Train::Train(int nbWagons)
  : roule{false}                ////////////// comme ça !!!
{                
	for (int i = 1; i <= nbWagons; ++i) {
	       lesWagons.push_back(Wagon{i});
	}
}

void Train::demarrer() {
	roule = true;
}

void Train::arreter() {
	roule = false;
}

bool Train::isRoule() const {
	return roule;
}

int Train::monterDansLeTrain(int numeroWagon, Passager& lePassager) {
	if (roule) {
		return -1;
	}
        if (numeroWagon < 1 || numeroWagon > (int)lesWagons.size()) {
		return -2;
	}

	//////	if (lesWagons[numeroWagon - 1].ajouter(lePassager) == -1) {          //// non
	Wagon& w = lesWagons.at(numeroWagon-1);
 	if (w.ajouter(lePassager) == 0) {       ///// c'est comme ça vu le code de ajouter dans wagon !!!
		return -3;
	}
	lePassager.wagonActuel = & w;        //// sans oublier cette ligne !!!
	
	return 0;
}

int Train::descendreDuTrain(Passager &lePassager) {
	if (roule) {
		return -1;
	}

	///////////////////  NON pas comme ça, ne veut rien dire
	/* 
	if (lePassager.getWagonActuel().getNumero() < 1 || lePassager.getWagonActuel().getNumero() > lesWagons.size()) {
		return -2;
	}
	*/
	Wagon* w  = lePassager.getWagonActuel();           ////// comme ça
	if(w==nullptr) return -2;
	
	if (w->enlever(lePassager) == 0) {
		return -3;
	}
	lePassager.wagonActuel=nullptr;   /////// et on n'oublie pas cette ligne !!!!
	return 0;
}
/*
int Train::deplacerAuWagonSuivant(Passager &lePassager) {
	if (lePassager.getWagonActuel().getNumero() < 1 || lePassager.getWagonActuel().getNumero() > lesWagons.size()) {
		return -1;
	}
	if (lePassager.getWagonActuel().getNumero() == lesWagons.size()) {
		return -2;
	}
	// if (lePassager.getWagonActuel().enlever(lePassager) == -1) { Pas besoins si on l'ajout après ???!!!
	// 	return -4;
	// }
	if (lesWagons[lePassager.getWagonActuel().getNumero()].ajouter(lePassager) == -1) {
		return -3;
	}
	return 0;
}
*/

///// pour cette fonction on peut faire ainsi : 
int Train::deplacerAuWagonSuivant(Passager &lePassager) {
  Wagon* w = lePassager.getWagonActuel();        ///// comme ça on va éviter de faire plusieurs fois le même appel

  if(w==nullptr){
    return -1 ; //n'a pas de wagon => pas dans le train
  }

  unsigned int num = w->getNumero(); //! unsigned permet d'avoir que des nombres positifs !
  if(num == lesWagons.size())     return -2;

  // on tente de faire passer le passager au wagon suivant (num+1) qui est dans la case num
  int fait = lesWagons[num].ajouter(lePassager);
  if(fait == 0) return-3;         // il n'y avait plus de place

  // il ne nous reste plus qu'à l'enlever du wagon num
  lesWagons[num-1].enlever(lePassager); //! une fois ajouter dans le prochain wagon, ne pas oublier de l'enlever du wagon actuel !

  // et on met à jour l'attribut wagonActuel du passager
  lePassager.wagonActuel = & lesWagons[num]; 
  return 0;
  
}



/*
int Train::deplacerAuWagonPrecedent(Passager &lePassager) {
	if (lePassager.getWagonActuel().getNumero() < 1 || lePassager.getWagonActuel().getNumero() > lesWagons.size()) {
		return -1;
	}
	if (lePassager.getWagonActuel().getNumero() == 1) {
		return -2;
	}
	// if (lePassager.getWagonActuel().enlever(lePassager) == -1) { Pareil ici que l.66 ??!!
	// 	return -4;
	// }
	if (lesWagons[lePassager.getWagonActuel().getNumero() - 2].ajouter(lePassager) == -1) {
		return -3;
	}
	return 0;
}
*/

int Train::deplacerAuWagonPrecedent(Passager &lePassager) {
 Wagon* w = lePassager.getWagonActuel();
  if(w==nullptr){
    return -1 ; //n'a pas de wagon => pas dans le train
  }
  
  unsigned num = w->getNumero();
  if (num==1) return -2; //* le wagon 1 n'a pas de wagon précédent

  // on tente de faire passer le passager au wagon précédent (num-1)
  int fait = lesWagons[num-2].ajouter(lePassager);
  if(fait == 0) return-3;

  // il ne nous reste plus qu'à l'enlever du wagon num
  lesWagons[num-1].enlever(lePassager);

  // et on met à jour l'attribut wagonActuel du passager
  lePassager.wagonActuel = & lesWagons[num-2];
  return 0;

}

/*
int Train::monterDansLeTrainAPartirDe(int numeroWagon, Passager &lePassager) {
	int i;
	if (numeroWagon < 1 || numeroWagon > lesWagons.size()) {
		return -2;
	}
	if (roule) {
		return -1;
	}
	for (i = numeroWagon - 1; i < lesWagons.size() && lesWagons[i].ajouter(lePassager) == 0; ++i) {
		return -3;
	}
	return i--;
}
*/

int Train::monterDansLeTrainAPartirDe(int numeroWagon, Passager& lePassager){
  int codeErreur;
  
  codeErreur = monterDansLeTrain(numeroWagon,lePassager);
  if(codeErreur==-1) return -1;
  if(codeErreur==0) return numeroWagon; //! Pas oublier de rajouter ça ??!!

  unsigned int taille = lesWagons.size(); //! unsigned permet d'avoir que des nombres positifs !
  										//? Que se passe-t-il si on a un nombre négatif ???

  if(numeroWagon <=0 || numeroWagon > (int)taille) return -2; //? Pk on met le int ici ???
  // rajouter (unsigned int) avant le numwagon
  
  for(unsigned i = numeroWagon+1; i<=taille; i++){ //? Pourquoi on le declare pas en int ???
    codeErreur = monterDansLeTrain(i,lePassager);
    if(codeErreur==0) return i;
    
  }
  for(unsigned i = numeroWagon-1; i>0; i--){  //* A revoir les utilisation des for ici ==> on monte d'abord en + puis en -
    codeErreur = monterDansLeTrain(i,lePassager);
    if(codeErreur==0) return i;
  }
  return -3; 
}



ostream &operator<<(ostream &s, const Train &t) {
	s << "Train : " << t.lesWagons.size() << " wagon(s)." << endl;
	s << "Liste des wagons :\n";

	/*        NON pas comme ça
	for (Wagon* w : t.lesWagons){	
		s << "\t" << "Wagon n°" << w->numero << " : " << w->lesPassagers.size() << " passager(s)." << endl;
		s << "\t" << "Reste " << w->capacite - w->lesPassagers.size() << " places(s)." << endl;
		s << "\t" << "Liste des passagers :\n";
		for (Passager* p : w->lesPassagers)
			s << "\t\t" << *p << endl;
	}
	*/
	
	// comme ça : 
	for (const Wagon & w : t.lesWagons){
	  cout << w << endl;             //// on utilise l'operator<< des wagons !!!!
	}
	return s;                                                     
}
