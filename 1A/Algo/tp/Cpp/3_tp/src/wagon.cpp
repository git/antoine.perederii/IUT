#include "wagon.hpp"
#include "passager.hpp"
#include <iostream>
#include <string>
#include <algorithm>          // pour que find soit connu

using namespace std;

Wagon::Wagon(int numero)
    :numero{numero}, lesPassagers{}
{}

int Wagon::getNumero() const {
    return numero;
}

int Wagon::ajouter(Passager& lePassager) {
    if(lesPassagers.size() >= capacite) {
        return 0;
    }
    lesPassagers.push_back(&lePassager);
    return 1;
}

int Wagon::enlever(const Passager& lePassager) {
    list<Passager*>::iterator it = find(lesPassagers.begin(), lesPassagers.end(), &lePassager);
    if(it == lesPassagers.end()) return 0;
    lesPassagers.erase(it);
    return 1;
}

ostream &operator<<(ostream &s, const Wagon &w) {
    s << "Wagon n° " << w.numero << " : " << w.lesPassagers.size() << " passager(s)." << endl;
    s << "Reste " << w.capacite - w.lesPassagers.size() << " places(s)." << endl;
    s << "Liste des passagers :\n";                        /////
    for(Passager* ptp : w.lesPassagers)                    /////  comme ça
      s<<"\t"<< *ptp<<endl;                                /////
    return s;                                                      
}
