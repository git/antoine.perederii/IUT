#ifndef WAGON_HPP
#define WAGON_HPP

#include <string>
#include <list>
#include <iostream>

class Passager;

class Wagon {

private :
    
    int numero;

    std::list<Passager*> lesPassagers;

public :

    static const int capacite = 20;

    Wagon(int numero);

    int getNumero() const;
    
    int ajouter(Passager& lePassager);
    int enlever(const Passager& lePassager);

    friend std::ostream &operator<<(std::ostream &s, const Wagon &w);

    ~Wagon() = default; //~Wagon(){};
};


#endif // WAGON_HPP
