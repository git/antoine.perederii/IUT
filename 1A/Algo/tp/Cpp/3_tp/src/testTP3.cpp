#include "passager.hpp"
#include "train.hpp"

using namespace std;

void testMonterDansTrain(void){
  Passager p1("Dupont", "Pierre");
  Train t(3);
  int codeRetour;

  //cout<< "-----------------------\n";
  //cout << t << endl;
  //cout << "----------------------\n";

  cout << "vérif si impossible de monter dans un train qui roule :";
  t.demarrer();
  codeRetour=t.monterDansLeTrain(1,p1);
  if(codeRetour == -1) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  cout << "vérif si impossible de monter si num de wagon non existant :"; 
  t.arreter();
  codeRetour=t.monterDansLeTrain(4,p1);
  if(codeRetour == -2) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";


  cout << "vérif si possible de monter dans wagon existant, non complet et train arrêté : ";
  Passager* tab[20];
  int tlog;
  for(tlog=0; tlog<20; tlog++){
    tab[tlog]=new Passager("Pierre", "Nom"+to_string(tlog+1));
    codeRetour=t.monterDansLeTrain(1,*tab[tlog]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << tlog+1 << "eme passager( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }
  if(codeRetour==0) cout << " OK\n";
  else cout << "ERREUR (CodeRetour = " << codeRetour << ")\n";
  
  cout << "vérif si impossible de monter dans le wagon si déjà complet : ";
  codeRetour=t.monterDansLeTrain(1,p1);
  if (codeRetour == -3) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";


  cout << "vérif si le passager sait qu'il est monté dans un wagon : ";
  Wagon* w = tab[0]->getWagonActuel();
  if(w==nullptr) cout << "ERREUR : il ne connait pas son wagon\n";
  else cout << "OK\n"; // il sait être dans un wagon

  
  // libération des passagers
  for(int i=0; i<tlog; i++){
    delete tab[i];
  }
}


void testDescendreDuTrain(void){
  Passager p1("Dupont", "Pierre");
  Passager p2("Durand", "Paul");
  Train t(3);
  int codeRetour;

  //cout<< "--------Pierre monte dans le wagon 1 ---------------\n";
  t.monterDansLeTrain(1,p1);
  //cout << t << endl;
  //cout << "----------------------\n";

  
  cout << "vérif si impossible de descendre du train qui roule :";
  t.demarrer();
  codeRetour=t.descendreDuTrain(p1);
  if(codeRetour == -1) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  cout<< "--------Paul n'est pas dans le train (ne connait pas son wagon) ---------------\n";
  cout << "vérif si impossible de descendre si le passager ne connaît pas son wagon (il n'est sûrement pas dans le train)  :"; 
  t.arreter();
  codeRetour=t.descendreDuTrain(p2);
  if(codeRetour == -2) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";


  cout << "vérif si possible de descendre du wagon existant et train arrêté : ";
  codeRetour=t.descendreDuTrain(p1);
  if(codeRetour==0) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";
    
  cout << "vérif si le passager a bien conscience de ne plus être dans le train : ";
  if(p1.getWagonActuel()==nullptr) cout << "OK\n";
  else{
    cout << "ERREUR ( il croit être dans le wagon : ";
    cout << p1.getWagonActuel() << ")\n";
  }
}

void testDeplacerAuWagonSuivant(){
  Passager p0("Dups", "Alice");
  Passager p1("Dupont", "Pierre");
  Passager p2("Durand", "Paul");
  Passager p3("Dumont", "Marie");
  Train t(3);
  int codeRetour;

  t.monterDansLeTrain(1,p1);
  t.monterDansLeTrain(2,p2);
  t.monterDansLeTrain(3,p3);

  /*
  cout<< "-----------------------\n";
  cout << t << endl;
  cout << "----------------------\n";
  */
  
  codeRetour=t.deplacerAuWagonSuivant(p0);
  cout << "vérif si impossible car le passager n'est pas dans le train : ";
  if(codeRetour == -1) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  codeRetour=t.deplacerAuWagonSuivant(p3);
  cout << "vérif si impossible car est déjà dans le dernier wagon :"; 
  if(codeRetour == -2) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";


  Passager* tab[19];
  int tlog;
  for(tlog=0; tlog<19; tlog++){
    tab[tlog]=new Passager("Pierre", "Nom"+to_string(tlog+1));
    codeRetour=t.monterDansLeTrain(3,*tab[tlog]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << tlog+1 << "eme passager n'a pas pu monter( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }

  codeRetour=t.deplacerAuWagonSuivant(p2);
  cout << "vérif si impossible car wagon déjà complet : ";
  if (codeRetour == -3) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  codeRetour=t.deplacerAuWagonSuivant(p1);
  cout << "vérif si possible  de passer au wagon suivant (pas de pb) : ";
  if (codeRetour == 0) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  Wagon* w = p1.getWagonActuel();
  cout << "vérif si le passager sait qu'il est dans un wagon : ";
  if(w==nullptr) cout << "ERREUR : il ne connait pas son wagon\n";
  else cout << "OK\n"; // il sait être dans un wagon

  codeRetour=t.descendreDuTrain(p1);
  cout << "vérif si le passager est dans le bon wagon => il peut en descendre : ";
  if(codeRetour == 0) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";
  
  // libération des passagers qu'on a mis pour remplir le wagon 3
  for(int i=0; i<tlog; i++){
    delete tab[i];
  }
}


void testDeplacerAuWagonPrecedent(){
  Passager p0("Dups", "Alice");
  Passager p1("Dupont", "Pierre");
  Passager p2("Durand", "Paul");
  Passager p3("Dumont", "Marie");
  Train t(3);
  int codeRetour;

  t.monterDansLeTrain(1,p1);
  t.monterDansLeTrain(2,p2);
  t.monterDansLeTrain(3,p3);

  /*
  cout<< "-----------------------\n";
  cout << t << endl;
  cout << "----------------------\n";
  */
  codeRetour=t.deplacerAuWagonPrecedent(p0);
  cout << "vérif si impossible car le passager n'est pas dans le train : ";
  if(codeRetour == -1) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  codeRetour=t.deplacerAuWagonPrecedent(p1);
  cout << "vérif si impossible car est déjà dans le premier wagon :"; 
  if(codeRetour == -2) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";


  Passager* tab[19];
  int tlog;
  for(tlog=0; tlog<19; tlog++){
    tab[tlog]=new Passager("Pierre", "Nom"+to_string(tlog+1));
    codeRetour=t.monterDansLeTrain(1,*tab[tlog]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << tlog+1 << "eme passager n'a pas pu monter( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }

  codeRetour=t.deplacerAuWagonPrecedent(p2);
  cout << "vérif si impossible car wagon déjà complet : ";
  if (codeRetour == -3) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  codeRetour=t.deplacerAuWagonPrecedent(p3);
  cout << "vérif si possible  de passer au wagon précédent (pas de pb) : ";
  if (codeRetour == 0) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  Wagon* w = p3.getWagonActuel();
  cout << "vérif si le passager sait qu'il est dans un wagon : ";
  if(w==nullptr) cout << "ERREUR : il ne connait pas son wagon\n";
  else cout << "OK\n"; // il sait être dans un wagon

  codeRetour=t.descendreDuTrain(p3);
  cout << "vérif si le passager est dans le bon wagon => il peut en descendre : ";
  if(codeRetour == 0) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";
  
  // libération des passagers qu'on a mis pour remplir le wagon 1
  for(int i=0; i<tlog; i++){
    delete tab[i];
  }
}


void testMonterDansLeTrainAPartirDe(){
  Passager p0("Dups", "Alice");
  Passager p1("Dupont", "Pierre");
  Passager p2("Durand", "Paul");
  Passager p3("Dumont", "Marie");
  Train t(3);
  int codeRetour;

  codeRetour = t.monterDansLeTrainAPartirDe(4,p1);
  cout << "vérif si impossible de monter à partir d'un numero de wagon non existant :";
  if(codeRetour == -2) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";
  
  
  t.demarrer();
  codeRetour = t.monterDansLeTrainAPartirDe(1,p1);
  cout << "vérif si impossible de monter dans le train qui roule :";
  if(codeRetour == -1) cout << " OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  
  // on remplit tous les wagons
  t.arreter();
  Passager* tab1[20];
  int tlog1;
  for(tlog1=0; tlog1<20; tlog1++){
    tab1[tlog1]=new Passager("Pierre", "Nom"+to_string(tlog1+1));
    codeRetour=t.monterDansLeTrain(1,*tab1[tlog1]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << tlog1+1 << "eme passager n'a pas pu monter( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }
  Passager* tab2[20];
  int tlog2;
  for(tlog2=0; tlog2<20; tlog2++){
    tab2[tlog2]=new Passager("Paul", "Nom"+to_string(tlog2+1));
    codeRetour=t.monterDansLeTrain(2,*tab2[tlog2]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << tlog2+1 << "eme passager n'a pas pu monter( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }
  Passager* tab3[20];
  int tlog3;
  for(tlog3=0; tlog3<20; tlog3++){
    tab3[tlog3]=new Passager("Jacques", "Nom"+to_string(tlog3+1));
    codeRetour=t.monterDansLeTrain(3,*tab3[tlog3]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << tlog3+1 << "eme passager n'a pas pu monter( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }
  cout << "----------********train rempli*********-------------\n";
  cout << t;
  cout << "----------*****************-------------\n";
  codeRetour = t.monterDansLeTrainAPartirDe(1,p1);
  cout << "vérif si impossible car le train est complet : ";
  if (codeRetour == -3) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";


  cout << "---------- on vide le wagon 1-------------------\n";
  for(int i=0; i<tlog1; i++){
    codeRetour=t.descendreDuTrain(*tab1[i]);
    if(codeRetour != 0){
      cout << "ERREUR pour " << i+1 << "eme passager n'a pas pu descendre( CodeRetour = " << codeRetour << ")\n";
      break;
    }
  }
  cout << "----------*****************-------------\n";
  cout << t;
  cout << "----------*****************-------------\n";

  codeRetour = t.monterDansLeTrainAPartirDe(2,p1);
  if(codeRetour>0)
    cout << " OK, il est monté dans le wagon " << codeRetour << "\n";
  else
    cout << "ERREUR (CodeRetour = " << codeRetour << ")\n";  
  
  cout << "vérif si le passager est dans le bon wagon => il peut en descendre : ";
  codeRetour = t.descendreDuTrain(p1);
  if(codeRetour == 0) cout << "OK\n";
  else cout << "ERREUR ( CodeRetour = " << codeRetour << ")\n";

  
  // libération des passagers qu'on a mis pour remplir le wagon 1
  for(int i=0; i<tlog1; i++){
    delete tab1[i];
  }
  for(int i=0; i<tlog2; i++){
    delete tab2[i];
  }
  for(int i=0; i<tlog3; i++){
    delete tab3[i];
  }
}




int main(){
  cout<< "---------- TEST MONTER DANS LE TRAIN ------------\n";
  // testMonterDansTrain();

  cout << "---------- TEST DESCENDRE DU TRAIN ------------\n";
  //testDescendreDuTrain();

  cout << "---------- TEST DEPLACER AU WAGON SUIVANT ------------\n";
  //testDeplacerAuWagonSuivant();

  cout << "---------- TEST DEPLACER AU WAGON PRECEDENT ------------\n";
  //testDeplacerAuWagonPrecedent();

  cout<< "---------- TEST MONTER DANS LE TRAIN A PARTIR DE ------------\n";
  testMonterDansLeTrainAPartirDe();
  
  return 0;
}

