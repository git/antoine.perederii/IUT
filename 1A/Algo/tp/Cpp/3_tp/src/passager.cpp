#include "passager.hpp"
#include "wagon.hpp"

using namespace std;

Passager::Passager(const string &nom, const string &prenom)
    :nom{nom}, prenom{prenom}, wagonActuel{nullptr}
{}

Wagon* Passager::getWagonActuel() const {
  return wagonActuel;          ///// comme ça
}

ostream &operator<<(ostream &s, const Passager &p) {
    s << "Passager : " << p.nom << " " << p.prenom << endl; 
    return s;
}