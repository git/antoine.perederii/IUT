#include "recette.hpp"

using namespace std;

namespace appli {

    Recette::Recette(const string &nom, const string &description)
        : nom{nom}, description{description}
    {}

    string Recette::getNom() const {
        return nom;
    }

    string Recette::getDescription() const {
        return description;
    }

    bool operator<(const Recette &r1, const Recette &r2) {
        if(r1.getNom() < r2.getNom()) 
            return true;
        if(r1.getNom() > r2.getNom()) 
            return false;
            
        return r1.getDescription() < r2.getDescription();
    }

}