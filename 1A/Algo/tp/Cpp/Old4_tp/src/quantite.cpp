#include "quantite.hpp"
#include "unite.hpp"

using namespace std;

namespace appli {

    Quantite::Quantite(double nombre, const Unite &unite)
        : nombre{nombre}, unite{unite}
    {}

    double Quantite::getNombre() const {
        return nombre;
    }

    Unite Quantite::getUnite() const {
        return unite;
    }

    Quantite Quantite::normaliser() const {
        switch(unite) {
            case Unite::KG : 
                return *this;
                break;
            case Unite::G : 
                return Quantite{nombre / 1000,Unite::KG};
                break;
            case Unite::L  : 
                return *this;
                break;
            case Unite::DL : 
                return Quantite{nombre / 10, Unite::L};
                break;
            case Unite::CL : 
                return Quantite{nombre / 100, Unite::L};
                break;
            case Unite::ML : 
                return Quantite{nombre / 1000, Unite::L};
                break;
            case Unite::UNITE : 
                return Quantite{nombre / 1, Unite::UNITE};
                break;
        };
        return *this;
    }

    ostream &operator<<(ostream &os, const Quantite &q) {
        os << q.getNombre() << q.getUnite();
        return os;
    }

    Quantite operator+(const Quantite &q1, const Quantite &q2) {
        if(q1.getUnite() == q2.getUnite()) return Quantite(q1.getNombre() + q2.getNombre(), q1.getUnite());
        else {
            Quantite q1bis = q1.normaliser();
            Quantite q2bis = q2.normaliser();
            if(q1bis.getUnite() == q2bis.getUnite()) 
                return Quantite{q1bis.getNombre() + q2bis.getNombre(), q1bis.getUnite()};
            else {
                cout << "Impossible d'ajouter des unités non compatibles" << endl;
                return q1;
            }
        }
    }

}