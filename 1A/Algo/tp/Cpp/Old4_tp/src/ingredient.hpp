#ifndef INGREDIENT_HPP
#define INGREDIENT_HPP

#include "quantite.hpp"
#include <iostream>


namespace appli {

    class Ingredient {

        std::string aliment;
        Quantite quantite;

        public:

            Ingredient(const std::string &aliment, const Quantite &quantite);

            std::string getAliment() const;
            Quantite getQuantite() const; 
    };

    std::ostream &operator<<(std::ostream &os, const Ingredient &i);
    bool operator==(const Ingredient &i1, const Ingredient &i2);
    
}

namespace std {
    template <>
    struct hash<appli::Ingredient> {
        size_t operator()(const appli::Ingredient &i) const {
            return hash<string>()(i.getAliment());
        }
    };
}

#endif //INGREDIENT_HPP