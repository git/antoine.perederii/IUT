#include "ingredient.hpp"
#include "quantite.hpp"


using namespace std;

namespace appli {

    Ingredient::Ingredient(const string &aliment, const Quantite &quantite)
        : aliment{aliment}, quantite{quantite}
    {}

    string Ingredient::getAliment() const {
        return aliment;
    }

    Quantite Ingredient::getQuantite() const {
        return quantite;
    }

    ostream &operator<<(ostream &os, const Ingredient &i) {
        os << i.getQuantite() << " " << i.getAliment() << endl;
        return os;
    }

    bool operator==(const Ingredient &i1, const Ingredient &i2) {
        return(i1.getAliment() == i2.getAliment());
    }

}