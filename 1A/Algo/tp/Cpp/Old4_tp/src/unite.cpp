#include "unite.hpp"

using namespace std;
namespace appli {
    
    ostream &operator<<(ostream &os, const Unite &u) {
        switch(u) {
            case Unite::KG : os << "kg";
            break;
            case Unite::G : os << "g";
            break;
            case Unite::L : os << "L";
            break;
            case Unite::DL : os << "dL";
            break;
            case Unite::CL : os << "cL";
            break;
            case Unite::ML : os << "mL";
            break;
            case Unite::UNITE : os << "";
            break;
        }
        return os;
    }

}