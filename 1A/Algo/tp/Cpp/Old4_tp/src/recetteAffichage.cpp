#include "recetteAffichage.hpp"


using namespace std;
using namespace appli;

void afficherNomEtIngredients(const Recette &r, const unordered_set<Ingredient> &ingred) {
    cout << r.getNom() << endl;
    for(auto it = ingred.cbegin(); it != ingred.cend(); ++it) {
        cout << "\t" << *it;
    } 
    // for(Ingredient i : ingred) { //! OU CETTE FAÇON
    //     cout << i << endl;
    // }
}

void afficherTout(const Recette &r, const unordered_set<Ingredient> &ingredient) {
    cout << r.getNom() << endl;
    for(auto it = ingredient.cbegin(); it != ingredient.cend(); ++it) {
        cout << "\t" << *it;
    }
    cout << "description :\n" << r.getDescription() << endl;
}

void afficherTout(const map<Recette, unordered_set<Ingredient>> &recettes) {
    for(auto it = recettes.cbegin(); it != recettes.cend(); ++it) {
        afficherTout(it->first, it->second);
    }
}

void afficherNoms(const map<Recette, unordered_set<Ingredient>> &recettes) {
    for(map<Recette, unordered_set<Ingredient>>::const_iterator it = recettes.cbegin(); it != recettes.cend(); ++it){
        cout << it->first.getNom() << endl;
    }
}