#ifndef PARTIE_HPP
#define PARTIE_HPP

#include "joueur.hpp"
#include <string>

namespace jeu {

    constexpr int valeurPerte{1};
    constexpr int scoreVictoire{100};

class Partie {

private :
    bool gameOver;
    Joueur joueur1;
    Joueur joueur2;
    Joueur *joueurCourant;
    De de;

public :
    Partie(std::string nomJoueur1, std::string nomJoueur2);

    Joueur getJoueurCourant() const;

    void lancerDe();
    int getValeurDe() const;
    bool isGameOver() const;
    void passerMainJoueurSuivant();
    void rejouer();
};

} //namespace jeu


#endif // PARTIE_HPP