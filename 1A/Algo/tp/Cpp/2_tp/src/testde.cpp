#include "de.hpp"
#include "partie.hpp"
#include "joueur.hpp"
#include <iostream>

using namespace std;
using namespace jeu;

void testDe(void){
    De de6faces{};
    De de12faces{12};
    cout << "valeur du dé a 6 faces : " << de6faces.getValeur() << endl;
    cout << "valeur du dé a 12 faces : " << de12faces.getValeur() << endl;
    de6faces.lancer();
    de12faces.lancer();
    cout << "valeur du dé a 6 faces : " << de6faces.getValeur() << endl;
    cout << "valeur du dé a 12 faces : " << de12faces.getValeur() << endl;
    de6faces.effacer();
    de12faces.effacer();
    cout << "valeur du dé a 6 faces : " << de6faces.getValeur() << endl;
    cout << "valeur du dé a 12 faces : " << de12faces.getValeur() << endl;
}

void testJoueur(void){
    Joueur joueur{};
    Joueur marc{"marc"};
    cout << joueur << endl;
    cout << marc << endl;
    joueur.ajouterAuScoreCourant(50);
    marc.ajouterAuScoreCourant(50);
    cout << joueur << endl;
    cout << marc << endl;
    joueur.ajouterAuScoreCourant(150);
    marc.ajouterAuScoreCourant(150);
    cout << joueur << endl;
    cout << marc << endl;
    joueur.ajouterAuScoreTotal(50);
    marc.ajouterAuScoreTotal(50);
    cout << joueur << endl;
    cout << marc << endl;
    joueur.ajouterAuScoreTotal(150);
    marc.ajouterAuScoreTotal(150);
    cout << joueur << endl;
    cout << marc << endl;
    joueur.effacerScoreCourant();
    marc.effacerScoreCourant();
    cout << joueur << endl;
    cout << marc << endl;
    joueur.effacerScores();
    marc.effacerScores();
    cout << joueur << endl;
    cout << marc << endl;
}

void testPartie(void) {
    Partie partie{"Antoine", "Vivien"};
    int choix, rej;
    while(1) {
    while(! partie.isGameOver()) {
        partie.lancerDe();
        cout << partie.getJoueurCourant().getNom() << endl;
        cout << "voulez passer la main (0) ou bien rejouer (1) (possibilité de perdre votre score)" << endl;
        cin >> choix;
        if(choix == 0) {
            partie.passerMainJoueurSuivant();
        }
        else 
            cout << partie.getJoueurCourant().getNom() << " à vous de rejouer !" << endl;  
    }
    cout << "voulez vous rejouer ??? (0,non/1,oui)" << endl;
    cin >> rej;
    if(rej == 1) 
        partie.rejouer();
    else {
        cout << "Merci d'avoir joué à notre jeu !\nBonne journée !!!" << endl;
        exit(0);
    }
    }
}

int main(void){
    // testDe();
    // testJoueur();
    testPartie();
    return 0;
}