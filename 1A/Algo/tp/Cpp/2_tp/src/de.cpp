#include "de.hpp"
#include <iostream>

using namespace std;

namespace jeu {

    De::De(int nbFaces) 
        :nbFaces{nbFaces}, valeur{0}
    {
        srand(time(nullptr));
    };

    De::De()
        : De{6}
    {
        srand(time(nullptr));
    };

    int De::getValeur() const {
        if(valeur == 0){
            return 0;
        }
        return valeur;
    };

    int De::lancer() {
        valeur = rand() % nbFaces + 1;
        return valeur;
    };

    void De::effacer() {
        valeur = 0;
    };
}



