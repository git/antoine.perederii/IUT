#include "joueur.hpp"
#include <iostream>

using namespace std;

namespace jeu {
    int Joueur::numJoueur = 0;

    Joueur::Joueur(string nom)
        :nom{nom}, scoreTotal{0}, scoreCourant{0}
    {}

    Joueur::Joueur()
        :Joueur{"Joueur" + to_string(numJoueur+1)}
    {}

    string Joueur::getNom() const {
            return nom;
    }

    int Joueur::getScoreCourant() const{
        return scoreCourant;
    }

    int Joueur::getScoreTotal() const{
        return scoreTotal;
    }

    void Joueur::ajouterAuScoreCourant(int delta) {
        scoreCourant += delta;
    }

    void Joueur::effacerScoreCourant() {
        scoreCourant = 0;
    }

    void Joueur::ajouterAuScoreTotal(int delta) {
        scoreTotal += delta;
    }
    void Joueur::effacerScores() {
        scoreTotal = 0;
    }
} // fin du namspace jeu

ostream &operator<<(ostream &s, const jeu::Joueur &j) {
    s << "Le joueur " << j.getNom() << " possede un score courant de " << j.getScoreCourant() << " et un score de " << j.getScoreTotal() << endl;
    return s;
}