#ifndef JOUEUR_HPP
#define JOUEUR_HPP

#include <string>

namespace jeu {

class Joueur {
private:
    static int numJoueur;
    std::string nom;
    int scoreTotal;
    int scoreCourant;    
public:
    Joueur(std::string nom);
    Joueur();

    std::string getNom() const;
    int getScoreCourant() const;
    int getScoreTotal() const;

    void ajouterAuScoreCourant(int delta);
    void effacerScoreCourant();
    void ajouterAuScoreTotal(int delta);
    void effacerScores();
};

} //namespace jeu

std::ostream &operator<<(std::ostream &s, const jeu::Joueur &j);

#endif // JOUEUR_HPP