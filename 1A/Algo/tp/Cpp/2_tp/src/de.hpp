#ifndef DE_HPP
#define DE_HPP

#include <string>

namespace jeu {

class De {
private:
    int nbFaces;
    int valeur;
public:
    De(int nbFaces);
    De();
    int getValeur() const;
    int lancer();
    void effacer();
};

} //namespace jeu


#endif // DE_HPP