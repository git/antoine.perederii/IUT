#include "de.hpp"
#include "joueur.hpp"
#include "partie.hpp"

#include <iostream>

using namespace std;

namespace jeu {

    Partie::Partie(string nomJoueur1, string nomJoueur2)
        :gameOver{false}, joueur1{nomJoueur1}, joueur2{nomJoueur2}
    {
        joueurCourant = &joueur1;
    }

    Joueur Partie::getJoueurCourant() const {
        return *joueurCourant;
        
    }

    void Partie::lancerDe(){
        if(gameOver) {
            cout << "La partie est términé Jami !!!" << endl;
            return;
        }
        de.lancer();
        if(getValeurDe() == 1) {
            cout << "Vous avez perdu tt vos points !!!" << endl;
            joueurCourant->effacerScoreCourant();
            passerMainJoueurSuivant();
            cout << joueurCourant->getNom() << " C'est à vous de jouer !!" << endl;
            return;
        }
        joueurCourant->ajouterAuScoreCourant(getValeurDe());
    }

    int Partie::getValeurDe() const {
        return de.getValeur();
    }

    bool Partie::isGameOver() const {
        return gameOver;
    }

    void Partie::passerMainJoueurSuivant() {
        joueurCourant->ajouterAuScoreTotal(joueurCourant->getScoreCourant());
        if(joueurCourant->getScoreTotal() >= 100) {
            gameOver = true;
            cout << "Félicitation " << joueurCourant->getNom() << " vous avez gagné " << joueurCourant->getScoreTotal() << " points !" << endl;
            return;
        }
        
        if(joueurCourant->getNom() == joueur1.getNom())
            joueurCourant = &joueur2;
        else 
            joueurCourant = &joueur1;
    }

    void Partie::rejouer() {
        joueur1.effacerScores();
        joueur2.effacerScores();
        de.effacer();
        gameOver = false;
        if(joueur1.getScoreTotal() < joueur2.getScoreTotal())
            joueurCourant = &joueur1;
        else
            joueurCourant = &joueur2;

    }
} // fin du namespace jeu


// ostream &operator<<(ostream &s, const jeu::Joueur &j) {
//     s << "Le joueur " << j.getNom() << " possede un score courant de " << j.getScoreCourant() << " et un score de " << j.getScoreTotal() << endl;
//     return s;
// }