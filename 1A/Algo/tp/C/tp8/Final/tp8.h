#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int moyenne(void);

void initialiser(char mot[], int n);
void placer(char mot1[], char c, char mot2[]);
void jeuPendu(void);

int charger(char tab[][27]);
void afficher(char tab[][27], int nb);
void insert(char tab[][27], int *nb, char mot[]);
int sauvegarder(char tab[][27], int nb);

int chargerLong(char tab[][27]);
int sauvegarderLong(char tab[][27], int nb);

void chercher(char tab[][27], int nb, char mot[]);