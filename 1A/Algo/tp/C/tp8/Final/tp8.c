#include "tp8.h"

int moyenne(void)
{
	float note, moy=0, nb=0;
	char in[27], nom[27], mat[27];

	FILE *flot;
	flot = fopen("fichierNotes.txt", "r");
	if (flot == NULL)
	{
		printf("Problème lors de l'ouverture du fichier.\n");
		return -1;
	}

	printf("Tapez le code de la matière à traiter : ");
	scanf("%s", in);
	/*in[strlen(in)-1] = '\0';*/
	printf("Notes données en %s :\nETUDIANTS\tNOTES\n", in);

	fscanf(flot, "%s%s%f", nom, mat, &note);
	while(!feof(flot))
	{
		if (strcmp(in, mat) == 0)
		{
			nb = nb+1;
			moy = moy+note;
			printf("%s\t\t%.2f\n", nom, note);
		}
		fscanf(flot, "%s%s%f", nom, mat, &note);
	}

	if (nb == 0)
		printf("aucune note pour cette matière\n");
	else
		printf("\nMOYENNE :\t%.2f\n", moy/nb);

	fclose(flot);
	return moy;
}


void initialiser(char mot[], int n)
{
	for (int i = 0; i < n; ++i)
		*(mot +i) = '-';
}

void placer(char mot1[], char c, char mot2[])
{
	for (int i = 0; i < strlen(mot1); ++i)
		if (*(mot1+i) == c)
			*(mot2+i) = c;
}

void jeuPendu(void)
{
	int etapes = 4;
	char mot1[27], mot2[27], mot3[27], c;

	printf("Joueur 1, proposez un mot à deviner : ");
	system("stty -echo");
	scanf("%s%*c", mot1);
	system("stty echo");
	initialiser(mot2, strlen(mot1));
	printf("\nMot de %d lettres à trouver en %d étapes\n%s\n\n", strlen(mot1), etapes, mot2);

	for (int i = 0; i < etapes; ++i)
	{
		printf("proposez une lettre : ");
		scanf("%c%*c", &c);
		placer(mot1, c, mot2);
		printf("%s\navez vous reconnu le mot (o/n) ?", mot2);
		scanf("%c%*c", &c);

		if (c == 'o')
		{
			printf("\nmot ? ");
			scanf("%s%*c", mot3);
			if (strcmp(mot1, mot3) == 0)
			{
				printf("Bravo vous avez gagné !!!\nmot trouvé en %d étapes\n", i);
				return;
			}
			else
				printf("Désolé ...\n");
		}
		printf("\n");
	}
	printf("\nmot ? ");
	scanf("%s%*c", mot3);
	if (strcmp(mot1, mot3) == 0)
	{
		printf("Bravo vous avez gagné !!!\nmot trouvé en %d étapes\n", etapes+1);
		return;
	}
	else
		printf("Désolé ... Vous avez perdu.\n");
}


int charger(char tab[][27])
{
	int i=0;
	char mot[27];

	FILE *flot;
	flot = fopen("fichierMots.txt", "r");
	if (flot == NULL)
	{
		printf("Problème lors de l'ouverture du fichier.\n");
		return -1;
	}

	fscanf(flot, "%s%*c", mot);
	while(!feof(flot))
	{
		strcpy(tab[i], mot);
		i = i+1;
		fscanf(flot, "%s%*c", mot);
	}

	fclose(flot);
	return i;
}

/*void afficher(char tab[][27], int nb)
{
	for (int i = 0; i < nb; ++i)
	{
		for (int j = 0; j < 27; ++j)
		{
			printf("%c", tab[i][j]);
		}
		printf("\n");
	}
}*/
void afficher(char tab[][27], int nb)
{
	for (int i = 0; i < nb; ++i)
		printf("%s\n", tab[i]);
}

void insert(char tab[][27], int *nb, char mot[])
{
	int x = *nb;

	for (int i = 0; i < *nb; ++i)
	{
		if(strcmp(mot, tab[i]) == 0)
		{
			printf("mot déjà dans existant\n");
			return;
		}

		if(strcmp(tab[i], mot) > 0)
		{
			x = i;
			break;
		}
	}

	for (int i = *nb; i >= x; i=i-1)
		strcpy(tab[i+1], tab[i]);

	strcpy(tab[x], mot);
	*nb = *nb +1;
}

int sauvegarder(char tab[][27], int nb)
{
	int i=0;

	FILE *flot;
	flot = fopen("fichierMots.txt", "w");
	if (flot == NULL)
	{
		printf("Problème lors de l'ouverture du fichier.\n");
		return -1;
	}

	for (int i = 0; i < nb; ++i)
		fprintf(flot, "%s\n", tab[i]);

	fclose(flot);
	return 1;
}

int chargerLong(char tab[][27])
{
	int i=0;
	char mot[27];

	FILE *flot;
	flot = fopen("fichierMotsLong.txt", "r");
	if (flot == NULL)
	{
		printf("Problème lors de l'ouverture du fichier.\n");
		return -1;
	}

	fgets(mot, 27, flot);
	mot[strlen(mot) - 1] = '\0';
	while(!feof(flot))
	{
		strcpy(tab[i], mot);
		i = i+1;
		fgets(mot, 27, flot);
		mot[strlen(mot) - 1] = '\0';
	}

	fclose(flot);
	return i;
}

int sauvegarderLong(char tab[][27], int nb)
{
	int i=0;

	FILE *flot;
	flot = fopen("fichierMotsLong.txt", "w");
	if (flot == NULL)
	{
		printf("Problème lors de l'ouverture du fichier.\n");
		return -1;
	}

	for (int i = 0; i < nb; ++i)
		fprintf(flot, "%s\n", tab[i]);

	fclose(flot);
	return 1;
}

void chercher(char tab[][27], int nb, char mot[])
{
	int pass;
	char res[27];

	for (int i = 0; i < nb; ++i)
	{
		int num[27] = {0};

		for (int j = 0; j < strlen(tab[i]); ++j)
		{
			pass = 0;

			for (int k = 0; k < strlen(mot); ++i)
			{
				if (tab[i][j] == mot[k] && num[k] == 0)
			 	{
			 		num[k] = 1;
			 		pass = 1;
			 		break;
			 	}
			}

			if(pass == 0)
				break;
		}
		if(pass == 1 && strlen(tab[i]) > strlen(res))
			strcpy(res, tab[i]);
	}

	printf("%s\n", res);
}