#include "tp8.h"

int main(void)
{
	char tab[15][27], mot[27];
	int nb = charger(tab), in=1;

	if(nb < 0)
		return -1;

	while(in != 0)
	{
		afficher(tab, nb);
		printf("\nAjouter un mot ou chercher le plus long ? (0/1/2)\n");
		scanf("%d", &in);

		if (in == 1)
		{
			printf("\nMot à insert : ");
			scanf("%s%*c", mot);
			insert(tab, &nb, mot);
		}
		else if (in == 2)
		{
			printf("\nLettres à trouver : ");
			scanf("%s%*c", mot);
			chercher(tab, nb, mot);
		}
	}

	return sauvegarder(tab, nb);
}

/*int main(void)
{
	char tab[15][27], mot[27];
	int nb = chargerLong(tab), in=1;

	if(nb < 0)
		return -1;

	while(in != 0)
	{
		afficher(tab, nb);
		printf("\nAjouter un mot ? (0/1)\n");
		scanf("%d", &in);

		if (in == 1)
		{
			printf("\nMot à insert : ");
			fgets(mot, 27, stdin);
			insert(tab, &nb, mot);
		}
	}

	return sauvegarderLong(tab, nb);
}*/