#include "tp8.h"

void extraction(void)
{
    int nb =0;
    char login[8], code[6], codeMat[6];
    float note, som = 0, moyenne;
    FILE *flot;
    flot = fopen("Notes.don", "r");
    if (flot == NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return;
    }
    printf("Entrez le code de la matiere : ");
    scanf("%s", codeMat);
    printf("\n\nNotes données en %s\n", codeMat);
    printf("\nETUDIANTS\tNOTES\n");
    fscanf(flot, "%s%s%f", login, code, &note);
    while(!feof(flot))
    {
        if(strcmp(code, codeMat) == 0)
        {
            printf("%s\t\t%.2f\n", login, note);
            som = som + note;
            nb = nb + 1;
        }
        fscanf(flot, "%s%s%f", login, code, &note);
    }
    fclose(flot);
    if(nb == 0)
        printf("\nAucune note pour cette matiere\n\n");
    else
    {
        moyenne = som / nb;
        printf("\nMoyenne :\t%.2f\n\n", moyenne);
    }
}

void initialiser(char mot[], int n)
{
    int i;
    for(i = 0; i < n; i++)
        mot[i] = '_';
    mot[n] = '\0';
}

void placer(char mot1[], char c, char mot2[])
{
    int i;
    for(i = 0; mot1[i] != '\0'; i++)
    {
        if(mot1[i] == c)
            mot2[i] = c;
    }
    mot2[i] = '\0';
}

void jeuPendu(void)
{
    char mot[10], mot2[10], mot3[10], c;
    int i, nb = 0, nb2;
    printf("Joueur1, proposez un mot : \n");
    system("stty -echo");
    scanf("%s", mot);
    system("stty echo");
    nb = compterLettres(mot);
    initialiser(mot2, nb);
    printf("Joueur1, donnez le nombre de coups : \n");
    scanf("%d", &nb2);
    printf("Mot de %d lettres à trouver en %d étapes\n", nb, nb2);
    printf("%s", mot2);
    printf("\n");
    for(i = 0; i < nb2; i++)
    {
        printf("Joueur2, proposez une lettre :");
        scanf("%*c%c", &c);
        placer(mot, c, mot2);
        printf("%s", mot2);
        printf("\nJoueur2, avez-vous trouvé le mot ? (o/n) : ");
        scanf("%*c%c", &c);
        if(c == 'o')
        {
            printf("Joueur2, quel est le mot ? : ");
            scanf("%s", mot3);
            if(strcmp(mot, mot3) == 0)
            {
                printf("\nBravo ! Vous avez trouvé le mot en %d étapes\n", i + 1);
                return;
            }
            else
            {
                printf("Ce n'est pas le mot\n");
            }
        }
    }
    printf("Joueur2, vous avez perdu ! Le mot était %s\n", mot);
}

int compterLettres(char mot[])
{
    int i, nb = 0;
    for(i = 0; mot[i] != '\0'; i++)
        nb = nb + 1;
    return nb;
}

/*  Exercice 3  */

void chargement(int tMot[], int nbMots)
{
    int i = 0, j;
    FILE *flot;
    flot = fopen("mots.don", "r");
    if (flot == NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return;
    }
    fscanf(flot, "%s", tMot[0]);
    while(feof(flot))
    {
        if(i == nbMots)
        {
            printf("Erreur : trop de mots dans le fichier");
            return;
        }
        for(j = 0; j < nbLettres; j++)
        {
            fscanf(flot, "%d", &tMot[i][j]);
        }
    }
}