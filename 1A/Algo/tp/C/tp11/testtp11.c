#include "tp11.h"

void testExercice1()
{
    int n;
    while(1)
    {
        printf("Donnez le nombre de segments : ");
        scanf("%d", &n);
        if(n == 0)
            break;
        int result = nbSegments(n);
        printf("Il y a %d segments\n", result);
    }
}

void testExercice2Nb()
{
    int tab[10] = {0, 2, 3, 4, 5, 8, 2, 25, 0, 2}, tab2[10] = {1, 2, 3, 4, 5, 8, 2, 25, 0, 2};
    int tab3[10] = {0, 2, 3, 4, 5, 8, 2, 25, 0, 2};
    int n = 10, result, val = 2;
    result = sommeNentierT(tab, n);
    printf("La somme des entiers est %d\n", result);
    printf("====================================\n");
    result = minValT(tab, n);
    printf("La valeur minimale est %d\n", result);
    printf("====================================\n");
    result = nombreOccurencesT(tab, n, val);
    printf("Le nombre d'occurences de %d est %d\n", val, result);
    printf("====================================\n");
    result = rechercheValT(tab, n, val);
    if(result == 0)
        printf("La valeur %d n'est pas dans le tableau\n", val);
    else
        printf("La valeur %d est dans le tableau\n", val);
    printf("====================================\n");
    result = recherchePlusProcheXT(tab, n, val);
    printf("La valeur %d est la plus proche de %d, dans le tableau\n", result, val);
    printf("====================================\n");
    result = rechercheValMaxT(tab, n);
    printf("La valeur maximale est %d\n", result);
    printf("====================================\n");
    result = compare2T(tab, tab3, n);
    printf("Les tableaux sont %s\n", result ? "identiques" : "different");
    printf("====================================\n");
}


void testExercice2Chaine()
{
    char tab1[10]="Bonjour", tab2[10]="aurevoir";
    int n = 10, result;
    char result2[20];
    result = longueurChaine(tab1);
    printf("La longueur de la chaine est %d\n", result);
    printf("====================================\n");
    result = copieChaine(tab1, tab2);
    printf("La chaine copiée est %s\n", tab2);
    printf("====================================\n");
    result = concateneChaine(tab1, tab2, result2);
    printf("La chaine concatenee est %s\n", result2);
    printf("====================================\n");
    result = compareChaine(tab1, tab2);
    if(result == 0)
        printf("Les chaines sont differentes\n");
    else
    printf("Les chaines sont identiques\n");
    printf("====================================\n");
    result = mirroirChaine(tab1, result2);
    printf("La chaine mirroir est %s\n", result2);
    printf("====================================\n");
}

void testExercice3Jeu()
{
    int n, i = 0, result;
    bool tab[100];
    char rep;
    printf("Donnez le nombre de cases : ");
    scanf("%d", &n);
    printf("Y-a-t-il des pions sur les %d cases ? (o/n) :", n);
    scanf("%*c%c", &rep);
    while(rep == 'o' || rep == 'O' && 0 < i < n)
    {
        printf("Donnez les numéro de la case (0-%d) : ", n);
        scanf("%d", &i);
        tab[i]=true;
        printf("Y-a-t-il un pion sur la case (%d/%d) ? (o/n) : ", i, n);
        scanf("%*c%c", &rep);
    }
    result = baguenaudier(n, tab);
    printf("Le pion %d est le dernier\n", result);
}


int main(void)
{
    // testExercice1();
    // testExercice2Nb();
    // testExercice2Chaine();
    testExercice3Jeu();
    return 1;
}
