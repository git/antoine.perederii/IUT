#include "tp11.h"

/*  Exercice 1  */
int nbSegments(int n)
{
    if (n == 1)
        return 0;
    return (n-1) + nbSegments(n - 1);
}

/*  Exercice 2-a  */

int sommeNentierT(int tab[], int n)
{
    if (n == 1)
        return tab[0];
    return tab[0] + sommeNentierT(tab+1, n-1);
}

int minValT(int tab[], int n)
{
    int min;
    if (n == 1)
        return tab[0];
    min = minValT(tab+1, n-1);
    if (tab[0] < min)
        return tab[0];
    return min;
}

int nombreOccurencesT(int tab[], int n, int val)
{
    if (n == 1)
        return tab[0] == val;
    return (tab[0] == val) + nombreOccurencesT(tab+1, n-1, val);
}

int rechercheValT(int tab[], int n, int val)
{
    if (n == 0)
        return 0;
    if(val == tab[n])
        return 1;
    return rechercheValT(tab, n-1, val);
}

int recherchePlusProcheXT(int tab[], int n, int x)
{
    int min;
    if (n == 1)
        return tab[0];
    min = recherchePlusProcheXT(tab+1, n-1, x);
    if (abs(tab[0] - x) < abs(min - x))
        return tab[0];
    return min;
}

int rechercheValMaxT(int tab[], int n)
{
    int max;
    if (n == 1)
        return tab[0];
    max = rechercheValMaxT(tab+1, n-1);
    if (tab[0] > max)
        return tab[0];
    return max;
}

int compare2T(int tab1[], int tab2[], int n)
{
    if (n == 1)
        return tab1[0] == tab2[0];
    if (tab1[0] != tab2[0])
        return 0;
    return compare2T(tab1+1, tab2+1, n-1);
}

// Exercice 2-b

int longueurChaine(char chaine[])
{
    if (chaine[0] == '\0')
        return 0;
    return 1 + longueurChaine(chaine+1);
}

int copieChaine(char chaine1[], char chaine2[])
{
    if (chaine1[0] == '\0')
    {
        chaine2[0] = '\0';
        return 0;
    }
    chaine2[0] = chaine1[0];
    return 1 + copieChaine(chaine1+1, chaine2+1);
}

int concateneChaine(char chaine1[], char chaine2[], char chaine3[]) //a revoir
{
    if (chaine1[0] == '\0')
        return copieChaine(chaine2, chaine3);
    chaine3[0] = chaine1[0];
    return 1 + concateneChaine(chaine1+1, chaine2, chaine3+1);
}

int compareChaine(char chaine1[], char chaine2[]) //a revoir
{
    if (chaine1[0] == '\0' && chaine2[0] == '\0')
        return 1;
    if (chaine1[0] == chaine2[0])
        return 1*(compareChaine(chaine1+1, chaine2+1));
    return 0;
}

int mirroirChaine(char chaine1[], char chaine2[])
{
    if (chaine1[0] == '\0')
        return 0;
    chaine2[longueurChaine(chaine1)-1] = chaine1[0];
    return 1 + mirroirChaine(chaine1+1, chaine2);
}

/*  Exercice 3  */

int baguenaudier(int n, bool tab[])
{
    if (n == 1)
        return 1;
    if (tab[n] == 0)
    {
        tab[n] = 1;
        return baguenaudier(n-1, tab);
    }
    tab[n] = 0;
    return baguenaudier(n-1, tab);
}

/*  Pour vider n, il faut vider n-2, on remet les n-2 en vide les n-2 de ce n        */
/*  Pour remplir n, il faut remplir n-2, on remplis n-1 vider n-2 et on remplie n-1      */