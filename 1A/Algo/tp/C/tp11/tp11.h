#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int nbSegments(int n);
int sommeNentierT(int tab[], int n);
int minValT(int tab[], int n);
int nombreOccurencesT(int tab[], int n, int val);
int rechercheValT(int tab[], int n, int val);
int recherchePlusProcheXT(int tab[], int n, int x);
int rechercheValMaxT(int tab[], int n);
int compare2T(int tab1[], int tab2[], int n);
int longueurChaine(char chaine[]);
int copieChaine(char chaine1[], char chaine2[]);
int concateneChaine(char chaine1[], char chaine2[], char chaine3[]);
int compareChaine(char chaine1[], char chaine2[]);
int mirroirChaine(char chaine1[], char chaine2[]);
int baguenaudier(int n, bool tab[]);
void testExercice3();

