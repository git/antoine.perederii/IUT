#include "tp2.h"


void LocationSalle (void)
{
    float Montant;
    char Cat, Dec;
    printf("Quel catégorie de salle voulez-vous prendre (A/B/C) :\n");
    scanf("%c%*c",&Cat);
    printf("Voulez-vous la décoration (O/N) :\n");
    scanf("%c",&Dec);
    if(Dec == 'O' || Cat == 'o')
        if(Cat == 'A' || Cat == 'a')
            Montant = 1500 * (1+(10.0/100));
        else if(Cat == 'B' || Cat == 'b')
                Montant = 1000 * (1+(10.0/100));
            else
                Montant = 700 *(1+(10.0/100));
    else
        if(Cat == 'A' || Cat == 'a')
            Montant = 1500;
        else if(Cat == 'B' || Cat == 'b')
                Montant = 1000;
            else
                Montant = 700;

    printf("Le montant est donc de %f€\n", Montant);
}

void Aperitif(void)
{
    float Montant, PrixTot;
    char Formule;
    int nbrPers;
    printf("Quel formule voulez-vous (B/U) :\n");
    scanf("%c%*c",&Formule);
    printf("Combien êtes-vous :\n");
    scanf("%d",&nbrPers);
    if(Formule == 'B' || Formule == 'b')
        if(nbrPers <= 50)
            Montant = 300;
        else if(nbrPers <= 100)
                Montant = 500;
            else
                Montant = 800;
    else
        Montant = nbrPers * 12;

    PrixTot = Montant + 50;
    printf("Le montant est donc de %f€\n", PrixTot);
}

void Aperitif2(void)
{
    float MontantU, MontantB, PrixTot;
    char Formule;
    int nbrPers;
    printf("Quel formule voulez-vous (B/U) :\n");
    scanf("%c",&Formule);
    printf("Combien êtes-vous :\n");
    scanf("%d",&nbrPers);
    if(nbrPers <= 50)
        MontantB = 300;
    if(nbrPers <= 100 && nbrPers > 50)
        MontantB = 500;
    if(nbrPers >100)
        MontantB = 800;
    
    MontantU = nbrPers*12;

    if(Formule == 'B' || Formule == 'b')
        PrixTot = MontantB + 50;
    else
        PrixTot = MontantU + 50;

    printf("Le montant est donc de %f€\n", PrixTot);
}


void Banquet(void)
{
    float Montant;
    char Menu;
    int nbrPers;
    printf("Quel menu voulez-vous (I/D) :\n");
    scanf("%c%*c",&Menu);
    printf("Combien êtes-vous :\n");
    scanf("%d",&nbrPers);
    if(Menu == 'I' || Menu == 'i')
    {
        if(nbrPers <= 10)
            Montant = nbrPers * 30;
        if(nbrPers >10 && nbrPers <= 30)
            Montant = (nbrPers-10) * 28 +10*30 ;
        if(nbrPers >30 && nbrPers <= 80)
            Montant = (nbrPers-30)*27 + 10*30 +20*28;
        if(nbrPers > 80)
            Montant = (nbrPers-80)*26 + 10*30 +20*28 +50*27;
    }
    else
        Montant = nbrPers * 31;

    printf("Le montant est donc de %f€\n", Montant);
}

void EtudeGlobal(void)
{
    char Ape, Ban;
    printf("Voulez-vous un aperitif (O/N) :\n");
    scanf("%c%*c", &Ape);
    printf("Voulez-vous un banquet (O/N) :\n");
    scanf("%c%*c", &Ban);
    LocationSalle();
    if (Ape == 'O' || Ape == 'o')
    {
        Aperitif();
    }
    if (Ban == 'O' || Ban == 'o')
    {
        Banquet();
    }
    
    
}