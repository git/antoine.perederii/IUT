#include "tp4.h"

void testSaisirLogement (void)
{
    int nbPers, resultat;
    char type, pension;
    resultat = saisirLogement(&type, &nbPers, &pension);
    if(resultat == 0)
        printf("Logement : %c\n",type);
        printf("nb de Personnes : %d\n", nbPers);
        if(type == 'B')
            printf("pension : %c\n", pension);
        else printf("pension : non\n");
}

void testCoutLogement (void)
{
    char type, pension;
    int nbPers, resultat;
    float prix;
    resultat = saisirLogement(&type, &nbPers, &pension);
    if(resultat == 0)
    {
        prix = coutLogement(type, nbPers, pension);
        printf("type de logement : %c nb Personnes : %d ", type, nbPers);
        if(type == 'B' || type == 'b')
            printf("pension : %c ", pension);
        else printf("pension : non ");
        printf("cout : %.2f\n", prix);
    }
}

void testSki(void)
{
    int nbPers, nbPack, nbSki, resultat;
    char rep;
    resultat = saisirActiviteSki(&nbPers, &nbPack, &nbSki);
    if(resultat == 0)
    {
        printf("nb Personnes : %d nbPack : %d nbSki : %d\n",nbPers, nbPack, nbSki);
    }    
}

void testCoutActiviteSki(void)
{
    int nbPers, nbPack, nbSki, resultat;
    float reduc, prix;
    resultat = saisirActiviteSki(&nbPers, &nbPack, &nbSki);
    if(resultat == 0)
    {
        prix = coutActiviteSki(nbPers, nbPack, nbSki, &reduc);
        printf("nb Personnes : %d nbPack : %d nbSki : %d  prix : %.2f\n",nbPers, nbPack, nbSki, prix);
    } 
}

int main(void)
{
    //testSaisirLogement();
    //testCoutLogement();
    //testSki();
    testCoutActiviteSki();
    //globale(int nbPers, int nbPack, int nbSki, float reduc);
	return 0;
}