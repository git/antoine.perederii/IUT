#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char code[6];
    int coef;
    char design[32];
} Mat;

int chargeFmatieres(char *nomFich, Mat **tmat, int tmax);
void global(void);
Mat lireMat(FILE *flot);
void afficheMat(Mat m);
void afficheTmat(Mat **tMat, int nbmat);
void libereEspaceA(Mat *tmat[], int nb);
void sauvTbin(Mat *tab[], int nb);
int rechdich(char *code, Mat **tmat, int nbmat, int *trouve);
int supprime(Mat **tMat, int nbmat);
void testCharge(void);