#include "tp10.h"

Resultat lireResultat(FILE *fe)
{
    Resultat c;
    fscanf(fe, "%s\t%s\t%f", c.nom, c.cdmat, &c.moyenne);
    return c;
}

void afficheResultat(Resultat c)
{
    printf("%s\t%s\t%f\n", c.nom, c.cdmat, c.moyenne);
}

Resultat *chargeTresultat(char *nomFic, int *nbres)
{
    Resultat *tab;
    FILE *flot;
    int i;
    flot = fopen(nomFic, "r");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier resultat.don\n");
        exit(1);
    }
    fscanf(flot, "%d", nbres);
    tab = (Resultat *)malloc(*nbres * sizeof(Resultat));
    if(tab == NULL)
    {
        printf("Pb, il n'y a rien dans le fichier resultat.don\n");
        exit(1);
    }
    for(i=0; i < *nbres; i++)
        tab[i]=lireResultat(flot);
    fclose(flot);
    return tab;
}

void afficheTresultat(Resultat *tab, int nbres)
{
    int i;
    printf("\n");
    for(i = 0; i < nbres; i++)
        afficheResultat(tab[i]);
}

void sauveTresultat(Resultat *tRes, int nbres)
{
    FILE *flot;
    int i;
    flot = fopen("resultat.bin", "w");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier resultat.bin\n");
        exit(1);
    }
    fprintf(flot, "%d\n", nbres);
    fwrite(tRes, sizeof(Resultat), nbres, flot);
    fclose(flot);
}

Resultat *restaureTresultat(char *nomfichier, int *nbres)
{
    Resultat *tab;
    FILE *flot;
    flot = fopen(nomfichier, "r");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier resultat.don\n");
        exit(1);
    }
    fscanf(flot, "%d\n", nbres);
    tab = (Resultat *)malloc(*nbres * sizeof(Resultat));
    if(tab == NULL)
    {
        printf("Pb, il n'y a rien dans le fichier resultat.bin\n");
        exit(1);
    }
    fread(tab, sizeof(Resultat), *nbres, flot);
    fclose(flot);
    return tab;
}

void global(void)
{
    Resultat *tab;
    int nb;
    char nomFic[20];
    printf("Entrez le nom du fichier à lire : ");
    scanf("%s", nomFic);
    tab = chargeTresultat(nomFic, &nb);
    afficheTresultat(tab, nb);
    sauveTresultat(tab, nb);
    free(tab);
    tab = restaureTresultat("resultat.bin", &nb);
    afficheTresultat(tab, nb);
    free(tab);
}

