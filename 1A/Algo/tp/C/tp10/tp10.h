#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct{
    char nom[21];
    char cdmat[6];
    float moyenne;
} Resultat;

Resultat lireResultat(FILE *fe);
void afficheResultat(Resultat c);
Resultat *chargeTresultat(char *nomFic, int *nbres);
void afficheTresultat(Resultat *tab, int nbres);
void sauveTresultat(Resultat *tRes, int nbres);
Resultat *restaureTresultat(char *nomFic, int *nbres);
void global(void);
