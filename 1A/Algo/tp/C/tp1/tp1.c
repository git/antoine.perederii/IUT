/**
 */file tp1.c
 *\brief Ce fichier permet de réaliser le tp1 datant du 09.09.22 avec les exercices 1, 2 et 3 de la partie 2 et le 4 et 6 de la partie 1.
   Amusez-vous bien !!!
 *\author Antoine PEREDERII
 *\date 09 Septembre 2022
 *
 *
 *
 *
 */


#include "tp1.h"

void Bonjour(void)
{
	printf("Bonjour\n");
}
/**
 * \brief affiche l'année qu'on lui a donnée
 */
void Bonjourv1(void)
{
	int annee;
	printf("veuillez saisir une année :");
	scanf("%d", &annee);
	printf("Bonjour,\nNous somme en %d.", annee);
}

void CalculRed(void)
{
	float Prix, PrixTot;
	int Reduction;
	printf("Veuillez entrer un prix :");
	scanf("%f", &Prix);
	printf("Quelle est la réduction :");
	scanf("%d", &Reduction);
	PrixTot = Prix *(1-((Reduction)/100.0));
	printf("Le prix après la réduction est donc de %.2f\n", PrixTot);

}

void Viennoiserie(void)
{
	int NbrDeVienne, offerts, MoyenAchats, PetitAchat;
	float Prix;
	printf("Saississez le nombre de viennoiserie :");
	scanf("%d", &NbrDeVienne);

	offerts=NbrDeVienne/12;
	MoyenAchats=NbrDeVienne/5;
	PetitAchat=(NbrDeVienne%5);
	Prix=MoyenAchats*2.0 + PetitAchat*0.5;

	printf("Le client doit payer %.2f€, et %d viennoiseries lui sont offertes.\n", Prix, offerts);


}

void Caracteres(void)
{
	char carac;
	printf("saisir un unique caractère en minuscule :");
	carac = getchar();
	printf("le caractère saisi est %c\n", carac);
	printf("la valeur ASCII de ce caractère ets %d\n", carac);
}
void Majuscule(void)
{
	char caracM;
	printf("saisir un unique caractère en minuscule :");
	caracM = getchar() - 32;
	printf("Le caractère majuscule de cette lettre est %c\n", caracM);
}
