#include "tp9/tp9.h"

Mat lireMat(FILE *fe)
{
    Mat c;
    fscanf(fe, "%s%d%*c", c.code, &c.coef);
    fgets(c.desig, 32, fe);
    c.desig[strlen(c.desig)-1] = '\0';
    return c;
}

void afficheMat(Mat c)
{
    printf("%s\t%d\t%s\n", c.code, c.coef, c.desig);
}

int chargeFmat(Mat tmat[], int nbmax)
{
    Mat c;
    int i = 0;
    FILE *fe;
    fe = fopen("matieres.txt", "r");
    if (fe == NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return -1;
    }
    c = lireMat(fe);
    while(!feof(fe))
    {
        if(i == nbmax)
        {
            printf("Probleme, tableau plein !!!");
            return -2;
        }
        tmat[i] = c;
        i++;
        c = lireMat(fe);
    }
    fclose(fe);
    return i;
}

void afficheTmat(Mat tmat[], int nbmat)
{
    int i;
    printf("CodeM\tCoef\tDesignation\n");
    for(i = 0; i < nbmat; i++)
    {
        afficheMat(tmat[i]);
    }
    printf("\n");
}

int rechMat(char code[], Mat tmat[], int nbmat)
{
    int i;
    for(i = 0; i < nbmat; i++)
    {
        if(strcmp(code, tmat[i].code) == 0)
        {
            return i;
        }
    }
    return -1;
}

Mat saisieMat(Mat tmat[], int nbmat)
{
    int res;
    Mat c;
    printf("Saisie d'une matiere\n");
    printf("Code:\t ");
    scanf("%s", c.code);
    res = rechMat(c.code, tmat, nbmat);
    while(res != -1)
    {
        printf("\nErreur : Code déjà enregistré pour %s\n", tmat[res].desig);
        printf("Retapez : \t");
        scanf("%s", c.code);
        printf("%s", c.code);
        res = rechMat(c.code, tmat, nbmat);
    }
    printf("\nCoefficient : \t");
    scanf("%d%*c", &c.coef);
    printf("\nDesignation : \t");
    fgets(c.desig, 32, stdin);
    c.desig[strlen(c.desig)-1] = '\0';
    return c;
}

void ajoutMat(void)
{
    Mat tmat[50], c;
    int nbmat, res;
    char code[6], rep ='o';
    nbmat = chargeFmat(tmat, 50);
    if(nbmat < 0)
        return;
    afficheTmat(tmat, nbmat);
    while(rep == 'o')
    {
        if(nbmat == 50)
        {
            printf("Probleme, tableau plein !!!");
            break;
        }
        c = saisieMat(tmat, nbmat);
        tmat[nbmat] = c;
        nbmat++;
        printf("\nAjouter une autre matiere ? (o/n)\t");
        scanf("%*c%c", &rep);
    }
    sauvFmat(tmat, nbmat);
}

void sauvFmat(Mat tmat[], int nbmat)
{
    int i;
    FILE *fe;
    fe = fopen("matieres.txt", "w");
    if(fe == NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return;
    }
    for(i = 0; i < nbmat; i++)
        fprintf(fe, "%s\t%d\t%s\n", tmat[i].code, tmat[i].coef, tmat[i].desig);
    fclose(fe);
}