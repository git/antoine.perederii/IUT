#include "tp9/tp9.h"

void test(void)
{
    Mat tmat[50];
    int nbmat, res;
    char code[6];
    nbmat = chargeFmat(tmat, 50);
    if(nbmat < 0)
        return;
    afficheTmat(tmat, nbmat);
    printf("Code de la matiere a rechercher : ");
    scanf("%s", code);
    res = rechMat(code, tmat, nbmat);
    if(res == -1)
        printf("Matiere non trouvee, code incorrect !!!\n");
    else
        afficheMat(tmat[res]);
}

void main(void)
{
    //test();
    ajoutMat();
    return;
}