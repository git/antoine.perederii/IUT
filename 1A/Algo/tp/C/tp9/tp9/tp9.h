#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
    char code[6];
    int coef;
    char desig[32];
    } Mat;

Mat lireMat(FILE *fe);
void afficheMat(Mat c);
int chargeFmat(Mat tmat[], int nbmax);
void afficheTmat(Mat tmat[], int nbmat);
int rechMat(char code[], Mat tmat[], int nbmat);
Mat saisieMat(Mat tmat[], int nbmat);
void ajoutMat(void);
void sauvFmat(Mat tmat[], int nbmat);