#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
    char code [6];
    int coef;
    char design[32];
} Mat;

typedef struct maillon {
    Mat v;
    struct maillon *suiv;
} Maillon, *Liste;

Liste listenouv(void);
Liste insererEnTete(Liste l, Mat x);
Liste inserer(Liste l, Mat x);
Liste supprimerEnTete(Liste l);
Liste supprimer(Liste l, Mat x);
void afficher(Liste l);
Mat Tete(Liste l);
bool vide(Liste l);
bool rechercher(Liste l, Mat x);
int longueur(Liste l);
Liste charger(char *nomFichier);
void sauv(Liste l, char *nomFichier);
int menu(void);
void global(void);