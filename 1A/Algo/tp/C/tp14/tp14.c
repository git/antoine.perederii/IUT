#include "tp14.h"

Mat lireMat(FILE *flot)
{
    Mat x;
    fscanf(flot, "%s\t%d\t", x.code, &x.coef);
    fgets(x.design, 32, flot);
    x.design[strlen(x.design)-1] = '\0';
    return x;
}

void afficherMat(Mat x)
{
    printf("%s\t%d\t%s\n", x.code, x.coef, x.design);
}

Liste listenouv(void)
{
    Liste l = NULL;
    return l;
}

Liste insererEnTete(Liste l, Mat x)
{
    Maillon *m;
    m = (Maillon *)malloc(sizeof(Maillon));
    if(m == NULL)
    {
        printf("Erreur d'allocation mémoire");
        exit(1);
    }
    strcpy(m->v.code, x.code);
    m->v.coef = x.coef;
    strcpy(m->v.design, x.design);
    m->suiv = l;
    return m;
}

Liste inserer(Liste l, Mat x)
{
    if(l == NULL)
        return insererEnTete(l, x);
    if(strcmp(l->v.code, x.code) > 0)
        return insererEnTete(l, x);
    if(strcmp(x.code, l->v.code) == 0)
        return l;
    l->suiv = inserer(l->suiv, x);
    return l;
}

Liste supprimerEnTete(Liste l)
{
    Maillon *aux;
    if(l == NULL)
    {
        printf("Opération interdite !!!\n");
        exit(1);
    }
    aux = l;
    l = l->suiv;
    free(aux);
    return l;
}

Liste supprimer(Liste l, Mat x)
{
    if(l == NULL)
        return l;
    if(strcmp(l->v.code, x.code) > 0)
        return l;
    if(strcmp(x.code, l->v.code) == 0)
        return supprimerEnTete(l);
    l->suiv = supprimer(l->suiv, x);
    return l;
}

Mat Tete(Liste l)
{
    if(l == NULL)
    {
        printf("Opération à ne pas faire si la liste est vide !!!\n");
        exit(1);
    }
    return l->v;
}

bool vide(Liste l)
{
    return l == NULL;
}

void afficher(Liste l)
{
    printf("\n");
    while(! vide(l))
    {
        afficherMat(Tete(l));
        l = l->suiv;
    }
    printf("\n");
}

bool rechercher(Liste l, Mat x)
{
    if(vide(l))
        return false;
    if(strcmp(l->v.code, x.code) > 0)
        return false;
    if(strcmp(x.code,l->v.code) == 0)
        return true;
    return rechercher(l->suiv, x);
}

int longueur(Liste l)
{
    int cpt = 0;
    while(! vide(l))
    {
        cpt++;
        l = l->suiv;
    }
    return cpt;
}

Liste charger(char *nomFichier)
{
    FILE *flot;
    Mat x;
    Liste l = listenouv();
    flot = fopen(nomFichier, "r");
    if(flot == NULL)
    {
        printf("Erreur d'ouverture du fichier !!!\n");
        exit(1);
    }
    x = lireMat(flot);
    while(!feof(flot))
    {
        l = inserer(l, x);
        x = lireMat(flot);
    }
    fclose(flot);
    return l;
}

void sauv(Liste l, char *nomFichier)
{
    FILE *flot;
    flot = fopen(nomFichier, "w");
    if(flot == NULL)
    {
        printf("Erreur d'ouverture du fichier !!!\n");
        exit(1);
    }
    while(! vide(l))
    {
        fprintf(flot, "%s\t%d\t%s\n", l->v.code, l->v.coef, l->v.design);
        l = l->suiv;
    }
    fclose(flot);
}

int menu(void)
{
    int choix;
    printf("1. Inserer un élément dans la liste\n");
    printf("2. Supprimer un élément de la liste\n");
    printf("3. Afficher la liste\n");
    printf("4. Rechercher un élément dans la liste\n");
    printf("5. Afficher la longueur de la liste\n");
    printf("6. Quitter\n");
    printf("Votre choix : ");
    scanf("%d", &choix);
    return choix;
}

void global(void)
{
    Liste l;
    Mat x;
    char code[6], design[32];
    int choix, coef;
    char nomFichier[32];

    l = charger("matieres.don");
    choix = menu();
    while (choix != 6)
    {
        if(choix == 1)
        {
            printf("\nEntrer le code de la matière : ");
            scanf("%s", &code);
            printf("Entrer le coefficient de la matière : ");
            scanf("%d", &coef);
            printf("Entrer la désignation de la matière : ");
            scanf("%s", &design);
            printf("\n");
            strcpy(x.code, code);
            x.coef = coef;
            strcpy(x.design, design);
            l = inserer(l, x);
        }
        if(choix == 2)
        {
            printf("\nEntrer le code de la matière à supprimer : ");
            scanf("%s", &code);
            strcpy(x.code, code);
            l = supprimer(l, x);
        }
        if(choix == 3)
            afficher(l);
        if(choix == 4)
        {
            printf("\nEntrer le code de la matière à rechercher : ");
            scanf("%s", &code);
            strcpy(x.code, code);
            if(rechercher(l, x) == true)
                printf("La matière est dans la liste\n\n");
            else
                printf("La matière n'est pas dans la liste\n\n");
        }
        if(choix == 5)
            printf("\nLa longueur de la liste est : %d\n\n", longueur(l));
        choix = menu();
    }
    sauv(l, "matieres.txt");
}