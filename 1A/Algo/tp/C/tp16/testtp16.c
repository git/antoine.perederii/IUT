#include "tp16.h"

void test(void)
{
    Tour A, B, C;
    A.p = pilenouv();
    strcpy(A.nom, "Tour A");
    B.p = pilenouv();
    strcpy(B.nom, "Tour B");
    C.p = pilenouv();
    strcpy(C.nom, "Tour C");
    int n, i;
    printf("Donner le nombre d'objets : \n");
    scanf("%d", &n);
    for(i = n; i >= 1; i--)
    {
        A.p = empiler(A.p, i);
    }
    afficher(A.p);
    afficher(B.p);
    afficher(C.p);
    hanoi(n, &A, &B, &C);
    afficher(A.p);
    afficher(B.p);
    afficher(C.p);
}

int main(void)
{
	test();
	return 0;
}