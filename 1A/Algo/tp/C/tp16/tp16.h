#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct pile {
    int v;
    struct pile *suiv;
} Maillon, *Pile;

typedef struct {
    Pile p;
    char nom[20];
} Tour;

Pile pilenouv(void);
Pile empiler(Pile p, int x);
Pile depiler(Pile p);
int sommet(Pile p);
bool vide(Pile p);
void afficherX(int p);
void afficher(Pile p);
void test(void);
void hanoi(int n, Tour *A, Tour *B, Tour *C);