#include "tp16.h"

Pile pilenouv(void)
{
    Pile p = NULL;
    return p;
}

Pile empiler(Pile p, int x)
{
    Maillon *q;
    q = (Maillon *)malloc(sizeof(Maillon));
    if(q == NULL)
    {
        printf("Erreur d'allocation\n");
        exit(1);
    }
    q->v = x;
    q->suiv = p;
    return q;
}

Pile depiler(Pile p)
{
    Maillon *q;
    if(p == NULL)
    {
        printf("Opération Interdite !!!\n");
        exit(1);
    }
    q = p;
    p = p->suiv;
    free(q);
    return p;
}

int sommet(Pile p)
{
    if(p == NULL)
    {
        printf("Opération Interdite !!!\n");
        exit(1);
    }
    return p->v;
}

bool vide(Pile p)
{
    return p == NULL;
}

void afficherX(int p)
{
    printf("%d ", p);
}

void afficher(Pile p)
{
    printf("\n");
    while (! vide(p))
    {
        afficherX(sommet(p));
        p = p->suiv;
    }
    printf("\n");
}

void hanoi(int n, Tour *A, Tour *B, Tour *C)
{
    int s;
    if(n == 1)
    {
        s = sommet(A->p);
        A->p = depiler(A->p);
        C->p = empiler(C->p, s);
        printf("%d passe de la %s vers la %s\n", s, A->nom, C->nom);
        return;
    }
        hanoi(n - 1, A, C, B);
        hanoi(1, A, B, C);
        hanoi(n - 1, B, A, C);
}