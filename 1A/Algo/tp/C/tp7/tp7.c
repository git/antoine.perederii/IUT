	#include "tp7.h"

int remplirTableaux(int tMatieres[],int tCoeff[],int max)
{
    int nMatieres, coeff, i = 0;
    FILE *flot;
    flot = fopen("matieres.txt", "r");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier matieres.txt\n");
        return -1;
    }
    fscanf(flot, "%d%d", &nMatieres, &coeff);
    while(!feof(flot))
    {
        if(i == max)
        {
            printf("Le tableau est plein\n");
            return -1;
        }
        tMatieres[i] = nMatieres;
        tCoeff[i] = coeff;
        fscanf(flot, "%d%d", &nMatieres, &coeff);
        i = i + 1;
    }
    fclose(flot);
    return i;
}

void affichage(int tMatieres[],int tCoeff[], int nbMatieres)
{
    int i;
    printf("N° matieres coefficient\n");
    for(i=0; i < nbMatieres; i++)
        printf("\t%d\t%d\n", tMatieres[i], tCoeff[i]);
}


int choixMenu(void)
{
    int choix;
    printf("\t Gestion des matières\n");
    printf("1.\taffichage\n");
    printf("2.\tmodification du coefficient d'une matière\n");
    printf("3.\tcréation d'une matière\n");
    printf("4.\tsuppression d'une matière\n");
    printf("5.\tmoyenne des notes dans chaque matières\n");
    printf("6.\tmoyenne d'un étudiant\n");
    printf("9.\tquitter\n");
    printf("\noption choisie :\n");
    scanf("%d", &choix);
    return choix;
}

void gestionMatieres(void)
{
    
    int tMatieres[25] = {0}, tCoeff[25] = {0}, tNotes[25] = {0}, tNbNotes[25] = {0};
    int max = 25, nbMatieres, choix;
    FILE *flot;
    flot = fopen("matieres.txt", "a");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier matieres.txt\n");
    }
    nbMatieres = remplirTableaux(tMatieres, tCoeff, max);
    if(nbMatieres < 0)
    {
        printf("Erreur d'ouverture du fichier ou tableau plein !!!\n");
        return ;
    }
    choix = choixMenu();
    while(choix != 9)
    {
        if(choix == 1)
        {
            affichage(tMatieres, tCoeff, nbMatieres);
        }
        if(choix == 2)
        {
            modificationCoefficient(tMatieres, tCoeff, nbMatieres);
        }
        if(choix == 3)
        {
            nbMatieres = insertionNewMatiere(tMatieres, tCoeff, nbMatieres, max);
        }
        if(choix == 4)
        {
            nbMatieres = suppressionMatiere(tMatieres, tCoeff, nbMatieres);
        }
        if(choix == 5)
        {
            MoyenneNotesMatiere(tMatieres, tNotes, tNbNotes, nbMatieres);
        }
        if(choix == 6)
        {
            MoyenneEtudiant(tCoeff, tMatieres, tNotes, tNbNotes, nbMatieres);
        }
        choix = choixMenu();
    }
    sauvegarde(tMatieres, tCoeff, nbMatieres);
    fclose(flot);
    exit(1);
}


int rechercherMatiere(int tMatieres[], int nbMatiere, int matiere, int *trouve)
{
    int j;
    for(j = 0; j < nbMatiere; j++)
    {
        if(tMatieres[j] == matiere)
        {
            *trouve = 1;
            return j;
        }
        if(tMatieres[j] > matiere)
        {
            *trouve = 0;
            return j;
        }
    }
    *trouve = 0;
    return j;
}

int modificationCoefficient(int tMatieres[], int tCoeff[], int nbMatieres)
{
    int matieres, coeff, trouve, i;
    printf("Donnez le numéro de la matière à modifier :\n");
    scanf("%d", &matieres);
    i = rechercherMatiere(tMatieres, nbMatieres, matieres, &trouve);
    if(trouve == 0)
    {
        printf("La matière n'existe pas !!!\n");
        return -1;
    }
    printf("Voici l'ancien coefficient de la matière %d : %d\n", matieres, tCoeff[i]);
    printf("Donnez le nouveau coefficient :\n");
    scanf("%d", &coeff);
    tCoeff[i] = coeff;
    return 0;
}

int sauvegarde(int tMatieres[], int tCoeff[], int nbMatieres)
{
    int i;
    FILE *flot;
    flot = fopen("matieres.txt", "w");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier matieres.txt\n");
        return -1;
    }
    for(i = 0; i < nbMatieres; i++)
    {
        fprintf(flot, "%d\t%d\n", tMatieres[i], tCoeff[i]);
    }
    fclose(flot);
    return 0;
}

int insertionNewMatiere(int tMatieres[], int tCoeff[], int nbMatieres, int max)
{
    int matieres, coeff, trouve, i, j;
    printf("Donnez le numéro de la matière à insérer et son coefficient :\n");
    scanf("%d%d", &matieres, &coeff);
    i = rechercherMatiere(tMatieres, nbMatieres, matieres, &trouve);
    if(trouve == 1)
    {
        printf("La matière existe déjà !!!\n");
        return -1;
    }
    for(j=nbMatieres; j>= i; j--)
    {
        tMatieres[j] = tMatieres[j-1];
        tCoeff[j] = tCoeff[j-1];
    }
    tMatieres[i]= matieres;
    tCoeff[i] = coeff;
    nbMatieres = nbMatieres + 1;
    return nbMatieres;
}

int suppressionMatiere(int tMatieres[], int tCoeff[], int nbMatieres)
{
    int matieres, coeff, trouve, i, j, max;
    printf("Donnez le numéro de la matière à supprimer :\n");
    scanf("%d", &matieres);
    i = rechercherMatiere(tMatieres, nbMatieres, matieres, &trouve);
    if(trouve == 0)
    {
        printf("La matière n'existe pas !!!\n");
        return -1;
    }
    printf("Coefficient de la matière à supprimer : %d\n", tCoeff[i]);
    for(j=i; j<(nbMatieres-1); j++)
    {
        tMatieres[j] = tMatieres[j+1];
        tCoeff[j] = tCoeff[j+1];
    }
    nbMatieres = nbMatieres -1;
    return nbMatieres;
}

int MoyenneNotesMatiere(int tMatieres[], int tNotes[], int tNbNotes[], int nbMatieres)
{
    int nbNotes, somme, Etudiant, i, matiere, note, trouve, moyenne;
    FILE *flot;
    flot = fopen("matieres2.txt", "r");
    if(flot == NULL)
    {
        printf("Pb d'ouverture du fichier matieres2.txt\n");
        return -1;
    }
    printf("Donnez le numéro de la matière pour laquelle vous voulez calculer la moyenne :\n");
    scanf("%d", &matiere);
    i = rechercherMatiere(tMatieres, nbMatieres, matiere, &trouve);
    if(trouve == 0)
    {
        printf("La matière n'existe pas !!!\n");
        return -1;
    }
    while(!feof(flot))
    {
        fscanf(flot, "%d%d%d", &Etudiant, &matiere, &note);
        if(matiere == tMatieres[i])
        {
            tNotes[i] = tNotes[i] + note;
            tNbNotes[i] = tNbNotes[i] + 1;
        }
    }
    somme = tNotes[i];
    nbNotes = tNbNotes[i];
    moyenne = Moyenne(somme, nbNotes);
    tNotes[i] = moyenne;
    printf("N°Matière\tMoyenne\n");  
    for(i = 0; i < nbMatieres; i++)
    {
        if(tNbNotes[i] != 0)
            printf("\t%d\t%d\n", tMatieres[i], tNotes[i]);
        else printf("\t%d\tpas de note\n", tMatieres[i]);
    }
    return 0;
}

float Moyenne(int somme, int nbNotes)
{
    float moyenne;
    moyenne = (float)somme / nbNotes;
    return moyenne;
}

void MoyenneEtudiant(int tCoeff[], int tMatieres[], int tNotes[], int tNbNotes[], int nbMatieres)
{
    int i, cumulNotes, cumulcoeff, moyenne, moyenneMatiere;
    cumulNotes = 0;
    for(i = 0; i < nbMatieres; i++)
    {
        cumulNotes = cumulNotes + tNotes[i] * tCoeff[i];
        cumulcoeff = cumulcoeff + tCoeff[i];
    }
    moyenneMatiere = cumulNotes / cumulcoeff;
    printf("N°Matière\tMoyenne\n");  
    for(i = 0; i < nbMatieres; i++)
    {
        if(tNbNotes[i] != 0)
            printf("\t%d\t%d\n", tMatieres[i], tNotes[i]);
        else printf("\t%d\tpas de note\n", tMatieres[i]);
    }
    printf("Moyenne générale\t%d\n", moyenneMatiere);
}
