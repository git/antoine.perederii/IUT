#include <stdio.h>
#include <stdlib.h>

int remplirTableaux(int tMatieres[],int tCoeff[],int max);
void affichage(int tMatieres[],int tCoeff[],int nbMatieres);
void gestionMatieres(void);
int rechercherMatiere(int tMatieres[], int i, int matieres, int *trouve);
int modificationCoefficient(int tMatieres[], int tCoeff[], int nbMatieres);
int sauvegarde(int tMatieres[], int tCoeff[], int nbMatieres);
int insertionNewMatiere(int tMatieres[], int tCoeff[], int nbMatieres, int max);
int suppressionMatiere(int tMatieres[], int tCoeff[], int nbMatieres);
int MoyenneNotesMatiere(int tMatieres[], int tNotes[], int tNbNotes[], int nbMatieres);
float Moyenne(int somme, int nbNotes);
void MoyenneEtudiant(int tCoeff[], int tMatieres[], int tNotes[], int tNbNotes[], int nbMatieres);