#include "tp5.h"

void saisieEmployeNbreFormations(int *idEmp, int *nbForm) {
	printf("Saisir identifiant de l'employé: ");
	while (1) {
		scanf("%d%*c", idEmp);
		if (*idEmp < 1000 || *idEmp > 9999) {
			printf("identifiant d'employé incorrect: il doit être à 4 chiffres, re saisir: ");
		} else break;
	}
	printf("Saisir nombre de formation: ");
	while (1) {
		scanf("%d%*c", nbForm);
		if (*nbForm < 0 || *nbForm > 10) {
			printf("nombre de formations incorrect, il doit être entre 0 et 10 compris, re saisir: ");
		} else break;		
	}
}

int saisieControleeIemeFormation(int i) {
	int id;
	printf("identifiant de la formation %d:", i);
	while(1) {
		scanf("%d%*c", &id);
		if (id >= 100 || id < 10) {
			printf("identifiant doit avoir 2 chiffres, re-saisir: ");
			continue;
		}

		int c1 = id / 10;
		int c2 = id - (10 * c1);
		if (c1 < 1 || c1 > 5) {
			printf("premier chiffre de l'identifiant (%d) doit être compris entre 1 et 5 compris, re-saisir: ", c1);
			continue;
		}
		if (c2 < 1 || c2 > 8){
			printf("deuxieme chiffre de l'identifiant (%d) doit être compris entre 1 et 8 compris, re-saisir: ", c2);
			continue;
		}
		break;
	}
	return id;
}

void enregistrementEmployee(FILE *flot) {
	int idEmp = 0, nbForm = 0;
	saisieEmployeNbreFormations(&idEmp, &nbForm);
	fprintf(flot, "%d 	%d", idEmp, nbForm);
	if (nbForm > 0) fprintf(flot, " 	");
	for (int i = 1; i <= nbForm; i++) {
		int id = saisieControleeIemeFormation(i);
		fprintf(flot, " %d", id);
		i++;
	}
	fprintf(flot, "\n");
}

void enregistrementEmployees(void) {
	FILE* flot = fopen("/home/UCA/mabatista1/algo/tp5/donneeEmployees.txt", "w");
	char c;
	while(1) {
		enregistrementEmployee(flot);
		printf("Voulez-vous un autre saisie ? (O/N)");
		scanf("%c", &c);
		if (c != 'O') break;
	}
	fclose(flot);
}

void traitementFichierEmployees(int idForm) {
	FILE *flot = fopen("/home/UCA/mabatista1/algo/tp5/donneeEmployees.txt","r");
	int idEmp, nbForm;
	if (!fscanf(flot, "%d%d", &idEmp, &nbForm)) {
		printf("il n'y a pas d'emplyés dans le fichier !");
		return;
	}
	printf("Les employés qui ont suivit la formation %d sont:\n", idForm);
	int nbEmployes = 0, someNombresFormations =0, max = 0, nbSansForm = 0;
	while(!feof(flot)) {
		int i = 0;
		int idFormEmp = 0;
		while(i < nbForm) {
			fscanf(flot, "%d", &idFormEmp);
			if (idFormEmp == idForm)
				printf("\temployé n° %d\n", idEmp);
			i++;
		}
		if (nbForm > max) max = nbForm;
		if (nbForm == 0) nbSansForm++;
		someNombresFormations += nbForm;
		nbEmployes++;

		fscanf(flot, "%d %d", &idEmp, &nbForm);
	}
	fclose(flot);
	printf("Statistiques: \n");
	printf("\tNombre max de formations réellement suivies par employé: %d\n", max);
	printf("\tNombre d'employés sans formation: %d\n", nbSansForm);
	printf("\tNombre moyen de formation par employés: %.2f\n", (float) someNombresFormations / nbEmployes);
}

void afficherFormations(int idx, int nb) {
	for (int i = 1; i < nb + 1; i++) 
		printf("%d%d ", idx, i);
	if (nb > 0) printf("\n");
}


void formations(void) {
	FILE* flot = fopen("/home/UCA/mabatista1/algo/tp5/formations.txt", "r");
	int nb = 0;
	if (!fscanf(flot, "%d", &nb)) {
		printf("Le fichier est vide !");
	}
	int idx = 0;
	while(!feof(flot)) {
		afficherFormations(++idx, nb);
		fscanf(flot, "%d", &nb);
	}

}

void globale(void) {
	formations();
}