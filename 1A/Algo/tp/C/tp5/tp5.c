#include "tp5.h"

void saisieEmployeNbreFormatrions(int *idEmp, int *nbForm)
{  
  printf("Veuillez saisir votre identifiant :");
  scanf("%d", idEmp);
  printf("identifiant de l'employé : %d\n", *idEmp);
  while(*idEmp < 1000 || *idEmp > 9999)
  {
    printf("identifiant d'employé incorrect, il doit être à 4 chiffrres. Re-saisir : ");
    scanf("%d", idEmp);
    printf("%d\n", *idEmp);
  }
  printf("Veuillez saisir le nombre de formations :");
  scanf("%d", nbForm);
  printf("nombre de formations choisies : %d\n", *nbForm);
  while(*nbForm < 0 || *nbForm > 10)
  {
    printf("nombre de formations incorrect, il doit être entre 0 et 10 compris. Re-saisir : ");
    scanf("%d", nbForm);
    printf("nombre de formations choisies : %d\n", *nbForm);
  }
}
  
int saisieControleeIemeFormation(int i)
{
  int idForm;
  printf("Veuillez saisir l'identifiant de la %dème formation :", i);
  scanf("%d", &idForm);
  while(((idForm/10)<1 || (idForm/10) >5) || ((idForm%10) < 1 || (idForm%10) > 8))
  {
    printf("identifiant de la %dème formation incorrect, il doit être à 2 chiffrres. Re-saisir : ", i);
    scanf("%d", &idForm);
    printf("identifiant de la %dème formations choisies correct : %d\n", i, idForm);
  }
  return idForm;
}

void enregistrementEmployeNbreFormations(void)
{
  int idEmp, nbForm, i = 1, idForm;
  FILE *f;
  f = fopen("donneesEmployes.txt", "a");
  if(f == NULL)
  {
    printf("erreur d'ouverture du fichier");
    exit(1);
  }
  saisieEmployeNbreFormatrions(&idEmp, &nbForm);
  fprintf(f, "\n%d  %d  ", idEmp, nbForm);
  while(i < (nbForm+1))
  {
    idForm = saisieControleeIemeFormation(i);
    fprintf(f, "%d ", idForm);
    i = i + 1;
  }
  fclose(f);
}

void traitementFichierEmployes(void)
{
  FILE *f;
  int idEmp, nbForm, i = 1, idForm;
  f = fopen("donneesEmployes.txt", "r");
  if(f == NULL)
  {
    printf("erreur d'ouverture du fichier");
    exit(1);
  }
  while(!feof(f))
  {
    printf("identifiant de l'employé : %d\n", idEmp);
    printf("nombre de formations choisies : %d\n", nbForm);
    if(nbForm > 0)
    {
      while(i < (nbForm+1))
      {
        fscanf(f, "%d", &idForm);
        printf("identifiant de la %dème formations choisies correct : %d\n", i, idForm);
        i = i + 1;
      }
    }
    i = 1;
    printf("nombre d'employé qui n'ont pas suivie de formations :")
  }
  fclose(f);
}
void affiche1Categ(int numCateg, int nbThem)
{
  int i = 1;
  printf("Categorie %d : ", numCateg);
  while(i < (nbThem+1))
  {
    printf("%d ", i);
    i = i + 1;
  }
  printf("");
}

int afficheFormationProposees(void)
{
  //affiche les identifiants de toutes les formations proposées cette année par l'entreprise
  //cette fonction li le fichier formation.txt afin de réaliser cet affichage et retourne un code : (-1) si problème ou 1 sinon
  FILE *f;
  int numCateg, nbThem, i = 1;
  f = fopen("formation.txt", "r");
  if(f == NULL)
  {
    printf("erreur d'ouverture du fichier");
    exit(1);
  }
  while(!feof(f))
  {
    fscanf(f, "%d %d", &numCateg, &nbThem);
    affiche1Categ(numCateg, nbThem);
  }
  fclose(f);
  return 1;
}