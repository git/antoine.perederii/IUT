#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct maillon {
    int v;
    struct maillon *suiv;
} Maillon;

typedef struct {
    Maillon *t;
    Maillon *q;
} File;

typedef struct maillo {
    int v;
    struct maillo *suiv;
} Maillo, *Fil;

File filenouv(void);
File adjq(File f, int x);
File supt(File f);
int tete(File f);
bool vide(File f);
int longueur(File f);
void afficher(File f);
Fil filenouv2(void);
Fil adjq2(Fil f, int x);
Fil supt2(Fil f);
bool vide2(Fil f);
int tete2(Fil f);
int longueur2(Fil f);
void afficher2(Fil f);
void jeu(void);
void jeu2(void);