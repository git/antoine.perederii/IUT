#include "tp15.h"

/*  Exercice 1      Partie I   */

File filenouv(void)
{
    File f;
    f.t = NULL;
    f.q = NULL;
    return f;
}

File adjq(File f, int x)
{
    Maillon *m;
    m = (Maillon *)malloc(sizeof(Maillon));
    if(m == NULL)
    {
        printf("Erreur d'allocation memoire");
        exit(1);
    }
    m->v = x;
    m->suiv = NULL;
    if(vide(f))
    {
        f.t = m;
        f.q = m;
        return f;
    }
    f.q->suiv = m;
    f.q = m;
    return f;
}

File supt(File f)
{
    Maillon *aux;
    if(vide(f))
    {
        printf("opération impossible");
        exit(1);
    }
    if(f.t == f.q)
    {
        free(f.t);
        return filenouv();
    }
    aux = f.t;
    f.t = f.t->suiv;
    free(aux);
    return f;
}

bool vide(File f)
{
    return f.t == NULL && f.q == NULL;
}

int tete(File f)
{
    if(vide(f))
    {
        printf("opération impossible");
        exit(1);
    }
    return f.t->v;
}

int longueur(File f)
{
    int cpt = 0;
    while(f.t != NULL)
    {
        cpt++;
        f.t = f.t->suiv;
    }
    return cpt;
}

void afficher(File f)
{
    while(f.t != NULL)
    {
        printf("%d ", f.t->v);
        f.t = f.t->suiv;
    }
    printf("\n");
}

/*  Exercice 1      Partie II   */



Fil filenouv2(void)
{
    Fil f;
    f = NULL;
    return f;
}

Fil adjq2(Fil f, int x)
{
    Maillo *m;
    m = (Maillo *)malloc(sizeof(Maillo));
    if(m == NULL)
    {
        printf("opération interdite");
        exit(1);
    }
    m->v = x;
    if(vide2(f))
    {
        m->suiv = m;
        return m;
    }
    m->suiv = f->suiv;
    f->suiv = m;
    return m;
}

Fil supt2(Fil f)
{
    Maillo *aux;
    if(vide2(f))
    {
        printf("opération interdite");
        exit(1);
    }
    if(f->suiv == f)
    {
        free(f);
        return filenouv2();
    }
    aux = f->suiv;
    f->suiv = aux->suiv;
    free(aux);
    return f;
}

bool vide2(Fil f)
{
    return f == NULL;
}

int tete2(Fil f)
{
    if(vide2(f))
    {
        printf("opération interdite");
        exit(1);
    }
    return f->suiv->v;
}

int longueur2(Fil f)
{
    int cpt = 1;
    Maillo *aux;
    if(vide2(f))
    {
        return 0;
    }
    aux = f->suiv;
    while(aux != f)
    {
        cpt++;
        aux = aux->suiv;
    }
    return cpt;
}

void afficher2(Fil f)
{
    Maillo *m=f;
    if(vide2(f))
    {
        printf("opération interdite");
        return;
    }
    printf("%d", f->suiv->v);
    while(f->suiv != m)
    {
        f = f->suiv;
        printf(" %d", f->suiv->v);
    }
    printf("\n");
}


/*  Exercice 2      Partie I   */

void jeu(void)
{
    File f = filenouv();
    int i, t;
    for(i = 1; i <= 40; i++)
    {
        f = adjq(f, i);
    }
    while(longueur(f) > 1)
    {
        for(i = 1; i <= 6; i++)
        {
            t = tete(f);
            f = supt(f);
            f = adjq(f, t);
        }
        f = supt(f);
    }
    printf("Le gagnant est le joueur %d\n", tete(f));
}

/*  Exercice 2      Partie II   */

void jeu2(void)
{
    Fil f = filenouv2();
    int i, t;
    for(i = 1; i <= 40; i++)
    {
        f = adjq2(f, i);
        
    }
    while(longueur2(f) > 1)
    {
        for(i = 1; i <= 6; i++)
        {
            t = tete2(f);
            f = supt2(f);
            f = adjq2(f, t);
        }
        f = supt2(f);
    }
    printf("Le gagnant est le joueur %d\n", tete2(f));
}
