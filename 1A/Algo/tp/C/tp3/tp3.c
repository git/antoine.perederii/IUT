#include "tp3.h"


float coutSalle (char deco, char categ)
{
    float prix;
    if(deco == 'o' || deco == 'O')
        if(categ == 'A' || categ == 'a')
            prix = 1500 *(1+(10.0/100));
        else if(categ == 'B' || categ == 'b')
                prix = 1000 *(1+(10.0/100));
            else prix = 700 *(1+(10.0/100));
    else 
        if(categ == 'A' || categ == 'a')
            prix = 1500;
        else if(categ == 'B' || categ == 'b')
                prix = 1000;
            else prix = 700; 
    return prix;       
}

float PrixAperitif(char Formule, int nbrPers)
{
    float Montant, PrixTot;
    if(Formule == 'B' || Formule == 'b')
        if(nbrPers <= 50)
            Montant = 300;
        else if(nbrPers <= 100)
                Montant = 500;
            else
                Montant = 800;
    else
        Montant = nbrPers * 12;

    PrixTot = Montant + 50;
    return PrixTot;
}

void faffichage (float cout, float Montant, float coutTot)
{
    printf("Location de Salle : %.2f\nApéritif :\t%.2f\nTotal : \t%.2f\n", cout, Montant, coutTot);
}

void Global (void)
{
    float cout, Montant, coutTot;
    char categ, deco, Formule, Apero;
    int nbrPers;
    printf("Quelle est la salle (A,B,C) :");
    scanf("%c%*c", &categ);
    printf("voulez-vous la deco (O,N) :");
    scanf("%c%*c", &deco);
    printf("Voulez-vous un aperitif (O,N) :");
    scanf("%c%*c", &Apero);
    if(Apero == 'O' || Apero == 'o')
    {
        printf("Quel formule voulez-vous (B/U) :");
        scanf("%c%*c",&Formule);
        printf("Combien êtes-vous :");
        scanf("%d",&nbrPers);
        Montant = PrixAperitif(Formule, nbrPers);
    }
    else Montant = 0;
    cout = coutSalle(deco, categ);
    coutTot = Montant + cout;
    faffichage(cout, Montant, coutTot);
}
