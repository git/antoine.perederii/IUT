#include "tp3.h"

void testLocSalle(void)
{
    float cout;
    char categ, deco;
    printf("Quelle est la salle (A,B,C) :");
    scanf("%c%*c", &categ);
    printf("voulez-vous la deco (O,N) :");
    scanf("%c%*c", &deco);
    cout = coutSalle(deco, categ);
    printf("Catégorie %c     Décoration %c    Coût %.2f\n",categ, deco, cout);
}

void testApero(void)
{
    float Montant;
    char Formule;
    int nbrPers;
    printf("Quel formule voulez-vous (B/U) :\n");
    scanf("%c%*c",&Formule);
    printf("Combien êtes-vous :\n");
    scanf("%d",&nbrPers);
    Montant = PrixAperitif(Formule, nbrPers);
    printf("Le montant est donc de %f€\n", Montant);
}

int main(void)
{
    //testLocSalle();
    //testApero();
    Global();
	return 0;
}