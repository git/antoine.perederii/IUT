#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char pays[20];
    int or;
    int ar;
    int br;
    int tot;
} Pays;

Pays lirePays(FILE *flot);
void afficherPays(Pays p);
int chargeResults(Pays *tpays[], int nbmax);
void afficheTab(Pays *tpays[], int nb);
int plusGrand(Pays *tpays[], int nb);
void echanger(Pays *tpays[], int i, int j);
void triEchange(Pays *tpays[], int nb);
int recherche(Pays *tpays[], int n, char *pays, int *trouve);
int miseajour(Pays *tpays[], int nb, int nbmax);
void sauvegarde(Pays *tpays[], int nb);
void global(void);
