#include "tp13.h"

Pays lirePays(FILE *flot)
{
    Pays p;
    fscanf(flot, "%s %d %d %d", p.pays, &p.or, &p.ar, &p.br);
    p.tot = p.or + p.ar + p.br;
    return p;
}

void afficherPays(Pays p)
{
    if(strlen(p.pays) < 8)
        printf("%s\t\t%d\t%d\t%d\t%d\n", p.pays, p.or, p.ar, p.br, p.tot);
    else
        printf("%s\t%d\t%d\t%d\t%d\n", p.pays, p.or, p.ar, p.br, p.tot);
}

int chargeResults(Pays *tpays[], int nbmax)
{
    Pays p;
    int i=0;
    FILE *flot;
    flot=fopen("joresult1.don","r");
    if(flot==NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return -1;
    }
    p = lirePays(flot);
    while(!feof(flot))
    {
        if(i == nbmax)
        {
            printf("Erreur, tableau plein, trop de pays !!!\n");
            fclose(flot);
            return -2;
        }
        tpays[i] = (Pays*)malloc(sizeof(Pays));
        if(tpays[i] == NULL)
        {
            printf("Erreur d'allocation mémoire");
            fclose(flot);
            return -3;
        }
        *tpays[i] = p;
        i++;
        p = lirePays(flot);
    }
    fclose(flot);
    return i;
}

void afficheTab(Pays *tpays[], int nb)
{
    int i;
    for(i=0; i<nb; i++)
    {
        afficherPays(*tpays[i]);
    }
    printf("\n");
}

int plusGrand(Pays *tpays[], int nb)
{
    int i, pge=0;
    for(i=1; i<nb; i++)
    {
        if(strcmp(tpays[i]->pays, tpays[pge]->pays) > 0)
            pge = i;
    }
    return pge;
}

void echanger(Pays *tpays[], int i, int j)
{
    Pays *aux;
    aux = tpays[i];
    tpays[i] = tpays[j];
    tpays[j] = aux;
}

void triEchange(Pays *tpays[], int nb)
{
    int pge;
    while(nb > 1)
    {
        pge = plusGrand(tpays, nb);
        echanger(tpays, pge, nb-1);
        nb--;
    }
}

int copieR(Pays *tpays[], int i, int j, Pays *R[])
{
    int r, pas = 0;
    for(r=i; r<j; r++)
    {
        R[pas] = tpays[r];
        pas++;
    }
}

int copieS(Pays *tpays[], int i, int j, Pays *S[])
{
    int s, pas = 0;
    for(s=i; s<j; s++)
    {
        S[pas] = tpays[s];
        pas++;
    }
}

void fusion(Pays *R[], int n, Pays *S[], int m, Pays *tpays[])
{
    int i = 0, j = 0, k = 0;
    while(i < n && j < m)
    {
        if(strcmp(R[i]->pays, S[j]->pays) < 0)
        {
            tpays[k] = R[i];
            i++;
            k++;
        }
        else
        {
            tpays[k] = S[j];
            j++;
            k++;
        }
    }
    while(i < n)
    {
        tpays[k] = R[i];
        i++;
        k++;
    }
    while(j < m)
    {
        tpays[k] = S[j];
        j++;
        k++;
    }
}

void triFusion(Pays *tpays[], int nb)
{
    Pays **R, **S;
    if(nb == 1)
        return;
    R = (Pays**)malloc(sizeof(Pays*)*nb/2);
    S = (Pays**)malloc(sizeof(Pays*)*(nb-nb/2));
    if(R == NULL || S == NULL)
        exit (1);
    copieR(tpays, 0, nb/2, R);
    copieS(tpays, nb/2, nb, S);
    triFusion(R, nb/2);
    triFusion(S, nb-nb/2);
    fusion(R, nb/2, S, nb-nb/2, tpays);
    free(R);
    free(S);
}

int recherche(Pays *tpays[], int n, char *pays, int *trouve)
{
    int inf=0, sup=n-1, m;
    while(inf <= sup)
    {
        m = (inf+sup)/2;
        if(strcmp(tpays[m]->pays, pays) == 0)
        {
            *trouve = 1;
            return m;
        }
        else if(strcmp(pays, tpays[m]->pays) < 0)
            sup = m-1;
        else
            inf = m+1;
    }
    *trouve = 0;
    return m;
}

int miseajour(Pays *tpays[], int nb, int nbmax)
{
    Pays p;
    int trouve, pos, i;
    FILE *flot;
    flot=fopen("resdujour.don","r");
    if(flot==NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return -1;
    }
    p = lirePays(flot);
    while(!feof(flot))
    {
        pos = recherche(tpays, nb, p.pays, &trouve);
        if(trouve == 1)
        {
            tpays[pos]->or = tpays[pos]->or + p.or;
            tpays[pos]->ar = tpays[pos]->ar + p.ar;
            tpays[pos]->br = tpays[pos]->br + p.br;
            tpays[pos]->tot = tpays[pos]->tot + p.tot;
        }
        else
        {
            if(nb == nbmax)
            {
                printf("Erreur, tableau plein, trop de pays !!!\n");
                fclose(flot);
                return -2;
            }
            tpays[nb] = (Pays*)malloc(sizeof(Pays));
            if(tpays[nb] == NULL)
            {
                printf("Erreur d'allocation mémoire");
                fclose(flot);
                return -4;
            }
            *tpays[nb] = p;
            nb++;
        }
        p = lirePays(flot);
    }
    fclose(flot);
    return nb;
}

void sauvegarde(Pays *tpays[], int nb)
{
    FILE *flot;
    int i;
    flot=fopen("paysalpha.don","w");
    if(flot==NULL)
    {
        printf("Erreur d'ouverture du fichier");
        return;
    }
    triEchange(tpays, nb);
    for(i=0; i<nb; i++)
    {
        fprintf(flot, "%s\t%d\t%d\t%d\t%d\n", tpays[i]->pays, tpays[i]->or, tpays[i]->ar, tpays[i]->br, tpays[i]->tot);
    }
    fclose(flot);
}

void global(void)
{
    Pays *tpays[130000];
    int nb, trouve, pos,choix;
    char pays[20], rep;
    nb = chargeResults(tpays, 130000);
    if(nb < 0)
        return;
    afficheTab(tpays, nb);
    printf("1.tri echange  \n2. trie dico \n");
    printf("votre choix : ");
    scanf("%d",&choix);
    if(choix == 1)
     triEchange(tpays, nb);
    else triFusion(tpays, nb);
    printf("\n--------- tableau après tri ---------\n");
    afficheTab(tpays, nb);
   /*printf("Voulez-vous modifier un pays ? (o/n) : ");
    scanf("%c", &rep);
    while(rep == 'o' || rep == 'O')
    {
        printf("Entrez le nom du pays recherché : ");
        scanf("%s", pays);
        pos = recherche(tpays, nb, pays, &trouve);
        if(trouve == 1)
            afficherPays(*tpays[pos]);
        else
            printf("Pays non trouvé\n");
        printf("Voulez-vous continuer ? (o/n) : ");
        scanf("%*c%c", &rep);
    }
    nb = miseajour(tpays, nb, 50);
    if(nb < 0)
        return;
    afficheTab(tpays, nb);
    sauvegarde(tpays, nb);*/
}