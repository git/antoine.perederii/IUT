int led=2;
int T=20; //50hz. s'allume et s'eteint 5à fois par seconde
int t=2; // durée état haut
 
void setup()
{
  pinMode(led, OUTPUT);
}
 
void loop()
{ // boucle d'approximativement T ms
 
  digitalWrite(led, HIGH); // Allumé pendant t ms
  delay(t);            
  digitalWrite(led, LOW);  // Éteint pendant T-t ms
  delay(T-t); 
 
}
// Comparez la luminosité des 2 leds. Essayez à nouveau avec t=5 puis t=1. Que constatez-vous ? Justifiez !
// Plus on baisse t, plus on baisse la luminosité de la lampe car elle s'eteint s'allume 50 fois par seconde

// Que se passe-t-il si vous passez à une fréquence de 20hz (T=50 et par exemple t=5) ? Justifiez !
// On attaque à la voir clignoter, 20 fois allume eteint < 24 images par secondes que l'oeuil peut voir