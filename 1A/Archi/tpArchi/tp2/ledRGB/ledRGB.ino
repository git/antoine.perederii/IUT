int ledR=9;
int ledV = 10;
int ledB = 11;
int sensorValuePhoto = 0;
int sensorValuePoten = 0;
int rouge = 0, vert = 0, bleu = 0, i, j;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledR, OUTPUT);
  pinMode(ledV, OUTPUT);
  pinMode(ledB, OUTPUT);
  pinMode(A0, INPUT); // Declaration du potentiometre
  pinMode(A1, INPUT); // Declaration de la photoresistance
}

// void loop() {
//   sensorValuePoten = analogRead(A0)/4; // On divise par 4 pour arriver au bon nombre de bit
//   sensorValuePhoto = analogRead(A1);
//   // put your main code here, to run repeatedly:
//   analogWrite(ledR, 255);
//   analogWrite(ledV, sensorValuePhoto);

//   analogWrite(ledB, sensorValuePoten);
// }

void loop() {
  for(i=0, j=255; i<=255; i++, j--) {
    analogWrite(ledR,i);
    analogWrite(ledB, j);
    delay(30);
  }

  for(i=0, j=255; i<=255; i++, j--) {
    analogWrite(ledV,i);
    analogWrite(ledR, j);
    delay(30);
  }

  for(i=0, j=255; i<=255; i++, j--) {
    analogWrite(ledB,i);
    analogWrite(ledV, j);
    delay(30);
  }
}