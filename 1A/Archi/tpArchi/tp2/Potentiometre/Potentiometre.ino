//This example code is in the public domain.
 
int sensorValue = 0;
int ledValue = 0;
int led = 3; // Mettre sur une broche analogique
 
void setup()
{
  pinMode(led, OUTPUT);
  pinMode(A0, INPUT);
  Serial.begin(9600);
}
 
void loop()
{
  // sensorValuePoten = analogRead(A0)/4;
  // read the input on analog pin 0:
  sensorValuePhoto = analogRead(A0);
  // print out the value you read:
  Serial.println(sensorValue);
  analogWrite(led, sensorValue);
  delay(10); // Delay a little bit to improve simulation performance
}

// Testez. Quelles sont les valeurs min et max observées ? Est-ce conforme à ce que vous avez vu en cours ?
// de 8 à 1021.

// Dans un deuxième temps, ajoutez une led à votre montage et complétez votre code afin de moduler son “intensité”, sa luminosité, en fonction de la valeur lue depuis le potentiomètre. Dit autrement, la led est éteinte avec la valeur min et s'éclaire de plus en plus quand on s'approche de la valeur max générée par le potentiomètre.


// Modifiez si besoin votre code afin que plus vous faites d'obscurité au-dessus de votre capteur, plus la led est lumineuse.