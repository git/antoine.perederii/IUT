/*
 Faire clignoter une led toutes les secondes
*/

int i=2;
int bt=9;
int val;
 
// La fonction setup est exécutée une fois à la mise en secteur de la carte ou si le bouton reset est actionné
 
void setup() 
{
  pinMode(2, OUTPUT); // La broche 2 est initialisée en sortie.
  pinMode(3, OUTPUT); // La broche 3 est initialisée en sortie.
  pinMode(4, OUTPUT); // La broche 4 est initialisée en sortie.
  pinMode(5, OUTPUT); // La broche 5 est initialisée en sortie.
  pinMode(6, OUTPUT); // La broche 6 est initialisée en sortie.
  pinMode(7, OUTPUT); // La broche 7 est initialisée en sortie.
  pinMode(8, OUTPUT); // La broche 8 est initialisée en sortie.
  pinMode(9, INPUT); // La broche 9 est initialisée en entrée.
  Serial.begin(9600);
}
 
 
// La fonction loop est exécutée en boucle indéfiniment
 
void loop() 
{
  val=digitalRead(bt);
  Serial.println(val); 
  if(val == HIGH) {
      if(i == 9) {
        i=2;
      }
    digitalWrite(i, HIGH);   // Allume la led  (HIGH est la valeur du voltage)
    delay(1000);              // Attend une  seconde
    digitalWrite(i, LOW);    // Eteint la LED  (LOW c'est la masse)
    delay(500);      
    i=i+1;        // Attend une seconde
  }
  else {
    if(i == 1) {
        i=8;
      }
    digitalWrite(i, HIGH);   // Eteint la LED  (HIGH c'est la masse)
    delay(1000);              // Attend une  seconde
    digitalWrite(i, LOW);    //  Allume la led (LOW c'est le 5v)
    delay(500);      
    i=i-1;        // Attend une seconde
  }    
  delay(5);
}

// Reliez ensuite la led non plus à la masse mais au 5V. Est-ce que votre programme fonctionne toujours ?
//! NON

// Modifiez votre montage pour que un appui sur le bouton poussoir génère un niveau haut (valeur 1). 
// La led est donc allumée par défaut et s'éteint quand on appuie sur le bouton poussoir.
// Changer le schema

// void setup() 
// {
//   pinMode(2, OUTPUT); // La broche 2 est initialisée en sortie.
//   pinMode(3, OUTPUT); // La broche 3 est initialisée en sortie.
//   pinMode(4, OUTPUT); // La broche 4 est initialisée en sortie.
//   pinMode(5, OUTPUT); // La broche 5 est initialisée en sortie.
//   pinMode(6, OUTPUT); // La broche 6 est initialisée en sortie.
//   pinMode(7, OUTPUT); // La broche 7 est initialisée en sortie.
//   pinMode(8, OUTPUT); // La broche 8 est initialisée en sortie.
//   pinMode(9, OUTPUT); // La broche 9 est initialisée en entrée.
//   Serial.begin(9600);
// }
 
 
// // La fonction loop est exécutée en boucle indéfiniment
 
// void loop() 
// {
//   val=digitalRead(bt);
//   Serial.println(val); 
//   if(val == HIGH) {
//       if(i == 9) {
//         i=2;
//       }
//     digitalWrite(i, HIGH);   // Allume la led  (HIGH est la valeur du voltage)
//     delay(1000);              // Attend une  seconde
//     digitalWrite(i, LOW);    // Eteint la LED  (LOW c'est la masse)
//     delay(500);      
//     i=i+1;        // Attend une seconde
//   }
//   else {
//     if(i == 1) {
//         i=8;
//       }
//     digitalWrite(i, HIGH);   // Eteint la LED  (HIGH c'est la masse)
//     delay(1000);              // Attend une  seconde
//     digitalWrite(i, LOW);    //  Allume la led (LOW c'est le 5v)
//     delay(500);      
//     i=i-1;        // Attend une seconde
//   }    
//   delay(5);
// }
