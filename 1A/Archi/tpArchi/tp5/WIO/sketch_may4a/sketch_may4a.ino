#include "TFT_eSPI.h"
 
TFT_eSPI tft;
float f=10.0/3;
String chaine="et oui !";
 
void setup() { 
    tft.begin();
    tft.setRotation(1); // Définis le coin de l'écran de coordonnées 0,0
    tft.fillScreen(TFT_RED); // fond rouge
 
    tft.drawPixel(150,10, TFT_WHITE); // Dessine un pixel blanc aux coordonnées X=150, Y=10
    tft.drawLine(5, 5, 25, 20,TFT_BLACK); // Dessine une ligne noire des coordonnées (5,5) aux coordonnées (25,20)
    tft.drawFastHLine(3, 25, 150, TFT_BLUE); //Dessine une ligne Horizontale bleue de 150 pixels de long depuis les coordonnées (3,25)
    tft.drawFastVLine(3, 25, 150, TFT_GREEN); // ici elle est verte et Verticale
    tft.drawRect(7,27,25,50,TFT_YELLOW); 
    tft.fillRect(7,150,25,50,TFT_YELLOW);
    tft.drawCircle(50,50,9,TFT_CYAN);
    tft.fillCircle(50, 70, 9, TFT_CYAN);
    tft.drawEllipse(100,50,10,20,TFT_NAVY);
    tft.fillEllipse(100, 100,20,10, TFT_NAVY);
    tft.fillTriangle(100,150, 100,170, 130,170, TFT_DARKGREEN);
    tft.fillRoundRect(170,25,70,50,9,TFT_PURPLE);
    tft.drawChar(280,30,'A',TFT_BLACK,TFT_WHITE,4);
 
    tft.setTextColor(TFT_BLACK);
    tft.setTextSize(2);
    tft.drawNumber(12,170, 160);
    tft.drawFloat(f,4,230,160); /* 4 chiffres apres virgule) */
    tft.drawString("Salut", 170, 180);
    tft.setTextSize(1);
    tft.drawString("Des exemples de dessins, de couleurs",70, 200);
    tft.setCursor(70,220); //positionne le curseur aux coordonnées X=70 t Y=220
    tft.print(chaine);   // Écrit la chaine à partir du curseur courant et déplace le curseur d'autant
}
 
void loop() {
 
  }