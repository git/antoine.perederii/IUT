// /* Commentaires et bugs à olivier.guinaldo@uca.fr
// Dépt Informatique - IUT Clermont - Avril 2022  */
 
// #include "TFT_eSPI.h"
// #define R 5 //Rayon de la boule donc diamètre 10
// #define XMAX 320
// #define YMAX 240
 
// TFT_eSPI tft;
// int x=XMAX/2,y=YMAX/2 ; //boule positionnée au milieu de l'écran
// unsigned int modif=1 ; //si déplacement, on affiche. 1 pour premier affichage
 
// void setup() {
//   tft.begin();
//   tft.setRotation(3); // (0,0) à l'oppossé du joyssitck
//   tft.fillScreen(TFT_WHITE); // fond blanc
 
//   pinMode(WIO_5S_UP, INPUT_PULLUP);
//   pinMode(WIO_5S_DOWN, INPUT_PULLUP);
//   pinMode(WIO_5S_LEFT, INPUT_PULLUP);
//   pinMode(WIO_5S_RIGHT, INPUT_PULLUP);
// }
 
// void loop() {
//    if (digitalRead(WIO_5S_UP) == LOW) {
//     modif=1;
//     y--;
//     if (y < 0) {
//       tft.fillScreen(TFT_WHITE);
//       y=YMAX ;
//     }
//    }
//    else if (digitalRead(WIO_5S_DOWN) == LOW) {
//     modif=1;
//     y++;
//     if (y >= YMAX) {
//       tft.fillScreen(TFT_WHITE);
//       y=0 ;
//     }
//    }
//    else if (digitalRead(WIO_5S_LEFT) == LOW) {
//     modif=1;
//     x--;
//     if (x<0) {
//       tft.fillScreen(TFT_WHITE);
//       x=XMAX ;
//     }
//    }
//    else if (digitalRead(WIO_5S_RIGHT) == LOW) {
//     modif=1;
//     x++;
//     if (x >= XMAX) {
//       tft.fillScreen(TFT_WHITE);
//       x=0;
//     }
//    }
 
//    if (modif == 1){      
//     // remettre un fond blanc prend du temps (320x240 pixels à changer) l'affichage n'est plus fluide
//     // donc on efface le précédent avec un cercle blanc plus grand
//         tft.fillCircle(x,y,R + 1,TFT_WHITE); 
//     // on affiche le nouveau cercle   
//         tft.fillCircle(x,y, R, TFT_RED);
//         modif=0;  
//    }
//    //le delay permet de régler la vitesse de déplacement
//    delay(20);
// }