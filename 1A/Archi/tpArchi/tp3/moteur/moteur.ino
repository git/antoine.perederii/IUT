#define MOTORMIN 90
#define MOTORMAX 250
#define MOTORSTART 200 

int motorPin = 9;
int speedUpPin = 7;
int speedDownPin = 6;
int speed;
int ledoutPin=5;
int emergencyPin=2;

/* ------------------------------------------------------------------
 * Démarre le moteur en commençant pas envoyer
 * une impulsion un peu forte sinon il n'y a pas assez de puissance
 * pour lancer la machine. Une fois lancé, il est possible de limiter
 * la puissance et donc la vitesse
 * ------------------------------------------------------------------
 */
void motorSet(int value) {
  if ( value < MOTORSTART ) {
    analogWrite(motorPin, MOTORSTART);
    delay(50);
  }
  analogWrite(motorPin, value);
}
 
// // Demarre le moteur avec un signal PWM permettant de fixer la vitesse
// void motorSet(int value) {
//   analogWrite(motorPin, value);
// }
 
// Arret le moteur
void motorStop() {
  analogWrite(motorPin,0);
}
 
// nouveau setup
void setup() {
  pinMode(motorPin, OUTPUT);
  pinMode(speedUpPin, INPUT_PULLUP);
  pinMode(speedDownPin, INPUT_PULLUP);
  pinMode(ledoutPin, OUTPUT);
  pinMode(emergencyPin, INPUT);
 
  speed = MOTORMIN;
  motorStop();

  pinMode(ledoutPin, OUTPUT);
  pinMode(emergencyPin, INPUT);
  digitalWrite(emergencyPin, HIGH);  // force internal pullup
  attachInterrupt(0,emergencyStop,FALLING);
  clearEmergencyLed();
 
  Serial.begin(9600);
}

void setEmergencyLed() {
  digitalWrite(ledoutPin,HIGH);
}
 
void clearEmergencyLed() {
  digitalWrite(ledoutPin,LOW);
}

/* ------------------------------------------------------------------
 * Interruption déclenchée par l'appuie sur le bouton d’arrêt d'urgence
 * ------------------------------------------------------------------
 */
 
void emergencyStop() {
 
  motorStop();
  speed = MOTORMIN;
  setEmergencyLed();
 
}

// // Arduino Setup
// void setup() {
//   pinMode(motorPin, OUTPUT);
 
//   motorStop();
 
//   Serial.begin(9600);
 
//   motorSet(64);
// }
 
// Arduino loop
void loop() {
 if ( digitalRead(speedUpPin) == LOW ) {
   clearEmergencyLed();
   speed += 10;
   if ( speed > MOTORMAX ) speed = MOTORMAX;
   motorSet(speed);
   Serial.print("UP ");
   Serial.println(speed);
  }
  if ( digitalRead(speedDownPin) == LOW ) {
    clearEmergencyLed();
    speed -=10;
    if ( speed <= MOTORMIN ) {
      speed = MOTORMIN;
      motorStop();
    } else {
      motorSet(speed);
      }
    Serial.print("DOWN ");
    Serial.println(speed);
  }
  delay(500);
  //  delay(1000);
}

// Essayez le programme en remplaçant XXX par les valeurs 220, 150, 64. Essayez éventuellement de lancer le moteur à la main. Que constatez-vous?
// 220 Il tourne comme une horloge
// 150 Il faut le lancer mais c'est bon
// 64 Meme en lancant, il veut pas#define MOTORMIN  80

//  Maintenant, essayez de nouveaux avec des valeurs 200, 80, 64 . Que constatez-vous ?
// jusque 80 ca passe 
