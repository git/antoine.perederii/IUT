#!/bin/sh

val=0

while [ "$val" -ne "9" ] ; do
	if [ "$val" = "1" ] ; then
		echo ""
		echo "$(date)"
	elif [ "$val" = "2" ] ; then 
		echo ""
		echo "Il y a $(who | wc -l) personnes connéctées sur ta session."
	elif [ "$val" = "3" ] ; then  #les "" et = permettent d'eviter les pb si l'utilisateur tape une chaine de carac
		echo ""
		echo "$(ps -elf)"
	elif [ "$val" != "9" ]; then
		echo "C'est quoi ce chiffre ??!! Je ne connais pas !!" >&2
	fi
	
echo""
echo "Menu général"
echo "******************************"
echo ""
echo "<1> Afficher la date (date)"
echo "<2> Afficher le nombre de personnes connéctées (who)"
echo "<3> Afficher la liste des processus (ps)"
echo "<9> Quitter"
echo ""
read val
done
