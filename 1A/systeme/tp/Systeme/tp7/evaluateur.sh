#!/bin/sh

val='a'

until [ "$val" = "q" ] ; do 
	echo -n "Donnez une commande (<q> pour quitter) : "
	read "val"
	if [ "$val" = "q" ]; then 
		exit 0
	fi
	echo "$($val)"
done
