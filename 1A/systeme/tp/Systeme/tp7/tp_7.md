# TP 7 - Scripts

Pas de compte rendu dédié aux TP sur les scripts shell. Vos programmes
feront foi !

<note tip>

Quelques règles simples pour éviter les erreurs de syntaxe :

* jamais d'espace autour d'une affectation (exemple : `a=3`)
* des espaces partout ailleurs (exemple `if`   `test`   `$a` 
 `=`   `4;`   `then...` ou `if`   `[`   `$a`   `=` 
 `4`   `]`   `;`   `then...`

Pour observer les commandes exécutées, passez en mode trace en plaçant
la commande `set -x` en début de programme.

</note>

#### 6 - La commande menu1

En utilisant la structure `while`, écrire un script qui :

Tant que l'utilisateur n'a pas tapé 9

* affiche un menu
* demande à l'utilisateur de saisir une option du menu
* affiche à l'utilisateur le résultat de sa commande

Exemple de ce qui doit s'afficher à l'écran :

```
Menu général
***************************************

<1> Afficher la date (date)

<2> Afficher le nombre de personnes connectées (who)

<3> Afficher la liste des processus (ps)

<9> Quitter
```

#### 7 - La commande evaluateur

En utilisant la structure *until...do...done*, écrire un script qui :

* demande à un utilisateur de saisir une commande
* exécute la commande ou affiche un message d'erreur si la commande ne s'est pas correctement exécutée.
* répète cette opération tant que l'utilisateur le désire

Indication : si vous disposez d'une variable contenant une ligne de
commandes il est possible de l'exécuter via la fonction interne au shell
`eval`. Le code de retour retourné par eval est le code de retour
retourné par la commande. Exemple : `c="ls -l | wc -l"` `eval $c`

Voici un exemple de ce que votre programme doit afficher à l'écran :

```
Saisissez une commande, commande <q> pour quitter.
> date
mar sep 30 21:20:45 CEST 2008
Saisissez une commande, commande <q> pour quitter.
> cd /toto
bash: cd: /toto: Aucun fichier ou répertoire de ce type
cette commande a généré une erreur !!!!
Saisissez une commande, commande <q> pour quitter.
> q
```
