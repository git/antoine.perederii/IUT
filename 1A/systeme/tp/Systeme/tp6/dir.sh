#!/bin/sh

if ! test -d "$1"; then
  echo 'Usage: dir dirname'
  exit 1
fi

colors=$(tput colors)
dir_color=''
exe_color=''
ln_color=''
if test -n "$colors"; then
  dir_color='\e[34m'
  exe_color='\e[32m'
  ln_color='\e[36m'
fi

for file in "$1"/*; do
  if test -d "$file"; then
    echo "$dir_color[$file]\e[0m"
  elif test -x "$file"; then
    echo "$exe_color*$file\e[0m"
  elif test -h "$file"; then
      echo "$ln_color*$file\e[0m"
  else
    echo "$file"
  fi
done

