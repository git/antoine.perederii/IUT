#!/bin/sh

if [ $# -ne 2 ] ; then
	echo "Attention ! merci de donner 2 entiers" >&2
	exit 1
fi

if echo "$1$2" | egrep -q '^[0-9]+$' ; then
	if [ $1 -lt $2 ] ; then
		echo "$1"
	else
		echo "$2"
	fi
else
	echo "attention ! Vos arg doivent etre 2 entiers" >&2
	exit 2
fi
