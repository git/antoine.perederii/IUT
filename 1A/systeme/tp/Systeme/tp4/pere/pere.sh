#!/bin/bash
#dit que tout est en bash

SEC=5
#affiche le PID puis le temps auquel il dors et qd se reveile demande un code de retour au fils
echo "(le père): J'ai pour PID:$$"
./fils.sh &
echo "(le père): Je vais dormir $SEC s."
sleep $SEC
echo "(le père): Je me réveille et attend le code de retour de mon fils..."
wait $!
echo "(le père): Le code de retour de mon fils est: $?"

