# TP 4 2/2 - Processus

Ce TP donnera lieu à la remise d'un compte-rendu écrit exclusivement
avec l'éditeur VI (On pratique!).

##  – Enchainement conditionné de processus

Le but est d'utiliser l'enchainement conditionné de processus vu en
cours pour indiquer si une chaine de caractère est présente dans un
fichier. L'enchainement de commande devra afficher « *good* » si la
chaine est présente « *bad* » sinon. En outre, l'enchainement devra
renvoyer le code retour 0 si la chaîne est trouvée, 1 sinon.
        grep text tp4.txt
        echo $?
        grep tect tp4.txt
        echo $?
        
* La commande `grep` retourne 0 si la chaine est trouvée
        dans le fichier, 1 sinon ; réalisez une exécution conditionnée
        qui affiche « *good* » dans le cas où la chaîne est trouvée dans
        le fichier (sans afficher la ligne contenant la chaîne à l'écran
        – pour cela, pensez aux redirections ou à l'option du `grep` qui
        va bien).
                grep text tp4.txt >/dev/null && echo good ou 
                grep text tp4.txt -q && echo good q comme quiet silenieux

* Complétez maintenant avec une nouvelle expression
        conditionnée qui affichera « *bad* » dans le cas où la chaîne ne
        serait pas présente dans le fichier.
                grep text tp4.txt -q && echo good || echo bad
                echo $?
                grep tet tp4.txt -q && echo good || echo bad
                echo $?


* Complétez enfin la commande précédente en ajoutant les
        codes retours comme demandés ... attention : la commande exit
        termine le shell courant ... il faut donc créer un sous-shell
        sans quoi *boom*.
                grep text tp4.txt -q && echo good || (echo bad; exit 1)
                echo $?
                grep tet tp4.txt -q && echo good || (echo bad; exit 1)
                echo $?
 

* Vérifiez vos codes retour en affichant la valeur
        retournée dans chaque cas. Votre compte rendu doit contenir les
        commandes passées et valeurs affichées.
                grep text tp4.txt -q && (echo good; exit 0) || (echo bad; exit 1)
                echo $?
                grep tet tp4.txt -q && (echo good; exit 0) || (echo bad; exit 1)
                echo $?

##  – Utilisation des commandes ps, pstree, top


### Top 

La commande *top* permet de consulter l'état des principaux paramètres de système.
        top
  * De combien de mémoire dispose votre PC ?
        16 000MB
  * Combien de cette mémoire est libre ?
        12 840MB
  * Combien de mémoire est occupée par des données de cache ?
        1 469MB
  * Depuis combien de temps votre machine est-elle démarrée ?
        10h11
  * Répondre à ces mêmes questions en vous connectant sur le serveur londres par `ssh ` (note : helens pour les RT).
        top   permet de mettre les infos 
        swap : permet de prendre une partition de disque pour plus de mémoire vive et sert aussi au demarrage à chaud, permet de demarrer plus rapidement la machine car la memoire vive se stock dans ce swap pour demarrer plus vite en mise en veille

### `ps`

La commande « *ps* » permet de consulter les informations relatives aux processus

* Affichez l'arbre des processus à l'aide de  `pstree ` puis de ps en  utilisant les options «  `axjf ` »
                pstree 
                pstree -p ou -u
                ps axjf
* Recherchez le processus correspondant à votre shell courant (pour cela,  recherchez le père du processus «  `ps ` » lui-même. Quel est son PID ?  Vérifiez en recherchant le PID trouvé.
        ps -l
        ps -lf
        ps -elf
        ps -elf | bash
        ps -elf | wc -l
        ps -elf | grep anper
* Indiquez la taille mémoire (SZ) de ce processus et le temps depuis lequel il tourne (etime) (regardez du coté de l'option  `-eo `)
        ps -eo ppid,pid,cmd,size,etime affiche que ce qu'on lui indique

##  – Manipulation de l'ordonnanceur

* Vous trouverez dans le manuel de *ps* la correspondance entre les codes et l'état des processus. Quels sont les processus running au moment de l'exécution de la commande *ps* ?
        Voici les différentes valeurs que les indicateurs de sortie s, stat et state
       (en-tête « STAT » ou « S ») afficheront pour décrire l'état d'un processus :

               D    en sommeil non interruptible (normalement entrées et sorties) ;
               I    fil inactif du noyau ;
               R    s'exécutant ou pouvant s'exécuter (dans la file d'exécution) ;
               S    en sommeil interruptible (en attente d'un événement pour finir) ;
               T    arrêté par le signal de contrôle de la tâche ;
               t    arrêté par le débogueur lors du traçage ;
               W    pagination (non valable depuis le noyau 2.6.xx) ;
               X    tué (ne devrait jamais être vu) ;
               Z    processus zombie (<defunct>), terminé mais pas détruit par son parent.

       Pour les formats BSD et quand le mot-clé stat est utilisé, les caractères
       supplémentaires suivants peuvent être affichés :

               <    haute priorité (non poli pour les autres utilisateurs) ;
               N    basse priorité (poli pour les autres utilisateurs) ;
               L    avec ses pages verrouillées en mémoire (pour temps réel et entrées et
                    sorties personnalisées) ;
               s    meneur de session ;
               l    possède plusieurs processus légers (« multi-thread », utilisant
                    CLONE_THREAD comme NPTL pthreads le fait) ;
               +    dans le groupe de processus au premier plan.
 

* Recherchez les processus en attente d'évènements – si vous reconnaissez certains processus, imaginez ce qu'ils peuvent
        attendre ...
                le bash par exemple qui attends notre action pour s'éxécuter
 

* Lancez un processus «*xeyes*» dans un second terminal,
        identifiez son PID. Nous allons maintenant lui envoyez des
        signaux.
                xeyes
                ps -aux (tous au formats utilisateurs et formats BSD) ou -elf(toutes les progs sans utilisateurs et BSD)
* Envoyez vers le processus  `xeyes ` le signal SIGSTOP – constatez ce qu'il se passe dans le terminal utilisé pour lancer  `xeyes ` et ce qu'il se passe avec le programme lui-même. Vérifiez le statut du processus retourné par la commande  `ps `.
                kill -SIGSTOP 47698 (n° de PID) arrete le processus
                ps -elf  == T
* Envoyez maintenant le signal SIGCONT – constatez ce qu'il se passe dans le terminal utilisé pour lancer  `xeyes ` et ce qu'il se passe avec le programme lui-même. Quel est le nouveau status retourné par ps ?
                kill -SIGCONT 47698 remet en route le processus et si il était en premier plan il passe en arrière plan
                ps -aux  == S
* Vous avez constaté que le processus est passé en arrière-plan (vous pouvez de nouveau utiliser le terminal utilisé pour lancé  `xeyes `). Dans ce terminal, tapez la commande «  `fg ` » qui permet de passer le dernier programme au premier plan. Que se passe-t-il dans le terminal ? quel est le nouvel état du processus ?
                fg xeyes   il passe en premier plan
* Envoyez maintenant le signal SIGQUIT. Ensuite, relancez le programme  `xeyes ` puis appuyez sur les touches  `CTRL + Z` (`CTRL` et en même temps `Z`).
Vérifiez que cela correspond à l'émission d'un signal SIGSTOP. Repassez le programme au premier plan puis envoyez un `CTRL+C`.
                kill -SIGQUIT 47698
                CTRL + Z = Stoppe le processus
                fg xeyes relance et met en premier plan
                CTRL C = arret




## Filiation des processus

1. Sauvez les scripts-shells [pere.sh](pere.sh) et [fils.sh](fils.sh)  dans un de vos répertoires.
                ./pere.sh pour l'excuter
2. Décrivez ce que réalisent les scripts-shells `pere.sh` et `fils.sh` (une phrase pour chacun).
                pere : affiche le PID puis le temps auquel il dors et qd se reveile demande un code de retour au fils
                fils : affiche le PID puis le temps auquel il dors et dit la valeur qu'il renvoie au pere
3. Mettez les droits d'exécution sur ces deux scripts.
                chmod 755 *.sh
4. Lancez le processus *pere* et déterminez à l'aide de la commande `ps` l'état des processus créés. Indice : un `ps axjf` peut vous donner un bon affichage dans ce cas.
                ./pere.sh
                ps -aux ou axjf | grep pere

5. Le processus fils devient *orphelin* lorsque le père meurt sans attendre la fin de son fils. Quel processus adopte le processus fils si vous tuez (émission du signal
`SIGKILL`) le processus père ?
                le processus lider de session
6. Si vous stoppez le père (émission du signal SIGSTOP) :
        kill -SIGSTOP
  - Dans quel état passe le fils à sa terminaison ?
                en defunct dysfonctionnement Z pour zombie
  - Le père récupère-t-il le code de retour de son fils lorsque vous le relancez (émission du signal SIGCONT) ?
                kill -SIGCONT
                Oui 

7. Si vous tuez le fils, que constatez-vous en ce qui concerne le code de retour récupéré par le père ?
        kill -SIGKILL 45422 (fils)
        il affiche le message d'erreur du code d'erreur du processus arreter
