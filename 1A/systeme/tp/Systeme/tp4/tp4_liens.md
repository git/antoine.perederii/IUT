# TP 4 1/2 - Liens

Ce TP donnera lieu à la remise d'un compte-rendu écrit exclusivement
avec l'éditeur VI (On pratique!).

## Gameshell

Progressez vaillamment jusqu'à la question 32 !

##  – Encore un peu de grep, cut, et head ou tail

La commande *ip addr* permet d'obtenir l'adresse IP de l'ordinateur (Pas
l'interface *lo*, l'autre !), sous une forme 192.168.127.X ou
192.168.128.X par exemple. Cette adresse permettant à l'ordinateur de
communiquer sur un réseau. Écrivez une commande permettant d'extraire
cette information de cette commande.


## Création de liens symboliques


* En ligne de commande, créez les répertoires et fichiers suivants:

```
systeme
└── tp4
    └── question1
        └── tp4.txt
```

* Créez un lien symbolique en utilisant la commande « *ln*
        » et le manuel. Ce lien devra pointer sur le fichier *tp4.txt*
        et être créé dans le répertoire *tp4* où il s'appellera
        *tp4lien.txt*. Observez le résultat à l'aide de la commande «
        *ls -l* » exécutée depuis le répertoire *tp4*. Vous est-il
        possible d'ouvrir le lien symbolique avec *cat* par exemple ?
            ln -s question1/tp4.txt tp4lien.txt
            ls -l

* Renommez le fichier *tp4.txt* en *tp4.old*. Observez le
        résultat à l'aide de la commande « *ls -l* » exécutée depuis le
        répertoire *tp4*. Vous est-il possible d'ouvrir le lien
        symbolique avec *cat* par exemple ? Pourquoi ?
            mv question1/tp4.txt question1/tp4.old
            cat tp4lien.txt
            ls -l 

* Renommez le fichier *tp4.old* en *tp4.txt*. Vous est-il possible
d'ouvrir le lien symbolique avec *cat* par exemple ? Pourquoi ?
Supprimez maintenant le lien symbolique. Vous est-il possible d'ouvrir
le fichier *tp4.txt* ? Pourquoi ?
            mv question1/tp4.old question1/tp4.txt
            cat tp4lien.txt
            ls -l
            tree
