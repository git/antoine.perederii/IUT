#!/bin/bash

doc() {
       cat << EOF >&2
Affiche le nombre de processus de chaque utilisateur possédant au moins
1 processus dans le système.

usage : \'basename $0\' \[ -h \]

    -h : affiche cette aide.
EOF
#affiche (cat) tout ce qui est entre cat et EOF (End Of File == CTRL + D)
}

usage() {
    doc
    exit 1
}


# 1. Compte le nombre de processus de l'utilisateur dont le login est passé en argument.
# 2. $1 : le login.
# 3. stdout : le nombre de processus de l'utilisateur.
processOneUser() {
    # Les remarques indiquées dans la fonction processAllUsers
    # s'appliquent également à cette fonction
    ps h -eo user | grep $1 | wc -l
}

# 1. Affiche le tableau (non trié) du nombre de processus par utilisateur.
# 2. Pas d'argument.
# 3. stdout : le tableau.
processAllUsers() {
    # traitement
    # ----------
    # début
    # L<-liste des utilisateurs possédant un processus en mémoire
    # pour i élément de L faire
    #       n=nombre de processus de l'utilisateur i dans le système
    #       afficher "i n"
    # fin pour
    # fin

    # Remarques :
    # 1. pour obtenir la liste des propriétaire des processus, utilisez la
    #    commande : ps h -eo user
    # 2. Pour éliminer les doublons d'une liste filtrez la avec la commande :
    #    sort -u.
    local L
    local i
    local n
    # ou local L i n=0
    L=$(ps h -eo user | sort -u)
    for i in $L; do 
	    local n=$(processOneUser $i)
	    echo "$i $n"
    done
    
    
}

toHTML() {
	local L R
	echo "<html>"
	echo "<body>"
	echo "<table border=\"1\">"
	while read L R;do # ou read L
	       echo "<tr><td>$L</td><td>$R</td></tr>" 
	       #echo $L | sed -re 's/^⁽[^ ]+) (.*)$/<tr><td>\1<\/td><td>\2<\/td><\/tr>/'
       done
       echo "</table>"
       echo "</body>"
       echo "</html>"
}       

# Début du traitement des arguments.
while [ $# -ne 0 ]; do
    case $1 in
        -h|--help)
            doc
            exit 0
            ;;
        *) usage
            ;;
    esac
    shift
done
# Fin du traitement des arguments.

processAllUsers | sort -t" " -k2 -r -n | toHTML > user.html
