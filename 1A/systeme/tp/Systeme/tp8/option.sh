#!/bin/bash

L="%d/%M/"  # l'annee en langue française par default
A="%y"  # anne sur 2 chiffre par default
t=""  # pas d'heure par defaut

while [ $# -ne 0 ]; do
	case "$1" in 
		'-lang')
			l=$2
			shift 2
			if [ "$2" = 0 ]; then
				echo "manque la langue" >&2
					exit 1
			fi
			if [ "$l" = "fr" ] ; then 
				L="%d/%m/"
			elif [ "$l" = "en" ] ; then
				L="%m/%d/"
			else
				echo "la langue $l n'est pas gérée" >&2
				exit 2
			fi
			;;
		'-fullYear')
			shift
			# echo "vous avez choisi l'anne sur 4 chiffres"
			A="%Y"
			;;
		'-addTime')
			shift
			# echo "vous avez choisi d'afficher aussi l'heure"
			T="-%H:%M"
			;;
		*)
			echo "Argument $1 inconnu" >&2 && exit 2 
			;;
	esac
done

date +$L$A$T
