#!/bin/bash

while [ $# -ne 0 ]; do
	case "$1" in 
		'-user')
			l=$2
			shift 2
			if [ "$2" = 0 ]; then
				echo "manque le nom d'utilisateur" >&2
					exit 1
			fi
			echo $(ps -U)
			echo $(find -name "$l")
			if [ $? -ne 0 ] ; then
				echo "aucun utilisateur avec ce nom n'a été trouvé !!"
			fi
			;;
		*)
			echo "Argument $1 inconnu" >&2 && exit 2 
			;;
	esac
done
