#!/bin/bash
i=1
a=1

while read L; do
	if [ $i -eq 0 ]; then
		echo -n "$a "
		echo "$L" | egrep [0-9]{2} | tr a-z A-Z
	fi
	a=$(($a + 1))
	i=$(($a%2))
done
