#!/bin/bash

for f in $(ls $1); do
	if [ $(echo "$?") -eq "127" ]; then
		shift 
	elif [ $# -eq 0 ]; then
	      	echo "Aucun arg ou aucun fichier dans ce repertoire" >&2
	fi
	if [ ls -l --time-style +%Y-%m-%d $1/$f | tr -s " " | cut -d " " -f 8 -e ]; then
		cp $f $(ls -l --time-style +%Y-%m-%d $1/$f | tr -s " " | cut -d " " -f 8)
	else 
		mkdir $(ls -l --time-style +%Y-%m-%d $1/$f | tr -s " " | cut -d " " -f 8)
		cp $f $(ls -l --time-style +%Y-%m-%d $1/$f | tr -s " " | cut -d " " -f 8)
	fi
done
