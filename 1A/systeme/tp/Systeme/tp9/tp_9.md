# Petits exercices 

## num

Écrivez le script `num` acceptant une liste d'arguments et qui n'affiche que ceux qui sont numériques et les affiche dans l'ordre inverse.

## alpha

Écrivez le script `alpha` lisant son entrée standard et

 - n'affichant que les lignes paires 
 - qui en plus contiennent un nombre d'au moins deux chiffres 
 - en les passant en majuscules (via la commande `tr a-z A-Z`) 
 - en les faisant précéder de leur numéro (d'origine) 

## Classement de photos

Les vacances sont finies \! et j'ai plein de photos dans mon répertoire *images*. 
J'aimerais que vous m'écriviez un script qui les classe automatiquement en fonction de leur date de dernière modification dansdes répertoires de noms `AAAA-MM-JJ` (année-mois-jour: exemple : 2010-10-29). 
Le répertoire devra être créé si nécessaire avant que la photo soit déplacée à l'intérieur.

Algorithme :

* __debut__ 
* __pour__ tous les **fichiers** F du répertoire passé en argument __faire__ 
  * DIR ← date sous la forme *AAAA-MM-JJ* du fichier F 
  * __si__ le répertoire de DIR n'existe pas __alors__ 
    * créer le répertoire DIR 
  * __fin si__ 
  * déplacer F dans DIR 
* __fin pour__ 
* __fin__ 


- Écrivez le script shell associé.

<note>

Indications :

* la commande  `ls -l --time-style +%Y-%m-%d ` permet de lister les fichiers avec leur date sous la forme `AAAA-MM-DD`.  
* Pour tester avec des photos fictives, vous pouvez utiliser la commande `touch -m -t 200910130000 img-0001.jpg`. Cette commande crée un fichier vide dont la date de dernière modification est fixée au jour du 13 octobre(10) 2009. 

</note>

## Un petit jeu d'adresse

Ce petit jeu d'adresse consiste à présenter à l'utilisateur, pendant un
temps limité (1.5 secondes), une fenêtre `xeyes` placée aléatoirement à
l'écran. Si l'utilisateur réussi à "tuer" la fenêtre en la fermant à
l'aide de la croix avant le temps imparti il gagne 1 point. Sinon c'est
le programme qui "tue" la fenêtre avant de présenter la suivante. Une
partie se joue en 10 coups.

Écrivez le script shell associé à ce jeu.

<note>

Indications :

* Une fenêtre peut généralement être placée à une position donnée de l'écran via l'option -geometry. Par exemple la commande  `xeyes -geometry +500+300` lancera  `xeyes` qui placera sa fenêtre à la position (500,300). Remarque : certains gestionnaires de fenêtres, comme `Gnome 3` ou `i3` par exemple, ne tiennent pas toujours compte du placement demandé ! 
* La commande `sleep` est l'une des rares commandes standard acceptant en argument un nombre flottant exprimant le temps en seconde. Ainsi sleep 0.2 fera dormir le processus 200 millisecondes. 
* La commande `shuf -i 0-100 -n 1` permet d'obtenir un entier aléatoire dans l'intervalle [0-100]. 

</note>

- Modifiez votre programme pour qu'il réponde au synopsis suivant :

 `prog [-n num] [-t delai]`

où

 *  `num` est le nombre de coups de la partie (10 par défaut). 
 *  `delai` est le temps imparti pour tuer la fenêtre. 