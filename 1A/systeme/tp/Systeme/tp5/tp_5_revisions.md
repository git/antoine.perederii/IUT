# TP 5 2/2 - Révisions

## Informations sur le matériel

* En utilisant le contenu du fichier `/proc/cpuinfo`, afficher le nom du processeur de votre machine.
	cat cpuinfo | grep 'model name' | uniq | cut -d ':' -f2
* Les cœurs logiques sont numérotés de *0* à *n-1* (*n* étant le nombre de cœurs logiques). Affichez le nombre de cœurs de votre processeur.
	cat cpuinfo | grep 'cpu core' | uniq
* En utilisant les deux questions précédentes, écrivez une commande composée qui stocke dans le fichier `config.txt` le nom du processeur et son nombre de cœurs.
	cat cpuinfo | grep 'model name' | uniq | cut -d ':' -f2 | tr -d ' ' >> ~/IUT/systeme/tp/tp5/config.txt; cat cpuinfo | grep 'cpu core' | uniq | tr -s '\t' ' ' >> ~/IUT/systeme/tp/tp5/config.txt

## Liste des machines

Le fichier `~frdelobe/serveurs.ssh` contient le résultat d'un scan réseau des machines sur le réseau des serveurs pédagogiques. Trouvez une commande qui, à partir de ce fichier, crée un second fichier `serveurs.txt` qui contient la liste des adresses IP des serveurs (par exemple `192.168.130.181`), triée par ordre alphabétique.
	cat serveurs.ssh | grep ' 192' | uniq | cut -d ' ' -f5 (ou f5,6)>> ~/IUT/systeme/tp/tp5/serveurstest.txt ; cat serveurs.ssh | grep '(192' | uniq | cut -d ' ' -f6 | tr -d '(' | tr -d ')' >> ~/IUT/systeme/tp/tp5/serveurstest.txt ; sort < ~/IUT/systeme/tp/tp5/serveurstest.txt > ~/IUT/systeme/tp/tp5/serveurs.txt ;  uniq ~/IUT/systeme/tp/tp5/serveurs.txt >> ~/IUT/systeme/tp/tp5/serveurs.txt

	ps -elf | tr -s ' ' | cut -d ' ' -f5 | grep '1' | sort | uniq -c | head -n 1 | tr -s " " | cut -d ' ' -f2

	ps -eo ppid affiche juste la ligne du ppid
	ps -eo ppid | tr -s ' ' | cut -d ' ' -f2 | wc -l

	ps -elf | sort -nk5    == trie par la colonne PPID