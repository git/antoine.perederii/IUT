---
title: SAE 1.03, Installation d'un poste pour le développement
---

# Aperçu du sujet

Cette SAÉ permet d'expérimenter les missions d'installation de poste de travail sous Linux, ainsi que de se familiariser avec la maintenance. Vous pourrez utiliser une machine secondaire (fortement conseillé mais optionnel), un deuxième disque (ou clé USB) ou un dual boot.

Vous devrez:

  * Comprendre en quoi une phase d'installation impacte le système.
  * Configurer la machine pour que le support matériel soit effectif.

Pour configurer finement la machine, vous devrez non seulement apprendre certaines commandes d'administration mais aussi les *comprendre* (savoir comment elles affectent le système).
  
La liste des savoirs est fournie et est divisée en deux parties correspondant à chacune des périodes. L'ensemble des procédures acquises doit pouvoir être automatisé et ne doit donc pas dépendre d'un outil graphique.

Ce travail est fait en *monôme*, mais vous pouvez bien sûr vous entraider. Pour réussir, il est important de commencer tôt: vous serez sans doute bloqués par moment et n'aurez pas le temps de demander de l'aide si vous vous y prenez au dernier moment.

**Note** : Ce sujet est susceptible d'évoluer de façon mineure au cours du semestre.

## Conseils pratiques pour le matériel.

* Linux tourne même sur des petites configurations, genre le vieux PC que vous avez au grenier, ou que tonton veut jeter. Utiliser une machine dédiée n'est pas obligatoire mais peut vous tranquilliser.
* Si vous prenez une installation sur clé USB, faite gaffe à la vitesse de celle ci (pas seulement la catégorie USB).

## Livrables

* Une installation de Linux avec les outils adaptés au développement.
* Un·e étudiant·e capable de faire une installation, de comprendre ce qu'elle implique pour le système, et de configurer la machine (voir les cas d'usage plus bas).

## Conseils si vous voulez foirer la SAÉ

* Attendre le dernier moment pour commencer. Vous serez sûr d'être coincé sans avoir le temps de vous renseigner.
* Ne pas prendre de notes au cours de l'installation. Des notes risqueraient de vous aider à vous souvenir de ce que fait telle ou telle commande.
* Bâcler: faire sans comprendre, au plus vite. Vous serez sûr de ne pas avoir compris, et donc incapable de fournir le second livrable.
* Variante du point précédent : prendre une distribution qui vous masque tout. Ça marche nickel quand il n'y a pas de soucis mais c'est l'horreur dès qu'un problème arrive. En vrai, nous vous conseillons les distribution Debian ou ArchLinux. 
* Copier/coller sans comprendre ce que ça fait: non seulement, vous n'allez pas comprendre, mais en plus, vous avez une chance de foirer votre système! Une valeur sûre!
* Ne jamais expérimenter, vérifier vos hypothèses, analyser. 
* Quand ça foire, réinstaller sans chercher à comprendre. 
* Surtout, ne pas lire les messages d'erreurs, ils risqueraient de vous mettre sur la voie. Ne jamais consulter les logs.
* Utiliser des outils graphiques pour mal-comprenant qui vous mâchent le travail: c'est une solution qui vous interdit toute automatisation d'une tâche fastidieuse.

# Détail des compétences attendues


### Période 1

  * /etc/apt/sources.list : contient la liste des dépôts pour la récupération des packages. Ces dépôts peuvent être des CD-ROM, un emplacement sur votre disque dur, des URL.

  * /etc/apt/apt.conf : fichier de configuration de apt-get

  * /var/cache/apt/archives : contient les packages qui ont été downloadés pour être installés.
  
  * fichier  fstab  contient  des  informations decrivant les systemes de fichiers que le systeme peut monter
  
  * journalctl  ==> permet d'avoir les logs
    * -p err  ==> pour afficher les erreurs
  * logger ''  ==> creer un log
    * journalctl -f pour l'afficher
   
  * lsmod affiche de façon agréable le contenu du fichier /proc/modules, montrant ainsi quels sont les modules actuellement chargés.
  * Dans l'ordre :
    * le nom du module,
    * la quantité de mémoire qu'il utilise,
    * le nombre des autres modules qui utilisent celui-ci et leurs noms.


  * Pour afficher des informations détaillées sur un paquet :
    * apt show paquet

  * Pour afficher les dépendances d'un paquet :
    * apt-cache depends paquet

  * Pour afficher des informations détaillées des versions disponibles pour un paquet et les paquets ayant des dépendances inverses sur lui :
    * apt-cache showpkg paquet

  * Installer un paquet.
    * /etc/apt/sources.list ou .list.d ==> permet de creer des fic sources.list dans .d pour les non-free ex: google chrome
    * apt edit-sources  ==> edite les sources
  		* Pour installer le paquet foo et toutes ses dépendances :
		    * apt install {nomdupaquet}
        * apt install aptitude  ==> Permet de gerer les paquets mais de façon graphique

  * Supprimer un paquet.
  	* Pour supprimer le paquet foo et ses fichiers de configuration de votre système :
		  * apt purge foo
      * apt remove

  * Lister les paquets installés.
  	* Pour lister tous les paquets, lancez :
		  * apt list --installed
      * dpkg -l  (= --list)

  * Mettre à jour la liste des paquets disponibles.
  		* Pour mettre à jour la liste des paquets connus par votre système, vous pouvez lancer :
  		  * apt update

  * Mettre à jour les paquets installés.
  		* Pour mettre à jour tous les paquets de votre système, sans installer de paquets supplémentaires ou en supprimer :
		    * apt upgrade

  * Chercher quels paquets (installés ou non) contiennent un fichier donné.
      * dpkg -S (nom du fichier)
      * sudo apt-file search (nom du fichier)  ==> cherche un nom de fic
      * apt-file list (nom du fichier)
      * apt-cache search  ==> cherche si bien installé sur le systeme et permet hors ligne

  * Connaître l'espace disque utilisé dans chaque répertoire.
      * du - ah /home/UCA/  a = tous et h = taille
        * -sh taille totale du repertoire*
      * df -h

  * Créer un utilisateur.
      * useradd [option] id  == -D default 
        * -m creer le repertoire home s'il n'existe pas
        * -M ne le creer pas
      * adduser tata  ==> plus simple

  * passwd id    permet de reinitialiser le mot de passe

  * Détruire un utilisateur (pas physiquement, hein!).
      * userdel -r id
      * deluser id  ==> plus 

  * Modifie l'utilisateur
      * chown [OPTION]... [PROPRIETAIRE][:[GROUPE]] FICHIER...
      * chown [OPTION]... --reference=FICHIER-R FICHIER...  modifie l'utilisateur et/ou le groupe proprietaire de chacun des fichiers indiques -c affiche les messages d'erreur et -f silentieux

  * Partitionner un disque.
      * blkid  ==> liste et dit si c'est swap et tt
      * fdisk -l  liste les partitions
      * fdisk [chemin du disk] [option]
        * p == liste ttes les partitions sur le disque
        * d == supprimme toutes les partitions
        * n == nouvelle partition puis p comme primaire ou d comme deernier secteur
        * w == Sauv et ferme

  * Créer un système de fichier dans une partition.
      * sudo mkfs -t [nomdutypedefichier] [emplacementdudisque]
      * mkfs.{systeme fic} {chemin /dev/XXX}  ex : mkfs.ext4 /dev/XXX

  * Créer une partition d'échange et l'utiliser dans le système.
    * sudo mkfs -t swap emplacement disque
    * sudo fallocate -l 1G /mnt/1GB.swap
    * swapon, swapoff - Activer et desactiver les peripheriques et fichiers pour la pagination et l'echange sur disque
    * mkswap cree une zone d'echange Linux sur un peripherique ou dans un fichier

* Lister les partitions montées et trouver leur espace libre.
  * df   ==> df [OPTION]... [FICHIER]... indique l'espace disque utilise et disponible sur le systeme de fichiers contenant chaque fichier donne en parametre
  * lsblk pour ttes les partitions  permet d'obtenir la liste et les caractéristiques des disques et de leurs partitions

* Monter, démonter, et accéder à une clé usb
  * Monter
    * sudo mount /dev/dusk/by-label/{nom_clef} /mnt/cle
  * Demonter
    * sudo umount /mnt/cle 
  * Pour acceder
    * cd mnt/cle
. 
* lsblk puis sudo mount emplacement du disque emplacement de la clef

    * Lister le matériel présent sur la machine.
      * sudo lshw * Savoir identifier sur quels bus ils sont connectés.
      * lspci
      * lspci -b  ==> Savoir identifier sur quels bus ils sont connéctés

* Vérifier quel module gère un périphérique.
  * lspci -nnv

  * Lister les modules chargés en mémoire.
    * lsmod
  
### Période 2

  * Créer un système de fichiers utilisable aussi depuis Windows. Il doit être monté automatiquement au démarrage du système.
    * mkfs.NTFS /dev/XXX

  * Monter une partition.
    * sudo mount /dev/dusk/by-label/{nom_clef} /mnt/cle

  * Démonter une partition.
    * sudo umount /mnt/cle

  * Pouvoir se connecter à une autre machine avec SSH.
    * ssh ip

  * Pouvoir se connecter sur votre machine avec SSH (testez au moins sur la même machine avec deux utilisateurs différents).
    * apt install openssh-server

  * Installer un dépôt alternatif à ceux de la distribution (par exemple [debian backports](https://backports.debian.org/Instructions/)).
    * vi /etc/apt/sources.list

  * Trouver à quel paquet appartient un fichier.
    * dpkg -s [nompaquet]   ==> recherche un fic dans un paquet installé

  * Lister les fichiers contenus dans un paquet.
    * dkpg -l [nompaquet]  

  * Trouver les fichiers dépassant une certaine taille.
    * find -msize +/-5G

  * Installer un logiciel propriétaire en restreignant ses droits (flatpak).
    * apt install flatpak
    * flatpak remote-add –if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

  * Services Systemd
    * Savoir démarrer/stopper un service.
      * systemclt start {nomFic}

    * Savoir en vérifier l'état.
      * systemclt stop {nomFic}
      * systemectl statue {nomFic}

    * Savoir afficher les messages d'erreur.

    * Passer le man en FR
      * apt install man
      * apt install manpages-fr
      * vi /etc/locale.gel  decomment fr_FR UTF8 UTF8  ==> sauv
      * saisir dans le terminal locale-gen  ==> si fr_FR...  c bon

    * Installer i3 (gnome)
      * apt install i3 (gnome)

    * Services
      * service --status-all
      * serice {nomStats} start stop
      

### Trouver des logiciels (Période 1 et 2)

Cherchez des outils pour répondre aux besoins suivants.



| Besoin                                                                             | Nombre d'outils différents demandés | Logiciels proposés                      |
| ---------------------------------------------------------------------------------- | :----------------------------------:| ----------------------------------------|
| Avoir des Gestionnaires de bureau/fenêtre (dont un qui vous plaise)                | 4                                   |        gnome                            |
| Avoir deux utilisateurs: vous et "stagiaire"                                       |                                     |adduser anperederi; adduser stagiaire    |
| Compiler un programme C                                                            | 2                                   |apt install gcc;                         |
| Regarder les pages de man de la libC                                               | 2                                   |man libC                                 |
| Lancer un `make`                                                                   |                                     |apt install make; make ...               |
| Éditer du code source                                                              | 4                                   |apt install vim; vi ...;apt install nano;|
| Déboguer du code                                                                   | 2                                   |      apt install sublime-text;          |
| Naviguer sur le Web                                                                | 2                                   |apt search firefox;install firefox-esr-..|
| Naviguer sur le Web avec la version de firefox dans backports                        1                                    sudo add-apt-repository                  |
|                                                                                                                           ppa:costamagnagianfranco/firefox         |
|                                                                                                                           sudo apt update                          |
| Éditer une image matricielle (png...)                                              | 2                                   |apt install gimp inkscape                |
| Éditer une image vectorielle (svg)                                                 | 1                                   |apt install inkscape                     |
| (Dé)Compresser les formats `targz` et `7z` et `rar`                                | 1                                   |apt install 7z; 7z {dest} {d'ou ca vient}|


## Évaluation

### Fin de la première période

* Vous devez déjà disposer de votre installation de Linux.

#### Écrit 

* Les questions porteront sur l'installation.

### Fin de la seconde période

* Évaluation sur machine à l'IUT pour vérifier que vous êtes capable d'administrer une machine. 
  * Nous fournissons une machine partiellement configurée
  * Vous avez un certain nombre de tâches à réaliser (Voir les listes *période 1* et *période 2*)
  
