#!/bin/bash

SEC=20

echo "(le père): J'ai pour PID:$$"
./fils.sh &
echo "(le père): Je vais dormir $SEC s."
sleep $SEC
echo "(le père): Je me réveille et attend le code de retour de mon fils..."
wait $!
echo "(le père): Le code de retour de mon fils est: $?"

