# TP 5 2/2 - Révisions

## Informations sur le matériel

* En utilisant le contenu du fichier `/proc/cpuinfo`, afficher le nom du processeur de votre machine.
* Les cœurs logiques sont numérotés de *0* à *n-1* (*n* étant le nombre de cœurs logiques). Affichez le nombre de cœurs de votre processeur.
* En utilisant les deux questions précédentes, écrivez une commande composée qui stocke dans le fichier `config.txt` le nom du processeur et son nombre de cœurs.

## Liste des machines

Le fichier `~frdelobe/serveurs.ssh` contient le résultat d'un scan réseau des machines sur le réseau des serveurs pédagogiques. Trouvez une commande qui, à partir de ce fichier, crée un second fichier `serveurs.txt` qui contient la liste des adresses IP des serveurs (par exemple `192.168.130.181`), triée par ordre alphabétique.
