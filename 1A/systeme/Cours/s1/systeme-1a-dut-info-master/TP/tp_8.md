# Système utilisateur – TP n°8

Pas de compte rendu dédié aux TP sur les scripts shell. Vos programmes feront foi \!

## Variables et environnement d'exécution

 - Vérifiez si la variable  `PATH` contient  le répertoire courant. Sinon ajoutez-le. 
 - Créez et affectez une variable de "jour" (pour aujourd'hui). 
 - Créez, affectez et exportez une variable "mois". 
 - Créez un fichier  `variables.sh ` avec `vi` contenant les lignes suivantes: 
   - L’indication que le shell utilisé doit être `bash`:  `#!/bin/bash`   
   - La création et l’affectation d’une variable `annee` ; 
   - L’affichage des contenus des trois variables :  `jour`, `mois` et  `annee`. 
 - Fixez les droits `rw-` pour le fichier `variables.sh`. 

Exécutez les commandes suivantes et **commentez le résultat obtenu**.

 - Exécutez la commande `sh variables.sh`. 
 - Exécutez la commande `echo $mois, $annee`. 
 - Exécutez la commande `source variables.sh`. 
 - Exécutez la commande `echo $jour, $mois, $annee`. 
 - Supprimez la variable `annee`.
 - Ajoutez le droit d’exécution pour le fichier `variables.sh`. 
 - Exécutez la commande `variables.sh`. 
 - Exécutez la commande `echo  $jour, $mois, $annee`. 

## La commande option

Écrire un programme capable d'afficher la date et l'heure suivant différents formats en fonction des options qui lui sont passées :

L'option `-lang lang` permet de choisir un affichage *jj mm aa* lorsqu'elle vaut *fr* » ou un affichage *mm jj aa* si elle vaut *en*.

L'option `-fullYear` force l'affichage de l'année sur 4 chiffres au lieu de deux.

L'option `-addTime` permet d'ajouter l'heure à l'affichage.

La commande retournera un message d'erreur et un code retour de 1 si la syntaxe des options est incorrecte.
0 sinon.

### Note

Pour éviter que votre programme ne soit une "usine à gaz", décomposez le en trois parties :

- Associer une variable à la valeur par défaut de chacune des options.
- Traiter les arguments et modifier la valeur des variables.
- Effectuer le traitement en fonction de la valeur des variables.

Remarque : il est possible de préciser un format d'affichage à la commande `date` (exemple : `date +%M:%S`)

## La commande `waitfor`

Le programme `waitfor -user ident` doit, en scrutant chaque seconde la liste des processus, détecter la connexion de l'utilisateur dont le login est indiqué par l'option `-user`.
Une fois détectée, le programme se termine avec un message d'information.

## Exercice sur les fonctions

Il est courant d'avoir à compter des d'objets d'un certain type (processus, fichiers, ...) et de produire un tableau synthétique de la forme : `objetDeType1 nombre1` `objetDeType2 nombre2` `...` Un exemple classique est de compter le nombre de processus pour chaque utilisateur du système. Exemple : 

```
$ ./compteProcessus 
avahi 2 
clamav 1 
daemon 2 
dansguardian 11
davalan 87
Debian-exim 1 
haldaemon 2 
messagebus 1
ntp 1
proxy 2
root 136
statd 1
uml-net 1 
www-data 1 
```

Le résultat ci-dessus a été produit par le script suivant qu'il vous faudra compléter.

```bash
#!/bin/bash

doc() {
       cat << EOF >&2 
Affiche le nombre de processus de chaque utilisateur possédant au moins
1 processus dans le système.

usage : \`basename $0\` \[ -h \]

    -h : affiche cette aide.
EOF
}

usage() {
    doc 
    exit 1 
}


# 1. Compte le nombre de processus de l'utilisateur dont le login est passé en argument.
# 2. $1 : le login.
# 3. stdout : le nombre de processus de l'utilisateur.
processOneUser() {
    # Les remarques indiquées dans la fonction processAllUsers 
    # s'appliquent également à cette fonction 

    ### A COMPLETER (1 ligne) ### 
}

# 1. Affiche le tableau (non trié) du nombre de processus par utilisateur.
# 2. Pas d'argument.
# 3. stdout : le tableau.
processAllUsers() {
    # traitement
    # ----------
    # début
    # L<-liste des utilisateurs possédant un processus en mémoire
    # pour i élément de L faire
    #       n=nombre de processus de l'utilisateur i dans le système
    #       afficher "i n"
    # fin pour
    # fin

    # Remarques :
    # 1. pour obtenir la liste des propriétaire des processus, utilisez la
    #    commande : ps h -eo user
    # 2. Pour éliminer les doublons d'une liste filtrez la avec la commande :
    #    sort -u.

    ### A COMPLETER (5 lignes) ###
}

# Début du traitement des arguments.
while [ $# -ne 0 ]; do
    case $1 in
        -h|--help)
            doc
            exit 0
            ;;
        *) usage
            ;;
    esac
    shift
done
# Fin du traitement des arguments.

processAllUsers
```

* Complétez le script.
* Ajoutez un filtre permettant de trier les lignes affichées par ordre décroissant de nombre de processus (étudiez les différentes options de `sort`). Le résultat attendu sera de la forme : 

```
root 137
davalan 90
dansguardian 11
proxy 2
haldaemon 2 
daemon 2
avahi 2
www-data 1
uml-net 1
statd 1
sshd 1
ntp 1 
messagebus 1
Debian-exim 1
clamav 1 
```

* Ajouter une fonction `toHtml` à vos scripts pour modifier la sortie afin de produire un document HTML contenant une table. Voici ci-dessous un exemple de résultat attendu se basant sur l'exemple 1.1 : 

```html
<html>
<body>
<table border="1">
<tr><td>root</td><td>136</td></tr>
<tr><td>davalan</td><td>91</td></tr>
<tr><td>dansguardian</td><td>11</td></tr>
<tr><td>proxy</td><td>2</td></tr>
<tr><td>haldaemon</td><td>2</td></tr>
<tr><td>daemon</td><td>2</td></tr>
<tr><td>avahi</td><td>2</td></tr>
<tr><td>www-data</td><td>1</td></tr>
<tr><td>uml-net</td><td>1</td></tr>
<tr><td>statd</td><td>1</td></tr>
<tr><td>ntp</td><td>1</td></tr>
<tr><td>messagebus</td><td>1</td></tr>
<tr><td>Debian-exim</td><td>1</td></tr>
<tr><td>clamav</td><td>1</td></tr>
</table>
</body>
</html>
```

 En enregistrant la sortie dans un fichier d'extension .html et
en ouvrant ce fichier dans un navigateur vous devrez observer le
résultat suivant :

<html>
<body>
<table border="1">
<tr><td>root</td><td>136</td></tr>
<tr><td>davalan</td><td>91</td></tr>
<tr><td>dansguardian</td><td>11</td></tr>
<tr><td>proxy</td><td>2</td></tr>
<tr><td>haldaemon</td><td>2</td></tr>
<tr><td>daemon</td><td>2</td></tr>
<tr><td>avahi</td><td>2</td></tr>
<tr><td>www-data</td><td>1</td></tr>
<tr><td>uml-net</td><td>1</td></tr>
<tr><td>statd</td><td>1</td></tr>
<tr><td>ntp</td><td>1</td></tr>
<tr><td>messagebus</td><td>1</td></tr>
<tr><td>Debian-exim</td><td>1</td></tr>
<tr><td>clamav</td><td>1</td></tr>
</table>
</body>

Si vous avez besoin d'informations complémentaires sur html, vous pouvez allez voir [w3schools.com](https://www.w3schools.com/tags/default.asp).
