# TP 2

## GameShell

* À faire : questions 20 à 26

```
cd ~/GameShell
./start.sh
```
## Fini de jouer :)

### Caractères génériques (Joker)

Pour que les ensembles fonctionnent correctement, vous devez commencer par positionner la variable `LANG` à la valeur `C` ; tapez : `export LANG=C`

Dans le répertoire : `/usr/bin` ; en utilisant seulement la commande
`ls -d` et les jokers vus en cours

* Recherchez les fichiers dont le nom contient le caractère `-` (tiret `-`, pas underscore `_`).
* Idem à la question précédente sauf que le caractère `-` doit être placé en 3ème position.
* Recherchez les fichiers dont le nom commence par une lettre majuscule et se termine par un chiffre.

#### Redirection 1

` echo "ligne1 imprimée à l écran"`  

* Rappelez de nouveau cette commande, mais en ajoutant une redirection vers un fichier. Que se passe-t-il a l'écran ? Listez le répertoire, que constatez-vous ? Affichez le contenu du fichier, que constatez-vous ?

* Ajoutez une seconde ligne à la précédente en utilisant une redirection avec concaténation (ajout à la suite). Quelle commande avez-vous tapé ? Affichez le contenu du fichier.

#### Redirection 2

* Lancez la commande *sort*. Cette commande attend des données sur l'entrée standard. Tapez la suite de lettres "c a d f b e" en tapant `Entrée` à chaque lettre (une lettre par ligne). Puis appuyer sur `Ctrl + D` pour terminer la saisie. Vous constatez que la commande affiche votre suite de lettres, remise dans l'ordre alphabétique.

* Créez un fichier contenant une suite de mots (1 par ligne) (environ 10 lignes). Utilisez ensuite une redirection de l'entrée standard pour que la commande sort trie les données de ce fichier. Le résultat sera affiché à l'écran.

* Faite en sorte qu'un fichier `fichier.trie` contienne la liste des mots triés (en utilisant une redirection supplémentaire par rapport à la question précédente).

#### Redirection 3 

* Lancez la commande suivante : `ls /root/`. Lancez ensuite cette même commande en redirigeant la sortie standard vers un fichier. Regardez la différence par rapport au premier lancement de la commande et éditez le fichier. Que constatez-vous ? Expliquez pourquoi la redirection n'a pas fonctionné ? Que faut-il faire pour que le message soit redirigé dans le fichier ?

### Manipulation de texte

Une des principales utilisations du shell est la manipulation de fichier texte. Ces opérations mettent en œuvre les commandes `cat`, `cut`, `grep`, `sort` associé à la redirection de flux ` | ` permettant de passer le résultat d'une commande comme entrée de la commande suivante.

Nous allons créer la liste des étudiants à partir des
        résultats de la commande `ls -l ~/../`

* La commande *tr* peut être utilisée pour supprimer les espaces multiples, cherchez dans le manuel comment faire et faite ensuite en sorte d'afficher le résultat de la commande `ls` précédente sans espaces multiples (en utilisant un ` | `).

* La commande `cut` permet de sélectionner un champ dans une ligne, voyez l'option `-f` couplée à l'option `-d` pour ne sélectionner dans le résultat de la commande précédente que le champ `name` (nom des répertoires).

* En utilisant de nouveau la commande `cut`, conservez uniquement les caractères 3 à 10.

* En utilisant la commande *sort*, triez le résultat par ordre alphabétique.

* Écrire ce résultat dans un fichier `listeEtudiants.txt`


### Utilisateurs d'une machine

  * La commande `last` affiche les dernières connexions sur la machine. La première colonne contient en général le login de l'utilisateur qui se connecte (à quelques exceptions près). Ajoutez une commande à votre `last` pour n'afficher que les logins.
  * Triez la liste affichée par ordre alphabétique.
  * La même personne apparaît plusieurs fois dans la liste. Ajouter une commande pour supprimer les doublons (indice : que chaque nom soit unique...).
  * Redirigez la liste affichée dans le fichier `/tmp/usuels.txt`.

### Trouver tous les UID étudiants 

Comme vous l'avez peut-être constaté, le fichier `/etc/passwd` ne contient pas directement les utilisateurs sur les machines de TP. C'est parce que ce rôle est dévolu à l'outil `ldap`. La commande suivante permet d'interroger `ldap` (via `getent`) pour récupérer les utilisateurs manquants.

```shell
cd /home/UCA ; for login in *; do getent passwd "$login"; done 
```

* En utilisant la commande précédente, créez un fichier dans `~/logins.txt` qui contient tous les logins des étudiants ainsi que leur UID (premier champs numérique).
* Combien d'étudiants sont référencés (il y a encore les anciens) ?

### Trouver les groupes des étudiants

La commande `id un_login` permet d'avoir la liste des groupes auquel appartient l'utilisateur `un_login`. En partant de ça, on peut avoir la liste des groupes de *tous* les utilisateurs par la commande :

```shell
cd /home/UCA ; for i in *; do id "$i"; done | sed -re 's/utilisateurs du domaine/utilisateurs_du_domaine/g'
```

Pour ceux qui se poseraient la question, le `sed` permet de remplacer le groupe *utilisateurs du domaine* qui contient à tort des espaces pour vous simplifier la suite du traitement.

* En partant de la commande précédente, affichez uniquement les groupes.
* En partant de la commande précédente, débarrassez-vous de l'entête inutile `groupe=` (un autre `cut` peut faire le travail)
* La commande `tr` permet aussi d'effectuer des substitutions sur son entrée standard. Appliquez un `tr ',' "\n"` pour avoir un groupe par ligne.
* À l'aide de l'option `-c` de `uniq` et de la commande précédente, affichez le nombre d'étudiants par groupe. Stockez ça dans un fichier.
* Faites un `id` et identifiez le groupe de première année.
* À l'aide du fichier précédent, et du groupe de première année, affichez le nombre d'étudiants de première année.
* Défi (avec 8 tubes!): comment obtenir la liste suivante (Tous les groupes, sauf ceux de 1 utilisateur, triés par nombre) :

```
2 1069952(g_etu_uca_abc)
2 1072401(g_etu_uca_gxc)
2 1072526(g_etu_uca_ira)
2 1073817(uca_ca-iut63-bc-bde)
2 1081655(g_etu_uca_cdc)
2 1145255(uca_etu_2020_cec_2l114c)
2 1145430(uca_etu_2020_scl_pl131a)
2 1149909(etudgim_vpn_cz)
2 1156011(data_olso)
2 1160717(uca_share-iut-gt-vieuniversitaire)
2 1170182(uca_etu_2021_cec_2l114c)
2 1170198(uca_etu_2021_scl_pl146a)
2 1170252(uca_etu_2021_iqa_1l225a)
2 1170278(uca_etu_2021_itc_zu059c)
3 1034117(srcimpr)
3 1034118(etudiants-licproinfo)
3 1034119(etudiants-licprosrc)
3 1034120(etudiants-lepuy)
3 1034131(etudiants-chimie)
3 1034132(etudiants-info1a)
3 1034133(etudiants-info2a)
3 1034134(etudiants-src1a)
3 1034135(etudiants-src2a)
3 1064250(g_etu_uca_ite)
3 1170253(uca_etu_2021_eka_3m245a)
6 1084543(g_etu_uca_scl)
6 1145293(uca_etu_2020_iqa_zm148a)
6 1145333(uca_etu_2020_iqa_zl325a)
7 1064122(g_rol_uca)
7 1064123(g_roles)
7 1066621(g_etu_uca_cec)
7 1145285(uca_etu_2020_iqa_zl225a)
7 1170168(uca_etu_2021_iqa_1l325a)
8 1064511(g_etu_uca_eka)
8 1170184(uca_etu_2021_iqa_zy101a)
10 1042687(t1aqc1)
10 1044070(dpt_info)
10 1172332(uca_etu_2021_iqa_zy201a)
12 1145637(uca_etu_2020_iqa_zy201a)
14 1145323(uca_etu_2020_iqa_zy101a)
25 1170730(uca_etu_2021_itc_1lp40c)
26 1145469(uca_etu_2020_itc_1lp41c)
26 1170736(uca_etu_2021_itc_1lp41c)
27 1145383(uca_etu_2020_itc_1lp40c)
61 1072402(g_etu_uca_iqa)
104 1170219(uca_etu_2021_itc_zt212c)
107 1145362(uca_etu_2020_itc_zt212c)
133 1145329(uca_etu_2020_itc_zt112c)
138 1170141(uca_etu_2021_itc_zt136c)
432 1155448(guac_users_info)
441 1064124(g_etu_uca_itc)
506 1064125(g_etu_uca)
506 1064126(g_etudiants)
508 10001(BUILTIN\users)
508 1010513(utilisa)
```
