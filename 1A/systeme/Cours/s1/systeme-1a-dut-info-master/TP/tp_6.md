# TP 6 - Scripts

Pas de compte rendu dédié aux TP sur les scripts shell. Vos programmes feront foi!

<note tip>

Quelques règles simples pour éviter les erreurs de syntaxe :

* Jamais d'espace autour d'une affectation (exemple : `a=3`)
* des espaces partout ailleurs (exemple `if test $a = 4; then...`
* `$` devant le nom d'une variable pour accéder à son contenu.

Pour observer les commandes exécutées, passez en mode trace en plaçant la commande `set -x` en début de programme.

</note>

## La commande `existe`

Écrire un programme qui :

- Demande à l'utilisateur de saisir un nom de fichier
- Vérifie que le fichier existe et qu'il est accessible en lecture
- Affiche alors le contenu du fichier à l'aide de la commande `cat` 
- Affiche sinon un message d'erreur si le fichier n'existe pas

## La commande `min1`

Écrire un script-shell qui demande à l'utilisateur de saisir une suite d'entiers (un par ligne) et qui affichera le plus petit des entiers saisis.

## La commande `min2 entier [entier ...]`

### Étape 1

Écrire un script-shell recevant en arguments (sur la ligne de commande, en paramètre) une suite d'entier et affichant le plus petit d'entre eux.

### Étape 2

Lorsque votre programme fonctionne ajouter le nécessaire pour gérer le cas d'un nombre d'argument nul. Dans ce cas le programme affichera un message d'erreur et retournera un code retour de 1 (il retournera 0 dans les autres cas).

## La commande `arg1`

Écrire un programme qui affiche son nom (seulement la dernière partie après le dernier '/'), puis chacun de ses arguments, séparés par des caractères '\#'. Attention, le dernier argument ne doit pas être suivi d'un '\#'.

Indications :

* l'option `-n` (nonewline) de la commande `echo` peut-être utile.
* la commande `basename` également`

Exemple :

```
./arg1 toto titi tata

arg1 toto#titi#tata
```

## La commande `dir [répertoire]`

Écrivez un script-shell qui affiche les fichiers du répertoire passé en argument (ou du répertoire courant si aucun répertoire n'est passé en argument) en encadrant de crochets le nom des fichiers correspondant à des répertoires et faisant suivre d'une étoile le nom des fichiers exécutables (autres que les répertoires).

Indication : utilisez la commande `test` pour déterminer le type du fichier.
