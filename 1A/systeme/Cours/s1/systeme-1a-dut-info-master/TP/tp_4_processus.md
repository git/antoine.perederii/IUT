# TP 4 2/2 - Processus

Ce TP donnera lieu à la remise d'un compte-rendu écrit exclusivement
avec l'éditeur VI (On pratique!).

##  – Enchainement conditionné de processus

Le but est d'utiliser l'enchainement conditionné de processus vu en
cours pour indiquer si une chaine de caractère est présente dans un
fichier. L'enchainement de commande devra afficher « *good* » si la
chaine est présente « *bad* » sinon. En outre, l'enchainement devra
renvoyer le code retour 0 si la chaîne est trouvée, 1 sinon.

* La commande `grep` retourne 0 si la chaine est trouvée
        dans le fichier, 1 sinon ; réalisez une exécution conditionnée
        qui affiche « *good* » dans le cas où la chaîne est trouvée dans
        le fichier (sans afficher la ligne contenant la chaîne à l'écran
        – pour cela, pensez aux redirections ou à l'option du `grep` qui
        va bien).

* Complétez maintenant avec une nouvelle expression
        conditionnée qui affichera « *bad* » dans le cas où la chaîne ne
        serait pas présente dans le fichier.
 
* Complétez enfin la commande précédente en ajoutant les
        codes retours comme demandés ... attention : la commande exit
        termine le shell courant ... il faut donc créer un sous-shell
        sans quoi *boom*.

 

* Vérifiez vos codes retour en affichant la valeur
        retournée dans chaque cas. Votre compte rendu doit contenir les
        commandes passées et valeurs affichées.

##  – Utilisation des commandes ps, pstree, top


### Top 

La commande *top* permet de consulter l'état des principaux paramètres de système.

  * De combien de mémoire dispose votre PC ?
  * Combien de cette mémoire est libre ?
  * Combien de mémoire est occupée par des données de cache ?
  * Depuis combien de temps votre machine est-elle démarrée ?
  * Répondre à ces mêmes questions en vous connectant sur le serveur londres par `ssh ` (note : helens pour les RT).


### `ps`

La commande « *ps* » permet de consulter les informations relatives aux processus

* Affichez l'arbre des processus à l'aide de  `pstree ` puis de ps en  utilisant les options «  `axjf ` »
* Recherchez le processus correspondant à votre shell courant (pour cela,  recherchez le père du processus «  `ps ` » lui-même. Quel est son PID ?  Vérifiez en recherchant le PID trouvé.
* Indiquez la taille mémoire (SZ) de ce processus et le temps depuis lequel il tourne (etime) (regardez du coté de l'option  `-eo `)

##  – Manipulation de l'ordonnanceur

* Vous trouverez dans le manuel de *ps* la correspondance entre les codes et l'état des processus. Quels sont les processus running au moment de l'exécution de la commande *ps* ?

 

* Recherchez les processus en attente d'évènements – si vous reconnaissez certains processus, imaginez ce qu'ils peuvent
        attendre ...

 

* Lancez un processus «*xeyes*» dans un second terminal,
        identifiez son PID. Nous allons maintenant lui envoyez des
        signaux.

* Envoyez vers le processus  `xeyes ` le signal SIGSTOP – constatez ce qu'il se passe dans le terminal utilisé pour lancer  `xeyes ` et ce qu'il se passe avec le programme lui-même. Vérifiez le statut du processus retourné par la commande  `ps `.
* Envoyez maintenant le signal SIGCONT – constatez ce qu'il se passe dans le terminal utilisé pour lancer  `xeyes ` et ce qu'il se passe avec le programme lui-même. Quel est le nouveau status retourné par ps ?
* Vous avez constaté que le processus est passé en arrière-plan (vous pouvez de nouveau utiliser le terminal utilisé pour lancé  `xeyes `). Dans ce terminal, tapez la commande «  `fg ` » qui permet de passer le dernier programme au premier plan. Que se passe-t-il dans le terminal ? quel est le nouvel état du processus ?
* Envoyez maintenant le signal SIGQUIT. Ensuite, relancez le programme  `xeyes ` puis appuyez sur les touches  `CTRL + Z` (`CTRL` et en même temps `Z`).
Vérifiez que cela correspond à l'émission d'un signal SIGSTOP. Repassez le programme au premier plan puis envoyez un `CTRL+C`.



## Filiation des processus

1. Sauvez les scripts-shells [pere.sh](pere.sh) et [fils.sh](fils.sh)  dans un de vos répertoires.
2. Décrivez ce que réalisent les scripts-shells `pere.sh` et `fils.sh` (une phrase pour chacun).
3. Mettez les droits d'exécution sur ces deux scripts.
4. Lancez le processus *pere* et déterminez à l'aide de la commande `ps` l'état des processus créés. Indice : un `ps axjf` peut vous donner un bon affichage dans ce cas.
5. Le processus fils devient *orphelin* lorsque le père meurt sans attendre la fin de son fils. Quel processus adopte le processus fils si vous tuez (émission du signal
`SIGKILL`) le processus père ?
6. Si vous stoppez le père (émission du signal SIGSTOP) :
   
  - Dans quel état passe le fils à sa terminaison ?
  - Le père récupère-t-il le code de retour de son fils lorsque vous le relancez (émission du signal SIGCONT) ?

7. Si vous tuez le fils, que constatez-vous en ce qui concerne le code de retour récupéré par le père ?
