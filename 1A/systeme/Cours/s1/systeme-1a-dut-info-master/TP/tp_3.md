# TP 3 - `find` et droits d'accès

## Droits d'accès

Ce TP donnera lieu à la remise d'un compte rendu écrit exclusivement avec l'éditeur `vim`.

* En ligne de commande, créez les répertoires suivants :

```
systeme
└── tp2
    └── question1
        ├── open
        ├── prive
        └── public
```        


* À l'aide de l'éditeur `vim`, créez un fichier (*fichier1.txt*) contenant un texte de votre choix sur 1 à 2 lignes dans le répertoire `~/systeme/tp3/question1/public


* À qui appartient le fichier que vous venez de créer, quels sont les droits associés à ce fichier ? (Ne faites pas un copier/coller des droits dans le compte rendu, indiquez pour chaque ensemble (user, group, other), quelles opérations il peut effectuer).

* Copiez ce fichier dans les répertoires *prive* et *open*, puis modifiez les droits sur le fichier de sorte que :
  * Dans le répertoire *public*, tout le monde peut lire le fichier, mais seul l'utilisateur peut le modifier.
  * Dans le répertoire *prive*, seul l'utilisateur peut lire le fichier.
  * Dans le répertoire *open*, l'utilisateur et le groupe peuvent lire et modifier le fichier.

* Quelles sont les commandes passées pour cela ?
* Faites vérifier les propriétés de vos répertoires par un de vos voisins. Note : `cd ~frdelobe` permet d'aller dans le compte de l'utilisateur `frdelobe`. 
* Vérifiez s'il est possible de changer les droits d'un des fichiers lorsque l'on n'est pas l'utilisateur propriétaire du fichier.

* Changez les droits du répertoire *public* en lui ajoutant le droit d'écriture au groupe. (attention, c'est bien le droit du répertoire qu'il faut modifier et non celui du fichier). Ne changez pas les autres droits initialement existants. Quelle commande utilisez-vous ?

* Positionnez de nouveau les droits initiaux sur le répertoire *public* : `rwxr-xr-x

* Le répertoire privé peut être listé et par défaut les fichiers sont créés comme étant lisible par tous, il faut donc modifier les droits de ce répertoire pour qu'il soit réellement privé.

* Changez les droits pour que le propriétaire soit le seul à pouvoir « lire » le répertoire. Ne modifiez pas les autres droits. Avec le compte de votre binôme, allez dans le répertoire et essayez la commande `ls`, que constatez-vous ? Pensez-vous que la sécurité recherchée soit acquise ?

* Une fois dans le répertoire, essayez de créer un nouveau fichier (`vim fichier2.txt`). Que constatez-vous ? Quel est le rôle du droit `w` sur un répertoire ? Expliquez alors ce que vous avez constaté.

* Changez maintenant les droits sur le répertoire *prive* pour ajouter de nouveau la lecture et supprimer l'exécution à tous. Essayez de nouveau de rentrer `s` répertoire ou d'éditer un fichier. Quel est le rôle du droit `x` sur un répertoire ?

* Configurez le répertoire *open* pour qu'il permette de créer/supprimer des fichiers en plus de la consultation de ceux présents. Testez à l'aide du second utilisateur.




## La commande find

### Échauffement

 * Affichez la liste des répertoires de votre arborescence (de votre //home directory//).
 * Faites de même pour le répertoire `/usr/share/man` (sans changer de répertoire courant).
 * Affichez la liste des fichiers simples de votre arborescence dont la taille est **supérieure** à 1MiB (consultez la page de man pour les unités et pensez à rajouter éventuellement un `+` pour dire //au moins// cette taille).

### Dernières modifications

 * À l'aide de la commande `find`, recherchez tous les fichiers simples de votre home directory qui ont été modifiés durant les 3 dernières heures.

### Images

 * A l'aide de la commande `find` recherchez tous les fichiers `*.png` présents sur `/usr` (et ses sous répertoires). 
 * Vous pourrez rediriger la sortie erreur sur la poubelle (`/dev/null`) pour éclaircir l'affichage.
 * Pour chaque image, en utilisant l'option `-exec`, exécutez la commande `file` sur chacun des fichiers trouvés. Celle-ci inscrit en fin de ligne la taille de l'image 
 * Faites en sorte, alors de ne sélectionner que les images de taille 128 x 128.
 * Modifiez la commande précédente pour que seul le nom du fichier (avec son chemin devant) soit affiché.

### Ménage sur votre compte

 * Faites la liste de tous les fichiers de votre arborescence dont le nom est de la forme `*.o`.
 * Supprimez tous ces fichiers automatiquement avec le prédicat `-exec` (aidez vous du `man`). Indice: la commande `rm -i` permet une suppression avec confirmation par l'utilisateur pour éviter les maladresses d'un débutant.

### Limitation de la profondeur du parcours

 * Affichez la liste des répertoires de l'arborescence sous `/usr`.
 * Ajoutez `-maxdepth 1` à votre `find`. Qu'est ce que ça fait? Essayez `-maxdepth 0`, et `-maxdepth 2`...
 * Ajoutez une commande (en plus du `find`) pour compter le nombre de ces répertoires (en comptant simplement les lignes).

## Permissions, cut et compagnie

<note warning>

Afin de limiter la charge de notre serveur de disque qui a du mal à gérer tous les TP en parallèle, nous nous contenterons de parcourir deux niveaux de profondeur dans le compte de chaque utilisateur.

</note>

 * Parcourez l'arborescence située sous `/home/UCA` et affichez la liste de tous les répertoires.

<note important>

 * Votre commande ne doit pas parcourir toute l'arborescence et doit donc comporter un `-maxdepth 4`
 * Comme les droits de //lecture// sont systématiquement ôtés des homedir des utilisateurs, on ne peut en lister directement le contenu. Néanmoins, il suffit d'indiquer à `find` les sous-répertoires que l'on va visiter (donc plus besoin pour lui de //lire// le contenu du répertoire. Exemple:

`find /home/UCA/*/{algo,html,systeme,système,1A,2A}`

</note>

 * Complétez la question précédente pour n'afficher que la liste des répertoires accessibles en écriture pour tous.
 * Ajoutez le prédicat `-ls` à la fin de votre `find`. Que fait ce prédicat ?
 * En utilisant `cut`, affichez uniquement le nom du propriétaire.
 * Trouvez une façon de supprimer les doublons de la liste affichée.
 * Sauvegardez cette liste dans le fichier `imprudents.txt` à la racine de votre compte. 

## Lister les fichiers d'une arborescence du plus vieux au plus récent

Comme l'indique le titre de cet exercice, nous souhaitons trier les fichiers d'une arborescence du plus ancien au plus récent. En d'autres termes pouvoir effectuer sur une arborescence ce qu'un `ls -lRt` fait dans un répertoire. Le résultat attendu sera de la forme `"date chemin"`. Exemple sur `/etc` (de nombreuses lignes intermédiaires ont été supprimées) :

```
2020-12-29 04:20:38.000000000 +0100 /etc/security/faillock.conf
2020-12-29 04:20:38.000000000 +0100 /etc/security/namespace.conf
2020-12-29 04:20:38.000000000 +0100 /etc/security/pam_env.conf
2020-12-29 16:46:26.000000000 +0100 /etc/default/hwclock
2020-12-29 16:46:26.000000000 +0100 /etc/init.d/hwclock.sh
2020-12-30 11:07:27.772150347 +0100 /etc/apparmor.d/local/usr.bin.redshift
```

*Indications* : une seule commande composée de tubes suffit (pas de fichier temporaire) sachant :

 * que `find` est capable de parcourir les fichiers d'une arborescence en appliquant une commande sur chacun d'eux.
 * que la commande `stat` permet, grâce à son option `-c`, de définir les informations à afficher sur un fichier (regardez par exemple les séquences de format `%Y`, `%y` et `%n`) 
 * que `sort` possède une option permettant de trier suivant la valeur numérique d'une colonne.
 * et que `cut` permet d'éliminer des colonnes.
