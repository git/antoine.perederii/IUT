# TP 1

Prenez le temps de découvrir votre environnement. Vous êtes connecté sous un gestionnaire de fenêtre qui s'appelle `Gnome`. Sous Linux, les gestionnaires de fenêtre sont légion. Quelques-uns sont disponibles à l'IUT. Nous vous conseillons d'essayer `Xfce`, et pour ceux qui connaissent déjà Linux, `i3`.

 On peut changer le gestionnaire de fenêtre sur l'écran de connexion, **après** avoir entré son *login*.

## Gameshell

Gameshell est un petit jeu pour apprendre les bases de la ligne de commande.

### Installation

Cette étape est à faire une seule fois.
GameShell est un projet libre. Nous allons le récupérer avec `git`.

Ouvrez un *terminal* et copiez-collez la ligne suivante (Vous pouvez simplement la sélectionner avec la souris puis la coller avec le bouton du milieu dans votre terminal).

```
git clone https://github.com/phyver/GameShell.git
```

### Lancement du jeu

```
cd GameShell
./start.sh
```

### Application directe du cours

Faites les missions de 1 à 11.

### Il faut réfléchir un peu plus

Avancez jusqu'à la mission 20. N'hésitez pas à ouvrir un autre terminal pour faire du `man` sur les commandes présentées.

## Un peu plus classique comme sujet

### 1 - utilisation du mode graphique

Quelques précisions utiles :

-   Linux est le nom du noyau, GNU est le projet qui est à l'origine de
    l'essentiel de l'écosystème de logiciels autour du noyau (dont le
    shell et les commandes de base). Les contributeurs à l'ensemble du
    parc logiciel sont nombreux.
-   Il existe souvent beaucoup de logiciels candidats à la réalisation
    d'une même tâche. Ainsi, on peut choisir son gestionnaire de bureau
    parmi de nombreux candidats (*Gnome3* par défaut sous *debian
    stable*, mais aussi *kde*, *xfce*, *enlightenment*, *icewm*, *fvwm*,
    *fluxbox*...) qui n'auront pas tous la même apparence. De même, la
    plupart des outils sont disponibles sous plusieurs formes.
    Choisissez ce qu'il vous plait.

GNU/Linux offre évidemment des outils permettant de naviguer dans
l'arborescence des fichiers. Selon votre environnement graphique ces
outils porteront des noms différents. Si vous utilisez l'environnement
graphique *Gnome*, il s'agit de *nautilus* pour la navigation dans les
fichiers (vous pouvez y accéder en cliquant, par exemple, sur l'icône
poste de travail) et par exemple *gedit* pour l'édition de fichiers
textes.

**A** - Dans un premier temps, naviguez dans l'arborescence des fichiers
de votre ordinateur et repérez qu'il n'y a pas de notion de partition
apparente comme sous Windows (Exemple; "C:", "D:"). Jetez un œil
particulier au répertoire `/home` qui contient les répertoires des
utilisateurs, dont votre répertoire personnel dans `/home/UCA`.

**B** - Dans un second temps, à l'aide de l'interface graphique vous
allez créer l'arborescence suivante dans votre répertoire home.

-   `systeme`
    -   `tp1`
        -   `question1`
        -   `question2`

**C** - Utilisez ensuite un éditeur de texte de votre choix (`geany` par
exemple) pour créer 2 fichiers textes que vous sauvegarderez dans le
répertoire `question1` précédemment créé. Le premier fichier
(`fichier1.txt`) va contenir le texte suivant:

`UNIX : Système d'exploitation multi-utilisateur et multitâche mis au point en 1969 par Ken Thompson et Dennis Ritchie, au sein des laboratoires Bell, entité possédée à l'époque par AT&T et sa filiale Western Electric.`

`Ce système a subi de nombreuses évolutions, dont la principale a été son écriture en langage C dans la première moitié des années 1970, ce qui a facilité son portage sur tout type de processeur. La plupart des grands constructeurs ont ensuite offert leur propre version d'Unix: AIX chez IBM, HP-UX chez Hewlett-Packard, Solaris chez Sun, etc.`

`Il a inspiré le développement du système libre Linux. Le nom Unix serait un clin d'oeil à son prédécesseur, MULTICS (Multiplexed Information and Computing Service), développé à la fin des années 1960 au Massachusetts Institute of Technology.`

`Celui-ci mettait en oeuvre pour la première fois le modèle de système d'exploitation en couches, encore en vigueur aujourd'hui. Pour afficher ses origines, Unix s'est d'abord écrit UNICS, pour UNiplexed Information and Computing Service.`

Dans le second fichier (`fichier2.txt`) vous copierez 10 fois le texte
ci-dessus. Pour ce faire, essayez la méthode usuelle (CTRL+C / CTRL+V)
mais essayez aussi la méthode avec uniquement la souris, utilisable
partout sous Linux : sélectionnez à la souris votre texte à copier
(juste en surlignant le texte), puis placez votre curseur sur l'endroit
où coller et pressez sur le bouton central de la souris (ou la molette)
pour réaliser le « coller ».

### 2- utilisation du mode texte pour la gestion des fichiers

Ouvrez un terminal (que nous appellerons parfois abusivement shell ).
Par défaut le terminal s'ouvre en vous positionnant dans votre
répertoire personnel, appelé « *home directory* ». Vous pourrez à tout
moment vous y replacer en tapant la commande « `cd` » sans argument.

A - Situez votre répertoire personnel en essayant la commande « `pwd` »

B - En utilisant la commande « `cd` », rendez vous dans les répertoires
suivants et visualisez leur contenu : `/` puis `/etc` puis `/etc/init.d`
puis `/var/log` et enfin revenez dans votre home. Réalisez ces
opérations en utilisant des chemins absolus et relatifs.

C - Revenez dans votre home puis entrez dans le répertoire `systeme`
précédemment créé

\<note tip> La commande `cd -` permet de revenir dans le dernier
répertoire quitté.\</note>

D - Essayez la commande « `ls` » qui permet de lister le contenu du
répertoire, vous devriez voir apparaître le répertoire `tp1` à l'écran.
Affichez ensuite l'arborescence de ce répertoire en utilisant la
commande « `tree` » ; Une autre façon d'effectuer cela est d'utiliser la
commande ls en ajoutant l'option `-R` « `ls -R` »

E - Déplacez vous dans le répertoire question1 et affichez le contenu
des deux fichiers que vous avez créé en question n°1 ; pour le premier,
la commande « `cat` » sera suffisante (mettez le nom de fichier en
argument); pour le second, essayez avec les commandes « `cat`, `more` ou
`less` » (tapez 'q' pour quitter `less`).

F - D'autres commandes permettent un affichage sélectif d'un fichier,
regardez les pages de manuel (commande man commande) de « `tail` » et «
`head` ». A quoi servent ces commandes ? Affichez les 8 premières lignes
et les 8 dernières lignes du second fichier.

G - Déplacez vous maintenant dans le répertoire `question2` précédemment
créé. La commande « `mkdir` » permet de créer un répertoire. Vous allez
créer 6 sous répertoires appelés `reponseA`, `reponseB`, `...`,
`reponseF` ; pour ce faire, vous utiliserez le rappel des commandes
précédentes en utilisant les flèches haut et bas du clavier.

H – Créez un répertoire supplémentaire : `reponseH/solution1` avec la
commande suivante : « `mkdir [option à trouver] reponseH/solution1` »
Pour ce faire vous devez trouver l'option permettant de créer à la fois
le répertoire `reponseH` et le répertoire `solution1` sans quoi la
commande vous renverra une erreur. Utilisez le manuel pour cela.

I – Cherchez la commande permettant de supprimer un répertoire vide à
l'aide de la commande "`apropos`". Utilisez les mots clefs `directory`
ou `répertoire` selon la langue de vos pages de manuel. Supprimez le
répertoire `reponseA`.

J – La commande « `cp` » permet de copier des fichiers ; les pages de
manuel vous indiqueront comment l'utiliser. A l'aide de cette commande,
copiez les fichiers `fichier1.txt` et `fichier2.txt` dans le répertoire
`reponseB` ; vérifiez que votre arborescence est bonne à l'aide de la
commande (non standard) « `tree` » et éventuellement du navigateur
graphique.

K – Copiez maintenant le fichier `fichier1.txt` dans le répertoire
`reponseB`, en le nommant `.fichier1.txt` (attention il y a un « `.` »
devant le nom du fichier. Listez ensuite les fichiers du répertoire
`responseB` ; voyez vous votre fichier ? essayez avec l'option « `-a` »
qui permet d'afficher les fichiers cachés. Qu'en concluez vous ?

L – La commande « `rm` » permet de supprimer des fichiers, essayez la
pour supprimer le `fichier1.txt` qui se trouve dans le répertoire
`question1`.

M – Cette commande ne permet pas de supprimer un répertoire : faites
l'essai en essayant de supprimer le répertoire `reponseH` ; cherchez
dans le manuel quelle option doit alors être utilisée pour effacer les
sous-répertoires.

### 3 - utilisation des alias

Un alias est une commande stockée dans une variable. L'usage principal
est de personnaliser ses commandes en ajoutant des options ou en
définissant ses propres commandes. Les alias sont gérés directement par
le shell lui même, vous trouverez donc plus de documentation dans la
page de man de *bash*.

\<note tip> Il est possible de faire des recherches dans les pages de
manuel en tapant '/unMot' (pas d'espace sauf à condition de le faire
précéder par un anti-slash '\\'), l'occurrence suivante sera atteinte en
tapant simplement « n » . Pour "remonter" d'une occurrence 'p'. Pour
revenir en début 'Home' et pour aller à la fin 'Fin'. Pour quitter le
man tapez "`q`".\</note>

A – Vous allez créer des alias de commande. Commençons par des choses
inutiles : créez un alias appelé bonjour qui exécutera la commande
suivante « `echo bonjour à toi xxxxx` » où `xxxx` est votre prénom.
Exécutez ensuite la commande `bonjour`.

B – Supprimez cet alias décidément inutile.

C – Passez en revue les options de la commande « `ls` » à partir du
manuel et créez des alias sur les options vous semblant intéressantes (3
options différentes de votre choix).

D – Ouvrez une seconde fenêtre de terminal, vous constaterez que vos
alias ne fonctionnent pas dans cette nouvelle fenêtre. Pour les rendre
permanents, vos alias doivent être exécutés à chaque lancement de
nouveau shell. Avec le navigateur graphique, éditez le fichier `.bashrc`
situé dans votre répertoire home (Si vous ne le trouvez pas, copier
/etc/skel/.bashrc dans votre \~). Ce fichier contient une liste de
commandes qui seront exécutées par chaque nouveau shell. Ajoutez en fin
de fichier vos alias, sauvegardez, ouvrez un nouveau shell et testez.

### 4 - Quelques questions en plus

A – Quelle commande permet de déplacer des fichiers ? illustrez.

B – Quelle commande permet de renommer un fichier ? illustrez.

C – Comment afficher le manuel de la commande en langage C `printf` au
lieu de celui de la commande Unix `printf` ? (regardez dans `man man` et
choisissez la bonne section)

D – Où se trouve le programme correspondant à la commande `ls` ?
(commande `which`)

E – Quel type de données contiennent les fichiers suivants ?
`/sbin/ifconfig` et `/etc/hosts` ? (commande `file`)

F – Quelle est la taille occupée par votre répertoire home et ses sous
répertoires ? (commandes « `du` » et « `quota` » )

### 5 - Personnalisation de votre environnement

La personnalisation de votre environnement utilisateur sous Unix passe
principalement par deux aspects: personnaliser son gestionnaire de
fenêtre et personnaliser son shell.

Si vous êtes responsable de votre compte (cf. la [charte
informatique](http://ent.u-clermont1.fr/static/divers/charteInformatiqueUdA.pdf)
que vous devez avoir validé), vous en êtes aussi le gestionnaire et vous
êtes en droit d'en faire un outil agréable pour travailler.

#### Personnaliser son shell

Certaines variables permettent de configurer le shell. Elles sont
définies dans les pages de manuel du shell, c'est à dire bash dans votre
cas. Etudiez les valeurs que peut prendre la variable PS1. A quoi sert
PS1 ? Essayez de modifier cette valeur dans votre shell en lui affectant
une nouvelle valeur : PS1=... Remarque : attention à ne pas mettre
d'espace avant et après le symbole « = »

-   Pour un plus joli prompt: changer son prompt:
    <http://wiki.archlinux.org/index.php/Color_Bash_Prompt>
-   Ajouter des alias (vu au dessus)

##### Fichier de configuration du shell

-   Chargé à chaque lancement du shell: `.bashrc` . Si ce fichier
    n'existe pas chez vous, copiez en une copie depuis '/etc/skel/:
    `cp /etc/skel/.bashrc ~`
-   Chargé à chaque connexion: `.bash_profile`
-   Chargé à chaque déconnexion: `.bash_logout`

##### Choisir son shell

Au passage, vous avez aussi le choix dans les shell: nous utilisons
*bash*, le shell utilisateur par défaut sous *debian*. Nous essayons
néanmoins de nous restreindre à la partie normalisée (Posix) de *bash*.
Vous pouvez aussi essayer *zsh*, un shell avec plein de fonctionnalités
supplémentaires, ou *dash*, un shell simple, Posix, destiné à
l'exécution rapide de scripts.

#### Personnaliser son gestionnaire de fenêtre

##### Paramétrer Gnome3

Gnome3 est le gestionnaire de fenêtre par défaut sous Debian. Voici
quelques liens pour vous aider à le configurer un peu plus que ce qui
est possible avec une installation de base.

-   Présentation des outils utiles:
    <http://www.mrhide.fr/index.php/post/2011/08/13/Personnaliser-Gnome-4>
-   Site officiel des extensions de Gnome3:
    <http://extensions.gnome.org/>

##### Changer de gestionnaire de fenêtre

Les goûts pour les gestionnaires de fenêtre/bureau, ça ne se discutent
pas! À vous de trouver celui qui vous plaît le plus! Voici un liste de
ce qui est disponible à l'IUT, avec plein de parti pris :

-   *gnome3* : + ergonomie novatrice, + plein de gadgets, - gros
    consommateur de ressources, - ergonomie en rupture avec nos
    habitudes, - besoin de 3d pour fonctionner.
-   *kde* : + beaucoup de fonctionnalités, + plein de gadgets, + grande
    intégration des applications entre elles, - gros consommateur de
    ressources.
-   *xfce*: + simple et efficace, + consomme peu de ressources &
    rapide, - pas le truc pour frimer (sobre).
-   *windowmaker*: + assez joli bien que simple, + consommation de
    ressources raisonnable, - évolution lente et WM vieillissant.
-   *enlightenment*: + consommation de ressources très faible, + très
    versatile, - manque d'utilitaires.

### Saines lectures linuxiennes ou libristes (et francophones... pour commencer)

-   [linuxfr](https://linuxfr.org), forum d'utilisateurs de logiciel
    libre.
-   [Linux Arverne](http://www.linuxarverne.org/), un GULL (Groupe
    d'Utilisateurs de Logiciels Libres) auvergnat (Association + mailing
    list).
-   [Lea Linux](http://lea-linux.org/), un site de présentations de
    logiciels libres avec pas mal de doc vraiment très simples.
-   [Framasoft](http://www.framasoft.net/), une association de promotion
    du libre avec pas mal de services proposés (livres en ligne,
    annuaire de logiciel, forums...)
