---
title: SAE 1.03, Installation d'un poste pour le développement
---

# Aperçu du sujet

Cette SAÉ permet d'expérimenter les missions d'installation de poste de travail sous Linux, ainsi que de se familiariser avec la maintenance. Vous pourrez utiliser une machine secondaire (fortement conseillé mais optionnel), un deuxième disque (ou clé USB) ou un dual boot.

Vous devrez:

  * Comprendre en quoi une phase d'installation impacte le système.
  * Configurer la machine pour que le support matériel soit effectif.

Pour configurer finement la machine, vous devrez non seulement apprendre certaines commandes d'administration mais aussi les *comprendre* (savoir comment elles affectent le système).
  
La liste des savoirs est fournie et est divisée en deux parties correspondant à chacune des périodes. L'ensemble des procédures acquises doit pouvoir être automatisé et ne doit donc pas dépendre d'un outil graphique.

Ce travail est fait en *monôme*, mais vous pouvez bien sûr vous entraider. Pour réussir, il est important de commencer tôt: vous serez sans doute bloqués par moment et n'aurez pas le temps de demander de l'aide si vous vous y prenez au dernier moment.

**Note** : Ce sujet est susceptible d'évoluer de façon mineure au cours du semestre.

## Conseils pratiques pour le matériel.

* Linux tourne même sur des petites configurations, genre le vieux PC que vous avez au grenier, ou que tonton veut jeter. Utiliser une machine dédiée n'est pas obligatoire mais peut vous tranquilliser.
* Si vous prenez une installation sur clé USB, faite gaffe à la vitesse de celle ci (pas seulement la catégorie USB).

## Livrables

* Une installation de Linux avec les outils adaptés au développement.
* Un·e étudiant·e capable de faire une installation, de comprendre ce qu'elle implique pour le système, et de configurer la machine (voir les cas d'usage plus bas).

## Conseils si vous voulez foirer la SAÉ

* Attendre le dernier moment pour commencer. Vous serez sûr d'être coincé sans avoir le temps de vous renseigner.
* Ne pas prendre de notes au cours de l'installation. Des notes risqueraient de vous aider à vous souvenir de ce que fait telle ou telle commande.
* Bâcler: faire sans comprendre, au plus vite. Vous serez sûr de ne pas avoir compris, et donc incapable de fournir le second livrable.
* Variante du point précédent : prendre une distribution qui vous masque tout. Ça marche nickel quand il n'y a pas de soucis mais c'est l'horreur dès qu'un problème arrive. En vrai, nous vous conseillons les distribution Debian ou ArchLinux. 
* Copier/coller sans comprendre ce que ça fait: non seulement, vous n'allez pas comprendre, mais en plus, vous avez une chance de foirer votre système! Une valeur sûre!
* Ne jamais expérimenter, vérifier vos hypothèses, analyser. 
* Quand ça foire, réinstaller sans chercher à comprendre. 
* Surtout, ne pas lire les messages d'erreurs, ils risqueraient de vous mettre sur la voie. Ne jamais consulter les logs.
* Utiliser des outils graphiques pour mal-comprenant qui vous mâchent le travail: c'est une solution qui vous interdit toute automatisation d'une tâche fastidieuse.

# Détail des compétences attendues


### Période 1

  * Installer un paquet.
  * Supprimer un paquet.
  * Lister les paquets installés.
  * Mettre à jour la liste des paquets disponibles.
  * Mettre à jour les paquets installés.
  * Chercher quels paquets (installés ou non) contiennent un fichier donné.
  * Connaître l'espace disque utilisé dans chaque répertoire.
  * Créer un utilisateur.
  * Détruire un utilisateur (pas physiquement, hein!).
  * Partitionner un disque.
  * Créer un système de fichier dans une partition.
  * Créer une partition d'échange et l'utiliser dans le système.
  * Lister les partitions montées et trouver leur espace libre.
  * Monter, démonter, et accéder à une clé usb.
    * Lister le matériel présent sur la machine.
    * Savoir identifier sur quels bus ils sont connectés.
  * Vérifier quel module gère un périphérique.
  * Lister les modules chargés en mémoire.
  
### Période 2

  * Créer un système de fichiers utilisable aussi depuis Windows. Il doit être monté automatiquement au démarrage du système.
  * Monter une partition.
  * Démonter une partition.
  * Pouvoir se connecter à une autre machine avec SSH.
  * Pouvoir se connecter sur votre machine avec SSH (testez au moins sur la même machine avec deux utilisateurs différents).
  * Installer un dépôt alternatif à ceux de la distribution (par exemple [debian backports](https://backports.debian.org/Instructions/)).
  * Trouver à quel paquet appartient un fichier.
  * Lister les fichiers contenus dans un paquet.
  * Trouver les fichiers dépassant une certaine taille.
  * Installer un logiciel propriétaire en restreignant ses droits (flatpak).
  * Services Systemd
    * Savoir démarrer/stopper un service.
    * Savoir en vérifier l'état.
    * Savoir afficher les messages d'erreur.

### Trouver des logiciels (Période 1 et 2)

Cherchez des outils pour répondre aux besoins suivants.



| Besoin                                                                             | Nombre d'outils différents demandés | Logiciels proposés |
| ---------------------------------------------------------------------------------- | :----------------------------------:| -------------------|
| Avoir des Gestionnaires de bureau/fenêtre (dont un qui vous plaise)                | 4                                   |                    |
| Avoir deux utilisateurs: vous et "stagiaire"                                       |                                     |                    |
| Compiler un programme C                                                            | 2                                   |                    |
| Regarder les pages de man de la libC                                               | 2                                   |                    |
| Lancer un `make`                                                                   |                                     |                    |
| Éditer du code source                                                              | 4                                   |                    |
| Déboguer du code                                                                   | 2                                   |                    |
| Naviguer sur le Web                                                                | 2                                   |                    |
| Naviguer sur le Web avec la version de firefox dans backports                      | 1                                   |                    |
| Éditer une image matricielle (png...)                                              | 2                                   |                    |
| Éditer une image vectorielle (svg)                                                 | 1                                   |                    |
| (Dé)Compresser les formats `targz` et `7z` et `rar`                                | 1                                   |                    |

  
## Évaluation

### Fin de la première période

* Vous devez déjà disposer de votre installation de Linux.

#### Écrit 

* Les questions porteront sur l'installation.

### Fin de la seconde période

* Évaluation sur machine à l'IUT pour vérifier que vous êtes capable d'administrer une machine. 
  * Nous fournissons une machine partiellement configurée
  * Vous avez un certain nombre de tâches à réaliser (Voir les listes *période 1* et *période 2*)
  
