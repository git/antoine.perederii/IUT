---
title: Un peu d'aide pour la SAÉ
---

# Sujet et Markdown

*Sujet* : La [version PDF](sujet.pdf) est obtenue depuis la [version Markdown](sujet.md) via le [script en zsh](mkd2pdf.sh) (qui utilise `pandoc` et contient un zsh-ism). Vous pouvez utiliser ces documents comme une première initiation à *markdown* (regardez le markdown en mode texte en cliquant sur `</>` (display source), car `gitlab` en fait naturellement un rendu graphique).

Essayez markdown. C'est pratique et répandu.

# Un peu de documentation

* [Manuel d'installation Debian](https://www.debian.org/releases/bullseye/amd64/). Plein d'informations.
* [Repo iso Debian avec drivers propriétaires](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/firmware-11.5.0-amd64-netinst.iso), si vous vous avez du matériel exotique.
* Avez-vous essayé un [moteur de recherche](https://duckduckgo.com/?t=ffab&q=virtualbox+installer+linux)?
* Pour vous aider à trouver des alternatives à un logiciel que vous connaissez, allez voir [FramaLibre](https://framalibre.org/) et [AlternativeTo](https://alternativeto.net/).

# Explorer le matériel

L'outil `lshw` (Pour *list hardware*) est très adapté pour voir le matériel disponible sur la machine...
