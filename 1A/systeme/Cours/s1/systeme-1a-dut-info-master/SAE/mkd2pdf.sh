#!/bin/zsh

doPandoc() { 
    dest="/tmp/$(basename $2)"
    pandoc --metadata link-citations -N -V papersize:a4paper  --pdf-engine=xelatex --variable urlcolor=cyan -V geometry:margin=1cm "$1" -o "$dest" 
     echo "$dest"
}


for file; do
  dest=${file//%\.(mkd|md)/.pdf}
  if [ "$dest" != "$file" ]; then
    doPandoc "$file" "$dest"
  fi
done
