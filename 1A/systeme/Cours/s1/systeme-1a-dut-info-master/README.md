# Système 1A DUT info

Bienvenue sur `gitlab` !

Cours et TP pour "introduction aux systèmes communiquant en réseau" (sic...)
Première année du DUT info.

Vous trouverez un [PDF](cours_linux_1a.pdf) avec toutes les diapos du cours, ainsi qu'un répertoire contenant le sujet des TP.

Pour vous aider, vous trouverez [les erreurs qu'on voit le plus souvent](cheatshell.md) et leur solution.
