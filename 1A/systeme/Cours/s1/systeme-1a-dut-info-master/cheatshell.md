# Les erreurs les plus fréquentes

## Espaces

* L'espace insécable
  * `cat /etc/passwd | wc -l` : *bash:  wc : commande introuvable* ???
  * Inséré par mégarde en faisant `AltGr+espace`. Reconnu comme un caractère alphabétique → bug.
* Les espaces 
  * Jamais quand affectation `a='le lapin'`
  * Toujours dans les tests `if [ "$a" = 'Le lapin' ];`
  * Protéger ses variables quand elles peuvent avoir des espaces `"$fichier"`

## Variables

* On accède au *contenu* d'une variable par `$` : `if [ $var = value ]`
* L'affectation d'une variable a besoin de son *nom* : `file="$selected"`

## Utiliser le bon outil

* Utiliser l'`*` pour énumérer les fichier : `for fichier in /tmp/*.pdf; do ...`
* Pas de logique de tableau en shell de base, une logique de flux
  * Parcours des arguments avec `for arg; do echo $arg; done`
* Mettez un `set -x` pour activer le déboguer et un `set +x` pour le désactiver.
* Si vous avez tendance à vous tromper sur les noms de variable, le `set -u` est votre ami.


## Bonne pratiques

* Soyez *précis* : *Argument* et *entrée standard*, ce n'est pas pareil !
* On lit une question avant en intégralité avant d'y répondre.
* On ne prend pas des bouts de code sur le net qu'on ne comprend pas (j'ai vu du PHP dans du shell!).
* Le cours, ça sert. Le `man` aussi. Si ce n'est pas dedans, on cherche sur le net. Quand on trouve de la doc, on lit "autour", pas juste la partie qui semble vous intéresser sur le moment.
* Maintenez un document avec une liste des commandes que vous connaissez : vous perdez trop de temps à retrouver des choses que vous connaissiez avant.
